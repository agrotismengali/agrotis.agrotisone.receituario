﻿using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.Core.Log;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using Agrotis.Framework.Commons;
using Agrotis.Framework.Data;
using Agrotis.Framework.Main.Application.Connections;
using SAPbouiCOM;
using System;
using System.IO;
using System.Reflection;
using static Agrotis.Framework.Main.Application.AddOnEnum;

namespace Agrotis.AgrotisOne.Receituario
{
    class Program
    {
        static void Main(string[] aArguments)
        {
            try
            {
                Globals.EvLog = new EventLogHandler(Produto.Receituario);
                Util.LimparPastaTemp();
                InstanceStartup ISup = new InstanceStartup(aArguments);

                Assembly oAssembly = ISup.oAssembly;
                Type oType = ISup.oType;
                Object[] oObject = ISup.oObject;
                AssemblyProductAttribute oAssemblyProductAttribute = ISup.oAssemblyProductAttribute;
                SboConnection lConexao = ISup.lConexao;
                StreamReader lMenus = ISup.lMenus;

                Logger.Trace(string.Format("Iniciando instancia do {0}", Settings.AddOnShortName));
                //Inicia o Addon, criando a conexão com o B1 e iniciando os EventHandlers  
                Globals.Master = new Master(lConexao, Produto.Receituario);
                Install.VerificarInstalacao(lConexao);
                Install.CarregarMenus();
                Globals.Master.Connection.Interface.StatusBar.SetText("AddOn " + oAssemblyProductAttribute.Product + " iniciado com sucesso!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                System.Windows.Forms.Application.Run();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Ocorreu o seguinte erro: " + e.Message);
                Logger.Trace(e.ToString());

                if (e.InnerException != null)
                    Logger.Trace(e.InnerException.Message);
            }
        }

    }
}
