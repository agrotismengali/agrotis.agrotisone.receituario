﻿using Agrotis.AgrotisOne.Core;
using Agrotis.Framework.Commons;
using Agrotis.Framework.Main.Application.Connections;
using System;
using System.IO;
using System.Reflection;

namespace Agrotis.AgrotisOne.Receituario
{
    public class InstanceStartup
    {
        public Assembly oAssembly = null;
        public Type oType = null;
        public Object[] oObject = null;
        public AssemblyProductAttribute oAssemblyProductAttribute = null;
        public SboConnection lConexao = null;
        public StreamReader lMenus = null;

        public InstanceStartup(string[] aArguments)
        {
            oAssembly = Assembly.GetExecutingAssembly();
            oType = typeof(AssemblyProductAttribute);
            oObject = oAssembly.GetCustomAttributes(oType, false);
            oAssemblyProductAttribute = (AssemblyProductAttribute)oObject[0];

            Globals.EvLog.WriteInfo(aArguments.Length > 1 ? aArguments[0] : "Sem argumentos");
            Globals.EvLog.WriteInfo("Buscando conexão");
            lConexao = aArguments.Length == 1 ? new SboConnection(
                Master.cAddOnIndentification, aArguments[0]) : new SboConnection(Master.cAddOnIndentification);
        }
    }
}
