﻿using SAPbouiCOM;
using System;
using System.IO;
using Agrotis.AgrotisOne.Core.Utils;
using System.Reflection;
using Agrotis.AgrotisOne.Core.Menus;
using Agrotis.AgrotisOne.Core;

namespace Agrotis.AgrotisOne.AgrotisOne.Menus
{
    public class AddMenusReceituario : IAddMenus
    {
        public void AddMenus()
        {
            try
            {
                foreach (MenusAddons.Menu menu in new MenusReceituario().GetMenus())
                {
                    Util.AddMenuItem(menu.FatherID, menu.Type, menu.UniqueID, menu.Caption, menu.Position, menu.Icon);
                }
            }
            catch (Exception Ex)
            {
                Globals.EvLog.WriteAlert($"Erro ao carregar criar o menu: {Ex.StackTrace} ");
                Util.ExibirMensagemStatusBar($"Houve um erro ao criar o menu: {Ex.ToString()}", BoMessageTime.bmt_Medium, true);
                Util.ExibirDialogo($"Houve um erro ao criar o menu: {Ex.Message}");
            }

        }
    }
}
