﻿using SAPbouiCOM;
using System.IO;
using Agrotis.AgrotisOne.Core.Utils;
using System.Reflection;
using Agrotis.AgrotisOne.Core.Menus;
using System.Collections.Generic;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;

namespace Agrotis.AgrotisOne.AgrotisOne.Menus
{
    public class MenusReceituario : MenusAddons
    {
        public override List<Menu> GetMenus()
        {            
            string Imagem = string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"\Content\AgrotisOneReceituário.png");

            var listMenu = new List<Menu>();
            #region [ AgrotisOne - Receituário ]

            listMenu.Add(new Menu("43520", BoMenuType.mt_POPUP, "AGRTMnuAGRTRecOne", "AgrotisOne Receituário", 16, Imagem, "", "AGRTPmuRec", "43520"));

            #endregion

            #region [ Configurações ] 

            listMenu.Add(new Menu("AGRTMnuAGRTRecOne", BoMenuType.mt_POPUP, "AGRTMnuConfRec", "Configurações", 1, Imagem, "", "AGRTPmuConfRec", "AGRTPmuRec"));
            listMenu.Add(new Menu("AGRTMnuConfRec", BoMenuType.mt_STRING, "AGRTMnuConfigRec", "Configurações Receituário", 1, Imagem, "AGRTFrmConfigRec", "AGRTPmuConfigRec","AGRTPmuConfRec"));
            listMenu.Add(new Menu("AGRTMnuConfRec", BoMenuType.mt_STRING, "AGRTMnuGrupoUtililizacaoRec", "Grupo de Utilizações", 2, Imagem, "AGRTFrmGrpUtilRec", "AGRTPmuGrpUtilRec", "AGRTPmuConfRec"));
            listMenu.Add(new Menu("AGRTMnuConfRec", BoMenuType.mt_STRING, "AGRTMnuGrItensGerRec", "Grupo de Itens - Gerencial", 3, Imagem, "AGRTFrmGrItensGerRec", "AGRTPmuGrItensGerRec", "AGRTPmuConfRec"));

            #endregion

            #region [ Exportação ] 
            listMenu.Add(new Menu("AGRTMnuAGRTRecOne", BoMenuType.mt_POPUP, "AGRTMnuExport", "Exportação", 2, Imagem, "", "AGRTPmuExport", "AGRTPmuRec"));
            listMenu.Add(new Menu("AGRTMnuExport", BoMenuType.mt_STRING, "AGRTMnuExportacao", "Movimentação de agrodefensívos", 1, Imagem, FormConstants.AGRTFrmExportacao, "AGRTPmuExportMov", "AGRTPmuExport"));            

            #endregion

            return listMenu;
        }
    }
}
