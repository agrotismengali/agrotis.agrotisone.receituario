﻿using System.Collections.Generic;
using SAPbobsCOM;
using Agrotis.Framework.Data.DatabaseCreation;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    /// <summary>
    /// Classe que contém os nomes e UIDs das UDOs do sistema.
    /// </summary>
    public static partial class UDOs
    {
        public static class RestricaoVendas
        {
            public const string UniqueID = "AGRT_CONFREC";
            public const string Nome = "AGRT: Config Receituário";
            public const string Tabela = Tabelas.ConfiguracoesReceituario.Nome;
            public const BoUDOObjType Tipo = BoUDOObjType.boud_MasterData;
            public const BoYesNoEnum CanFind = BoYesNoEnum.tYES;
            public const BoYesNoEnum CanDelete = BoYesNoEnum.tNO;
            public const BoYesNoEnum CanCancel = BoYesNoEnum.tNO;
            public const BoYesNoEnum CanLog = BoYesNoEnum.tYES;
            public const BoYesNoEnum CanApprove = BoYesNoEnum.tNO;
            public static readonly List<ChildTables> TabelasFilhas = 
                new List<ChildTables>() { new ChildTables(Tabelas.ConfiguracaoPorFilial.Nome, Tabelas.ConfiguracaoPorFilial.Nome)};
        }
    }
}
