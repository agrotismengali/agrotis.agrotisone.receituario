﻿using System.Collections.Generic;
using SAPbobsCOM;
using static Agrotis.AgrotisOne.Core.Model.Version;
using Enumerators = Agrotis.AgrotisOne.Core.Enums.Enumerators;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    /// <summary>
    /// Classe que contém a definição das tabelas utilizadas na ferramenta de restrição de vendas
    /// </summary>
    public static partial class Tabelas
    {
        public static class ConfiguracaoPorFilial
        {
            public const string Nome = "AGRT_CONFREC1";
            public const string Descricao = "AGRT: Config. por Filial";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_MasterDataLines;

            public static class Campos
            {
                public static class BPLId
                {
                    public const string Nome = "AGRT_BPLId";
                    public const string Descricao = "Cód. Filial";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Chave estrangeira para o cadastro de filiais OBPL";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class BPLName
                {
                    public const string Nome = "AGRT_BPLName";
                    public const string Descricao = "Razão Social";
                    public const int Tamanho = 100;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Razão social da filial (tabela OBPL)";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class CodeAgronomo
                {
                    public const string Nome = "AGRT_CodeAgron";
                    public const string Descricao = "Código Agrônomo";
                    public const int Tamanho = 30;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Campo utilizado para armazenar o Código do responsável técnico listado a partir da API do receituário";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NomeAgronomo
                {
                    public const string Nome = "AGRT_NomeAgron";
                    public const string Descricao = "Nome Agrônomo";
                    public const int Tamanho = 250;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Campo utilizado para armazenar o Nome do responsável técnico listado a partir da API do receituário";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class ValidarCulturaProduto
                {
                    public const string Nome = "AGRT_ValidarCulturaProduto";
                    public const string Descricao = "Validar Cultura Produto";
                    public const int Tamanho = 1;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = new List<ValidValue> {
                        new ValidValue("N", "Não"), new ValidValue("Y", "Sim") };
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = 
                        Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Define se a cultura selecionada na emissão de documento de marketing pode ser utilizada com os produtos MAPA relacionados ao item da nota";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }                
            }
        }
        public static class ConfiguracoesReceituario
        {
            public const string Nome = "AGRT_CONFREC";
            public const string Descricao = "AGRT: Config Receituário";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_MasterData;

            public static class Campos
            {
                public static class Token
                {
                    public const string Nome = "AGRT_Token";
                    public const string Descricao = "Token";
                    public const int Tamanho = 250;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Token para acesso a WEB API";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class GrupoUtilizacao
                {
                    public const string Nome = "AGRT_GrpUtiliz";
                    public const string Descricao = "Grupo de utilização";
                    public const int Tamanho = 50; 
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Grupo das utilizações que geram receita quando usadas no faturamento";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class AssinarDigitalmente
                {
                    public const string Nome = "AGRT_Assinar";
                    public const string Descricao = "Assinar Receitas";
                    public const int Tamanho = 1;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = new List<ValidValue> { new ValidValue("N", "Não"), new ValidValue("Y", "Sim")};
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Verifica se assina digitalmente as receitas";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NumeroViasImprimir
                {
                    public const string Nome = "AGRT_NumVias";
                    public const string Descricao = "Número vias para impressão";
                    public const int Tamanho = 10;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Numeric;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Número de vias para impressão da Receita.";
                    public const string ValorPadrao = "1";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class DtInicioIntegracao
                {
                    public const string Nome = "AGRT_DtIniIntegra";
                    public const string Descricao = "Data Inicial de Integração";
                    public const int Tamanho = 20;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Date;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Todos os registros incluídos/alterados a partir dessa data serão integrados.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class UrlPlataforma
                {
                    public const string Nome = "AGRT_UrlPlataforma";                    
                    public const string Descricao = "URL Plataforma";
                    public const int Tamanho = 250;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "https://agrotis.io/";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo =
                        Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "URL para acesso/integração com a plataforma Agrotis.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class DocImprimeReceita
                {
                    public const string Nome = "AGRT_DocImprimeReceita";
                    public const string Descricao = "Impressão receita";
                    public const int Tamanho = 1;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tYES;
                    public const string ValorPadrao = "0";
                    public static readonly List<ValidValue> ValidValues = new List<ValidValue> {
                        new ValidValue("0", "Nota fiscal"),
                        new ValidValue("1", "Pedido de venda"),
                        new ValidValue("2", "Não imprimir") };
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo =
                        Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Define onde será impressa a receita e documentos relacionados.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1008008;                    
                }
                public static class BuscarInfoNfPlataforma
                {
                    public const string Nome = "AGRT_BuscarInfoNfPlataforma";
                    public const string Descricao = "Buscar Inf. NF do plataforma";
                    public const int Tamanho = 1;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "Y";
                    public static readonly List<ValidValue> ValidValues = new List<ValidValue> {
                        new ValidValue("N", "Não"),
                        new ValidValue("Y", "Sim") };
                    public const Enumerators.Modulos.AgrotisOneReceituario Modulo =
                        Enumerators.Modulos.AgrotisOneReceituario.Configuracoes;
                    public const string Resumo = "Se marcado e por padrão, as informações de NFe vão ser puxadas do plataforma para a nota ao emitir receita, caso contrário serão tratadas na AGRT_TEXTONFE.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1009009;
                }
            }
        }
    }
}
