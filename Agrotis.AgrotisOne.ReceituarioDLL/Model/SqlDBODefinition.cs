﻿using Agrotis.AgrotisOne.Core.Enums;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public static class SqlDBODefinition
    {
        public static class ProcCalculaCustoGerencialFuturo
        {
            public const string Nome = "AGRT_SP_RCT_MovimentacaoAgrotoxico";
            public const string Descricao = "Proc. que retorna movimentações de agrotóxicos";
            public const Enumerators.TipoScript Tipo = Enumerators.TipoScript.Procedure;
            public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Receituario;
            public const string Resumo = "Retorna todas as movimentações (Entrada/Saída) que envolvem agrotóxicos. Entende-se por movimentação, as operações que alteram o saldo de estoque do Item.";
            public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
        }
        public static class ProcCopiarReceituarioPedido
        {
            public const string Nome = "AGRT_SP_RCT_CopiarReceituarioPedido";
            public const string Descricao = "Proc. que copia o receituário de um pedido para NF";
            public const Enumerators.TipoScript Tipo = Enumerators.TipoScript.Procedure;
            public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Receituario;
            public const string Resumo = "Copia todas as receitas/agrupamento vinculado a um pedido para a nota fiscal.";
            public const ReceituarioVersion Versao = ReceituarioVersion.v1009009;
        }
        public static class SeqDocumentos
        {
            public const string Nome = "AGRT_SEQ_REC_SequenciaDocumentos";
            public const string Descricao = "Sequence que alimenta o SeqID do pedido de venda e nota fiscal de saída";
            public const Enumerators.TipoScript Tipo = Enumerators.TipoScript.Sequence;
            public const Enumerators.Modulos.AgrotisOneReceituario Modulo = Enumerators.Modulos.AgrotisOneReceituario.Receituario;
            public const string Resumo = "Sequence que alimenta o SeqID do pedido de venda e nota fiscal de saída.";
            public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
        }
    }
}