﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    /// <summary>
    /// Classe que contém os nomes e descrições das tabelas do sistema e seus respectivos campos
    /// </summary>
    public static partial class Tabelas
    {
        public static class SubProduto
        {
            public const string Nome = "AGRT_RECEITSUBPROD";
            public const string Descricao = "AGRT: Receituário SubProduto";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_NoObjectAutoIncrement;

            public static class Campos
            {
                public static class ItemCode
                {
                    public const string Nome = "AGRT_ItemCode";
                    public const string Descricao = "Código do Item";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.CadastroItens;
                    public const string Resumo = "Campo utilizado para fazer a ligação com o ItemCode da tabela OITM";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class ItemCodeSubProduto
                {
                    public const string Nome = "AGRT_CodSubProd";
                    public const string Descricao = "Código MAPA";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.CadastroItens;
                    public const string Resumo = "Campo utilizado para armazenar o Código do registro MAPA e relacionar com a API do receituário";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class ItemNameSubProduto
                {
                    public const string Nome = "AGRT_NomeSubProd";
                    public const string Descricao = "Descrição MAPA";
                    public const int Tamanho = 100;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.CadastroItens;
                    public const string Resumo = "Campo utilizado para armazenar o Nome do registro MAPA";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class QtdeKgLt
                {
                    public const string Nome = "AGRT_QtdeKgLt";
                    public const string Descricao = "Qtde Kg/Lt";
                    public const int Tamanho = 15;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Float;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_Quantity;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.CadastroItens;
                    public const string Resumo = "Campo utilizado para armazenar a equivalência em Kilos/Litros ou Unidades entre o Item SAP e o produto MAPA";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class TarjaFichaEmergencia
                {
                    public const string Nome = "AGRT_TarjaFichaEmergencia";
                    public const string Descricao = "Imprimir Tarja";
                    public const int Tamanho = 1;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const EnumReceituario Modulo = EnumReceituario.CadastroItens;
                    public const string Resumo = "Flag que indica se a ficha de emergência gerada pelo receituário será impressa com ou sem tarja.";
                    public static string ValorPadrao = ((int)SimNao.Nao).ToString();
                    public static readonly List<ValidValue> ValidValues = new List<ValidValue> {
                        new ValidValue(((int) SimNao.Nao).ToString(), "Não"),
                        new ValidValue(((int) SimNao.Sim).ToString(), "Sim")
                    };
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
