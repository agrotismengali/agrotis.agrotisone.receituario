﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public static partial class Campos
    {
        public static class Documentos
        {
            public const string Nome = "OINV";
            public static class Campos
            {
                public static class CodigoReceituario
                {
                    public const string Nome = "AGRT_CodRec";
                    public const string Descricao = "Código Receituário";
                    public const int Tamanho = 15;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.NotaFiscalSaida;
                    public const string Resumo = "Campo utilizado para armazenar a relação entre a nota fiscal OINV e suas receitas @AGRT_RECEIT";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class SequenceID
                {
                    public const string Nome = "AGRT_SequenceID";
                    public const string Descricao = "Sequence ID";
                    public const int Tamanho = 10;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Numeric;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.NotaFiscalSaida;
                    public const string Resumo = "Campo utilizado para armazenar um ID do tipo auto-incremento que é passado para a API do receituário no momento de vincular as receitas à nota fiscal";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
