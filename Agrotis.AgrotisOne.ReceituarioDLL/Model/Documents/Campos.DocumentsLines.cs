﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public static partial class Campos
    {
        public static class INV1
        {
            public const string Nome = "INV1";
            public static class Campos
            {
                public static class CodigoCultura
                {
                    public const string Nome = "AGRT_CodCult";
                    public const string Descricao = "Código Cultura";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.NotaFiscalSaida;
                    public const string Resumo = "Campo utilizado para armazenar o Código da cultura vinculada ao Item da nota fiscal. Essas culturas são sincronizadas com a plataforma Agrotis";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NomeCultura
                {
                    public const string Nome = "AGRT_NomeCultura";
                    public const string Descricao = "Nome Cultura";
                    public const int Tamanho = 250;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o nome da cultura informado na linha do item da nota fiscal";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
