﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSProduto
    {        
        public static Produto GetProduto(DTOApi aAPI, string aCodProd)
        {
            Produto oProduto = null;
            bool bResult = false;
            string sResult = string.Empty;
            string sParams = APIReceituario.Produto.Replace("@IdProduto", aCodProd);

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        oProduto = JsonConvert.DeserializeObject<Produto>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Nenhum produto encontrado com o código {aCodProd}");
            return oProduto;
        }
        public static string GetDescricaoProduto(DTOApi aAPI, string aCodProd)
        {
            Produto oProduto = GetProduto(aAPI, aCodProd);
            if (oProduto != null)
            {
                return oProduto.nomeComum;
            }
            else
                return "";
        }
        public static InfoProdPerigoso GetInfoProdPerigoso(DTOApi aAPI, string aRegMapa)
        {
            InfoProdPerigoso oInfoProd = null;
            string sResult = string.Empty;
            string sParams = $"{APIReceituario.DadosNota}/{aRegMapa}"; 

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                        oInfoProd = JsonConvert.DeserializeObject<InfoProdPerigoso>(sResult);
                });
            task.Wait();                
            return oInfoProd;
        }
        public static IEnumerable<DadosNotaRetorno> GetDadosNota(DTOApi aAPI, IList<string> aProdutos)
        {
            IEnumerable<DadosNotaRetorno> Retornos = null;
            DadosNotaParams oParams = new DadosNotaParams(aProdutos);
            bool bResult = false;
            string sResult = "";
            string sResource = APIReceituario.DadosNota;

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sResource)),
                Method = HttpMethod.Put,
            };
            string sBodyContent = JsonConvert.SerializeObject(oParams);
            request.Content = new StringContent(sBodyContent, Encoding.UTF8, "application/json");
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        Retornos = JsonConvert.DeserializeObject<IEnumerable<DadosNotaRetorno>>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Parâmetros: {sBodyContent}");
            return Retornos;
        }
        public static string GetDadosNota(DTOApi aAPI, string aProduto)
        {
            string sResult = "";
            IList<string> produtos = new List<string>();
            produtos.Add(aProduto);                        
            foreach (DadosNotaRetorno Retorno in GetDadosNota(aAPI, produtos))
                sResult = string.Concat(sResult, Retorno.descricaoNF);            
            return sResult;
        }
        public static IEnumerable<ProdutoLista> ListarProdutos(DTOApi aAPI, string aFiltro)
        {
            IEnumerable<ProdutoLista> Produtos = null;
            bool bResult = false;
            string sResult = "";                        
            var client = new HttpClient();
            string sParams = "";
            if (string.IsNullOrEmpty(aFiltro))
                sParams = APIReceituario.ListarProdutos;
            else
                sParams = APIReceituario.ListarProdutosComFiltro.Replace("@filtro", aFiltro);
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        Produtos = JsonConvert.DeserializeObject<IEnumerable<ProdutoLista>>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Nenhum produto encontrado.");
            return Produtos;
        }
    }
}
