﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSProblemas
    {
        public static IEnumerable<Problema> ListarProblemas(DTOApi aAPI, string aCodMapaProduto, string aEstado, Int32 aCodigoCultura)
        {
            IEnumerable<Problema> Problemas = null;
            bool bResult = false;
            string sResult = "";
            string sParams = $"{APIReceituario.ListarProblemas}?codMapaProduto={aCodMapaProduto.ToString()}&estado={aEstado}&codCultura={aCodigoCultura.ToString()}";

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        Problemas = JsonConvert.DeserializeObject<IEnumerable<Problema>>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Nenhum problema listado para o produto {aCodMapaProduto}, UF {aEstado} e cultura {aCodigoCultura}");
            return Problemas;
        }
    }
}
