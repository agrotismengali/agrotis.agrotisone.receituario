﻿using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSReceitas
    {        
        public static IEnumerable<ReceitaConsulta> ListarReceitas(DTOApi aAPI, string aDtIni, string aDtFin, 
            string aIdProdutor, string aIdPropriedade, string aProdMapa, string aCultura)
        {
            IEnumerable<ReceitaConsulta> Receitas = null;
            string jsonString = "";
            //sURI = string.Concat(sURI, "?", "dataInicial=2018-10-1&dataFinal=2018-10-11&
            //idProdutor =25160&idPropriedade=25465&codMapaProduto=0610&idCultura=3");            
            string sParams = string.Concat(APIReceituario.ListarReceita, "?", 
                $@"dataInicial={aDtIni}&dataFinal={aDtFin}&idProdutor={aIdProdutor}&idPropriedade={aIdPropriedade}&codMapaProduto={aProdMapa}&idCultura={aCultura}");

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    jsonString = await response.Content.ReadAsStringAsync();
                    Receitas = JsonConvert.DeserializeObject<IEnumerable<ReceitaConsulta>>(jsonString);
                });
            task.Wait();
            return Receitas;
        }        
        public static Receita ConsultarReceita(DTOApi aAPI, string aIdReceita)
        {
            Receita Receita = null;
            string jsonString = "";
            string sParams = APIReceituario.ConsultarReceita.Replace("@IdReceita", aIdReceita);

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    jsonString = await response.Content.ReadAsStringAsync();
                    Receita = JsonConvert.DeserializeObject<Receita>(jsonString);
                });
            task.Wait();
            return Receita;
        }
        public static StatusReceita ConsultarReceitaStatus(DTOApi aAPI, string aIdReceita)
        {
            Receita Receita = ConsultarReceita(aAPI, aIdReceita);
            if (Receita != null)
            {
                switch (Receita.status)
                {
                    case "CONCLUIDA": { return StatusReceita.CONCLUIDA; }
                    case "CONSUMIDA": { return StatusReceita.CONSUMIDA; }
                    case "NAO_ASSINADA_DIGITALMENTE": { return StatusReceita.NAO_ASSINADA_DIGITALMENTE; }
                    default: return StatusReceita.NAODEFINIDA;
                }
            }
            else
                return StatusReceita.NAODEFINIDA;                
        }
        public static string ConsumirReceita(DTOApi aAPI, string aIdReceita)
        {
            IEnumerable<RetornoPost> Retornos = null;            
            string sResult = "";
            string sParams = APIReceituario.ConsumirReceita.Replace("@IdReceita", aIdReceita);

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Post,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    sResult = await response.Content.ReadAsStringAsync();
                    // Quando o retorno 200 for ajustado na API, deve-se tirar o if abaixo.
                    if (sResult.Equals("true"))
                        sResult = "[" + JsonConvert.SerializeObject(new RetornoPost("200", StatusReceita.CONSUMIDA.ToString(), "")) + "]";                    
                    Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                });
            task.Wait();
            return GetMensagemRetorno("ConsumirReceita", Retornos);
        }
        public static string VincularPedido(DTOApi aAPI, string aSeqId, string aQtde, string aProdMapa, string aIdReceita, string aIdItemReceita)
        {
            IEnumerable<RetornoPost> Retornos = null;
            string sResult = "";
            string sParams = APIReceituario.VincularPedido;
            string MensagemErro = string.Empty;

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Post,
            };
            string sBodyContent = APIReceituario.VincularPedidoBodyContent.Replace("@SequenceId", aSeqId).Replace("@Qtde", aQtde)
                .Replace("@QtEmb", "1").Replace("@CodProdMapa", aProdMapa).Replace("@IdReceita", aIdReceita).Replace("@idItemReceita", aIdItemReceita);
            request.Content = new StringContent(sBodyContent, Encoding.UTF8, "application/json");
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        // Quando o retorno 200 for ajustado na API, deve-se tirar o if abaixo.
                        if (sResult.Equals("true"))
                            sResult = "[" + JsonConvert.SerializeObject(new RetornoPost("200", StatusReceita.CONSUMIDA.ToString(), "")) + "]";
                        Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                    }
                    else
                    {
                        IEnumerable<RetornoPost> MensagensRetorno = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                        MensagemErro = UtilReceituario.RetornaMensagensRetorno(MensagensRetorno);
                    }
                });
            task.Wait();

            if (!String.IsNullOrEmpty(MensagemErro))
                throw new Exception(MensagemErro);

            return GetMensagemRetorno("VincularPedido", Retornos);
        }
        public static string DesvincularPedido(DTOApi aAPI, string aSeqId, string aIdReceita)
        {            
            IEnumerable<RetornoPost> Retornos = null;
            string sResult = "";
            string sParams = string.Concat(APIReceituario.VincularPedido, $"?idReceita={aIdReceita}&pedido={aSeqId}");
            string MensagemErro = string.Empty;

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Delete,
            };            
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        // Quando o retorno 200 for ajustado na API, deve-se tirar o if abaixo.
                        if (sResult.Equals("true"))
                            sResult = "[" + JsonConvert.SerializeObject(new RetornoPost("200", StatusReceita.NAODEFINIDA.ToString(), "")) + "]";
                        Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                    }
                    else
                    {
                        IEnumerable<RetornoPost> MensagensRetorno = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                        MensagemErro = UtilReceituario.RetornaMensagensRetorno(MensagensRetorno);
                    }
                });
            task.Wait();

            if (!string.IsNullOrEmpty(MensagemErro))
                throw new Exception(MensagemErro);

            return GetMensagemRetorno("DesvincularPedido", Retornos);
        }
        public static string VincularFaturamento(DTOApi aAPI, FaturamentoParams aParams)
        {
            IEnumerable<RetornoPost> Retornos = null;
            string sResult = "";
            string sResource = APIReceituario.VincularFaturamento;

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sResource)),
                Method = HttpMethod.Post,
            };
            string sBodyContent = JsonConvert.SerializeObject(aParams);
            request.Content = new StringContent(sBodyContent, Encoding.UTF8, "application/json");
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    sResult = await response.Content.ReadAsStringAsync();
                    // Quando o retorno 200 for ajustado na API, deve-se tirar o if abaixo.
                    if (sResult.Equals("true"))                    
                        sResult = "[" + JsonConvert.SerializeObject(new RetornoPost("200", StatusReceita.FINALIZADA.ToString(), "")) + "]";
                    Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                });
            task.Wait();
            return GetMensagemRetorno("VincularFaturamento", Retornos);
        }
        public static string DesvincularFaturamento(DTOApi aAPI, string aIdReceita, string aNumeroNota, string aCodProdMapa)
        {
            IEnumerable<RetornoPost> Retornos = null;
            string sResult = "";
            string sParams = string.Concat(APIReceituario.VincularFaturamento, $"?idReceita={aIdReceita}&numeroNF={aNumeroNota}&codMapaProduto={aCodProdMapa}");
            string MensagemErro = string.Empty;

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Delete,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        // Quando o retorno 200 for ajustado na API, deve-se tirar o if abaixo.
                        if (sResult.Equals("true"))
                            sResult = "[" + JsonConvert.SerializeObject(new RetornoPost("200", StatusReceita.NAODEFINIDA.ToString(), "")) + "]";
                        Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                    }
                    else
                    {
                        IEnumerable<RetornoPost> MensagensRetorno = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                        MensagemErro = UtilReceituario.RetornaMensagensRetorno(MensagensRetorno);
                    }
                });
            task.Wait();

            if (!string.IsNullOrEmpty(MensagemErro))
                throw new Exception(MensagemErro);

            return GetMensagemRetorno("DesvincularFaturamento", Retornos);
        }
        private static string GetMensagemRetorno(string aServico, IEnumerable<RetornoPost> aRetornos)
        {
            if (aRetornos != null)
            {
                string sMensagem = "";
                foreach (RetornoPost Retorno in aRetornos)
                {
                    sMensagem = Retorno.mensagem.Replace("'", "");
                    break;
                }
                return sMensagem;
            }
            else
                return $@"Não foi possível consumir o serviço {aServico} da API Receituário.";
        }
        public static string CriarRascunhoReceita(DTOApi aAPI, DTORascunhoReceituario DtoRascunho)
        {
            string sResult = "";
            bool bSuccess = false;

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, APIReceituario.Receita)),
                Method = HttpMethod.Post,
                Content = new StringContent(JsonConvert.SerializeObject(DtoRascunho), Encoding.UTF8, "application/json")
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    sResult = await response.Content.ReadAsStringAsync();
                    bSuccess = response.IsSuccessStatusCode;                        
                });
            task.Wait();
            if (!bSuccess)
                throw new Exception($"idEmpresa: {DtoRascunho.idEmpresa}, idFazenda: {DtoRascunho.idFazenda}, idProdutor: {DtoRascunho.idProdutor}, idRT: {DtoRascunho.idRT}, Emissão: {DtoRascunho.dataEmissao}. Erro receituário: " + sResult);
            return sResult;
        }
        public static ReceituarioItemRetorno AdicionaItemReceita(DTOApi aAPI, DTOReceituarioItem DtoRecItem)
        {
            ReceituarioItemRetorno RetornoReceituarioItem = null;
            string jsonString = String.Empty;
            string MensagemErro = String.Empty;

            string AdicionarItem = APIReceituario.AdicionarReceitaItem.Replace("@idreceita", DtoRecItem.idReceita.ToString());

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, AdicionarItem)),
                Method = HttpMethod.Post,
                Content = new StringContent(JsonConvert.SerializeObject(DtoRecItem), Encoding.UTF8, "application/json")
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    jsonString = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                        RetornoReceituarioItem = JsonConvert.DeserializeObject<ReceituarioItemRetorno>(jsonString);
                    else
                    {
                        IEnumerable<RetornoPost> Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(jsonString);
                        MensagemErro = UtilReceituario.RetornaMensagensRetorno(Retornos);
                    }
                });
            task.Wait();

            if (!String.IsNullOrEmpty(MensagemErro))
                throw new Exception(MensagemErro);

            return RetornoReceituarioItem;
        }
        public static string DesvincularItemRascunho(DTOApi aAPI, string aIdReceita, string aIdItemReceita)
        {
            IEnumerable<RetornoPost> Retornos = null;
            string sResult = string.Empty;
            string MensagemErro = string.Empty;

            string sParams = string.Concat(APIReceituario.AdicionarReceitaItem.Replace("@idreceita", aIdReceita), $"/{aIdItemReceita}");

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Delete                
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        // Quando o retorno 200 for ajustado na API, deve-se tirar o if abaixo.
                        if (sResult.Equals("true"))
                            sResult = "[" + JsonConvert.SerializeObject(new RetornoPost("200", StatusReceita.NAODEFINIDA.ToString(), "")) + "]";
                        Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                    }
                    else
                    {
                        IEnumerable<RetornoPost> MensagensRetorno = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(sResult);
                        MensagemErro = UtilReceituario.RetornaMensagensRetorno(MensagensRetorno);
                    }
                });
            task.Wait();

            if (!string.IsNullOrEmpty(MensagemErro))
                throw new Exception(MensagemErro);

            return GetMensagemRetorno("DesvincularItemRascunho", Retornos);
        }
        public static IEnumerable<ReceitaRetorno> ConcluirRascunho(DTOApi aAPI, string NumeroRascunho)
        {
            IEnumerable<ReceitaRetorno> ReceitasGeradas = null;
            string MensagemErro = String.Empty;
            string UrlChamada = $"{APIReceituario.Receita}{NumeroRascunho}?usaFrasesPadroes=true";

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, UrlChamada)),
                Method = HttpMethod.Put,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    string jsonString = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                        ReceitasGeradas = JsonConvert.DeserializeObject<IEnumerable<ReceitaRetorno>>(jsonString);
                    else
                    {
                        IEnumerable<RetornoPost> Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(jsonString);
                        MensagemErro = UtilReceituario.RetornaMensagensRetorno(Retornos);
                    }
                });
            task.Wait();

            if (!String.IsNullOrEmpty(MensagemErro))
                throw new Exception(MensagemErro);

            return ReceitasGeradas;
        }

        


    }
}
