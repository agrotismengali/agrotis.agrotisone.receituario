﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSEmpresa
    {
        public static Empresa GetEmpresaByCNPJ(DTOApi aAPI, string aCNPJ)
        {
            Empresa Empresa = null;
            bool bResult = false;
            string sResult = "";
            string sParams = APIReceituario.PesquisarInscricaoEmpresa.Replace("@inscricao", aCNPJ);

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        Empresa = JsonConvert.DeserializeObject<Empresa>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Parâmetros do serviço CNPJ {aCNPJ}");
            return Empresa;
        }
    }
}
