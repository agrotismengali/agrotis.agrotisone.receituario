﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSPropriedadeRural
    {
        public static PropriedadeRural GetPropriedadeRuralByUUID(DTOApi aAPI, string aUUID)
        {
            PropriedadeRural Propriedade = null;
            bool bResult = false;
            string sResult = "";
            string sParams = string.Concat(APIReceituario.PropriedadeRuralPorUUID, aUUID);

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {                
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        Propriedade = JsonConvert.DeserializeObject<PropriedadeRural>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Propriedade rural de ID {aUUID} não encontrada.");
            return Propriedade;
        }
        public static int GetIDPropriedadeRuralByUUID(DTOApi aAPI, string aUUID)
        {
            PropriedadeRural Propriedade = GetPropriedadeRuralByUUID(aAPI, aUUID);
            if (Propriedade == null)
                return 0;
            else
                return Propriedade.id;
        }
    }
}
