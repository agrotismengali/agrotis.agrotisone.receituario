﻿using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSRelatorios
    {        
        public static Stream EmitirTermoParaquate(DTOApi aAPI, string aIdReceita)
        {
            Stream streamResult = null;
            string sParams = APIReceituario.EmitirTermoParaquate.Replace("@IdReceita", aIdReceita);

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    if (response.IsSuccessStatusCode)
                        streamResult = await response.Content.ReadAsStreamAsync();
                });
            task.Wait();
            return streamResult;
        }
        public static Stream EmitirFichaEmergencia(DTOApi aAPI, string aIdReceita, bool aImprimeTarja)
        {
            Stream streamResult = null;
            string sParams = APIReceituario.EmitirFichaEmergencia.Replace("@codReceita", aIdReceita).Replace("@semTarja", (!aImprimeTarja).ToString());

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    if (response.IsSuccessStatusCode)
                        streamResult = await response.Content.ReadAsStreamAsync();
                });
            task.Wait();
            return streamResult;
        }
        public static Stream EmitirReceita(DTOApi aAPI, string aIdReceita, string Senha = "")
        {
            Stream streamResult = null;
            string MensagemErro = string.Empty;
            string sParams = APIReceituario.EmitirReceita.Replace("@IdReceita", aIdReceita);

            if (!String.IsNullOrEmpty(Senha))
                sParams += $"&senhaAssinatura={Senha}";

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    if (response.IsSuccessStatusCode)
                        streamResult = await response.Content.ReadAsStreamAsync();
                    else
                    {
                        string jsonString = await response.Content.ReadAsStringAsync();
                       IEnumerable<RetornoPost> Retornos = JsonConvert.DeserializeObject<IEnumerable<RetornoPost>>(jsonString);
                        MensagemErro = UtilReceituario.RetornaMensagensRetorno(Retornos);
                    }
                });
            task.Wait();

            if (!String.IsNullOrEmpty(MensagemErro))
                throw new Exception(MensagemErro);

            return streamResult;
        }              
    }
}
