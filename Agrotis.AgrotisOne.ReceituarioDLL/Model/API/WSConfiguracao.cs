﻿using System;
using System.Net.Http;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSConfiguracao
    {        
        public static bool ConexaoValida(string aURI, string aToken)
        {
            bool bResult = false;
            string sParams = APIReceituario.ConsultaVersao;

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aURI, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aToken);
            var task = client.SendAsync(request)
                .ContinueWith((taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                });
            task.Wait();
            return bResult;
        }        
    }
}
