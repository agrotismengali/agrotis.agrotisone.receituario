﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSCultura
    {
        public static IEnumerable<Cultura> ListarCulturas(DTOApi aAPI, CulturaParams aParams)
        {
            IEnumerable<Cultura> Culturas = null;                        
            bool bResult = false;
            string sResult = "";
            
            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, APIReceituario.ListarCulturaPorProduto)),
                Method = HttpMethod.Put,
            };
            string sBodyContent = JsonConvert.SerializeObject(aParams);
            request.Content = new StringContent(sBodyContent, Encoding.UTF8, "application/json");
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        Culturas = JsonConvert.DeserializeObject<IEnumerable<Cultura>>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Parâmetros: {sBodyContent}");
            return Culturas;
        }
        public static IEnumerable<Cultura> ListarCulturas(DTOApi aAPI, string aProdutoMapa, string aEstado)
        {
            CulturaParams oParams = new CulturaParams(aProdutoMapa, aEstado);
            return ListarCulturas(aAPI, oParams);
        }

    }
}
