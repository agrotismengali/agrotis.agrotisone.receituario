﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSProdutorRural
    {
        public static ProdutorRural GetProdutorRuralByUUID(DTOApi aAPI, string aUUID)
        {
            ProdutorRural Produtor = null;
            bool bResult = false;
            string sResult = "";
            string sParams = string.Concat(APIReceituario.ProdutorRuralPorUUID, aUUID);

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {                
                RequestUri = new Uri(string.Concat(aAPI.Uri, sParams)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        Produtor = JsonConvert.DeserializeObject<ProdutorRural>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Produtor rural de ID {aUUID} não encontrado.");
            return Produtor;
        }
        public static int GetIDProdutorRuralByUUID(DTOApi aAPI, string aUUID)
        {
            ProdutorRural Produtor = GetProdutorRuralByUUID(aAPI, aUUID);
            if (Produtor == null)
                return 0;
            else
                return Produtor.id;
        }
    }
}
