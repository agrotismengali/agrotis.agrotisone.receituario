﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSCalcularArea
    {
        public static CalculaArea CalculaArea(DTOApi aAPI, CalculaAreaEnvio oCalculaArea)
        {
            CalculaArea CalcularArea = null;
            bool bResult = false;
            string sResult = "";
            string sContent = JsonConvert.SerializeObject(oCalculaArea);

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, APIReceituario.CalcularArea)),
                Method = HttpMethod.Put,
                Content = new StringContent(sContent, Encoding.UTF8, "application/json")
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        CalcularArea = JsonConvert.DeserializeObject<CalculaArea>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Parâmetros do serviço {sContent}");
            return CalcularArea;
        }
    }
}
