﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSRelacoes
    {
        /// <summary>
        /// Consulta a Restrição e a relação por Produto, cultura e problema
        /// </summary>
        /// <param name="aAPI">Dto que tem os dados da API</param>
        /// <param name="codProduto">Código MAPA do Produto</param>
        /// <param name="codCultura">Código da Cultura</param>
        /// <param name="codProblema">Código do Problema</param>
        /// <returns></returns>
        public static Relacoes ConsultaRestricaoRelacao(String codProduto, Int32 codCultura, List<Int32> codProblemas, bool filtrarTodosProblemas, string ufSigla, bool listarRelacoesRestritas)
        {
            DTOApi aAPI = new DTO.DTOApi();
            Relacoes RelacoesRestricao = null;            
            bool bResult = false;
            string sResult = string.Empty;
            string CodProblemaFiltro = string.Empty;
            Int32 Count = 0;           

            DTOConsultaRelacoes DtoConsultaRelacoes = new DTO.DTOConsultaRelacoes();
            DtoConsultaRelacoes.codProblema = new int[codProblemas.Count];
            DtoConsultaRelacoes.codMapaProduto = codProduto;
            DtoConsultaRelacoes.codCultura = codCultura;
            foreach (var codProblema in codProblemas)
            {
                if (codProblema != 0)
                    DtoConsultaRelacoes.codProblema[Count] = codProblema;
                Count++;
            }
            DtoConsultaRelacoes.filtrarTodosProblemas = filtrarTodosProblemas;
            DtoConsultaRelacoes.ufSigla = ufSigla;
            DtoConsultaRelacoes.listarRelacoesRestritas = listarRelacoesRestritas;
            
            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, APIReceituario.ConsultaRelacoes)),
                Method = HttpMethod.Put,
                Content = new StringContent(JsonConvert.SerializeObject(DtoConsultaRelacoes), Encoding.UTF8, "application/json")
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        RelacoesRestricao = JsonConvert.DeserializeObject<Relacoes>(sResult);                    
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Nenhuma relação encontrada");
            return RelacoesRestricao;
        }
        public static Relacao ConsultaRelacaoEspecifica(DTOApi aAPI, DTORelacaoEspecifica DtoRelacaoEspecificaConsulta)
        {
            Relacao oRelacaoEspecifica = new Relacao();            
            bool bResult = false;
            string sResult = string.Empty;
            string CodProblemaFiltro = string.Empty;
            
            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, APIReceituario.ConsultarRelacaoEspecifica)),
                Method = HttpMethod.Post,
                Content = new StringContent(JsonConvert.SerializeObject(DtoRelacaoEspecificaConsulta), Encoding.UTF8, "application/json")
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        oRelacaoEspecifica = JsonConvert.DeserializeObject<Relacao>(sResult);                    
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Nenhuma relação encontrada");
            return oRelacaoEspecifica;

        }
    }
}
