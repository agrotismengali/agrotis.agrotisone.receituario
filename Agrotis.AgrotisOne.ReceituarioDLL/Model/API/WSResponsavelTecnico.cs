﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.API
{
    public class WSResponsavelTecnico
    {
        public static IEnumerable<ResponsavelTecnico> ListarResponsaveisTecnicos(DTOApi aAPI)
        {
            IEnumerable<ResponsavelTecnico> Responsaveis = null;
            string sResult = "";
            bool bResult = false;

            var client = new HttpClient();
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(string.Concat(aAPI.Uri, APIReceituario.ListarResponsavelTecnico)),
                Method = HttpMethod.Get,
            };
            client.DefaultRequestHeaders.Add("X-AUTH-TOKEN", aAPI.Token);
            var task = client.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    bResult = response.IsSuccessStatusCode;
                    sResult = await response.Content.ReadAsStringAsync();
                    if (bResult)
                        Responsaveis = JsonConvert.DeserializeObject<IEnumerable<ResponsavelTecnico>>(sResult);
                });
            task.Wait();
            if (!bResult)
                throw new Exception($"Nenhum responsável técnico encontrado.");
            return Responsaveis;
        }
    }
}
