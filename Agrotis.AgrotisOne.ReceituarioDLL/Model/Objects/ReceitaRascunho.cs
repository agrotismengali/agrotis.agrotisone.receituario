﻿using Newtonsoft.Json;
using System;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public partial class ReceitaRascunho
    {
        [JsonProperty("idProdutor")]
        public Int32 idProdutor { get; set; }
        [JsonProperty("dataEmissao")]
        public DateTime dataEmissao { get; set; }
        [JsonProperty("idFazenda")]
        public Int32 idFazenda { get; set; }
        [JsonProperty("idTalhao")]
        public Int32 idTalhao { get; set; }
        [JsonProperty("idRT")]
        public Int32 idRT { get; set; }
        [JsonProperty("idEmpresa")]
        public Int32 idEmpresa { get; set; }
    }
}
