﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{    
    public class Receita
    {
        [JsonProperty("numeroReceita")]
        public int numeroReceita { get; set; }

        [JsonProperty("ART")]
        public string ART { get; set; }

        [JsonProperty("clasCultSC")]
        public string clasCultSC { get; set; }

        [JsonProperty("idProdutor")]
        public string idProdutor { get; set; }

        [JsonProperty("dataEmissao")]
        public string dataEmissao { get; set; }

        [JsonProperty("cpfCnpjCli")]
        public string cpfCnpjCli { get; set; }

        [JsonProperty("nomeCli")]
        public string nomeCli { get; set; }

        [JsonProperty("nomeFaz")]
        public string nomeFaz { get; set; }

        [JsonProperty("enderecoFaz")]
        public string enderecoFaz { get; set; }

        [JsonProperty("cidadeFaz")]
        public string cidadeFaz { get; set; }

        [JsonProperty("estadoFaz")]
        public string estadoFaz { get; set; }

        [JsonProperty("ibgeCidadeFaz")]
        public string ibgeCidadeFaz { get; set; }

        [JsonProperty("idFazenda")]
        public string idFazenda { get; set; }

        [JsonProperty("idTalhao")]
        public string idTalhao { get; set; }

        [JsonProperty("nomeTalhao")]
        public string nomeTalhao { get; set; }

        [JsonProperty("telefoneCli")]
        public string telefoneCli { get; set; }

        [JsonProperty("numeroFaz")]
        public string numeroFaz { get; set; }

        [JsonProperty("cepFaz")]
        public string cepFaz { get; set; }

        [JsonProperty("idRT")]
        public int idRT { get; set; }

        [JsonProperty("nomeRT")]
        public string nomeRT { get; set; }

        [JsonProperty("creaRT")]
        public string creaRT { get; set; }

        [JsonProperty("cpfRT")]
        public string cpfRT { get; set; }

        [JsonProperty("tituloProfissaoRT")]
        public string tituloProfissaoRT { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }        

        [JsonProperty("idEmpresa")]
        public int idEmpresa { get; set; }

        [JsonProperty("razaoSocialEmpresa")]
        public string razaoSocialEmpresa { get; set; }

        [JsonProperty("logradouroEmpresa")]
        public string logradouroEmpresa { get; set; }

        [JsonProperty("numeroLogradouroEmpresa")]
        public string numeroLogradouroEmpresa { get; set; }

        [JsonProperty("cidadeEmpresa")]
        public string cidadeEmpresa { get; set; }

        [JsonProperty("estadoEmpresa")]
        public string estadoEmpresa { get; set; }

        [JsonProperty("cnpjEmpresa")]
        public string cnpjEmpresa { get; set; }        
    }       
}
