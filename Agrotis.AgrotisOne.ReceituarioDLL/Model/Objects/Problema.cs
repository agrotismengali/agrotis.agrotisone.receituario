﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class Problema
    {
        [JsonProperty("codProblema")]
        public int codProblema { get; set; }
        [JsonProperty("nomeVulgar")]
        public string nomeVulgar { get; set; }
        [JsonProperty("nomeCientifico")]
        public string nomeCientifico { get; set; }
        [JsonProperty("nomeFamilia")]
        public string nomeFamilia { get; set; }
        [JsonProperty("nomeOrdem")]
        public string nomeOrdem { get; set; }

    }
}
