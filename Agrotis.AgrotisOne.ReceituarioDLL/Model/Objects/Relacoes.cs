﻿using Newtonsoft.Json;
using System;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public partial class Relacoes
    {
        [JsonProperty("relacao")]
        public Relacao[] Relacao { get; set; }
    }

    public partial class Relacao
    {
        [JsonProperty("codRelacao")]
        public int codRelacao { get; set; }
        [JsonProperty("codMapaProduto")]
        public string codMapaProduto { get; set; }
        [JsonProperty("produto")]
        public string produto { get; set; }
        [JsonProperty("codCultura")]
        public int codCultura { get; set; }
        [JsonProperty("cultura")]
        public string cultura { get; set; }
        [JsonProperty("codProblema")]
        public int codProblema { get; set; }
        [JsonProperty("problema")]
        public string problema { get; set; }
        [JsonProperty("codFormaConducaoCultura")]
        public int codFormaConducaoCultura { get; set; }
        [JsonProperty("formaConducaoCultura")]
        public string formaConducaoCultura { get; set; }
        [JsonProperty("codMetodoAplicacao")]
        public int codMetodoAplicacao { get; set; }
        [JsonProperty("metodoAplicacao")]
        public string metodoAplicacao { get; set; }
        [JsonProperty("liberadaParaUso")]
        public bool liberadaParaUso { get; set; }
        [JsonProperty("codLocalCultura")]
        public string codLocalCultura { get; set; }
        [JsonProperty("localCultura")]
        public string localCultura { get; set; }
        [JsonProperty("codEstagioCultura")]
        public string codEstagioCultura { get; set; }
        [JsonProperty("estagioCultura")]
        public string estagioCultura { get; set; }
        [JsonProperty("codEstagioProblema")]
        public string codEstagioProblema { get; set; }
        [JsonProperty("estagioProblema")]
        public string estagioProblema { get; set; }
        [JsonProperty("labelDosagem")]
        public string labelDosagem { get; set; }
        [JsonProperty("labelUnidade")]
        public string labelUnidade { get; set; }
        [JsonProperty("labelNumAplic")]
        public string labelNumAplic { get; set; }
        [JsonProperty("orientacaoDosagem")]
        public string orientacaoDosagem { get; set; }
        [JsonProperty("orientacaoCalda")]
        public string orientacaoCalda { get; set; }
        [JsonProperty("doseMinima")]
        public double doseMinima { get; set; }
        [JsonProperty("doseMaxima")]
        public double doseMaxima { get; set; }
        [JsonProperty("calda")]
        public double calda { get; set; }
        [JsonProperty("temArea")]
        public bool temArea { get; set; }
        [JsonProperty("modoAplicacao")]
        public string modoAplicacao { get; set; }
        [JsonProperty("descTipoDosagem")]
        public string descTipoDosagem { get; set; }
        [JsonProperty("tipoDeSolo")]
        public string tipoDeSolo { get; set; }
        [JsonProperty("codTipoDeSolo")]
        public long codTipoDeSolo { get; set; }
        [JsonProperty("intervaloSeguranca")]
        public long intervaloSeguranca { get; set; }
        [JsonProperty("labelUnidadePlural")]
        public string labelUnidadePlural { get; set; }
        [JsonProperty("numMaxAplic")]
        public long numMaxAplic { get; set; }
        [JsonProperty("numMinAplic")]
        public long numMinAplic { get; set; }
        [JsonProperty("classifTox")]
        public string classifTox { get; set; }
        [JsonProperty("fatorArea")]
        public long fatorArea { get; set; }
    }
}
