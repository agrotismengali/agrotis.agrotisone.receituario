﻿using Agrotis.AgrotisOne.Core.Utils;
using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class CulturaParams
    {      
        public CulturaParams(string aProdutoMapa, string aEstado, bool aFiltrarProblemas = true)
        {
            codMapaProduto = aProdutoMapa;
            estado = aEstado;
            filtrarTodosProblemas = aFiltrarProblemas.ToString();            
        }

        [JsonProperty("codMapaProduto")]
        public string codMapaProduto { get; set; }

        [JsonProperty("estado")]
        public string estado { get; set; }

        [JsonProperty("filtrarTodosProblemas")]
        public string filtrarTodosProblemas { get; set; }
       
    }

}
