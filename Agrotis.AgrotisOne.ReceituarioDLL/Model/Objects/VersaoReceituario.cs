﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class VersaoReceituario
    {
        [JsonProperty("versao")]
        public int Versao { get; set; }

        [JsonProperty("dataLiberacao")]
        public string DataLiberacao { get; set; }        
    }    
}
