﻿using Newtonsoft.Json;
using System;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public partial class Empresa
    {
        [JsonProperty("empresaApi")]
        public EmpresaApi empresaApi { get; set; }        
    }

    public partial class EmpresaApi
    {
        [JsonProperty("id")]
        public int id { get; set; }        
    }    
}
