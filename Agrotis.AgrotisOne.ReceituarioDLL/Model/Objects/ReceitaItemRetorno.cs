﻿using Newtonsoft.Json;
using System;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public  class ReceituarioItemRetorno
    {
        [JsonProperty("idItem")]
        public Int32 idItem { get; set; }

        [JsonProperty("idPedidoItem")]
        public long idPedidoItem { get; set; }

        [JsonProperty("diagnostico")]
        public string diagnostico { get; set; }

        [JsonProperty("diagnosticoSiagro")]
        public string diagnosticoSiagro { get; set; }

        [JsonProperty("quantidade")]
        public long quantidade { get; set; }

        [JsonProperty("numeroAplicacoes")]
        public long numeroAplicacoes { get; set; }

        [JsonProperty("calda")]
        public long calda { get; set; }

        [JsonProperty("dosagem")]
        public long dosagem { get; set; }

        [JsonProperty("areaAplicacao")]
        public double areaAplicacao { get; set; }

        [JsonProperty("obsPrecaucoesUso")]
        public string obsPrecaucoesUso { get; set; }

        [JsonProperty("obsEPI")]
        public string obsEPI { get; set; }

        [JsonProperty("obsRecomendacoes")]
        public string obsRecomendacoes { get; set; }

        [JsonProperty("obsModoAplicacao")]
        public string obsModoAplicacao { get; set; }

        [JsonProperty("obsManejo")]
        public string obsManejo { get; set; }

        [JsonProperty("obsIncompatibilidade")]
        public string obsIncompatibilidade { get; set; }

        [JsonProperty("obsDisposicaoFinalEmbalagens")]
        public string obsDisposicaoFinalEmbalagens { get; set; }

        [JsonProperty("obsPrimeirosSocorros")]
        public string obsPrimeirosSocorros { get; set; }

        [JsonProperty("obsMeioAmbiente")]
        public string obsMeioAmbiente { get; set; }

        [JsonProperty("idProblema")]
        public long idProblema { get; set; }

        [JsonProperty("idMapaProduto")]
        public string idMapaProduto { get; set; }

        [JsonProperty("nomeComercialProd")]
        public string nomeComercialProd { get; set; }

        [JsonProperty("nomeTecnicoProd")]
        public string nomeTecnicoProd { get; set; }

        [JsonProperty("dose")]
        public string dose { get; set; }

        [JsonProperty("unidade")]
        public string unidade { get; set; }

        [JsonProperty("clToxicologica")]
        public string clToxicologica { get; set; }

        [JsonProperty("intSeguranca")]
        public long intSeguranca { get; set; }

        [JsonProperty("obsEquipAplicacao")]
        public string obsEquipAplicacao { get; set; }

        [JsonProperty("idCultura")]
        public long idCultura { get; set; }

        [JsonProperty("nomeCultura")]
        public string nomeCultura { get; set; }

        [JsonProperty("idRelacao")]
        public long idRelacao { get; set; }

        [JsonProperty("idTipoSolo")]
        public string idTipoSolo { get; set; }

        [JsonProperty("detalhe")]
        public Detalhe detalhe { get; set; }
    }

    public class Detalhe
    {
        [JsonProperty("usuarioInclusao")]
        public long usuarioInclusao { get; set; }

        [JsonProperty("usuarioAlteracao")]
        public long usuarioAlteracao { get; set; }

        [JsonProperty("dataHoraInclusao")]
        public string dataHoraInclusao { get; set; }

        [JsonProperty("dataHoraAlteracao")]
        public string dataHoraAlteracao { get; set; }

        [JsonProperty("versao")]
        public long versao { get; set; }

        [JsonProperty("uuid")]
        public string uuid { get; set; }

        [JsonProperty("id")]
        public long id { get; set; }

        [JsonProperty("registroAgrotis")]
        public bool registroAgrotis { get; set; }
    }

}
