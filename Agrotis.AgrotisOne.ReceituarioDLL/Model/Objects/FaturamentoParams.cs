﻿using Agrotis.AgrotisOne.Core.Utils;
using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class FaturamentoParams
    {      
        public FaturamentoParams(string aPedido, string aNumeroNota, string aSerieNota, string aDataEmissao)
        {
            pedido = aPedido;
            numeroNF = aNumeroNota;
            serieNF = aSerieNota;
            dataNF = aDataEmissao;
        }

        [JsonProperty("pedido")]
        public string pedido { get; set; }

        [JsonProperty("numeroNF")]
        public string numeroNF { get; set; }

        [JsonProperty("serieNF")]
        public string serieNF { get; set; }

        [JsonProperty("dataNF")]
        public string dataNF { get; set; }        
    }

}
