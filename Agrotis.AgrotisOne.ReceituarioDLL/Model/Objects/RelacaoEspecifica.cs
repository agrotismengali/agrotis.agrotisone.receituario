﻿using Newtonsoft.Json;
using System;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public partial class RelacaoEspecifica
    {
        [JsonProperty("codRelacao")]
        public Int32 codRelacao { get; set; }

        [JsonProperty("codMapaProduto")]
        public Int32 codMapaProduto { get; set; }

        [JsonProperty("produto")]
        public string produto { get; set; }

        [JsonProperty("codCultura")]
        public Int32 codCultura { get; set; }

        [JsonProperty("cultura")]
        public string cultura { get; set; }

        [JsonProperty("codProblema")]
        public Int32 codProblema { get; set; }

        [JsonProperty("problema")]
        public string problema { get; set; }

        [JsonProperty("codVariedadeCultura")]
        public Int32 codVariedadeCultura { get; set; }

        [JsonProperty("variedadeCultura")]
        public string variedadeCultura { get; set; }

        [JsonProperty("codFormaConducaoCultura")]
        public Int32 codFormaConducaoCultura { get; set; }

        [JsonProperty("formaConducaoCultura")]
        public string formaConducaoCultura { get; set; }

        [JsonProperty("codMetodoAplicacao")]
        public Int32 codMetodoAplicacao { get; set; }

        [JsonProperty("metodoAplicacao")]
        public string metodoAplicacao { get; set; }

        [JsonProperty("codTipoDeSolo")]
        public Int32 codTipoDeSolo { get; set; }

        [JsonProperty("tipoDeSolo")]
        public string tipoDeSolo { get; set; }

        [JsonProperty("liberadaParaUso")]
        public bool liberadaParaUso { get; set; }

        [JsonProperty("codLocalCultura")]
        public string codLocalCultura { get; set; }

        [JsonProperty("localCultura")]
        public string localCultura { get; set; }

        [JsonProperty("codEstagioCultura")]
        public string codEstagioCultura { get; set; }

        [JsonProperty("estagioCultura")]
        public string estagioCultura { get; set; }

        [JsonProperty("codEstagioProblema")]
        public string codEstagioProblema { get; set; }

        [JsonProperty("estagioProblema")]
        public string estagioProblema { get; set; }

        [JsonProperty("labelDosagem")]
        public string labelDosagem { get; set; }

        [JsonProperty("labelUnidade")]
        public string labelUnidade { get; set; }

        [JsonProperty("labelNumAplic")]
        public string labelNumAplic { get; set; }

        [JsonProperty("orientacaoDosagem")]
        public string orientacaoDosagem { get; set; }

        [JsonProperty("orientacaoCalda")]
        public string orientacaoCalda { get; set; }

        [JsonProperty("doseMinima")]
        public double doseMinima { get; set; }

        [JsonProperty("doseMaxima")]
        public double doseMaxima { get; set; }

        [JsonProperty("calda")]
        public double calda { get; set; }

        [JsonProperty("temArea")]
        public bool temArea { get; set; }

        [JsonProperty("modoAplicacao")]
        public string modoAplicacao { get; set; }

        [JsonProperty("descTipoDosagem")]
        public string descTipoDosagem { get; set; }

        [JsonProperty("intervaloSeguranca")]
        public Int32 intervaloSeguranca { get; set; }

        [JsonProperty("ativo")]
        public bool ativo { get; set; }

        [JsonProperty("descTipoDosagemEq")]
        public string descTipoDosagemEq { get; set; }

        [JsonProperty("labelUnidadePlural")]
        public string labelUnidadePlural { get; set; }

        [JsonProperty("codProbSiagroPr")]
        public Int32 codProbSiagroPr { get; set; }

        [JsonProperty("codCultSiagroPr")]
        public Int32 codCultSiagroPr { get; set; }

        [JsonProperty("codSiagroPR")]
        public Int32 codSiagroPR { get; set; }

        [JsonProperty("numMaxAplic")]
        public Int32 numMaxAplic { get; set; }

        [JsonProperty("numMinAplic")]
        public Int32 numMinAplic { get; set; }

        [JsonProperty("classifTox")]
        public string classifTox { get; set; }

        [JsonProperty("fatorArea")]
        public Int32 fatorArea { get; set; }

        [JsonProperty("estadosRestricao")]
        public string[] estadosRestricao { get; set; }

        [JsonProperty("culturaApi")]
        public CulturaApi culturaApi { get; set; }

        [JsonProperty("produtoApi")]
        public ProdutoApi produtoApi { get; set; }

        [JsonProperty("problemaApi")]
        public ProblemaApi problemaApi { get; set; }
    }

    public partial class CulturaApi
    {
        [JsonProperty("codCultura")]
        public Int32 codCultura { get; set; }

        [JsonProperty("nomeComum")]
        public string nomeComum { get; set; }

        [JsonProperty("nomeCientifico")]
        public string nomeCientifico { get; set; }

        [JsonProperty("codigoSC")]
        public Int32 codigoSC { get; set; }

        [JsonProperty("codigoSiagroPR")]
        public Int32 codigoSiagroPR { get; set; }

        [JsonProperty("creaSc")]
        public string creaSc { get; set; }

        [JsonProperty("nomeFamilia")]
        public string nomeFamilia { get; set; }

        [JsonProperty("formasConducao")]
        public FormasConducao formasConducao { get; set; }

        [JsonProperty("variedades")]
        public Variedades variedades { get; set; }
    }

    public partial class FormasConducao
    {
        [JsonProperty("codFcCultura")]
        public Int32 codFcCultura { get; set; }

        [JsonProperty("descricao")]
        public string descricao { get; set; }
    }

    public partial class Variedades
    {
        [JsonProperty("codVarCultura")]
        public Int32 codVarCultura { get; set; }

        [JsonProperty("nome")]
        public string nome { get; set; }
    }

    public partial class ProblemaApi
    {
        [JsonProperty("codProblema")]
        public Int32 codProblema { get; set; }

        [JsonProperty("nomeVulgar")]
        public string nomeVulgar { get; set; }

        [JsonProperty("nomeCientifico")]
        public string nomeCientifico { get; set; }

        [JsonProperty("codigoSiagroPR")]
        public Int32 codigoSiagroPR { get; set; }

        [JsonProperty("nomeFamilia")]
        public string nomeFamilia { get; set; }

        [JsonProperty("nomeOrdem")]
        public string nomeOrdem { get; set; }
    }

    public partial class ProdutoApi
    {
        [JsonProperty("codMapaProduto")]
        public string codMapaProduto { get; set; }

        [JsonProperty("nomeComum")]
        public string nomeComum { get; set; }

        [JsonProperty("nomeTecnico")]
        public string nomeTecnico { get; set; }

        [JsonProperty("principioAtivo")]
        public PrincipioAtivo[] principioAtivo { get; set; }

        [JsonProperty("codigoSiagroPR")]
        public Int32 codigoSiagroPR { get; set; }

        [JsonProperty("codigoProbSiagroPR")]
        public Int32 codigoProbSiagroPR { get; set; }

        [JsonProperty("descProbSiagroPR")]
        public string descProbSiagroPR { get; set; }

        [JsonProperty("fabricante")]
        public Fabricante fabricante { get; set; }

        [JsonProperty("epi")]
        public string epi { get; set; }

        [JsonProperty("precaucaoUso")]
        public string precaucaoUso { get; set; }

        [JsonProperty("incompatibilidade")]
        public string incompatibilidade { get; set; }

        [JsonProperty("primeirosSocorros")]
        public string primeirosSocorros { get; set; }

        [JsonProperty("advMeioAmbiente")]
        public string advMeioAmbiente { get; set; }

        [JsonProperty("simboloClasseToxicologica")]
        public string simboloClasseToxicologica { get; set; }

        [JsonProperty("antidotoTratamento")]
        public string antidotoTratamento { get; set; }

        [JsonProperty("sintomas")]
        public string sintomas { get; set; }

        [JsonProperty("grupoQuimico")]
        public string grupoQuimico { get; set; }

        [JsonProperty("tipoFormulacao")]
        public string tipoFormulacao { get; set; }
    }

    public partial class Fabricante
    {
        [JsonProperty("codFabricante")]
        public Int32 codFabricante { get; set; }

        [JsonProperty("razaoSocial")]
        public string razaoSocial { get; set; }
    }

    public partial class PrincipioAtivo
    {
        [JsonProperty("nome")]
        public string nome { get; set; }

        [JsonProperty("percentual")]
        public string percentual { get; set; }
    }
}
