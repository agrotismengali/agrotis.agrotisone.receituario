﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class PropriedadeRural
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("nome")]
        public string nome { get; set; }

        [JsonProperty("area")]
        public double area { get; set; }

        [JsonProperty("ativo")]
        public bool ativo { get; set; }
    }

}
