﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class DadosNotaParams
    {
        public DadosNotaParams(IList<string> aProdutos)
        {
            codMapaProduto = aProdutos;
            elementos = new List<string>();
            elementos.Add("NomeEmbarque");
            elementos.Add("PrincipioAtivo");
            elementos.Add("NumeroRisco");
            elementos.Add("NumeroOnu");
            elementos.Add("GrupoEmbalagem");
            elementos.Add("Classe");
            elementos.Add("RegistroDiprof");
            elementos.Add("ClasseToxicologica");
        }
        [JsonProperty("codMapaProduto")]
        public IList<string> codMapaProduto { get; set; }
        [JsonProperty("elementos")]
        public IList<string> elementos { get; set; }
    }

}
