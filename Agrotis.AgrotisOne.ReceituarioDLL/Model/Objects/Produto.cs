﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class Produto
    {
        [JsonProperty("codMapaProduto")]
        public string codMapaProduto { get; set; }

        [JsonProperty("nomeComum")]
        public string nomeComum { get; set; }

        [JsonProperty("nomeTecnico")]
        public string nomeTecnico { get; set; }

        [JsonProperty("codigoSiagroPR")]
        public int codigoSiagroPR { get; set; }

        [JsonProperty("epi")]
        public string epi { get; set; }

        [JsonProperty("precaucaoUso")]
        public string precaucaoUso { get; set; }

        [JsonProperty("primeirosSocorros")]
        public string primeirosSocorros { get; set; }

        [JsonProperty("advMeioAmbiente")]
        public string advMeioAmbiente { get; set; }

        [JsonProperty("simboloClasseToxicologica")]
        public string simboloClasseToxicologica { get; set; }

        [JsonProperty("antidotoTratamento")]
        public string antidotoTratamento { get; set; }

        [JsonProperty("tipoFormulacao")]
        public string tipoFormulacao { get; set; }
    }

}
