﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class ReceitaItem
    {
        public partial class CriarReceitaItem
        {
            [JsonProperty("idReceita")]
            public Object idReceita { get; set; }
            [JsonProperty("codRelacao")]
            public long codRelacao { get; set; }
            [JsonProperty("quantidade")]
            public long quantidade { get; set; }
            [JsonProperty("numeroAplicacoes")]
            public Int32 numeroAplicacoes { get; set; }
            [JsonProperty("dose")]
            public Int32 dose { get; set; }
            [JsonProperty("codPedidoItem")]
            public Int32 codPedidoItem { get; set; }
            [JsonProperty("calda")]
            public Int32 calda { get; set; }
            [JsonProperty("areaAplicacao")]
            public double areaAplicacao { get; set; }
            [JsonProperty("validarImpressao")]
            public bool validarImpressao { get; set; }
            [JsonProperty("codModeloReceita")]
            public string codModeloReceita { get; set; }
            [JsonProperty("codCultura")]
            public Int32 codCultura { get; set; }
            [JsonProperty("tipoDeSolo")]
            public object tipoDeSolo { get; set; }
            [JsonProperty("obsRecomendacoes")]
            public string obsRecomendacoes { get; set; }
            [JsonProperty("obsEquipAplicacao")]
            public string obsEquipAplicacao { get; set; }
        }
    }
}
