﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class Cultura
    {
        [JsonProperty("codCultura")]
        public int codCultura { get; set; }
        [JsonProperty("nomeComum")]
        public string nomeComum { get; set; }
        [JsonProperty("nomeCientifico")]
        public string nomeCientifico { get; set; }        
        [JsonProperty("nomeFamilia")]
        public string nomeFamilia { get; set; }       
    }
}
