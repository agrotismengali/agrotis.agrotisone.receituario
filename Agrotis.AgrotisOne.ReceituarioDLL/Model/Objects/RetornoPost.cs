﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class RetornoPost
    {        
        public RetornoPost(string aCodigo, string aMensagem, string aTimestamp)
        {
            codigo = aCodigo;
            mensagem = aMensagem;
            timestamp = aTimestamp;
        }
        public RetornoPost()
        {
            //
        }

        [JsonProperty("codigo")]
        public string codigo { get; set; }

        [JsonProperty("mensagem")]
        public string mensagem { get; set; }

        [JsonProperty("timestamp")]
        public string timestamp { get; set; }
    }

}
