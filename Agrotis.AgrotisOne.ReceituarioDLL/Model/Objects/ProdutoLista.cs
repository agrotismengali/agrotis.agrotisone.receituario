﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class ProdutoLista
    {
        [JsonProperty("codMapaProduto")]
        public string codMapaProduto { get; set; }

        [JsonProperty("nomeComum")]
        public string nomeComum { get; set; }

        [JsonProperty("nomeTecnico")]
        public string nomeTecnico { get; set; }                

        [JsonProperty("simboloClasseToxicologica")]
        public string simboloClasseToxicologica { get; set; }

        [JsonProperty("grupoQuimico")]
        public string grupoQuimico { get; set; }
    }

}
