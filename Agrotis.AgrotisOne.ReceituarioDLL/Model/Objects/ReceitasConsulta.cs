﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{    
    public class ReceitaConsulta
    {
        [JsonProperty("idReceita")]
        public int idReceita { get; set; }

        [JsonProperty("dataEmissao")]
        public string dataEmissao { get; set; }

        [JsonProperty("numeroReceita")]
        public int numeroReceita { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("idRT")]
        public int idRT { get; set; }

        [JsonProperty("nomeRT")]
        public string nomeRT { get; set; }

        [JsonProperty("creaRT")]
        public string creaRT { get; set; }

        [JsonProperty("cpfRT")]
        public string cpfRT { get; set; }

        [JsonProperty("quantidade")]
        public double quantidade { get; set; }

        [JsonProperty("idItemReceita")]
        public int idItemReceita { get; set; }

        [JsonProperty("dosagem")]
        public double dosagem { get; set; }

        [JsonProperty("dose")]
        public string dose { get; set; }

        [JsonProperty("area")]
        public double area { get; set; }

        [JsonProperty("problema")]
        public string problema { get; set; }
    }       
}
