﻿using Newtonsoft.Json;
using System;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class ReceitaRetorno
    {
        [JsonProperty("numeroReceita")]
        public int numeroReceita { get; set; }
                
        [JsonProperty("idProdutor")]
        public int idProdutor { get; set; }        

        [JsonProperty("idFazenda")]
        public int idFazenda { get; set; }        

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("itens")]
        public Iten[] itens { get; set; }

        [JsonProperty("idEmpresa")]
        public int idEmpresa { get; set; }        

        [JsonProperty("detalhe")]
        public DetalheReceita detalhe { get; set; }
    }

    public class DetalheReceita
    {        
        [JsonProperty("id")]
        public int id { get; set; }
    }

    public partial class Iten
    {
        [JsonProperty("idItem")]
        public int idItem { get; set; }

        [JsonProperty("idPedidoItem")]
        public int idPedidoItem { get; set; }

        [JsonProperty("quantidade")]
        public double quantidade { get; set; }

        [JsonProperty("idMapaProduto")]
        public string idMapaProduto { get; set; }
    }
}
