﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class ResponsavelTecnico
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("nome")]
        public string nome { get; set; }

        [JsonProperty("nomeCurto")]
        public string nomeCurto { get; set; }

        [JsonProperty("inscricaoFiscal")]
        public string inscricaoFiscal { get; set; }

        [JsonProperty("ativo")]
        public bool ativo { get; set; }

        [JsonProperty("crea")]
        public string crea { get; set; }

        [JsonProperty("rg")]
        public string rg { get; set; }
        /*
        [JsonProperty("cidadeApi")]
        public CidadeApi cidadeApi { get; set; }

        [JsonProperty("paisApi")]
        public PaisApi paisApi { get; set; }
        */
        [JsonProperty("creaFormacaoRt")]
        public string creaFormacaoRt { get; set; }

        [JsonProperty("pessoaJuridica")]
        public bool pessoaJuridica { get; set; }

        [JsonProperty("logradouro")]
        public string logradouro { get; set; }

        [JsonProperty("numero")]
        public string numero { get; set; }

        [JsonProperty("complemento")]
        public string complemento { get; set; }

        [JsonProperty("cep")]
        public string cep { get; set; }

        [JsonProperty("bairro")]
        public string bairro { get; set; }
    }

}
