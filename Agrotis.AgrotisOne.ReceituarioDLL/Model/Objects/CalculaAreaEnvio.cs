﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class CalculaAreaEnvio
    {
        [JsonProperty("descTipoDosagem")]
        public string descTipoDosagem { get; set; }
        [JsonProperty("quantidade")]
        public Double quantidade { get; set; }
        [JsonProperty("dosagem")]
        public Double dosagem { get; set; }
        [JsonProperty("numeroAplicacoes")]
        public Double numeroAplicacoes { get; set; }
        [JsonProperty("calda")]
        public Double calda { get; set; }
    }
}
