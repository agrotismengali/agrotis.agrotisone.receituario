﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class DadosNotaRetorno
    {
        [JsonProperty("codProduto")]
        public int codProduto { get; set; }
        [JsonProperty("descricaoNF")]
        public string descricaoNF { get; set; }
        [JsonProperty("codMapaProduto")]
        public string codMapaProduto { get; set; }
    }

}
