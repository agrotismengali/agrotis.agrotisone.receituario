﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class InfoProdPerigoso
    {
        [JsonProperty("codMapaProduto")]
        public int codMapaProduto { get; set; }
        [JsonProperty("nomeEmbarque")]
        public string nomeEmbarque { get; set; }
        [JsonProperty("principioAtivo")]
        public string principioAtivo { get; set; }
        [JsonProperty("numeroRisco")]
        public string numeroRisco { get; set; }
        [JsonProperty("numeroOnu")]
        public string numeroOnu { get; set; }
        [JsonProperty("grupoEmbalagem")]
        public string grupoEmbalagem { get; set; }
        [JsonProperty("classe")]
        public string classe { get; set; }
        [JsonProperty("classeToxicologica")]
        public string classeToxicologica { get; set; }        
    }

}
