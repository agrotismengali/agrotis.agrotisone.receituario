﻿using Newtonsoft.Json;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects
{
    public class ProdutorRural
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("ativo")]
        public bool ativo { get; set; }
    }

}
