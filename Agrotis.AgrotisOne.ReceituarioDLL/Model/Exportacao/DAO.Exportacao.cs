﻿namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public class DaoExportacao
    {
        public static string GetSQLAgenteExportacaoCode()
        {
            return $@"select ""Code"" from ""@AGRT_AGTEXPORT"" ";
        }
        public static string GetSQLEmbalagemPadraoImaCadastrado(string aAgente)
        {
            return $@"select ""Code"" from ""@AGRT_EMBPADRAO"" where ""U_AGRT_CodeAgente"" = '{aAgente}' limit 1 ";
        }        
        public static string GetSQLFilial()
        {
            return $@"select
                          T0.""BPLId"",
                          T0.""BPLName""
                      from OBPL as T0
                      order by
                          T0.""BPLId"" ";
        }
        public static string GetSQLAgente()
        {
            return $@"select
                          T0.""Code"",
                          T0.""Name"" || ' - ' || T0.""U_AGRT_State"" as ""Name""
                      from
                          ""@AGRT_AGTEXPORT"" T0
                      order by
                          T0.""Code"" ";
        }
        public static string GetSQLMovimentacaoAgrotoxico(string aFilial, string aAgente, string aDataIni, string aDataFin)
        {
            return $@"call AGRT_SP_RCT_MovimentacaoAgrotoxico({aFilial}, {aAgente}, '{aDataIni}', '{aDataFin}'); ";
        }

        public static string GetSQLEmbalagemProdMapa(string aAgente, string aItemCode, string aProdMapa)
        {
            return 
                $@"select 
	                ""Code""
                from
                    ""@AGRT_EMBPRODMAPA""
                where
                    ""U_AGRT_CodeAgente"" = {aAgente} and
                    ""U_AGRT_ItemCode"" = '{aItemCode}' and
                    ""U_AGRT_ProdutoMapa"" = '{aProdMapa}' ";
        }
        
    }
}
