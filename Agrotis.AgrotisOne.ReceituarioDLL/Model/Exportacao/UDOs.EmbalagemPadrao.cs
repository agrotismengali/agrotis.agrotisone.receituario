﻿using System.Collections.Generic;
using SAPbobsCOM;
using Agrotis.Framework.Data.DatabaseCreation;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{    
    public static partial class UDOs
    {        
        public static class EmbalagemPadrao
        {
            public const string UniqueID = Tabelas.EmbalagemPadrao.Nome;
            public const string Nome = Tabelas.EmbalagemPadrao.Descricao;
            public const string Tabela = Tabelas.EmbalagemPadrao.Nome;
            public const BoUDOObjType Tipo = BoUDOObjType.boud_MasterData;
            public const BoYesNoEnum CanFind = BoYesNoEnum.tYES;
            public const BoYesNoEnum CanDelete = BoYesNoEnum.tNO;
            public const BoYesNoEnum CanCancel = BoYesNoEnum.tNO;
            public const BoYesNoEnum CanLog = BoYesNoEnum.tYES;
            public const BoYesNoEnum CanApprove = BoYesNoEnum.tNO;
            public static readonly List<ChildTables> TabelasFilhas = null;
        }
    }
}
