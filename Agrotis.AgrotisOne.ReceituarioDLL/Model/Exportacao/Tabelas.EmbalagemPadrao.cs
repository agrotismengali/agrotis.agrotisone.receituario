﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{    
    public static partial class Tabelas
    {
        public static class EmbalagemPadrao
        {
            public const string Nome = "AGRT_EMBPADRAO";
            public const string Descricao = "AGRT: Embalagem Padrão";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_MasterData;

            public static class Campos
            {
                public static class CodeAgente
                {
                    public const string Nome = "AGRT_CodeAgente";
                    public const string Descricao = "Código Agente";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para indicar o agente fiscalizador para onde será enviado o relatório de movimentação.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class CodeEmbalagem
                {
                    public const string Nome = "AGRT_CodeEmbalagem";
                    public const string Descricao = "Código Embalagem";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para informar o código padrão da embalagem para o agente fiscalizador.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class UnidadeMedida
                {
                    public const string Nome = "AGRT_DescUniMed";
                    public const string Descricao = "Descrição U.M.";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Descrição da unidade de medida equivalente ao código fornecido pelo agente fiscalizador.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
