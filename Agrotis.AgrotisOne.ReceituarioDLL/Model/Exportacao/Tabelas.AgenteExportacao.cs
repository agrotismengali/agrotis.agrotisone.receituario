﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public static partial class Tabelas
    {
        public static class AgenteExportacao
        {
            public const string Nome = "AGRT_AGTEXPORT";
            public const string Descricao = "AGRT: Agente Exportação";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_NoObjectAutoIncrement;

            public static class Campos
            {                
                public static class Estado
                {
                    public const string Nome = "AGRT_State";
                    public const string Descricao = "UF";
                    public const int Tamanho = 3;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Estado do agente fiscalizador.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class DataInicial
                {
                    public const string Nome = "AGRT_DtInicial";
                    public const string Descricao = "Data Inicial Período";
                    public const int Tamanho = 15;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Date;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Data de início das movimentações a serem exportadas.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class DataFinal
                {
                    public const string Nome = "AGRT_DtFinal";
                    public const string Descricao = "Data Final Período";
                    public const int Tamanho = 15;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Date;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Data final das movimentações a serem exportadas.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class DataUltimoEnvio
                {
                    public const string Nome = "AGRT_DtUltimoEnvio";
                    public const string Descricao = "Data Último Envio";
                    public const int Tamanho = 15;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Date;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Data/Hora da geração, ou envio do último arquivo.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
