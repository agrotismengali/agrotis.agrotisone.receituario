﻿using Agrotis.Framework.Data.Database;
using System;
using static Agrotis.AgrotisOne.ReceituarioDLL.Model.Tabelas;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public class UdtAgenteExportacao : UDTModelBase
    {
        #region Métodos Públicos
        public override string TableName
        {
            get
            {
                return AgenteExportacao.Nome;
            }
        }

        protected override bool AutoKey
        {
            get
            {
                return true;
            }
        }

        protected override string PREFIX_FIELD
        {
            get
            {
                return "U_AGRT_";
            }
        }
        #endregion

        public string State { get; set; }
        public DateTime DtInicial { get; set; }
        public DateTime DtFinal { get; set; }
        public DateTime DtUltimoEnvio { get; set; }        
    }
}
