﻿using Agrotis.Framework.Data.Database;
using static Agrotis.AgrotisOne.ReceituarioDLL.Model.Tabelas;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public class UdtEmbalagemProdMapa : UDTModelBase
    {
        #region Métodos Públicos
        public override string TableName
        {
            get
            {
                return EmbalagemProdutoMapa.Nome;
            }
        }

        protected override bool AutoKey
        {
            get
            {
                return true;
            }
        }

        protected override string PREFIX_FIELD
        {
            get
            {
                return "U_AGRT_";
            }
        }
        #endregion

        public string CodeAgente { get; set; }
        public string ItemCode { get; set; }
        public string CodeEmbalagem { get; set; }
        public string ProdutoMapa { get; set; }        
    }
}
