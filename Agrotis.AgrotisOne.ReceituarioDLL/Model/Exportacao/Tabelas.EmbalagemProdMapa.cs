﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public static partial class Tabelas
    {
        public static class EmbalagemProdutoMapa
        {
            public const string Nome = "AGRT_EMBPRODMAPA";
            public const string Descricao = "AGRT: Embalagem Prod. Mapa";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_NoObjectAutoIncrement;

            public static class Campos
            {
                public static class CodeAgente
                {
                    public const string Nome = "AGRT_CodeAgente";
                    public const string Descricao = "Código Agente";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para relacionar a tabela de embalagem x produto mapa com o agente fiscalizador.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class ItemCode
                {
                    public const string Nome = "AGRT_ItemCode";
                    public const string Descricao = "Código do Item";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para fazer a ligação com o ItemCode da tabela OITM";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class CodeEmbalagem
                {
                    public const string Nome = "AGRT_CodeEmbalagem";
                    public const string Descricao = "Código Embalagem";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para informar o código padrão da embalagem para o agente fiscalizador.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class ProdutoMapa
                {
                    public const string Nome = "AGRT_ProdutoMapa";
                    public const string Descricao = "Produto MAPA";
                    public const int Tamanho = 20;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o código do Produto MAPA correlacionado ao item de estoque SAP.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
