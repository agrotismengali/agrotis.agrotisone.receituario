﻿using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.Framework.Data;
using Agrotis.Framework.Data.UDO;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public class ControllerAgenteExportacao
    {
        List<Dictionary<string, object>> AgentesCadastrados;
        public enum MtxDadosColumns
        {
            NumeroNota,
            SerieNota,
            DataDoc,
            PropNome,
            ItemCode,
            ProdMapa,
            DescMapa,
            Quantidade,
            Embalagem,
            UniMedida
        }
        public enum DtDadosColumns
        {
            FilialCnpj,
            NumeroNota,
            SerieNota,
            DataDoc,
            PnRazaoSoc,
            ProdCnpj,
            ProdCpf,
            ProdRua,
            ProdBairro,
            ProdComple,
            ProdCidade,
            ProdCep,
            ProdEstado,
            ProdNumero,
            PropNome,
            PropIE,
            ProdIbge,
            FilialIbge,
            ItemCode,
            ProdMapa,
            DescMapa,
            Quantidade,
            Embalagem,
            UniMedida,
            DocEntry,
            TipoMov
        }        
        public static IList<Tuple<int, string, string>> Agentes = new List<Tuple<int, string, string>>()
        {
            new Tuple<int, string, string>(1, "SICCA/IMA", "MG")
        };       
        public List<Dictionary<string, object>> GetAgentesCadastrados()
        {
            if (AgentesCadastrados == null)
                AgentesCadastrados = SqlUtils.DoQueryH(DaoExportacao.GetSQLAgenteExportacaoCode());
            return AgentesCadastrados;
        }
        public bool isAgenteCadastrado(int aCode)
        {            
            foreach (Dictionary<string, object> Agente in GetAgentesCadastrados())
                if (Agente["Code"].Equals(aCode))
                    return true;
            return false;
        }
        public void CriarAgenteExportacaoPadrao()
        {
            foreach (Tuple<int, string, string> Agente in Agentes)
            {
                if (!isAgenteCadastrado(Agente.Item1))
                {
                    UdtAgenteExportacao UdtAgente = new UdtAgenteExportacao
                    {
                        Code = Agente.Item1,
                        Name = Agente.Item2,
                        State = Agente.Item3
                    };
                    UdtAgente.Upsert();
                }
            }
        }
        public int isRelacaoEmbProdMapaCadastrado(string aAgente, string aItemCode, string aProdMapa)
        {
            int Code;
            int.TryParse(SqlUtils.GetValue(
                DaoExportacao.GetSQLEmbalagemProdMapa(aAgente, aItemCode, aProdMapa)), out Code);
            return Code;
        }
        public void CriarRelacaoEmbalagemProdutoMapa(string aAgente, string aItemCode, string aProdMapa, string aEmbalagem)
        {
            int Code = isRelacaoEmbProdMapaCadastrado(aAgente, aItemCode, aProdMapa);
            UdtEmbalagemProdMapa UdtEmbalagemProdMapa = new UdtEmbalagemProdMapa();
            if (Code > 0)
                UdtEmbalagemProdMapa.Code = Code;
            UdtEmbalagemProdMapa.CodeAgente = aAgente;
            UdtEmbalagemProdMapa.ItemCode = aItemCode;
            UdtEmbalagemProdMapa.ProdutoMapa = aProdMapa;
            UdtEmbalagemProdMapa.CodeEmbalagem = aEmbalagem;
            UdtEmbalagemProdMapa.Upsert();
        }        
        public static void CriarEmbalagemPadrao(int aCodigo, string aAgente, string aEmbalagem, string aDescUniMed)
        {
            Util.ExibirMensagemStatusBar($"Criando embalagem para exportação do relatório IMA. Embalagem [{aDescUniMed}]");
            DTOMasterData DtoMasterData = new DTOMasterData()
            {
                UDO = Tabelas.EmbalagemPadrao.Nome,
                DataFields = new List<Tuple<string, string, Type>>()
                {
                    new Tuple<string, string, Type>("Code", aCodigo.ToString(), typeof(string)),
                    new Tuple<string, string, Type>("Name", aDescUniMed, typeof(string)),
                    new Tuple<string, string, Type>("U_AGRT_CodeAgente", aAgente, typeof(string)),
                    new Tuple<string, string, Type>("U_AGRT_CodeEmbalagem", aEmbalagem, typeof(string)),
                    new Tuple<string, string, Type>("U_AGRT_DescUniMed", aDescUniMed, typeof(string))
                }
            };

            string Error = string.Empty;
            UtilUDO.UDOPersistirDados(DtoMasterData, out Error);
        }
        public bool isEmbalagemPadraoIMACadastrado()
        {
            string CodEmb = SqlUtils.GetValue(
                DaoExportacao.GetSQLEmbalagemPadraoImaCadastrado(EmbalagemPadraoIMA.CodigoAgenteIMA));
            return !string.IsNullOrEmpty(CodEmb);
        }
        public void CriarEmbalagemPadraoIMA()
        {
            if (isEmbalagemPadraoIMACadastrado())
                return;
            try
            {
                int i = 1;
                foreach (Tuple<string, string> Embalagem in EmbalagemPadraoIMA.GetListaEmbalagensIMA())
                    CriarEmbalagemPadrao(i++, EmbalagemPadraoIMA.CodigoAgenteIMA, Embalagem.Item1, Embalagem.Item2);
            }
            finally
            {
                GC.Collect();
            }
        }
        public string RetirarCarcteresEspeciais(string aValor)
        {
            string replacement = "";
            Regex rgx = new Regex(@"[^0-9a-zA-Z ]");
            return rgx.Replace(aValor, replacement);
        }
        public bool GerarRelatorioMovimentacao(string aNomeArquivo, DataTable aDtDados)
        {            
            var csv = new StringBuilder();
            int DocEntry = aDtDados.GetValue(DtDadosColumns.DocEntry.ToString(), 0);
            string Cabecalho = string.Empty;
            string Itens = string.Empty;
            for (int iRow = 0; iRow < aDtDados.Rows.Count; iRow++)
            {
                // Para cada DocEntry diferente informa os dados de cabeçalho novamente
                if (!DocEntry.Equals(aDtDados.GetValue(DtDadosColumns.DocEntry.ToString(), iRow)))
                {
                    csv.AppendLine(string.Concat(Cabecalho, Itens));
                    DocEntry = aDtDados.GetValue(DtDadosColumns.DocEntry.ToString(), iRow);
                    Itens = string.Empty;
                }
                string FilialCnpj = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.FilialCnpj.ToString(), iRow));
                string TipoMov = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.TipoMov.ToString(), iRow));
                string NumeroNota = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.NumeroNota.ToString(), iRow).ToString());
                string SerieNota = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.SerieNota.ToString(), iRow).Trim());
                string DataDoc = aDtDados.GetValue(DtDadosColumns.DataDoc.ToString(), iRow).ToString("yyyy-MM-dd");
                string PnRazaoSoc = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.PnRazaoSoc.ToString(), iRow).Trim());
                string ProdCnpj = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdCnpj.ToString(), iRow));
                string ProdCpf = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdCpf.ToString(), iRow));
                string Endereco =
                    RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdRua.ToString(), iRow).Trim()) + ", " +
                    RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdNumero.ToString(), iRow).ToString()) + "  " +
                    RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdComple.ToString(), iRow).Trim()) + "  " +
                    RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdBairro.ToString(), iRow).Trim()) + " - " +
                    RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdCidade.ToString(), iRow).Trim()) + " " +
                    RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdEstado.ToString(), iRow).Trim()) + "  " +
                    RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdCep.ToString(), iRow).Trim());

                string ProdIbge = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.ProdIbge.ToString(), iRow).ToString());
                string NumReceita = "";
                string PropNome = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.PropNome.ToString(), iRow).Trim());
                string PropIE = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.PropIE.ToString(), iRow));
                string FilialIbge = RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.FilialIbge.ToString(), iRow).ToString());

                string ProdCpfCnpj = (string.IsNullOrEmpty(ProdCnpj) ? ProdCpf : ProdCnpj);
                Cabecalho = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}",
                    FilialCnpj, TipoMov, NumeroNota, SerieNota, DataDoc, PnRazaoSoc, ProdCpfCnpj, Endereco, ProdIbge,
                    NumReceita, PropNome, PropIE, FilialIbge);
                Itens += string.Format(";{0};{1};{2}",
                    aDtDados.GetValue(DtDadosColumns.ProdMapa.ToString(), iRow),
                    aDtDados.GetValue(DtDadosColumns.Quantidade.ToString(), iRow).ToString(),
                    RetirarCarcteresEspeciais(aDtDados.GetValue(DtDadosColumns.Embalagem.ToString(), iRow)));                
            }
            csv.AppendLine(string.Concat(Cabecalho, Itens));
            File.WriteAllText(aNomeArquivo, csv.ToString());
            return true;
        }
    }
}
