﻿using System;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTOReceituario
    {        
        public string CodeReceituario { get; set; }
        public string RegistroMapa { get; set; }
        public string CodCultura { get; set; }
        public string IdReceita { get; set; }
        public string NumReceita { get; set; }
        public string UUIDProdutor { get; set; }
        public string UUIDPropriedade { get; set; }
        public string SiglaUF { get; set; }
        public string Quantidade { get; set; }
        public string SequenceId { get; set; }
        public string CNPJEmpresa { get; set; }
        public string DataDoc { get; set; }
        public string CodFilial { get; set; }

        public string[] GetDataRecord()
        {
            return new string[13] {
                (CodeReceituario == null || CodeReceituario == String.Empty) ? "" : CodeReceituario,
                (RegistroMapa == null || RegistroMapa == String.Empty) ? "" : RegistroMapa,
                (CodCultura == null || CodCultura == String.Empty) ? "" : CodCultura,
                (IdReceita == null || IdReceita == String.Empty) ? "" : IdReceita,
                (NumReceita == null || NumReceita == String.Empty) ? "" : NumReceita,
                (UUIDProdutor == null || UUIDProdutor == String.Empty) ? "" : UUIDProdutor,
                (UUIDPropriedade == null || UUIDPropriedade == String.Empty) ? "" : UUIDPropriedade,
                (Quantidade == null || Quantidade == String.Empty) ? "" : Quantidade,
                (SequenceId == null || SequenceId == String.Empty) ? "" : SequenceId,
                (SiglaUF == null || SiglaUF == String.Empty) ? "" : SiglaUF,
                (CNPJEmpresa == null) ? CNPJEmpresa : CNPJEmpresa,
                (DataDoc == null) ? DataDoc : DataDoc,
                (CodFilial == null) ? CodFilial : CodFilial
            };
        }
        public DTOReceituario(string[] aArray)
        {
            CodeReceituario = aArray[0];
            RegistroMapa = aArray[1];
            CodCultura = aArray[2];
            IdReceita = aArray[3];
            NumReceita = aArray[4];
            UUIDProdutor = aArray[5];
            UUIDPropriedade = aArray[6];
            Quantidade = aArray[7];
            SequenceId = aArray[8];
            SiglaUF = aArray[9];
            CNPJEmpresa = aArray[10];
            DataDoc = aArray[11];
            CodFilial = aArray[12];
        }
        public DTOReceituario()
        {
            //
        }
    }        
}
