﻿namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTOInfoProdPerigoso
    {       
        public string itemCode { get; set; }
        public string regMapa { get; set; }
        public string nome { get; set; }
        public string nroONU { get; set; }
        public string clasTxc { get; set; }
        public string grauRisco { get; set; }
        public string classe { get; set; }
        public string nomeEmb { get; set; }
        public string princAtv { get; set; }
        public string grupoEmb { get; set; }
        public bool temInfProd { get; set; }

        public DTOInfoProdPerigoso(string aItemCode, string aRegMapa, string aNome)
        {
            this.itemCode = aItemCode;
            this.regMapa = aRegMapa;
            this.nome = aNome;
        }

        public DTOInfoProdPerigoso(string aItemCode, string aRegMapa, string aNome, string aNroONU, string aClasTxc, 
            string aGrauRisco, string aClasse, string aNomeEmb, string aPrincAtv, string aGrupoEmb, bool aTemInfProd)
        {
            this.itemCode = aItemCode;
            this.regMapa = aRegMapa;
            this.nome = aNome;
            this.nroONU = aNroONU;
            this.clasTxc = aClasTxc;
            this.grauRisco = aGrauRisco;
            this.classe = aClasse;
            this.nomeEmb = aNomeEmb;
            this.princAtv = aPrincAtv;
            this.grupoEmb = aGrupoEmb;
            this.temInfProd = aTemInfProd;
        }
    }
}
