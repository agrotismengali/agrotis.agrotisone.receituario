﻿using Agrotis.AgrotisOne.Core.Utils;
using System;
using System.Collections.Generic;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTOApi
    {        
        public string Uri { get; set; }
        public string Token { get; set; }     
        public string DiretorioRelatorios { get; set; }
        public string GrupoUtilizacao { get; set; }
        public string BuscarInfoNfPlataforma { get; set; }

        public string[] GetDataRecord()
        {
            return new string[5] {
                Uri ?? "",
                Token ?? "",
                DiretorioRelatorios ?? "",
                GrupoUtilizacao ?? "",
                BuscarInfoNfPlataforma ?? ""};
        }
        public DTOApi(string[] aArray)
        {
            Uri = aArray[0];
            Token = aArray[1];
            DiretorioRelatorios = aArray[2];
            GrupoUtilizacao = aArray[3];
            BuscarInfoNfPlataforma = aArray[4];
        }
        public void ValidarPreenchimentoDTO()
        {
            if ((string.IsNullOrEmpty(Uri)) || (string.IsNullOrEmpty(Token)) || 
                (string.IsNullOrEmpty(DiretorioRelatorios)) || (string.IsNullOrEmpty(GrupoUtilizacao))) {
                Util.ExibirDialogo("Atenção! Os parâmetros para emissão de receitas estão incompletos. " +
                    "Favor verificar as configurações em [AgrotisOne Receituário > Configurações > Configurações Receituário]");
            }
        }
        public DTOApi()
        {
            List<Dictionary<string, object>> lConfig = SqlUtils.DoQueryH(DaoReceituario.GetAgrtConfRec());
            if (lConfig.Count > 0)
            {                
                DiretorioRelatorios = System.IO.Path.GetTempPath();
                if (lConfig[0]["U_AGRT_UrlPlataforma"] != null)
                    Uri = (lConfig[0]["U_AGRT_UrlPlataforma"].ToString());
                if (lConfig[0]["U_AGRT_Token"] != null)
                    Token = (lConfig[0]["U_AGRT_Token"].ToString());
                if (lConfig[0]["U_AGRT_GrpUtiliz"] != null)
                    GrupoUtilizacao = (lConfig[0]["U_AGRT_GrpUtiliz"].ToString());
                if (lConfig[0]["U_AGRT_BuscarInfoNfPlataforma"] != null)
                    BuscarInfoNfPlataforma = (lConfig[0]["U_AGRT_BuscarInfoNfPlataforma"].ToString());
            }
            ValidarPreenchimentoDTO();
        }
    }        
}
