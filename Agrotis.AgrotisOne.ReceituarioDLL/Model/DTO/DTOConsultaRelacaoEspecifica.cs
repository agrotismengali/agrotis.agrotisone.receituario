﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTORelacaoEspecifica
    {
        public Int32 codRelacao { get; set; }
        public Int32 codTipoSolo { get; set; }
        public Int32 codCultura { get; set; }
    }
}
