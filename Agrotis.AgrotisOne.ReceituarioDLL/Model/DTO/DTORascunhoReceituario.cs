﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTORascunhoReceituario
    {
        public string idProdutor { get; set; }
        public string dataEmissao { get; set; }
        public string idFazenda { get; set; }
        /*public Int32 idTalhao { get; set; } PROVELMENTE NÃO TERÁ TALHÃO */
        public Int32 idRT { get; set; }
        public Int32 idEmpresa { get; set; }
    }
}
