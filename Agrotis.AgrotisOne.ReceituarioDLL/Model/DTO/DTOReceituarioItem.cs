﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTOReceituarioItem
    {
        public object idReceita { get; set; }
        public Int32 codRelacao { get; set; }
        public Double quantidade { get; set; }
        public Double numeroAplicacoes { get; set; }
        public Double dose { get; set; }
        public Int32 codPedidoItem { get; set; }
        public Double calda { get; set; }
        public double areaAplicacao { get; set; }
        public bool validarImpressao { get; set; }
        public string codModeloReceita { get; set; }
        public Int32 codCultura { get; set; }
        public long tipoDeSolo { get; set; }
        public string obsRecomendacoes { get; set; }
        public string obsEquipAplicacao { get; set; }
    }
}
