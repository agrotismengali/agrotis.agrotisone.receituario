﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTOConsultaRelacoes
    {
        public String codMapaProduto { get; set; }
        public Int32 codCultura { get; set; }
        public Int32[] codProblema { get; set; }
        public bool filtrarTodosProblemas { get; set; }
        public string ufSigla { get; set; }
        public bool listarRelacoesRestritas { get; set; }
    }
}
