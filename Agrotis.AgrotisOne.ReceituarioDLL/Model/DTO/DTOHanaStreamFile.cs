﻿using Agrotis.AgrotisOne.Core.DTOs;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTOHanaStreamReceituario: DTOHanaStreamFile
    {        
        public ReceituarioVersion Version { get; set; }
    }
}
