﻿using System;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO
{
    public class DTOCriarReceita
    {
        public string CodigoReceituarioB1 { get; set; }
        public int CodigoCultura { get; set; }
        public string NomeCultura { get; set; }
        public string CodMapaProduto { get; set; }
        public string NomeProduto { get; set; }
        public string SiglaUF { get; set; }
        public string SequenceId { get; set; }
        public string IdRascunho { get; set; }
        public string LineId { get; set; }
        public string CodRelacao { get; set; }
        public string CodProblema { get; set; }
        public string Problema { get; set; }
        public string IdProdutor { get; set; }
        public Int32 IdFazenda { get; set; }
        public Int32 IdTalhao { get; set; }
        public Int32 IdRT { get; set; }
        public Int32 IdEmpresa { get; set; }
        public double Quantidade { get; set; }
        public string CodCultAgrotis { get; set; }
        public string CodTipoSolo { get; set; }

        public DTOCriarReceita(string[] aArray)
        {
            CodigoCultura = Convert.ToInt32(aArray[0]);
            NomeCultura = aArray[1];
            CodMapaProduto = aArray[2];
            NomeProduto = aArray[3];
            SiglaUF = aArray[4];
            SequenceId = aArray[5];
            LineId = aArray[6];
            CodRelacao = aArray[7];
            CodProblema = aArray[8];
            Problema = aArray[9];
            IdProdutor = aArray[10];
            IdFazenda = Convert.ToInt32( aArray[11]);
            Quantidade = Convert.ToDouble(aArray[12]);
            IdTalhao = Convert.ToInt32(aArray[13]);
            IdEmpresa = Convert.ToInt32(aArray[14]);
            IdRT = Convert.ToInt32(aArray[15]);
            CodigoReceituarioB1 = aArray[16];
            IdRascunho = aArray[17];
            CodCultAgrotis = aArray[18];
            CodTipoSolo = aArray[19];
        }
        public string[] GetDataRecord()
        {
            return new string[20] {
                CodigoCultura.ToString(),
                (NomeCultura == null || NomeCultura == string.Empty) ? "" : NomeCultura,
                CodMapaProduto.ToString(),
                (NomeProduto == null || NomeProduto == string.Empty) ? "" : NomeProduto,
                (SiglaUF == null || SiglaUF == string.Empty) ? "" : SiglaUF ,
                (SequenceId == null ) ? string.Empty : SequenceId,
                (LineId == null ) ? string.Empty : LineId,
                (CodRelacao == null ) ? string.Empty : CodRelacao,
                (CodProblema == null ) ? string.Empty : CodProblema,
                (Problema == null ) ? string.Empty : Problema,
                IdProdutor,
                IdFazenda.ToString(),
                Quantidade.ToString(),
                IdTalhao.ToString(),
                IdEmpresa.ToString(),
                IdRT.ToString(),
                CodigoReceituarioB1,
                IdRascunho,
                (CodCultAgrotis == null ) ? string.Empty : CodCultAgrotis,
                (CodTipoSolo == null ) ? string.Empty : CodTipoSolo};
        }

        public DTOCriarReceita()
        { }
    }
}
