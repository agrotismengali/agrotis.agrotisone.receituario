﻿using System;
using Agrotis.Framework.Data;
using System.Globalization;
using Agrotis.AgrotisOne.Core.Utils;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    /// <summary>
    /// Classe que contém os campos de usuário das tabelas do sistema
    /// </summary>
    public class Pesquisas
    {
        /// <summary>
        /// Para uso do cálculo do CustoMédio, retorna a quantidade em estoque antes de determinada nota!
        /// </summary>
        /// <param name="DocEntry">DocEntry da Nota Fiscal de Compra</param>
        /// <param name="ItemCode">Código do Item</param>
        /// <returns></returns>
        public static Double QtdeEstoque(string DocEntry, string ItemCode)
        {
            string Estoque = SqlUtils.GetValue(@"SELECT AGRT_FN_GER_RetornaQtdeEstoque(" + DocEntry.ToString() + ", '" + ItemCode + "') FROM DUMMY;");

            if (string.IsNullOrEmpty(Estoque))
                Estoque = "0";

            return Convert.ToDouble(Estoque, CultureInfo.CurrentCulture);
        }
    }
}