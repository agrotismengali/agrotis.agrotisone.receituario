﻿using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using static Agrotis.AgrotisOne.Core.Model.Version;
using static Agrotis.Framework.Main.Application.AddOnEnum;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model.Scripts
{
    public class HanaScripts
    {
        public DTOHanaStreamReceituario[] HanaScriptsReceituario(long aVersaoBanco)
        {
            int Count = 0;
            List<string> HanaSequences = GetType().Assembly.GetManifestResourceNames().ToList().FindAll(
                delegate (string s) {return s.Contains("Sequences");
            });
            List<string> HanaProcedures = GetType().Assembly.GetManifestResourceNames().ToList().FindAll(
                delegate (string s) {return s.Contains("Procedures");
            });

            DTOHanaStreamReceituario[] HanaScriptsReceituario = new DTOHanaStreamReceituario[HanaSequences.Count + HanaProcedures.Count];

            #region HanaSequences
            for (int iPos = 0; iPos < HanaSequences.Count; iPos++)
            {
                HanaScriptsReceituario[Count] = new DTOHanaStreamReceituario();
                HanaScriptsReceituario[Count].Tipo = TipoScript.Sequence;
                HanaScriptsReceituario[Count].FileName = HanaSequences[iPos];
                HanaScriptsReceituario[Count].Bytes = new StreamReader(
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(HanaSequences[iPos]));
                HanaScriptsReceituario[Count].Version = GetScriptVersion(HanaSequences[iPos]);
                Count++;
            }
            #endregion

            #region HanaProcedures
            for (int iPos = 0; iPos < HanaProcedures.Count; iPos++)
            {                
                HanaScriptsReceituario[Count] = new DTOHanaStreamReceituario();
                HanaScriptsReceituario[Count].Tipo = TipoScript.Procedure;
                HanaScriptsReceituario[Count].FileName = HanaProcedures[iPos];
                HanaScriptsReceituario[Count].Bytes = new StreamReader(
                    Assembly.GetExecutingAssembly().GetManifestResourceStream(HanaProcedures[iPos]));
                HanaScriptsReceituario[Count].Version = GetScriptVersion(HanaProcedures[iPos]);
                Count++;
            }
            #endregion

            double versaoBanco = Math.Truncate((double)aVersaoBanco / GetVersionPrecision(Produto.Receituario));
            return HanaScriptsReceituario.Where(u => (long)u.Version >= versaoBanco).ToArray();
        }

        public static ReceituarioVersion GetScriptVersion(string aFileName)
        {
            bool isRegistered = false;
            ReceituarioVersion versionNum = ReceituarioVersion.v1001001;
            string scriptName = DBUtil.GetFileNameFromResourceName(aFileName).ToUpper();
            foreach (Type dbo in typeof(SqlDBODefinition).GetNestedTypes())
            {                
                string nome = dbo?.GetField("Nome").GetValue(null).ToString();
                var versao = dbo?.GetField("Versao")?.GetValue(null);                
                if (nome.ToUpper() == scriptName.ToUpper())
                {
                    isRegistered = true;
                    if (versao == null)
                        throw new Exception($"Objeto de banco de dados {scriptName} sem versão definida no AddOn Receituário.");
                    else
                        versionNum = ParseVersion<ReceituarioVersion>(versao.ToString());
                    break;
                }
            }
            if (!isRegistered)
                throw new Exception($"Objeto de banco de dados {scriptName} sem definição criada no AddOn Receituário.");
            return versionNum;
        }

    }
}
