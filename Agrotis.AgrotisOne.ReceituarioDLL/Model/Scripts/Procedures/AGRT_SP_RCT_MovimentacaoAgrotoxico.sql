Create PROCEDURE AGRT_SP_RCT_MovimentacaoAgrotoxico (
    in filial int,
	in agente int,
	in data_ini timestamp,
    in data_fin timestamp
)
language SQLSCRIPT
as	
begin		
	-- Variáveis globais
	declare gRegistroAtual int = 0;
	declare gTotalRegistro int = 0;
	declare Documento varchar(20) = '';
	declare DocumentoItem varchar(20) = '';
	declare DocumentoEndereco varchar(20) = '';
	declare TipoMovimentacao varchar(10) = '';
		
	call "AGRT_SP_DELETATABELATEMPORARIA" ('#TabelasMovimentacao');
	create local temporary table #TabelasMovimentacao
	(
	    Reg int,
	    TipoMovimentacao nvarchar(10),
		Documento varchar(20),
		DocumentoItem varchar(20),
		DocumentoEndereco varchar(20)
	);	
	
	EXEC 
	    'insert into #TabelasMovimentacao 
	     select 1, ''SAIDA'', ''OINV'', ''INV1'', ''INV12'' from dummy union all
	     select 2, ''SAIDA'', ''ODLN'', ''DLN1'', ''DLN12'' from dummy union all
	     select 3, ''SAIDA'', ''ORDN'', ''RDN1'', ''RDN12'' from dummy union all
	     select 4, ''SAIDA'', ''ORIN'', ''RIN1'', ''RIN12'' from dummy union all	     
	     select 5, ''ENTRADA'', ''OPDN'', ''PDN1'', ''PDN12'' from dummy union all
	     select 6, ''ENTRADA'', ''ORPD'', ''RPD1'', ''RPD12'' from dummy union all
	     select 7, ''ENTRADA'', ''ORPC'', ''RPC1'', ''RPC12'' from dummy union all
	     select 8, ''ENTRADA'', ''OPCH'', ''PCH1'', ''PCH12'' from dummy ';
	     
	call "AGRT_SP_DELETATABELATEMPORARIA" ('#DadosMovimentacao');
	create local temporary table #DadosMovimentacao
    (
        FilialCnpj nvarchar(50),
		NumeroNota int,
		SerieNota nvarchar(10),
		DataDocumento timestamp,
		PnRazaoSocial nvarchar(250),
		ProdCnpj nvarchar(50),
		ProdCpf nvarchar(50),
		ProdRua nvarchar(150),
		ProdBairro nvarchar(50),
		ProdComplemento nvarchar(250),
		ProdCidade nvarchar(50),
		ProdCep nvarchar(10),
		ProdEstado nvarchar(10),
		ProdNumero int,
		PropriedadeNome nvarchar(250),
		PropriedadeIE nvarchar(50),
		ProdIbgeCode int,
		FilialIbgeCode int,
		ItemCodeSap nvarchar(50),
		ProdutoMapa nvarchar(50),
		DescProdutoMapa nvarchar(250),
		Quantidade double,
		Embalagem nvarchar(50),
		UnidadeMedida nvarchar(50),
		DocEntry int,
		TipoMovimentacao nvarchar(10)
    );     
	     
	gRegistroAtual = 1;
	select count(Reg) into gTotalRegistro from #TabelasMovimentacao;					
	
	while gRegistroAtual <= gTotalRegistro
	do		    	
		select TipoMovimentacao, Documento,  DocumentoItem,  DocumentoEndereco
		into   TipoMovimentacao, Documento,  DocumentoItem,  DocumentoEndereco
		from  #TabelasMovimentacao 
		where Reg >= gRegistroAtual
		order by Reg limit 1;						
	    
	    EXEC	    	
	    'insert into #DadosMovimentacao
		    select 
				coalesce(Documento."VATRegNum", '''') as "FilialCnpj",
				coalesce(Documento."Serial", 0) as "NumeroNota",
				coalesce(Documento."SeriesStr", '''') as "SerieNota", 
				coalesce(Documento."DocDate", '''') as "DataDocumento",
				coalesce(Documento."CardName", '''') as "PnRazaoSocial",
				coalesce(DocumentoEndereco."TaxId0", '''') as "ProdCnpj",
				coalesce(DocumentoEndereco."TaxId4", '''') as "ProdCpf",
				coalesce(DocumentoEndereco."StreetS", CRD1."Street") as "ProdRua", 
				coalesce(DocumentoEndereco."BlockS", CRD1."Block") as "ProdBairro",
				coalesce(DocumentoEndereco."BuildingS", CRD1."Building", '''') as "ProdComplemento",
				coalesce(DocumentoEndereco."CityS", CRD1."City", '''') as "ProdCidade",
				coalesce(DocumentoEndereco."ZipCodeS", CRD1."ZipCode", '''') as "ProdCep",
				coalesce(DocumentoEndereco."StateS", CRD1."State", '''') as "ProdEstado",
				coalesce(DocumentoEndereco."StreetNoS", 0) as "ProdNumero",
				coalesce(CRD1."Address", DocumentoEndereco."Address2S", '''') as "PropriedadeNome",
				coalesce(DocumentoEndereco."TaxId1", '''') as "PropriedadeIE",
				coalesce(OCNT_PROD."IbgeCode", 0) as "ProdIbgeCode",	
				coalesce(OCNT_FILIAL."IbgeCode", 0) as "FilialIbgeCode",
				coalesce(OITM."ItemCode", '''') as "ItemCodeSap",
				coalesce(PROD_MAPA."U_AGRT_CodSubProd", '''') as "ProdutoMapa",
				coalesce(PROD_MAPA."U_AGRT_NomeSubProd", '''') as "DescProdutoMapa",
				coalesce(DocumentoItem."Quantity", 0) as "Quantidade",
				coalesce(EMB_PROD_MAPA."U_AGRT_CodeEmbalagem", '''') as "Embalagem",
				coalesce(EMB_PADRAO."U_AGRT_DescUniMed", '''') as "UnidadeMedida",
				coalesce(Documento."DocEntry", 0) as "DocEntry",
				''' || TipoMovimentacao || ''' as "TipoMovimentacao"
			from ' || Documento || ' as Documento 			
			inner join ' || DocumentoItem || ' as DocumentoItem on
				DocumentoItem."DocEntry" = Documento."DocEntry"	
			inner join ' || DocumentoEndereco || ' as DocumentoEndereco on
				DocumentoEndereco."DocEntry" = Documento."DocEntry"	
			left join CRD1 on
				CRD1."Address" = CASE WHEN ''' || TipoMovimentacao || ''' = ''SAIDA'' THEN Documento."ShipToCode" else Documento."PayToCode" end and
				CRD1."CardCode" = Documento."CardCode" and 
	            CRD1."AdresType" = CASE WHEN ''' || TipoMovimentacao || ''' = ''SAIDA'' THEN ''S'' else ''B'' end 
			inner join OITM on
				OITM."ItemCode" = DocumentoItem."ItemCode"
			inner join "@AGRT_RECEITSUBPROD" as PROD_MAPA on
				PROD_MAPA."U_AGRT_ItemCode" = OITM."ItemCode"	
			inner join OBPL on
				OBPL."BPLId" = Documento."BPLId"
			inner join "@AGRT_AGTEXPORT" as AGENTE on
				AGENTE."Code" = ' || :agente || ' and
				AGENTE."U_AGRT_State" = DocumentoEndereco."State"
			left join "@AGRT_EMBPRODMAPA" as EMB_PROD_MAPA on
				EMB_PROD_MAPA."U_AGRT_ItemCode" = OITM."ItemCode" and 
				EMB_PROD_MAPA."U_AGRT_ProdutoMapa" = PROD_MAPA."U_AGRT_CodSubProd" and
				EMB_PROD_MAPA."U_AGRT_CodeAgente" = AGENTE."Code"
			left join "@AGRT_EMBPADRAO" as EMB_PADRAO on
				EMB_PADRAO."U_AGRT_CodeEmbalagem" = EMB_PROD_MAPA."U_AGRT_CodeEmbalagem"	
			left join OCNT as OCNT_PROD on
				cast(OCNT_PROD."AbsId" as varchar) = DocumentoEndereco."CountyS"
			left join OCNT as OCNT_FILIAL on
				cast(OCNT_FILIAL."AbsId" as varchar) = OBPL."County"
			where 
			    Documento."BPLId" = ' || :filial || ' and
			    Documento."CANCELED" = ''N'' and
				Documento."DocDate" between ''' || :data_ini || ''' and ''' || :data_fin || '''
			order by
				Documento."DocEntry", Documento."DocDate" ';
				
	    gRegistroAtual = gRegistroAtual + 1;
	end while;	

	select * from #DadosMovimentacao;
end;