﻿create procedure AGRT_SP_RCT_CopiarReceituarioPedido
(
	CodNovoReceituario int,
	BaseEntry int,
	BaseLine int,
	Quantidade double
)
as
begin	
	declare SqlCommand varchar(5000) = '';
	declare CodReceituarioPedido int = 0;
	declare PercentualPedido double = 0;
	
	declare EXIT HANDLER for SQL_ERROR_CODE 1299
	begin
   		call "AGRT_SP_GRAVARLOG" (BaseEntry, 17, 'AGRT_SP_REC_CopiarReceituarioPedido', 'Erro durante execução da procedure', ::SQL_ERROR_CODE || ' - ' || ::SQL_ERROR_MESSAGE, 0);  
	end;
	declare EXIT HANDLER for sqlexception	
	begin
		call "AGRT_SP_GRAVARLOG" (BaseEntry, 17, 'AGRT_SP_REC_CopiarReceituarioPedido', 'Erro durante execução da procedure', ::SQL_ERROR_CODE || ' - ' || ::SQL_ERROR_MESSAGE, 0);  
	end;
	
	-- Grava o código de receituário do Pedido
	select 
		coalesce(ORDR."U_AGRT_CodRec",0) 
	into 
		CodReceituarioPedido 
	from 
		ORDR 
	where 
		ORDR."DocEntry" = :BaseEntry;

	-- Tratativa para caso o pedido seja parcialmente faturado
	select 
		Quantidade / coalesce(RDR1."Quantity", 1) 
	into 
		PercentualPedido 
	from 
		RDR1 
	where 
		RDR1."DocEntry" = :BaseEntry and
		RDR1."LineNum" = :BaseLine; 	

	SqlCommand = '
		update 
			"@AGRT_RECEIT" as REC
		set 
			REC."U_AGRT_CodeAgron" = PED."U_AGRT_CodeAgron", 
			REC."U_AGRT_NomeAgron" = PED."U_AGRT_NomeAgron",
			REC."U_AGRT_IdRascunho" = PED."U_AGRT_IdRascunho"
		from 	
			"@AGRT_RECEIT" as PED
		where
			REC."Code" = '||:CodNovoReceituario||' and 
			PED."Code" = '||:CodReceituarioPedido||'
	';
	exec SqlCommand;
		
	SqlCommand = '
		insert into "@AGRT_RECEITL"(
			"Code",	
			"LineId", 
			"LogInst", 
			"Object", 
			"U_AGRT_CodCultura",	
			"U_AGRT_ItemCode", 
			"U_AGRT_LineNum", 
			"U_AGRT_NomeCultura", 
			"U_AGRT_NomeRegMapa", 
			"U_AGRT_QtdeKgLt",	
			"U_AGRT_RegMapa"
		)
		select 
			'||:CodNovoReceituario||',	
			'||:CodReceituarioPedido||'+RECL."LineId",
			RECL."LogInst",
			RECL."Object",
			RECL."U_AGRT_CodCultura",
			RECL."U_AGRT_ItemCode",
			RECL."U_AGRT_LineNum",
			RECL."U_AGRT_NomeCultura",
			RECL."U_AGRT_NomeRegMapa",
			round('||coalesce(:PercentualPedido,1)||' * RECL."U_AGRT_QtdeKgLt", 2),
			RECL."U_AGRT_RegMapa"
		from 
			"@AGRT_RECEITL" as RECL
		inner join ORDR on 
			ORDR."U_AGRT_CodRec" = RECL."Code"
		inner join RDR1 on
			RDR1."DocEntry"	= ORDR."DocEntry" and
			RDR1."LineNum" = RECL."LineId"-1
		where 
			ORDR."DocEntry" = '||:BaseEntry||' and
			RDR1."LineNum" = '||:BaseLine||'
	';
	exec SqlCommand;
	
	SqlCommand = '
		insert into "@AGRT_RECEITAGRUP"(
			"Code", 
			"LineId", 
			"LogInst", 
			"Object", 
			"U_AGRT_CodCultura", 
			"U_AGRT_DadosNFe", 
			"U_AGRT_IDItemReceita", 
			"U_AGRT_IDReceita", 
			"U_AGRT_NomeCultura", 
			"U_AGRT_NomeRegMapa", 
			"U_AGRT_NumReceita", 
			"U_AGRT_QtdeKgLt", 
			"U_AGRT_RegMapa", 
			"U_AGRT_Situacao",
			"U_AGRT_StatusRelEmergencia", 
			"U_AGRT_StatusRelParaquate", 
			"U_AGRT_StatusRelReceita"
		)
		select 
			'||:CodNovoReceituario||',
			'||:CodReceituarioPedido||'+RECAGRUP."LineId",
			RECAGRUP."LogInst", 
			RECAGRUP."Object", 
			RECAGRUP."U_AGRT_CodCultura", 
			RECAGRUP."U_AGRT_DadosNFe", 
			RECAGRUP."U_AGRT_IDItemReceita", 
			RECAGRUP."U_AGRT_IDReceita", 
			RECAGRUP."U_AGRT_NomeCultura", 
			RECAGRUP."U_AGRT_NomeRegMapa", 
			RECAGRUP."U_AGRT_NumReceita", 
			round('||coalesce(:PercentualPedido,1)||' * RECAGRUP."U_AGRT_QtdeKgLt", 2), 
			RECAGRUP."U_AGRT_RegMapa", 
			RECAGRUP."U_AGRT_Situacao",
			RECAGRUP."U_AGRT_StatusRelEmergencia", 
			RECAGRUP."U_AGRT_StatusRelParaquate", 
			RECAGRUP."U_AGRT_StatusRelReceita"
		from 
			"@AGRT_RECEITAGRUP" as RECAGRUP
		inner join "@AGRT_RECEITL" as RECL on 
			RECL."Code" = RECAGRUP."Code" and
			RECL."U_AGRT_CodCultura" = RECAGRUP."U_AGRT_CodCultura" and
			RECL."U_AGRT_RegMapa" = RECAGRUP."U_AGRT_RegMapa"
		where 
			RECL."Code" = '||:CodReceituarioPedido||'	
	';
	exec SqlCommand;
end;