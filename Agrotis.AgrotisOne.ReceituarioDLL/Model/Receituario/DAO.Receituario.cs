﻿namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public class DaoReceituario
    {
        public static string GetItensInfProdPerigoso(string aItemCode = "")
        {
            string whereItemCode = (aItemCode != "") ? 
                $@" PRODMAPA.""U_AGRT_ItemCode"" = '{aItemCode}' and " : "";

            return $@"
                select 
	                PRODMAPA.""U_AGRT_ItemCode"" as ""ItemCode"",                     
	                PRODMAPA.""U_AGRT_CodSubProd"" as ""RegMapa"",
                    OITM.""ItemName"" as ""Nome"", 
	                INFPROD.""Code"" as ""TemInfProd"",
	                INFPROD.""U_AGRT_NroONU"" as ""NroONU"",
	                INFPROD.""U_AGRT_ClasTxc"" as ""ClasTxc"",
	                INFPROD.""U_AGRT_GrauRisco"" as ""GrauRisco"",
	                INFPROD.""U_AGRT_Classe"" as ""Classe"",
	                INFPROD.""U_AGRT_NomeEmb"" as ""NomeEmb"",
	                INFPROD.""U_AGRT_PrincAtv"" as ""PrincAtv"",
	                INFPROD.""U_AGRT_GrupoEmb"" as ""GrupoEmb""
                from 
	                ""@AGRT_RECEITSUBPROD"" as PRODMAPA
                left join OITM on
                    OITM.""ItemCode"" = PRODMAPA.""U_AGRT_ItemCode""
                left join ""@AGRT_OITM"" as INFPROD on
	                INFPROD.""Code"" = PRODMAPA.""U_AGRT_ItemCode""
                where 
                    {whereItemCode}
                    coalesce(PRODMAPA.""U_AGRT_CodSubProd"",'') <> '' and
                    PRODMAPA.""U_AGRT_ItemCode"" in (
		                select 
			                DISTPM.""U_AGRT_ItemCode"" 
		                from 
			                ""@AGRT_RECEITSUBPROD"" as DISTPM
		                where 
			                coalesce(DISTPM.""U_AGRT_CodSubProd"",'') <> '' 
		                group by 
			                DISTPM.""U_AGRT_ItemCode"" 
		                having 
			                count(DISTPM.""U_AGRT_ItemCode"") = 1)";
        }

        public static string GetAgrtConfRec()
        {
            return $@"
                select 
                    CONFREC.""U_AGRT_Token"", 
                    CONFREC.""U_AGRT_GrpUtiliz"", 
                    CONFREC.""U_AGRT_UrlPlataforma"",
                    CONFREC.""U_AGRT_BuscarInfoNfPlataforma"" 
                from 
                    ""@AGRT_CONFREC"" as CONFREC";
        }
    }
}
