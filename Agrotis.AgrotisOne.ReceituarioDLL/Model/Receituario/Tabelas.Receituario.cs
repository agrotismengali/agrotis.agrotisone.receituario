﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    /// <summary>
    /// Classe que contém os nomes e descrições das tabelas do sistema e seus respectivos campos
    /// </summary>
    public static partial class Tabelas
    {
        public static class Receituario
        {
            public const string Nome = "AGRT_RECEIT";
            public const string Descricao = "AGRT: Receituário";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_MasterData;

            public static class Campos
            {
                public static class CodeAgronomo
                {
                    public const string Nome = "AGRT_CodeAgron";
                    public const string Descricao = "Código Agrônomo";
                    public const int Tamanho = 30;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o Código do responsável técnico listado a partir da API do receituário";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NomeAgronomo
                {
                    public const string Nome = "AGRT_NomeAgron";
                    public const string Descricao = "Nome Agrônomo";
                    public const int Tamanho = 250;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o Nome do responsável técnico listado a partir da API do receituário";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class IdRascunho
                {
                    public const string Nome = "AGRT_IdRascunho";
                    public const string Descricao = "IdRascunho";
                    public const int Tamanho = 10;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o Id do rascunho gerado ao criar uma nova receita pelo SAP";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
