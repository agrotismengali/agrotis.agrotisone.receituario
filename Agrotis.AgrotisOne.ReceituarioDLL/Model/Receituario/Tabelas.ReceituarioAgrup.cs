﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    /// <summary>
    /// Classe que contém os nomes e descrições das tabelas do sistema e seus respectivos campos
    /// </summary>
    public static partial class Tabelas
    {
        public static class ReceituarioLinhasAgrup
        {
            public const string Nome = "AGRT_RECEITAGRUP";
            public const string Descricao = "AGRT: Receituário Agrupado";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_MasterDataLines;

            public static class Campos
            {
                public static class RegistroMapa
                {
                    public const string Nome = "AGRT_RegMapa";
                    public const string Descricao = "Registro MAPA";
                    public const int Tamanho = 20;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o código do Produto MAPA correlacionado ao item de estoque SAP.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NomeRegistroMapa
                {
                    public const string Nome = "AGRT_NomeRegMapa";
                    public const string Descricao = "Descrição MAPA";
                    public const int Tamanho = 100;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o nome do Produto MAPA correlacionado ao item de estoque SAP.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class CodigoCultura
                {
                    public const string Nome = "AGRT_CodCultura";
                    public const string Descricao = "Código Cultura";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o código da Cultura selecionada pelo usuário.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NomeCultura
                {
                    public const string Nome = "AGRT_NomeCultura";
                    public const string Descricao = "Cultura";
                    public const int Tamanho = 250;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o nome da Cultura selecionada pelo usuário.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class QtdeKgLt
                {
                    public const string Nome = "AGRT_QtdeKgLt";
                    public const string Descricao = "Qtde Kg/Lt";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Float;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_Quantity;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar a quantidade do Item na nota multiplicado por sua Equivalência do Produto MAPA.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class IDReceita
                {
                    public const string Nome = "AGRT_IDReceita";
                    public const string Descricao = "ID Receita";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o ID da receita.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NumReceita
                {
                    public const string Nome = "AGRT_NumReceita";
                    public const string Descricao = "Número Receita";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o Número da receita.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class SituacaoReceita
                {
                    public const string Nome = "AGRT_Situacao";
                    public const string Descricao = "Situação";
                    public const int Tamanho = 10;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Numeric;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para indicar o status atual da receita. Verificar Enum StatusReceita que contém a lista dos possíveis status.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class StatusRelReceita
                {
                    public const string Nome = "AGRT_StatusRelReceita";
                    public const string Descricao = "Emissão receita";
                    public const int Tamanho = 10;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Numeric;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para indicar se o relatório da Receita foi emitido ou não.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class StatusRelTermoParaquate
                {
                    public const string Nome = "AGRT_StatusRelParaquate";
                    public const string Descricao = "Emissão termo paraquate";
                    public const int Tamanho = 10;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Numeric;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para indicar se o relatório Termo de uso do Paraquate foi emitido ou não.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class StatusRelFichaEmergencia
                {
                    public const string Nome = "AGRT_StatusRelEmergencia";
                    public const string Descricao = "Emissão ficha emergência";
                    public const int Tamanho = 10;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Numeric;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para indicar se o relatório de Ficha de emergência foi emitido ou não.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class DadosNFe
                {
                    public const string Nome = "AGRT_DadosNFe";
                    public const string Descricao = "Dados para nota";
                    public const int Tamanho = 4096;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Memo;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;                    
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar as informações a serem impressas na nota fiscal (por produto MAPA).";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class IDItemReceita
                {                    
                    public const string Nome = "AGRT_IDItemReceita";
                    public const string Descricao = "ID Item Receita";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o ID do item na receita. Esta informação será passada para o serviço Vincular Pedido da API do receituário.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
