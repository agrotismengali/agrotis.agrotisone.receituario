﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    /// <summary>
    /// Classe que contém os nomes e descrições das tabelas do sistema e seus respectivos campos
    /// </summary>
    public static partial class Tabelas
    {
        public static class ReceituarioLinhas
        {
            public const string Nome = "AGRT_RECEITL";
            public const string Descricao = "AGRT: Receituário Linhas";
            public const BoUTBTableType Tipo = BoUTBTableType.bott_MasterDataLines;

            public static class Campos
            {
                public static class LineNum
                {
                    public const string Nome = "AGRT_LineNum";
                    public const string Descricao = "Linha";
                    public const int Tamanho = 5;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Numeric;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o LineNum da tabela INV1 que indica qual a linha da nota gerou esse registro";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class CodigoItem
                {
                    public const string Nome = "AGRT_ItemCode";
                    public const string Descricao = "Código Item";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para vincular a tabela AGRT_RECEITL a INV1. Grava o Código do Item SAP";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class RegistroMapa
                {
                    public const string Nome = "AGRT_RegMapa";
                    public const string Descricao = "Registro MAPA";
                    public const int Tamanho = 20;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o código do produto MAPA que será utilizado na API do receituário";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NomeRegistroMapa
                {
                    public const string Nome = "AGRT_NomeRegMapa";
                    public const string Descricao = "Descrição MAPA";
                    public const int Tamanho = 100;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o nome do produto MAPA";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class CodigoCultura
                {
                    public const string Nome = "AGRT_CodCultura";
                    public const string Descricao = "Código Cultura";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o código da cultura informado na linha do item da nota fiscal. A informação é usada na API do receituário.";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class NomeCultura
                {
                    public const string Nome = "AGRT_NomeCultura";
                    public const string Descricao = "Cultura";
                    public const int Tamanho = 250;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar o nome da cultura informado na linha do item da nota fiscal";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class QtdeKgLt
                {
                    public const string Nome = "AGRT_QtdeKgLt";
                    public const string Descricao = "Qtde Kg/Lt";
                    public const int Tamanho = 50;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Float;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_Quantity;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tNO;
                    public const string ValorPadrao = "";
                    public static readonly List<ValidValue> ValidValues = null;
                    public const EnumReceituario Modulo = EnumReceituario.Receituario;
                    public const string Resumo = "Campo utilizado para armazenar a razão entre a quantidade do item da nota pela equivalência do produto MAPA (informado no cadastro de itens)";
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }                
            }
        }
    }
}
