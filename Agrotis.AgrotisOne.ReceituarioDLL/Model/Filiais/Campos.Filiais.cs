﻿using System.Collections.Generic;
using SAPbobsCOM;
using ValidValue = Agrotis.Framework.Data.DatabaseCreation.ValidValue;
using EnumReceituario = Agrotis.AgrotisOne.Core.Enums.Enumerators.Modulos.AgrotisOneReceituario;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using static Agrotis.AgrotisOne.Core.Model.Version;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public static partial class Campos
    {
        public static class Filiais
        {
            public const string Nome = "OBPL";
            public static class Campos
            {
                public static class FilialGeraReceita
                {
                    public const string Nome = "AGRT_FGR";
                    public const string Descricao = "FGR";
                    public const int Tamanho = 1;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tYES;
                    public const EnumReceituario Modulo = EnumReceituario.CadastroFiliais;
                    public const string Resumo = "Campo que indica se a Filial gera receituário agronômico ou não.";
                    public static string ValorPadrao = ((int)SimNao.Sim).ToString();
                    public static readonly List<ValidValue> ValidValues = new List<ValidValue> {
                        new ValidValue(((int) SimNao.Nao).ToString(), "Não"),
                        new ValidValue(((int) SimNao.Sim).ToString(), "Sim")
                    };
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
                public static class FilialEmiteFichaEmergencia
                {
                    public const string Nome = "AGRT_FichaEmergencia";
                    public const string Descricao = "Emitir Ficha Emergência";
                    public const int Tamanho = 1;
                    public const BoFieldTypes Tipo = BoFieldTypes.db_Alpha;
                    public const BoFldSubTypes SubTipo = BoFldSubTypes.st_None;
                    public const BoYesNoEnum Obrigatorio = BoYesNoEnum.tYES;
                    public const EnumReceituario Modulo = EnumReceituario.CadastroFiliais;
                    public const string Resumo = "Define se ao emitir a receita o sistema gera automaticamente a ficha de emergência.";
                    public static string ValorPadrao = ((int)SimNao.Sim).ToString();
                    public static readonly List<ValidValue> ValidValues = new List<ValidValue> {
                        new ValidValue(((int) SimNao.Nao).ToString(), "Não"),
                        new ValidValue(((int) SimNao.Sim).ToString(), "Sim")
                    };
                    public const ReceituarioVersion Versao = ReceituarioVersion.v1001001;
                }
            }
        }
    }
}
