﻿using Agrotis.Framework.Data.Database;
using static Agrotis.AgrotisOne.ReceituarioDLL.Model.Tabelas;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public class UdtProdutoMapa : UDTModelBase
    {
        #region Métodos Públicos
        public override string TableName
        {
            get
            {
                return SubProduto.Nome;
            }
        }

        protected override bool AutoKey
        {
            get
            {
                return true;
            }
        }

        protected override string PREFIX_FIELD
        {
            get
            {
                return "U_AGRT_";
            }
        }
        #endregion

        public string ItemCode { get; set; }
        public string CodSubProd { get; set; }
        public string NomeSubProd { get; set; }
        public double QtdeKgLt { get; set; }
        public string TarjaFichaEmergencia { get; set; }
    }
}
