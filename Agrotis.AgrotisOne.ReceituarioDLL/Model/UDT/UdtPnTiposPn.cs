﻿using Agrotis.Framework.Data.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Model
{
    public class UdtPnTiposPn : UDTModelBase
    {
        #region Métodos Públicos
        public override string TableName
        {
            get
            {
                return Agrotis.AgrotisOne.Core.Model.Tabelas.ParceiroNegocioTipo.Nome;
            }
        }

        protected override bool AutoKey
        {
            get
            {
                return true;
            }
        }

        protected override string PREFIX_FIELD
        {
            get
            {
                return "U_AGRT_";
            }
        }
        #endregion

        public string CardCode { get; set; }
        public string TipoPN { get; set; }
    }
}
