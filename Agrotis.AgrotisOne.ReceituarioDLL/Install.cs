﻿using System;
using SAPbouiCOM;
using Agrotis.AgrotisOne.ReceituarioDLL.Model;
using Agrotis.AgrotisOne.Core;
using SAPbobsCOM;
using Agrotis.AgrotisOne.Core.DTOs;
using Agrotis.Framework.Main.Application.Connections;
using Agrotis.Framework.Data;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Scripts;
using Agrotis.Framework.Commons;
using Agrotis.AgrotisOne.AgrotisOne.Menus;
using Agrotis.AgrotisOne.Core.Menus;
using System.Diagnostics;
using System.Reflection;
using Agrotis.Framework.Data.Common;
using static Agrotis.Framework.Main.Application.AddOnEnum;

namespace Agrotis.AgrotisOne.ReceituarioDLL
{
    public class Install
    {
        /// <summary>
        /// Verifica a versão do banco de dados instalada e chama o processo de instalação quando necessário.
        /// </summary>
        public static void VerificarInstalacao(SboConnection lConexao)
        {
            long iVersaoBanco = Connection.VersaoBancoDados;
            long iVersaoAddOn = GetDLLVersion();

            if (iVersaoAddOn > iVersaoBanco)
                Instalar(iVersaoBanco);
            else if (iVersaoAddOn < iVersaoBanco)
                throw new Exception($"Versão do AddOn {iVersaoAddOn} menor do que a versão do Banco de dados {iVersaoBanco}. O AddOn não pôde ser iniciado.");
        }
        private static long GetDLLVersion()
        {
            return long.Parse(FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion.Replace(".", string.Empty));
        }        

        /// <summary>
        /// Realiza a instalação / atualização das tabelas e campos do sistema
        /// </summary>
        private static void Instalar(long aVersaoBanco)
        {
            Logger.Trace("Chamando processo de instalação do banco");
            Globals.Master.Connection.Interface.StatusBar.SetText("Iniciando processo de instalação...", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_None);
            Logger.Trace("Iniciando instalação.");

            try
            {
                Globals.Master.Connection.Interface.MetadataAutoRefresh = false;
                DTOHanaStreamFile[] HanaScripts = new HanaScripts().HanaScriptsReceituario(aVersaoBanco);
                DTOInstallProps DtoInstallProps = new DTOInstallProps()
                {
                    Tabelas = typeof(Tabelas).GetNestedTypes(),
                    Campos = typeof(Campos).GetNestedTypes(),
                    UDOs = typeof(UDOs).GetNestedTypes(),
                    HanaScripts = HanaScripts,
                    SqlViews = typeof(SqlDBODefinition).GetNestedTypes(),
                    SqlProcedures = typeof(SqlDBODefinition).GetNestedTypes(),
                    SqlFunctions = typeof(SqlDBODefinition).GetNestedTypes(),
                };

                InstallAddOnClass InstallAddOn = new InstallAddOnClass(DtoInstallProps, Produto.Receituario, aVersaoBanco);
                ControllerAgenteExportacao CtrlAgenteExp = new ControllerAgenteExportacao();
                CtrlAgenteExp.CriarAgenteExportacaoPadrao();
                CtrlAgenteExp.CriarEmbalagemPadraoIMA();
                // Criando dicionário de dados
                InstallDictionaryClass InstallDictionary = new InstallDictionaryClass(DtoInstallProps);

                CriarPermissoesUsuarios();

                Globals.Master.Connection.Interface.StatusBar.SetText("Finalizando instalação...", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                Util.ExibirDialogo("A instalação/atualização do addon foi finalizada com sucesso!\n\nObs.: Recomendamos reiniciar o B1 antes de começar a utilizar o addon após esta instalação/atualização.");

                #region Altera a versão do BD

                //AGRT_SETUP lSetup = Connection.GetRepositoryWithTypedId<AGRT_SETUP, string>().Get( string.Format( AGRT_SETUP.cDBVersionCode, Settings.AddOnShortName ) );
                string U_Value = Agrotis.Framework.Data.Connection.GetValue(string.Format("Select \"U_Value\" from \"" + Agrotis.Framework.Data.Connection.Schema() + "\".\"@AGRTSETUP\" where \"Code\" = '{0}.01'", Settings.AddOnShortName));


                U_Value = Cryptography.Encrypt(Settings.DBVersion);

                //Connection.GetRepositoryWithTypedId<AGRT_SETUP, string>().SaveOrUpdate(lSetup);

                SqlUtils.DoNonQueryH(string.Format("UPDATE \"@AGRTSETUP\" SET \"U_Value\" = '{0}' where \"Code\" = '{1}'", GetDLLVersion().ToString(), string.Format(AGRTSETUP.cDBVersionCode, Settings.AddOnShortName)));

                #endregion

                Logger.Trace("Versão do BD atualizada com sucesso..");

                Globals.Master.Connection.Interface.MetadataAutoRefresh = true;
            }
            catch (Exception)
            {
                Globals.Master.Connection.Interface.MetadataAutoRefresh = true;
                throw;
            }
        }

        private static void RemoveUDF(string TableID, string AliasID)
        {
            try
            {
                AliasID = AliasID.Replace("U_", "");
                SAPbobsCOM.UserFieldsMD oUserFied = Globals.Master.Connection.Database.GetBusinessObject(BoObjectTypes.oUserFields);
                if (oUserFied.GetByKey(TableID, UserFieldID(TableID, AliasID)))
                {
                    oUserFied.Remove();
                }

            }
            catch (Exception Ex)
            {
                Logger.Trace($"Erro ao Excluir {TableID} - {AliasID} - " + Ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private static Int32 UserFieldID(string TableID, string AliasID)
        {
            try
            {
               return Convert.ToInt16(SqlUtils.GetValue($"SELECT \"FieldID\" FROM CUFD WHERE \"TableID\" = '{TableID}' and \"AliasID\" = '{AliasID}'"));
            }
            catch
            {
                return -1;
            }
        }

        public static void CarregarMenus()
        {
            Globals.Master.Connection.Interface.Forms.GetFormByTypeAndCount(169, 1).Freeze(true);
            try
            {
                AddMenusReceituario menuReceituario = new AddMenusReceituario();
                menuReceituario.AddMenus();
            }
            finally
            {
                Globals.Master.Connection.Interface.Forms.GetFormByTypeAndCount(169, 1).Freeze(false);
                Globals.Master.Connection.Interface.Forms.GetFormByTypeAndCount(169, 1).Update();
            }

        }

        private static void CriarPermissoesUsuarios()
        {
            foreach (MenusAddons.Menu menu in new MenusReceituario().GetMenus())
            {
                Util.AddUserPermissionToSAP(menu.UserPermission);
            }
        }

    }
}
