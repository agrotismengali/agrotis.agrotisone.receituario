﻿using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.API;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons
{
    public class ControllerRelatorio
    {
        DTOApi dtoAPI = new DTOApi();                
        public void EmitirTermoParaquate(string aCodReceituario, string aIdReceita)
        {
            Thread thread = new Thread(() => {
                Util.ExibirMensagemStatusBar("Imprimindo termo paraquate. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                string sNomeRel = dtoAPI.DiretorioRelatorios + "TermoParaquate_" + aIdReceita.ToString() + ".pdf";
                try
                {
                    Stream streamResult = WSRelatorios.EmitirTermoParaquate(dtoAPI, aIdReceita);
                    if (streamResult == null)
                    {
                        OperacoesReceituario.AtualizarStatusRelParaquate(aCodReceituario, aIdReceita, StatusRelatorio.NAOAPLICA);
                        sNomeRel = "";
                    }
                    else
                    {
                        using (FileStream fs = new FileStream(sNomeRel, FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            Copy(streamResult, fs, 65536);
                            OperacoesReceituario.AtualizarStatusRelParaquate(aCodReceituario, aIdReceita, StatusRelatorio.EMITIDO);
                        }
                    }
                    if (!string.IsNullOrEmpty(sNomeRel))
                    {
                        SendToPrinter(sNomeRel);
                    }
                }
                catch (Exception Ex)
                {
                    sNomeRel = "";
                    Util.ExibirMensagemStatusBar($"Erro ao emitir termo paraquate - {Ex.Message}", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                    Util.ExibirDialogo($"Não foi possível emitir o termo paraquate - {Ex.Message}");
                }
                finally
                {
                    GC.Collect();
                }                
            });            
            thread.Start();                      
        }
        public void EmitirFichaEmergencia(string aCodReceituario, string aIdReceita)
        {
            Thread thread = new Thread(() => {
                Util.ExibirMensagemStatusBar("Imprimindo ficha de emergência. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                string sNomeRel = dtoAPI.DiretorioRelatorios + "FichaEmergencia_" + aIdReceita.ToString() + ".pdf";
                bool bImprimeTarja = OperacoesReceituario.ProdutoImprimeTarja(aCodReceituario, aIdReceita);
                try
                {
                    Stream streamResult = WSRelatorios.EmitirFichaEmergencia(dtoAPI, aIdReceita, bImprimeTarja);
                    if (streamResult == null)
                    {
                        OperacoesReceituario.AtualizarStatusRelEmergencia(aCodReceituario, aIdReceita, StatusRelatorio.NAOAPLICA);
                        sNomeRel = "";
                    }
                    else
                    {
                        using (FileStream fs = new FileStream(sNomeRel, FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            Copy(streamResult, fs, 65536);
                            OperacoesReceituario.AtualizarStatusRelEmergencia(aCodReceituario, aIdReceita, StatusRelatorio.EMITIDO);
                        }
                    }
                    if (!string.IsNullOrEmpty(sNomeRel))
                    {
                        SendToPrinter(sNomeRel);
                    }
                }
                catch (Exception Ex)
                {
                    sNomeRel = "";
                    Util.ExibirMensagemStatusBar($"Erro ao emitir ficha de emergência - {Ex.Message}", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                    Util.ExibirDialogo($"Não foi possível emitir a ficha de emergência - {Ex.Message}");
                }
                finally
                {
                    GC.Collect();
                }                
            });            
            thread.Start();                      
        }
        public static short NumeroCopiasReceita()
        {
            short iNumCopias = Convert.ToInt16(SqlUtils.GetValue("select COALESCE(\"U_AGRT_NumVias\", 1) from \"@AGRT_CONFREC\""));
            if (iNumCopias <= 0)
                iNumCopias = 1;
            return iNumCopias;
        }
        public void EmitirReceita(string aCodReceituario, string aIdReceita, string Senha = "")
        {
            Thread thread = new Thread(() => {
                Util.ExibirMensagemStatusBar("Imprimindo receita. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                string sNomeRel = dtoAPI.DiretorioRelatorios + "Receita_" + aIdReceita.ToString() + ".pdf";
                try
                {
                    Stream streamResult = WSRelatorios.EmitirReceita(dtoAPI, aIdReceita, Senha);
                    if (streamResult == null)
                    {
                        OperacoesReceituario.AtualizarStatusRelReceita(aCodReceituario, aIdReceita, StatusRelatorio.NAOAPLICA);
                        sNomeRel = "";
                    }
                    else
                    {
                        using (FileStream fs = new FileStream(sNomeRel, FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            Copy(streamResult, fs, 65536);
                            OperacoesReceituario.AtualizarStatusRelReceita(aCodReceituario, aIdReceita, StatusRelatorio.EMITIDO);
                        }
                    }
                    if (!string.IsNullOrEmpty(sNomeRel))
                    {
                        short iNumCopias = NumeroCopiasReceita();
                        for (short i = 0; i<iNumCopias; i++)
                            SendToPrinter(sNomeRel);
                    }
                }
                catch (Exception Ex)
                {
                    sNomeRel = "";
                    Util.ExibirMensagemStatusBar($"Erro ao emitir receita - {Ex.Message}", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                    Util.ExibirDialogo($"Não foi possível emitir a receita - {Ex.Message}");
                }                
                finally
                {
                    GC.Collect();
                }                
            });
            thread.Start();                       
        }
        public static void Copy(Stream source, Stream target, int blockSize)
        {
            int read;
            byte[] buffer = new byte[blockSize];
            while ((read = source.Read(buffer, 0, blockSize)) > 0)
            {
                target.Write(buffer, 0, read);
            }
        }
        public void SendToPrinter(string aFileName)
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.Verb = "print";
            info.FileName = aFileName;            
            info.CreateNoWindow = true;
            info.WindowStyle = ProcessWindowStyle.Hidden;

            Process p = new Process();
            p.StartInfo = info;
            p.Start();

            p.WaitForInputIdle();
            //Thread.Sleep(20000);
        }
        public void DeleteFile(string aFileName)
        {
            if (File.Exists(aFileName))            
                File.Delete(aFileName);            
        }        
    }
}
