﻿using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.API;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Agrotis.Framework.Data.Database;
using Agrotis.Framework.Data.UDO;
using System;
using System.Collections.Generic;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using CamposInfoProd = Agrotis.AgrotisOne.Core.Model.Tabelas.ItemCamposAdicionais.Campos;
// Tuple - 1-LineNum, 2-ItemCode, 3-ItemName, 4-CódCultura, 5-Cultura, 6-RegMapa, 7-Qtde
using ItemReceituarioType = System.Tuple<int, string, string, string, string, string, double>;
using DoQueryHType = System.Collections.Generic.Dictionary<string, object>;
using Agrotis.AgrotisOne.ReceituarioDLL.Model;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons
{
    public class OperacoesReceituario
    {
        public DTOApi dtoAPI = new DTOApi();
        private static string sqlSubProdutoReceituario(string aItemCode, string aCodCultura)
        {
            return $@"SELECT 
                          COALESCE(SUBPROD.""U_AGRT_CodSubProd"", '') AS ""RegMapa"", 
                          COALESCE(SUBPROD.""U_AGRT_NomeSubProd"", '') AS ""NomeRegMapa"", 
                          COALESCE(SUBPROD.""U_AGRT_QtdeKgLt"", 0) AS ""QtdeKgLt"",
                          COALESCE(GRUPO.""U_AGRT_GerRec"", 'N') AS ""ObrigReceit"",
                          CULTURA.""Name"" AS ""NomeCultura"",
                          CULTURA.""U_AGRT_IdFitoCult"" AS ""IdFitoCult""
                      FROM OITM
                      LEFT JOIN ""@AGRT_GRITENSGER"" GRUPO ON
                          GRUPO.""Code"" = OITM.""U_AGRT_GrpItem""
                      LEFT JOIN ""@AGRT_RECEITSUBPROD"" SUBPROD ON
                          SUBPROD.""U_AGRT_ItemCode"" = OITM.""ItemCode""
                      LEFT JOIN ""@AGRT_CULTURA"" CULTURA ON
                          CULTURA.""Code"" = '{aCodCultura}'
                      WHERE OITM.""ItemCode"" = '{aItemCode}'";            
        }
        private static string sqlUtilizacaoGeraReceita(string aGrupoUtilizacao, string aUtilizacao)
        {
            return $@"SELECT 1 FROM ""@AGRT_GRUTILL"" WHERE ""Code"" = '{aGrupoUtilizacao}' AND ""U_AGRTCodUti"" = '{aUtilizacao}'";
        }
        private static string sqlParceiroNegocioGeraReceita(string aCardCode)
        {
            return $@"SELECT 1 FROM ""@AGRT_PNTIPOS"" AS PNTIPOS
                      INNER JOIN ""@AGRT_TIPOPN"" AS TIPOS ON TIPOS.""Code"" = PNTIPOS.""U_AGRT_TipoPN""
                      WHERE ""U_AGRT_CardCode"" = '{aCardCode}' AND TIPOS.""U_AGRT_GerRec"" = 'Y' LIMIT 1 ";
        }        
        private static string sqlInsereAgrupamentoReceituario(string aItemCode)
        {
            return $@"INSERT INTO ""@AGRT_RECEITAGRUP"" (""Code"", ""LineId"", ""Object"", ""U_AGRT_CodCultura"", ""U_AGRT_NomeCultura"", ""U_AGRT_QtdeKgLt"", ""U_AGRT_RegMapa"", ""U_AGRT_NomeRegMapa"", ""U_AGRT_Situacao"", ""U_AGRT_StatusRelReceita"", ""U_AGRT_StatusRelParaquate"", ""U_AGRT_StatusRelEmergencia"")
                     (SELECT ITEM.""Code"", ROW_NUMBER() OVER(ORDER BY ITEM.""Code"") AS ""LineId"", ITEM.""Object"", ITEM.""U_AGRT_CodCultura"", ITEM.""U_AGRT_NomeCultura"", SUM(ITEM.""U_AGRT_QtdeKgLt""), ITEM.""U_AGRT_RegMapa"", ITEM.""U_AGRT_NomeRegMapa"", 0, 0, 0, 0 
                      FROM ""@AGRT_RECEITL"" AS ITEM WHERE ITEM.""Code"" = {aItemCode} GROUP BY ITEM.""Code"", ITEM.""Object"", ITEM.""U_AGRT_CodCultura"", ITEM.""U_AGRT_NomeCultura"", ITEM.""U_AGRT_RegMapa"", ITEM.""U_AGRT_NomeRegMapa"")";
        }
        private static string updAtualizarNumeroReceita(string aCodReceituario, string aRegMapa, string aIdReceita, 
            string aNumReceita, string aCodCultura, int aSituacao, string aIditemReceita)
        {
            return $@"UPDATE ""@AGRT_RECEITAGRUP"" SET ""U_AGRT_IDReceita"" = { aIdReceita }, ""U_AGRT_IDItemReceita"" = { aIditemReceita }, 
                ""U_AGRT_NumReceita"" = '{ aNumReceita }', ""U_AGRT_Situacao"" = { aSituacao } WHERE ""Code"" = { aCodReceituario } AND 
                ""U_AGRT_RegMapa"" = '{ aRegMapa }' AND ""U_AGRT_CodCultura"" = '{aCodCultura}' ";
        }        
        private static string sqlGetCodCulturaReceituario(string aCodCultura)
        {
            return $@"SELECT ""U_AGRT_IdFitoCult"" FROM ""@AGRT_CULTURA"" WHERE ""Code"" = '{aCodCultura}'";
        }
        private static string updAtualizarSituacaoReceita(string aCodReceituario, string aRegMapa, string aCodCultura, StatusReceita aStatus)
        {
            return $@"UPDATE ""@AGRT_RECEITAGRUP"" SET ""U_AGRT_Situacao"" = '{(int)aStatus}' WHERE ""Code"" = {aCodReceituario} 
                     AND ""U_AGRT_RegMapa"" = '{ aRegMapa }' AND  ""U_AGRT_CodCultura"" = '{aCodCultura}' ";            
        }
        private static string updAtualizarSituacaoReceitaGeral(string aCodReceituario, StatusReceita aStatus)
        {
            return $@"UPDATE ""@AGRT_RECEITAGRUP"" SET ""U_AGRT_Situacao"" = '{(int)aStatus}' WHERE ""Code"" = {aCodReceituario} ";
        }
        private static string updAtualizarStatusRelReceita(string aCodReceituario, string aIdReceita, StatusRelatorio aStatus)
        {
            return $@"UPDATE ""@AGRT_RECEITAGRUP"" SET ""U_AGRT_StatusRelReceita"" = '{(int)aStatus}' WHERE ""Code"" = {aCodReceituario} 
                     AND ""U_AGRT_IDReceita"" = '{ aIdReceita }' ";
        }
        private static string updAtualizarStatusRelParaquate(string aCodReceituario, string aIdReceita, StatusRelatorio aStatus)
        {
            return $@"UPDATE ""@AGRT_RECEITAGRUP"" SET ""U_AGRT_StatusRelParaquate"" = '{(int)aStatus}' WHERE ""Code"" = {aCodReceituario} 
                     AND ""U_AGRT_IDReceita"" = '{ aIdReceita }' ";
        }
        private static string updAtualizarStatusRelEmergencia(string aCodReceituario, string aIdReceita, StatusRelatorio aStatus)
        {
            return $@"UPDATE ""@AGRT_RECEITAGRUP"" SET ""U_AGRT_StatusRelEmergencia"" = '{(int)aStatus}' WHERE ""Code"" = {aCodReceituario} 
                     AND ""U_AGRT_IDReceita"" = '{ aIdReceita }' ";
        }
        private static string sqlGetSequenceID()
        {
            return $@"SELECT AGRT_SEQ_REC_SequenciaDocumentos.NextVal FROM DUMMY";
        }
        private static string sqlGetDistinctReceitas(string aCodReceituario)
        {
            return $@"SELECT DISTINCT ""U_AGRT_IDReceita"" FROM ""@AGRT_RECEITAGRUP"" WHERE ""Code"" = {aCodReceituario}";
        }
        private static string sqlGetDistinctProdutos(string aCodReceituario)
        {
            return $@"SELECT DISTINCT ""U_AGRT_RegMapa"" FROM ""@AGRT_RECEITAGRUP"" WHERE ""Code"" = {aCodReceituario}";
        }
        public static bool DesvincularReceita(string aCodReceituario, string aIdReceita, string aIdItemReceita = "")
        {
            string sCmd = $@"UPDATE ""@AGRT_RECEITAGRUP"" SET 
                                ""U_AGRT_IDReceita"" = 0,
                                ""U_AGRT_IDItemReceita"" = 0,
                                ""U_AGRT_NumReceita"" = '',
                                ""U_AGRT_Situacao"" = 0,
                                ""U_AGRT_StatusRelReceita"" = 0,
                                ""U_AGRT_StatusRelParaquate"" = 0,
                                ""U_AGRT_StatusRelEmergencia"" = 0,
                                ""U_AGRT_DadosNFe"" = ''
                            WHERE ""Code"" = {aCodReceituario} ";

            if (!string.IsNullOrEmpty(aIdItemReceita))
                sCmd = string.Concat(sCmd, $@" AND ""U_AGRT_IDItemReceita"" = '{aIdItemReceita}'");
            else
                sCmd = string.Concat(sCmd, $@" AND ""U_AGRT_IDReceita"" = '{aIdReceita}'");
            
            return (SqlUtils.DoNonQueryH(sCmd) > 0);
        }              
        public static bool AtualizarSituacaoReceita(string aCodReceituario, string aRegMapa, string aCodCultura, StatusReceita aStatus)
        {
            return (SqlUtils.DoNonQueryH(updAtualizarSituacaoReceita(aCodReceituario, aRegMapa, aCodCultura, aStatus)) > 0);
        }
        public static bool AtualizarSituacaoReceita(string aCodReceituario, StatusReceita aStatus)
        {
            return (SqlUtils.DoNonQueryH(updAtualizarSituacaoReceitaGeral(aCodReceituario, aStatus)) > 0);
        }
        public static bool AtualizarStatusRelReceita(string aCodReceituario, string aIdReceita, StatusRelatorio aStatus)
        {
            return (SqlUtils.DoNonQueryH(updAtualizarStatusRelReceita(aCodReceituario, aIdReceita, aStatus)) > 0);
        }
        public static bool AtualizarStatusRelParaquate(string aCodReceituario, string aIdReceita, StatusRelatorio aStatus)
        {
            return (SqlUtils.DoNonQueryH(updAtualizarStatusRelParaquate(aCodReceituario, aIdReceita, aStatus)) > 0);
        }
        public static bool AtualizarStatusRelEmergencia(string aCodReceituario, string aIdReceita, StatusRelatorio aStatus)
        {
            return (SqlUtils.DoNonQueryH(updAtualizarStatusRelEmergencia(aCodReceituario, aIdReceita, aStatus)) > 0);
        }
        public static List<Dictionary<string, object>> GetSubProdutos(string aItemCode, string aCodCultura)
        {            
            return SqlUtils.DoQueryH(sqlSubProdutoReceituario(aItemCode, aCodCultura));
        }
        public bool UtilizacaoGeraReceita(string aUtilizacao)
        {            
            string sResult = SqlUtils.GetValue(sqlUtilizacaoGeraReceita(dtoAPI.GrupoUtilizacao, aUtilizacao));
            return !string.IsNullOrEmpty(sResult);
        }
        public bool ParceiroNegocioGeraReceita(string aCardCode)
        {
            string sResult = SqlUtils.GetValue(sqlParceiroNegocioGeraReceita(aCardCode));
            return !string.IsNullOrEmpty(sResult);
        }        
        public static List<Dictionary<string, object>> GetDistinctReceitas(string aCodReceituario)
        {            
            return SqlUtils.DoQueryH(sqlGetDistinctReceitas(aCodReceituario));
        }
        public static List<DoQueryHType> GetDistinctReceitas(string aCodReceituario, string aLineNum)
        {
            string sQuery = 
                $@"SELECT DISTINCT 
                       AGRUP.""U_AGRT_NumReceita"" AS ""NumReceita""
                   FROM ""@AGRT_RECEITL"" LINE
                   INNER JOIN ""@AGRT_RECEITAGRUP"" AGRUP ON
                       LINE.""Code"" = AGRUP.""Code"" AND
                       LINE.""U_AGRT_RegMapa"" = AGRUP.""U_AGRT_RegMapa"" AND
                       LINE.""U_AGRT_CodCultura"" = AGRUP.""U_AGRT_CodCultura""
                   WHERE
                       LINE.""Code"" = {aCodReceituario} AND
                       LINE.""U_AGRT_LineNum"" = {aLineNum} ";
            return SqlUtils.DoQueryH(sQuery);
        }
        public static List<Dictionary<string, object>> GetDistinctProdutos(string aCodReceituario)
        {
            return SqlUtils.DoQueryH(sqlGetDistinctProdutos(aCodReceituario));
        }
        public static bool AtualizarNumeroReceita(string aCodReceituario, string aRegMapa, string aIdReceita, 
            string aNumReceita, string aCodCultura, int aSituacao, string aIditemReceita)
        {
            return (SqlUtils.DoNonQueryH(updAtualizarNumeroReceita(aCodReceituario, aRegMapa, aIdReceita, aNumReceita, aCodCultura, aSituacao, aIditemReceita)) > 0);
        }        
        public static void AgruparItensPorMapaCultura(string aCodeRec)
        {
            SqlUtils.DoNonQueryH(sqlInsereAgrupamentoReceituario(aCodeRec));
        }  
        public static string GetCodCulturaReceituario(string aCodCultura)
        {
            return SqlUtils.GetValue(sqlGetCodCulturaReceituario(aCodCultura));
        }        
        public string ConsumirReceita(string aIdReceita)
        {
            return WSReceitas.ConsumirReceita(dtoAPI, aIdReceita);
        }        
        public bool PodeEmitirNota(string aCodeReceituario)
        {            
            return !SqlUtils.GetValue($@"SELECT 1 
                                         FROM ""@AGRT_RECEITAGRUP"" 
                                         WHERE ""Code"" = {aCodeReceituario} AND 
                                               ""U_AGRT_Situacao"" NOT IN (3, 4)").Equals("1");
        }
        public bool ReceitasFinalizadas(string aCodeReceituario)
        {
            return !SqlUtils.GetValue($@"SELECT 1 
                                         FROM ""@AGRT_RECEITAGRUP"" 
                                         WHERE ""Code"" = {aCodeReceituario} AND 
                                               ""U_AGRT_Situacao"" NOT IN (4)").Equals("1");
        }
        public string GetResponsavelTecnicoPadrao(string aCodFilial)
        {
            return SqlUtils.GetValue($@"SELECT ""U_AGRT_CodeAgron"" 
                                        FROM ""@AGRT_CONFREC1"" 
                                        WHERE ""U_AGRT_BPLId"" = '{aCodFilial}' LIMIT 1");
        }   
        public bool ValidarCulturaPorProduto(string aCodFilial)
        {
            return SqlUtils.GetValue($@"SELECT ""U_AGRT_ValidarCulturaProduto"" 
                                        FROM ""@AGRT_CONFREC1"" 
                                        WHERE ""U_AGRT_BPLId"" = '{aCodFilial}' LIMIT 1").Equals("Y");
        }
        public string GetSequenceID()
        {
            return SqlUtils.GetValue(sqlGetSequenceID());
        }
        public static bool ProdutoImprimeTarja(string aCodeReceituario, string aIdReceita)
        {
            return SqlUtils.GetValue(
                $@"SELECT
                    SUBPROD.""U_AGRT_TarjaFichaEmergencia""
                FROM ""@AGRT_RECEITAGRUP"" AGRUP
                LEFT JOIN ""@AGRT_RECEITL"" ITEM ON
                    ITEM.""Code"" = AGRUP.""Code"" AND
                    ITEM.""U_AGRT_CodCultura"" = AGRUP.""U_AGRT_CodCultura"" AND
                    ITEM.""U_AGRT_RegMapa"" = AGRUP.""U_AGRT_RegMapa""
                LEFT JOIN ""@AGRT_RECEITSUBPROD"" SUBPROD ON
                    SUBPROD.""U_AGRT_ItemCode"" = ITEM.""U_AGRT_ItemCode"" AND
                    SUBPROD.""U_AGRT_CodSubProd"" = ITEM.""U_AGRT_RegMapa""
                WHERE
                    AGRUP.""Code"" = {aCodeReceituario} AND
                    AGRUP.""U_AGRT_IDReceita"" = {aIdReceita} AND
                    SUBPROD.""U_AGRT_TarjaFichaEmergencia"" = '1'
                LIMIT 1"
                ).Equals("1");            
        }
        public bool CulturaValidaParaProduto(string aCodCultura, string aCodProdMapa, string aEstado)
        {
            bool result = false; 
            try
            {
                Util.ExibirMensagemStatusBar($@"Validando Cultura por produto {aCodProdMapa}. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                IEnumerable<Cultura> ListaCultura = WSCultura.ListarCulturas(dtoAPI, aCodProdMapa, aEstado);
                foreach (Cultura cultura in ListaCultura)
                {
                    if (aCodCultura.Equals(cultura.codCultura.ToString()))
                    {
                        result = true;
                        break;
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível validar a cultura para o produto MAPA: {Ex.Message}");
                return false;
            }
        }
        public string GetDescricaoProduto(string aCodProd)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Buscando descrição do produto MAPA. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSProduto.GetDescricaoProduto(dtoAPI, aCodProd);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível buscar a descrição do produto MAPA: {Ex.Message}");
                return string.Empty;
            }            
        }              
        public string VincularPedido(string aSeqId, string aQtde, string aProdMapa, string aIdReceita, string aIdItemReceita)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Vinculando receita. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSReceitas.VincularPedido(dtoAPI, aSeqId, aQtde, aProdMapa, aIdReceita, aIdItemReceita);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível vincular esta receita: {Ex.Message}");
                return string.Empty;
            }            
        }
        public bool DesvincularPedido(string aCodReceituario, string aSeqId, string aIdReceita, bool aDesvincReceita = true)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Desvinculando receita. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                string sResult = WSReceitas.DesvincularPedido(dtoAPI, aSeqId, aIdReceita);
                if ((sResult.Equals(StatusReceita.NAODEFINIDA.ToString())) && aDesvincReceita)
                    return DesvincularReceita(aCodReceituario, aIdReceita);
                else
                    return false;                
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível desvincular esta receita: {Ex.Message}");
                return false;
            }            
        }
        public string VincularFaturamento(string aSeqId, string aNumeroNota, string aSerieNota, string aDataEmissao)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Vinculando faturamento. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSReceitas.VincularFaturamento(dtoAPI, new FaturamentoParams(aSeqId, aNumeroNota, aSerieNota, aDataEmissao));
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível vincular a nota fiscal: {Ex.Message}");
                return string.Empty;
            }            
        }
        public bool DesvincularFaturamento(string aCodReceituario, string aIdReceita, string aNumeroNota, string aCodProdMapa)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Vinculando faturamento. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);                
                string sResult = WSReceitas.DesvincularFaturamento(dtoAPI, aIdReceita, aNumeroNota, aCodProdMapa);
                if (sResult.Equals(StatusReceita.NAODEFINIDA.ToString()))
                    // Tem que fazer um loop pra todos os itens dessa receita
                    return true; // DesvincularReceita(aCodReceituario, aIdReceita, aIdItemReceita);                
                else
                    return false;
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível desvincular a receita da nota: {Ex.Message}");
                return false;
            }            
        }
        public StatusReceita ConsultarReceitaStatus(string aIdReceita)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Consultando status da receita. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSReceitas.ConsultarReceitaStatus(dtoAPI, aIdReceita);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível consultar o status da receita: {Ex.Message}");
                return StatusReceita.NAODEFINIDA;
            }            
        }
        public int GetIDProdutorRuralByUUID(string aUUIDProdutor)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Buscando produtor rural. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSProdutorRural.GetIDProdutorRuralByUUID(dtoAPI, aUUIDProdutor);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível identificar o produtor rural: {Ex.Message}");
                return 0;
            }            
        }
        public int GetIDPropriedadeRuralByUUID(string aUUIDPropriedade)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Buscando propriedade rural. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSPropriedadeRural.GetIDPropriedadeRuralByUUID(dtoAPI, aUUIDPropriedade);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível identificar a propriedade rural: {Ex.Message}");
                return 0;
            }            
        }
        public IEnumerable<ReceitaConsulta> ListarReceitas(string aDtIni, string aDtFin,
            string aIdProdutor, string aIdPropriedade, string aProdMapa, string aCultura)
        {
            try
            {                
                Util.ExibirMensagemStatusBar("Listando receitas. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSReceitas.ListarReceitas(dtoAPI, aDtIni, aDtFin,
                    aIdProdutor, aIdPropriedade, aProdMapa, aCultura);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível listar as receitas: {Ex.Message}");
                return null;
            }            
        }
        public IEnumerable<ResponsavelTecnico> ListarResponsaveisTecnicos()
        {
            try
            {
                Util.ExibirMensagemStatusBar("Listando responsáveis técnicos. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSResponsavelTecnico.ListarResponsaveisTecnicos(dtoAPI);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível listar os responsáveis técnicos: {Ex.Message}");
                return null;
            }            
        }
        public List<Tuple<string, string>> ListarProdutos(string aFiltro = "")
        {
            List<Tuple<string, string>> lResult = new List<Tuple<string, string>>();
            try
            {
                Util.ExibirMensagemStatusBar("Listando produtos MAPA. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                foreach(ProdutoLista Produto in WSProduto.ListarProdutos(dtoAPI, aFiltro))
                {
                    lResult.Add(new Tuple<string, string>(
                        Produto.codMapaProduto, Produto.nomeComum));
                }
                return lResult;
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível listar os produtos: {Ex.Message}");
                return null;
            }            
        }

        public static bool VerificaReceitaRascunho(string CodeReceituario)
        {
            return SqlUtils.SqlCount($@"select 1 from ""@AGRT_RECEITAGRUP"" WHERE ""U_AGRT_Situacao"" = 5 and ""Code"" = '{CodeReceituario}'") > 0;
        }

        /// <summary>
        /// Grava o receituário na base de dados para controle interno
        /// </summary>
        /// <param name="DocEntry">DocEntry da NF</param>
        /// <param name="ItensReceituario">Itens que devem gerar receituário</param>
        /// <returns></returns>
        public static string GravaReceituario(List<ItemReceituarioType> ItensReceituario)
        {
            Util.ExibirMensagemStatusBar("Gerando análise das receitas. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
            string CodeRec = ReturnCode.Code("@AGRT_RECEIT");

            DTOMasterData DtoMasterData = new DTOMasterData()
            {
                UDO = "AGRT_RECEIT",
                DataFields = new List<Tuple<string, string, Type>>()
                                    {
                                        new Tuple<string, string, Type>("Code", CodeRec, typeof(string)),
                                        new Tuple<string, string, Type>($"U_AGRT_CodeAgron", "", typeof(string)),
                                        new Tuple<string, string, Type>($"U_AGRT_NomeAgron", "", typeof(string))
                                    },
                DtoMasterDataLines = new List<DTOMasterDataLine>()
            };

            DTOMasterDataLine DtoMasterDataLine = new DTOMasterDataLine()
            {
                LineTable = "AGRT_RECEITL",
                DataLineFields = new List<Tuple<string, Type>>() {
                    new Tuple<string, Type>($"U_AGRT_LineNum", typeof(string)),
                    new Tuple<string, Type>($"U_AGRT_ItemCode", typeof(string)),
                    new Tuple<string, Type>($"U_AGRT_RegMapa", typeof(string)),
                    new Tuple<string, Type>($"U_AGRT_NomeRegMapa", typeof(string)),
                    new Tuple<string, Type>($"U_AGRT_CodCultura", typeof(string)),
                    new Tuple<string, Type>($"U_AGRT_NomeCultura", typeof(string)),
                    new Tuple<string, Type>($"U_AGRT_QtdeKgLt", typeof(double))
                }
            };            
            Dictionary<string, object> dic;
            DtoMasterDataLine.DataLines = new List<Dictionary<string, object>>();            

            // Tuple - 1-LineNum, 2-ItemCode, 3-ItemName, 4-CódCultura, 5-Cultura, 6-RegMapa, 7-Qtde
            foreach (ItemReceituarioType ItemReceituario in ItensReceituario)
            {
                dic = new Dictionary<string, object>();
                dic.Add("U_AGRT_LineNum", ItemReceituario.Item1);
                dic.Add("U_AGRT_ItemCode", ItemReceituario.Item2);
                dic.Add("U_AGRT_CodCultura", ItemReceituario.Item4);
                dic.Add("U_AGRT_NomeCultura", ItemReceituario.Item5);
                dic.Add("U_AGRT_RegMapa", ItemReceituario.Item6);
                dic.Add("U_AGRT_NomeRegMapa", ItemReceituario.Item3);
                dic.Add("U_AGRT_QtdeKgLt", ItemReceituario.Item7);

                DtoMasterDataLine.DataLines.Add(dic);
            }
            DtoMasterData.DtoMasterDataLines.Add(DtoMasterDataLine);

            string Error = string.Empty;
            UtilUDO.UDOPersistirDados(DtoMasterData, out Error);

            if (!string.IsNullOrEmpty(Error))
                throw new Exception($"Erro ao Inserir Receituário - {Error}");
            else
                AgruparItensPorMapaCultura(CodeRec);

            return CodeRec;
        }
        public static void CopiarReceituarioPedido(string aCodRec, string aBaseEntry, string aBaseLine, double aQuantity)
        {
            try
            {
                SqlUtils.DoNonQueryH($"call AGRT_SP_RCT_CopiarReceituarioPedido({aCodRec},{aBaseEntry},{aBaseLine},{aQuantity})");
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível consultar o status da receita: {Ex.Message}");                
            }            
        }        
        public CalculaArea CalculaArea(CalculaAreaEnvio oCalculaArea)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Calculando área. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSCalcularArea.CalculaArea(dtoAPI, oCalculaArea);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível calcular a área: {Ex.Message}");
                return null;
            }            
        }
        public string GeraRascunhoReceituario(DTORascunhoReceituario DtoRascunho)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Criando rascunho para receita. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSReceitas.CriarRascunhoReceita(dtoAPI, DtoRascunho);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível gerar o rascunho para a receita: {Ex.Message}");
                return string.Empty;
            }            
        }
        public Relacao ConsultaRelacaoEspecifia(DTORelacaoEspecifica DtoRelEspecifica)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Consultando relação específica. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSRelacoes.ConsultaRelacaoEspecifica(dtoAPI, DtoRelEspecifica);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível consultar a relação: {Ex.Message}");
                return null;
            }            
        }
        public ReceituarioItemRetorno AdicionarItemReceituario(DTOReceituarioItem DtoRecItem)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Adicionando Item a receita. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSReceitas.AdicionaItemReceita(dtoAPI, DtoRecItem);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível adicionar Item a receita: {Ex.Message}");
                return null;
            }            
        }
        public bool DesvincularItemRascunho(string aCodReceituario, string aIdReceita, string aIdItemReceita)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Desvinculando Item de rascunho. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                string sResult = WSReceitas.DesvincularItemRascunho(dtoAPI, aIdReceita, aIdItemReceita);
                if (sResult.Equals(StatusReceita.NAODEFINIDA.ToString()))                
                    return DesvincularReceita(aCodReceituario, aIdReceita, aIdItemReceita);                
                else
                    return false;
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível adicionar Item a receita: {Ex.Message}");
                return false;
            }            
        }
        public IEnumerable<ReceitaRetorno> EfetivarReceita(string NumeroRascunho)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Efetivando a receita. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSReceitas.ConcluirRascunho(dtoAPI, NumeroRascunho);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível concluir o rascunho das receitas: {Ex.Message}");
                return null;
            }            
        }
        public Empresa RetornaEmpresaPorCNPJ(string CNPJ)
        {
            try
            {
                Util.ExibirMensagemStatusBar("Buscando ID da empresa. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
                return WSEmpresa.GetEmpresaByCNPJ(dtoAPI, CNPJ);
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível identificar a empresa: {Ex.Message}");
                return null;
            }            
        }
        public int GetIdEmpresa(string CNPJ)
        {
            try
            {
                Empresa EmpresaRetorno = RetornaEmpresaPorCNPJ(CNPJ);
                if (EmpresaRetorno == null)
                    return 0;
                else
                {
                    if (EmpresaRetorno.empresaApi != null)
                        return EmpresaRetorno.empresaApi.id;
                    else
                        return 0;                    
                }
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível identificar a empresa: {Ex.Message}");
                return 0;
            }            
        }
        public string GetCommentLine(string aCodReceituario, string aLineNum, List<ItemReceituarioType> ItensReceituario)
        {
            string sResult = "";
            try
            {
                // Só gera receitas relacionadas para clientes que utilizam integração com receituário
                if (!string.IsNullOrEmpty(aCodReceituario))
                {
                    foreach (DoQueryHType Receita in GetDistinctReceitas(aCodReceituario, aLineNum))
                    {
                        if (Receita["NumReceita"] != null)
                            sResult = string.Concat(sResult, "Receita: " + Receita["NumReceita"].ToString() + "  ");
                    }
                }
                // Lista os dados obrigatórios dos produtos MAPA vinculados ao Item
                foreach (ItemReceituarioType ItemReceituario in ItensReceituario)
                {
                    if (aLineNum.Equals(ItemReceituario.Item1.ToString()))
                    {
                        if (sResult.IndexOf(ItemReceituario.Item6) == -1)
                        {
                            sResult = string.Concat(sResult, WSProduto.GetDadosNota(dtoAPI, ItemReceituario.Item6) + "  ");
                        }
                    }
                }
                return sResult;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Não foi possível buscar informações adicionais para o registro MAPA [{aCodReceituario}], linha da nota [{aLineNum}]. Verifique se o registro MAPA está correto no cadastro de itens.", Ex);
                return "";
            }
        }
        public bool FilialGeraReceita(string aBPLId)
        {
            string sResult = "0";
            if (!string.IsNullOrEmpty(aBPLId))
                sResult = SqlUtils.GetValue($@"SELECT ""U_AGRT_FGR"" FROM OBPL WHERE ""BPLId"" = {aBPLId}");            
            return sResult.Equals("1");
        }
        public static string GetReceitaVinculadaPedido(string aDocEntry)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(aDocEntry))
                result = SqlUtils.GetValue($@"select ""U_AGRT_CodRec"" from ORDR where ""DocEntry"" = {aDocEntry}");
            return result;
        }
        public static string GetUUIDPropriedade(string aCardCode, string aAddress)
        {
            string result = string.Empty;
            if ((!string.IsNullOrEmpty(aCardCode)) && (!string.IsNullOrEmpty(aAddress)))
                result = SqlUtils.GetValue($@"
                    select 
	                    CRD1.""U_AGRT_UUID_PPR""
                    from
                        CRD1
                    where
                        CRD1.""AdresType"" = 'S' and
                        CRD1.""CardCode"" = '{aCardCode}' and
                        CRD1.""Address"" = '{aAddress}' ");
            return result;
        }
        public static List<Dictionary<string, object>> GetReceitasPedido(string aBaseEntry)
        {
            string sQuery =
                $@"select distinct	
	                RCT.""U_AGRT_IDReceita"" as ""IDReceita"",
                    ORDR.""U_AGRT_SequenceID"" as ""SequenceID"",
	                ORDR.""U_AGRT_CodRec"" as ""CodRec""
                from
                    ""@AGRT_RECEITAGRUP"" RCT
                inner join ORDR on
                    ORDR.""U_AGRT_CodRec"" = RCT.""Code""
                where
                    ORDR.""DocEntry"" = {aBaseEntry} ";
            return SqlUtils.DoQueryH(sQuery);
        }
        public static List<Dictionary<string, object>> GetReceitasConcluidas(string aCodRec)
        {
            string sQuery = $@"
                select 
                    RCT.""U_AGRT_IDReceita"" as ""IDReceita"",
                    RCT.""U_AGRT_IDItemReceita"" as ""IDItemReceita"",
                    RCT.""U_AGRT_QtdeKgLt"" as ""Qtde"",
	                RCT.""U_AGRT_RegMapa"" as ""RegMapa""
                from
                    ""@AGRT_RECEITAGRUP"" RCT
                where

                    coalesce(RCT.""U_AGRT_IDReceita"", 0) > 0 and
                    RCT.""Code"" = {aCodRec} ";
            return SqlUtils.DoQueryH(sQuery);
        }
        public void SincronizarInfProdPerigoso(string aItemCode = "")
        {
            // Pega todos os itens SAP que possuem apenas um Reg. MAPA relacionado.
            foreach (DTOInfoProdPerigoso ItemInfProd in GetItensInfProdPerigoso(aItemCode))
            {
                Util.ExibirMensagemStatusBar($"Criando/modificando dados adicionais do item {ItemInfProd.itemCode}. Registro MAPA {ItemInfProd.regMapa}");
                PersistirInfProdPerigoso(ItemInfProd);
            }            
        }
        private Tuple<string, string, Type> GetFieldValue(string aFieldName, string aFieldValue)
        {
            string fieldName = aFieldName;
            if ((aFieldName != "Code") && (aFieldName != "Name"))
                fieldName = $"U_{aFieldName}";
            return new Tuple<string, string, Type>(fieldName, aFieldValue ?? "", typeof(string));
        }
        private void PersistirInfProdPerigoso(DTOInfoProdPerigoso aItemInfProd)
        {
            string oErro = string.Empty;
            string tableName = $"@{Core.Model.Tabelas.ItemCamposAdicionais.Nome}";
            DTOInfoProdPerigoso itemInfProd = GetInfoProdPerigosoProperties(aItemInfProd);
            DTOMasterData DtoMasterData = new DTOMasterData()
            {
                UDO = Core.Model.UDOs.ItemCamposAdicionais.UniqueID,
                DataFields = new List<Tuple<string, string, Type>>() {
                    GetFieldValue("Code", itemInfProd.itemCode),
                    GetFieldValue("Name", itemInfProd.nome),
                    GetFieldValue(CamposInfoProd.NroONU.Nome, itemInfProd.nroONU),
                    GetFieldValue(CamposInfoProd.ClasseToxicologica.Nome, itemInfProd.clasTxc),
                    GetFieldValue(CamposInfoProd.GrauRisco.Nome, itemInfProd.grauRisco),
                    GetFieldValue(CamposInfoProd.NomeEmbarque.Nome, itemInfProd.nomeEmb),
                    GetFieldValue(CamposInfoProd.PrincipioAtivo.Nome, itemInfProd.princAtv),
                    GetFieldValue(CamposInfoProd.GrupoEmbalagem.Nome, itemInfProd.grupoEmb)
                }
            };  
            
            string idItemInfProd = UtilUDO.UDOPersistirDados(DtoMasterData, out oErro);
            if (idItemInfProd == UtilUDO.ErrorCode.ToString())
                throw new Exception(oErro);
        }

        private DTOInfoProdPerigoso GetInfoProdPerigosoProperties(DTOInfoProdPerigoso aItemInfProd)
        {
            DTOInfoProdPerigoso itemInfProd = new DTOInfoProdPerigoso(aItemInfProd.itemCode, aItemInfProd.regMapa, aItemInfProd.nome);
            InfoProdPerigoso infoProd = WSProduto.GetInfoProdPerigoso(dtoAPI, aItemInfProd.regMapa);
            if (infoProd != null)
            {
                bool recordChanged = (aItemInfProd.nroONU != infoProd.numeroOnu);
                itemInfProd.nroONU = (string.IsNullOrEmpty(aItemInfProd.nroONU) || recordChanged) ? infoProd.numeroOnu : aItemInfProd.nroONU;
                itemInfProd.grauRisco = (string.IsNullOrEmpty(aItemInfProd.grauRisco) || recordChanged) ? infoProd.numeroRisco : aItemInfProd.grauRisco;
                itemInfProd.clasTxc = (string.IsNullOrEmpty(aItemInfProd.clasTxc) || recordChanged) ? infoProd.classe : aItemInfProd.clasTxc;                
                itemInfProd.nomeEmb = (string.IsNullOrEmpty(aItemInfProd.nomeEmb) || recordChanged) ? infoProd.nomeEmbarque : aItemInfProd.nomeEmb;
                itemInfProd.princAtv = (string.IsNullOrEmpty(aItemInfProd.princAtv) || recordChanged) ? infoProd.principioAtivo : aItemInfProd.princAtv;
                itemInfProd.grupoEmb = (string.IsNullOrEmpty(aItemInfProd.grupoEmb) || recordChanged) ? infoProd.grupoEmbalagem : aItemInfProd.grupoEmb;
            }
            else
                throw new ApplicationException($"Não foi possível recuperar as informações para o item [{aItemInfProd.itemCode}] Reg. MAPA [{aItemInfProd.regMapa}]. Verifique a disponibilidade do serviço.");
            return itemInfProd;
        }

        private static List<DTOInfoProdPerigoso> GetItensInfProdPerigoso(string aItemCode = "")
        {
            List<DTOInfoProdPerigoso> lResult = new List<DTOInfoProdPerigoso>();            
            foreach (Dictionary<string, object> ItemReceit in SqlUtils.DoQueryH(DaoReceituario.GetItensInfProdPerigoso(aItemCode)))
            {
                lResult.Add(new DTOInfoProdPerigoso(
                    ItemReceit["ItemCode"].ToString(), 
                    ItemReceit["RegMapa"].ToString(),
                    ItemReceit["Nome"]?.ToString(),
                    ItemReceit["NroONU"]?.ToString(), 
                    ItemReceit["ClasTxc"]?.ToString(), 
                    ItemReceit["GrauRisco"]?.ToString(),
                    ItemReceit["Classe"]?.ToString(), 
                    ItemReceit["NomeEmb"]?.ToString(),
                    ItemReceit["PrincAtv"]?.ToString(),
                    ItemReceit["GrupoEmb"]?.ToString(), 
                    !string.IsNullOrEmpty(ItemReceit["TemInfProd"]?.ToString())));
            }
            return lResult;
        }

        public bool BuscarInformacaoNfPlataforma()
        {
            if (dtoAPI.BuscarInfoNfPlataforma == "N")
                return false;
            return true;               
        }
    }
}
