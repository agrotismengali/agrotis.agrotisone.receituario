﻿using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.Core.FormTicket;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using Agrotis.Framework.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons
{
    public class ImpressaoRelatorios
    {
        public string CodReceituario { get; set; }
        public string Filial { get; set; }

        public ImpressaoRelatorios(string aCodReceituario, string aFilial)
        {
            CodReceituario = aCodReceituario;
            Filial = aFilial;
        }

        public void Executar()
        {
            if (SqlUtils.GetValue(@"SELECT COALESCE(""U_AGRT_Assinar"", '') FROM ""@AGRT_CONFREC"" WHERE ""Code"" = '1'") != "Y")
                ImprimirRelatorios(Filial, CodReceituario);
            else
            {
                StreamReader lForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Agrotis.AgrotisOne.ReceituarioDLL.View.Receituarios.SenhaAssinatura.AGRTFrmSenhaAssinatura.srf"));
                Util.FormAbrir(lForm);

                FormTicketThread ThReturnTicket = new FormTicketThread(FormConstants.AGRTFrmSenhaAssinatura, ImprimeRelatorioComSenha);
                ThReturnTicket.Iniciar();
            }
        }

        private void ImprimeRelatorioComSenha()
        {
            string[] FormReturnParameters = Globals.Master.FormTicket.GetFormParam(FormConstants.AGRTFrmSenhaAssinatura);
            string Senha = String.Empty;
            Senha = FormReturnParameters?[0];

            if (Senha == null)
                Senha = string.Empty;

            ImprimirRelatorios(Filial, CodReceituario, Senha);
        }  
        public bool FilialEmiteFichaEmergencia(string aFilial)
        {
            return SqlUtils.GetValue(
                $@"SELECT COALESCE(""U_AGRT_FichaEmergencia"", '0') 
                   FROM ""OBPL"" 
                   WHERE ""BPLId"" = {aFilial}").Equals("1");
        }
        public void ImprimirRelatorios(string aFilial, string aCodReceituario, string aSenha = "")
        {
            ControllerRelatorio CtrlRel = new ControllerRelatorio();
            Util.ExibirMensagemStatusBar("Imprimindo relatórios. Aguarde...", SAPbouiCOM.BoMessageTime.bmt_Short);
            try
            {
                foreach (Dictionary<string, object> Receita in OperacoesReceituario.GetDistinctReceitas(aCodReceituario))
                {
                    if (Receita["U_AGRT_IDReceita"] != null)
                    {
                        string sReceita = Receita["U_AGRT_IDReceita"].ToString();
                        if ((!string.IsNullOrEmpty(sReceita)) && (int.Parse(sReceita) > 0))
                        {
                            CtrlRel.EmitirReceita(aCodReceituario, sReceita, aSenha);
                            CtrlRel.EmitirTermoParaquate(aCodReceituario, sReceita);
                            if (FilialEmiteFichaEmergencia(aFilial))
                                CtrlRel.EmitirFichaEmergencia(aCodReceituario, sReceita);
                        }
                    }
                }                
            }
            catch (Exception Ex)
            {
                Util.ExibirDialogo($"Não foi possível imprimir os relatórios: {Ex.Message}");
            }
            finally
            {
                GC.Collect();
            }
        }
        public string DocumentoImprimeReceita()
        {
            return SqlUtils.GetValue($@"
                select 
                    coalesce(T0.""U_AGRT_DocImprimeReceita"", '0') 
                from 
                    ""@AGRT_CONFREC"" T0");
        }
    }
}
