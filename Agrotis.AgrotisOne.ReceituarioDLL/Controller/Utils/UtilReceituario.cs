﻿using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using SAPbouiCOM;
using System.Collections.Generic;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Controller.Utils
{
    public static class UtilReceituario
    {
        public static string RetornaMensagensRetorno(IEnumerable<RetornoPost> Retornos)
        {
            string MensagemErro = string.Empty;
            if (Retornos != null)
            {
                foreach (var retorno in Retornos)
                {
                    if (string.IsNullOrEmpty(MensagemErro))
                        MensagemErro += retorno.mensagem;
                    else
                        MensagemErro += $" - {retorno.mensagem}";
                }
            }
            return MensagemErro;
        }
        public static void FormFreeze(Form aForm, bool aFreeze)
        {
            try
            {
                aForm.Freeze(aFreeze);
            }
            catch
            {
                //
            }
        }

    }
}
