﻿using Agrotis.AgrotisOne.Core.Constantes;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes
{
    public class MenuConstants : SAPMenuConstants
    {
        // Por padrão, procurar acrescentar em ordem alfabética

        #region [ Agrotis Receituário AddOn ]

        public const string AGRTMnuAGRT = "AGRTMnuAGRTRecOne"; 
        
        #endregion
        
    }
}