﻿using Agrotis.AgrotisOne.Core.Constantes;

namespace Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes
{
    public class FormConstants : SAPFormConstants
    {        
        #region [ AgrotisOne Receituário AddOn ]
        public const string AGRTFrmConsultaReceita = "AGRTFrmConsultaReceita";
        public const string AGRTFrmReceituario = "AGRTFrmReceituario";        
        public const string FrmSalesDocuments = "FrmSalesDocuments";
        public const string AGRTFrmCriarReceita = "AGRTFrmCriarReceita";
        public const string AGRTFrmRecomendar = "AGRTFrmRecomendar";
        public const string AGRTFrmSenhaAssinatura = "AGRTFrmSenhaAssinatura";
        public const string AGRTFrmExportacao = "AGRTFrmExportacao";
        #endregion

    }
}