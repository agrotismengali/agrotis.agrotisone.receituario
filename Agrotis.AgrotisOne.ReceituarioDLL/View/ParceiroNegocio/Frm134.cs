﻿using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using System;
using Grid = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Grid;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using Folder = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Folder;
using Agrotis.Framework.Main.Application;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.Core;
using Agrotis.Framework.Main.Interface.Events;
using Agrotis.AgrotisOne.Core.Constantes;
using SAPbouiCOM;
using System.Collections.Generic;
using Agrotis.AgrotisOne.ReceituarioDLL.Model;
using Agrotis.Framework.Data;
using Agrotis.AgrotisOne.Core.View.ParceiroNegocio;

namespace Agrotis.AgrotisOne.ReceituarioDLLs.View.ParceiroNegocio
{
    [FormType("134")]
    public partial class Frm134 : Frm134Base
    {
        public Frm134(AddOn aAddOn, string aFormUniqueId) : base(aAddOn, aFormUniqueId)
        {
            mForm.Freeze(true);
            try
            {
                oFolderAgrotis = Items.Add<Folder>(ItensUIDs.oFolderAgrotis);
                oFolderAgrotis.OnAfterItemPressed += new EventVoidItem(oFolderAgrotis_OnAfterItemPressed);
                oFolderAgrotis.AffectsFormMode = false;

                oFolderReferencia = Items.Add<Folder>(ItensUIDs.oFolderReferencia);

                EdCardCode = Items.Add<EditText>(ItensUIDsBase.EdCardCode);
                DtTipoPN = mForm.DataSources.DataTables.Add(ItensUIDs.DtTipoPN);
                GdTiposPn = Items.Add<Grid>(ItensUIDs.GdTiposPn);
                GdTiposPn.OnAfterChooseFromList += new EventVoidChooseFromList(GdTiposPn_OnAfterChooseFromList);

                BtAddTipoPN = Items.Add<Button>(ItensUIDs.BtAddTipoPN);
                BtAddTipoPN.OnAfterClick += new EventVoidItem(BtAddTipoPN_OnAfterClick);

                BtDelTipoPN = Items.Add<Button>(ItensUIDs.BtDelTipoPN);
                BtDelTipoPN.OnAfterClick += new EventVoidItem(BtDelTipoPN_OnAfterClick);

                oItemRefSt = mForm.Items.Item("21");

                //UDTProdMapa = new UdtProdutoMapa();
                OnAfterFormDataLoad += new EventVoidBusinessInfo(Frm150_OnAfterFormDataLoad);
                OnBeforeFormDataAdd += new EventBoolBusinessInfo(Frm134_OnBeforeFormDataAdd);
                OnAfterFormDataAdd += new EventVoidBusinessInfo(Frm150_OnAfterFormDataAdd);
                OnAfterFormDataUpdate += new EventVoidBusinessInfo(Frm150_OnAfterFormDataUpdate);

                OnFormResize += Frm134_OnFormResize;

                //Primeiro seleciona Vazio apenas para carregar os campos no DataTable.
                SelecionaTiposPn(String.Empty);
                PosicionaItensForm();
                AdicionarCFL();
                AjustaCamposTela();
            }
            catch (Exception Ex)
            {                
                Util.ExibeErrosTela($"Erro ao abrir Form", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }
        private bool TipoPNRelacionado(string aCardCode, string aTipoPN)
        {
            return SqlUtils.GetValue($@"SELECT 1 FROM ""@AGRT_PNTIPOS"" WHERE ""U_AGRT_CardCode"" = '{aCardCode}' AND ""U_AGRT_TipoPN"" = {aTipoPN}").Equals("1");
        }
        private void GdTiposPn_OnAfterChooseFromList(ChooseFromListEvent aEvent)
        {            
            try
            {
                if (aEvent.SelectedObjects == null)
                    return;

                if (mForm.Mode == BoFormMode.fm_OK_MODE)
                    mForm.Mode = BoFormMode.fm_UPDATE_MODE;

                if (aEvent.ChooseFromListUID == ItensUIDs.CflTipoPn)
                {
                    int iRowReference = GdTiposPn.GetCellFocus().rowIndex;
                    for (int iRow = 0; iRow < aEvent.SelectedObjects.Rows.Count; iRow++)
                    {
                        if (!TipoPNRelacionado(EdCardCode.Value, aEvent.SelectedObjects.GetValue("Code", iRow)))
                        {
                            if (iRowReference + iRow >= DtTipoPN.Rows.Count)
                                DtTipoPN.Rows.Add();

                            DtTipoPN.SetValue("U_AGRT_TipoPN", iRowReference + iRow, aEvent.SelectedObjects.GetValue("Code", iRow));
                            DtTipoPN.SetValue("Name", iRowReference + iRow, aEvent.SelectedObjects.GetValue("Name", iRow));
                        }
                    }
                    DtTipoPN.Rows.Add();
                }
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método GdTiposPn_OnAfterChooseFromList - Frm134", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }

        private void Frm134_OnFormResize(ItemEvent aEvent)
        {
            mForm.Freeze(true);
            try
            {
                AjustaPosicoesItens();
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm134_OnFormResize - Frm134", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }

        private void BtDelTipoPN_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {            
            try
            {
                if (GdTiposPn.GetCellFocus() == null) return;

                int RowSelected = GdTiposPn.GetCellFocus().rowIndex;

                if (RowSelected == -1) return;

                string TipoPn = DtTipoPN.GetValue("Name", RowSelected);

                if (string.IsNullOrEmpty(TipoPn)) return;

                if (!Util.ExibirDialogoConfirmacao($"Deseja remover o Tipo {TipoPn} do PN?")) return;

                string Codigo = DtTipoPN.GetValue("Code", RowSelected).ToString();

                DtTipoPN.Rows.Remove(RowSelected);

                CodigosRemover.Add(Codigo);

                if (mForm.Mode == BoFormMode.fm_OK_MODE) mForm.Mode = BoFormMode.fm_UPDATE_MODE;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método BtDelTipoPN_OnAfterClick - Frm134", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }

        private void BtAddTipoPN_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {            
            try
            {
                if (GdTiposPn.Rows.Count == 0)
                {
                    DtTipoPN.Rows.Add();
                    return;
                }

                if (String.IsNullOrEmpty(DtTipoPN.GetValue("Name", DtTipoPN.Rows.Count - 1)))
                    return;

                DtTipoPN.Rows.Add();
                GdTiposPn.SetCellFocus(GdTiposPn.Rows.Count - 1, 3);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método BtAddTipoPN_OnAfterClick - Frm134", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }

        #region Propriedades
        public Folder oFolderAgrotis { get; set; }
        public Folder oFolderReferencia { get; set; }        
        public SAPbouiCOM.DataTable DtTipoPN { get; set; }
        //public UdtProdutoMapa UDTProdMapa { get; set; }
        public Grid GdTiposPn { get; set; }
        public Button BtAddTipoPN { get; set; }
        public Button BtDelTipoPN { get; set; }
        public Item oItemRefSt { get; set; }
        List<string> CodigosRemover = new List<string>();
        public string NewCardName { get; set; }
        #endregion


        #region Itens UIDs
        public static class ItensUIDs
        {
            public const string oFolderAgrotis = "oFldAgrt";
            public const string oFolderReferencia = "234000007";
            public class FormPane
            {
                public const int PaneFolderAgrotis = 455;
            }

            public const string TabelaTiposPNxPN = "@AGRT_PNTIPOS";
            public const string ColCode = "Code";
            public const string ColCardCode = "U_AGRT_CardCode";
            public const string ColCodeTipoPN = "U_AGRT_TipoPN";
            public const string ColNomeTipoPN = "Name";
            public const string DtTipoPN = "DtTipoPN";
            public const string CflTipoPn = "CflTipoPn";

            public const string EdCode = "5";
            public const string GdTiposPn = "GrTiposPN";
            public const string BtAddTipoPN = "BtAddTpPN";
            public const string BtDelTipoPN = "BtDelTpPN";
        }
        #endregion

        private void oFolderAgrotis_OnAfterItemPressed(SAPbouiCOM.ItemEvent aEvent)
        {            
            try
            {
                mForm.PaneLevel = ItensUIDs.FormPane.PaneFolderAgrotis;
                SelecionaTiposPn(EdCardCode.Value);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método oFolderAgrotis_OnAfterItemPressed - Frm134", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }

        private void AdicionarCFL()
        {
            SAPbouiCOM.ChooseFromListCollection oCFLs = null;

            oCFLs = mForm.ChooseFromLists;

            SAPbouiCOM.ChooseFromList oCFL = null;
            SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
            oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(Globals.Master.Connection.Interface.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));

            //  Adding 2 CFL, one for the button and one for the edit text.
            oCFLCreationParams.MultiSelection = true;
            oCFLCreationParams.ObjectType = Core.Model.UDOs.ParceiroNegocioTipo.UniqueID;
            oCFLCreationParams.UniqueID = ItensUIDs.CflTipoPn;

            oCFL = oCFLs.Add(oCFLCreationParams);
        }
        private void AjustaCamposTela()
        {
            oFolderAgrotis.Pane = ItensUIDs.FormPane.PaneFolderAgrotis;
            oFolderAgrotis.Height = oFolderReferencia.Height;
            oFolderAgrotis.Left = oFolderReferencia.Left + oFolderReferencia.Width;
            oFolderAgrotis.Width = oFolderReferencia.Width;
            oFolderAgrotis.GroupWith(oFolderReferencia.UniqueID);
            oFolderAgrotis.Caption = "Receituário";
            oFolderAgrotis.AutoPaneSelection = true;
            oFolderAgrotis.Visible = true;

            GdTiposPn.FromPane = ItensUIDs.FormPane.PaneFolderAgrotis;
            GdTiposPn.ToPane = ItensUIDs.FormPane.PaneFolderAgrotis;
            GdTiposPn.Width = 400;
            GdTiposPn.Height = oItemRefSt.Height;
            GdTiposPn.Columns.Item(0).Width = 0;
            GdTiposPn.Columns.Item(1).Width = 0;
            GdTiposPn.Columns.Item(2).Width = 0;
            GdTiposPn.Columns.Item(3).Width = 300;

            EditTextColumn colEdt = GdTiposPn.Columns.Item(3) as SAPbouiCOM.EditTextColumn;
            colEdt.ChooseFromListUID = ItensUIDs.CflTipoPn;
            colEdt.ChooseFromListAlias = "Name";

            BtAddTipoPN.Type = SAPbouiCOM.BoButtonTypes.bt_Image;
            BtAddTipoPN.Image = IconConstants.GetIconPath(IconConstants.Add);
            BtAddTipoPN.Width = 20;
            BtAddTipoPN.Height = 20;
            BtAddTipoPN.Visible = true;
            BtAddTipoPN.ToPane = GdTiposPn.ToPane;
            BtAddTipoPN.FromPane = GdTiposPn.FromPane;

            BtDelTipoPN.Type = SAPbouiCOM.BoButtonTypes.bt_Image;
            BtDelTipoPN.Image = IconConstants.GetIconPath(IconConstants.Remove);
            BtDelTipoPN.Width = 20;
            BtDelTipoPN.Height = 20;
            BtDelTipoPN.Visible = true;
            BtDelTipoPN.ToPane = BtAddTipoPN.ToPane;
            BtDelTipoPN.FromPane = BtAddTipoPN.FromPane;

            AjustaPosicoesItens();
        }

        private void Frm150_OnAfterFormDataAdd(BusinessObjectInfo aEvent)
        {            
            try
            {
                PersistirDadosTipoPN(SqlUtils.GetValue($@"SELECT ""CardCode"" FROM OCRD WHERE ""CardName"" = '{NewCardName}'"));
                base.Frm134Base_OnAfterFormDataAdd(aEvent);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm150_OnAfterFormDataAdd - Frm134", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }
        private void Frm150_OnAfterFormDataUpdate(BusinessObjectInfo aEvent)
        {            
            try
            {
                PersistirDadosTipoPN(EdCardCode.Value);
                base.Frm134Base_OnAfterFormDataUpdate(aEvent);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm150_OnAfterFormDataUpdate - Frm134", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }

        private void Frm150_OnAfterFormDataLoad(BusinessObjectInfo aEvent)
        {
            //             
        }

        private bool Frm134_OnBeforeFormDataAdd(SAPbouiCOM.BusinessObjectInfo aEvent)
        {            
            try
            {
                NewCardName = mForm.DataSources.DBDataSources.Item(0).GetValue("CardName", 0);
                return true;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm134_OnBeforeFormDataAdd - Frm134", Ex);
                return false;
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }

        private void PersistirDadosTipoPN(string CardCode)
        {
            UdtPnTiposPn UdtParceiroNegocioTipoPn = new ReceituarioDLL.Model.UdtPnTiposPn();
            for (int iPos = 0; iPos < CodigosRemover.Count; iPos++)
            {
                if (!String.IsNullOrEmpty(CodigosRemover[iPos]))
                {
                    if (UdtParceiroNegocioTipoPn.GetByKey(Convert.ToInt32(CodigosRemover[iPos])))
                        UdtParceiroNegocioTipoPn.Delete();
                }
            }

            for (int iPos = 0; iPos < DtTipoPN.Rows.Count; iPos++)
            {
                if (String.IsNullOrEmpty(DtTipoPN.GetValue("U_AGRT_TipoPN", iPos))) continue;

                UdtParceiroNegocioTipoPn = new ReceituarioDLL.Model.UdtPnTiposPn();

                if (!String.IsNullOrEmpty(DtTipoPN.GetValue("Code", iPos).ToString()) &&  Convert.ToInt32(DtTipoPN.GetValue("Code", iPos).ToString()) != 0)
                    UdtParceiroNegocioTipoPn.Code = Convert.ToInt32(DtTipoPN.GetValue("Code", iPos).ToString());
                UdtParceiroNegocioTipoPn.CardCode = CardCode;
                UdtParceiroNegocioTipoPn.TipoPN = DtTipoPN.GetValue("U_AGRT_TipoPN", iPos);
                UdtParceiroNegocioTipoPn.Upsert();
            }
        }

        private void SelecionaTiposPn(string aCardCode)
        {
            DtTipoPN.ExecuteQuery($@"SELECT 
                                    T1.""Code"", 
                                    T1.""U_AGRT_CardCode"", 
                                    T1.""U_AGRT_TipoPN"", 
                                    T0.""Name""         
                              FROM 
                                    ""@AGRT_TIPOPN"" T0 
                                    INNER JOIN ""@AGRT_PNTIPOS""  T1 ON T0.""Code"" = T1.""U_AGRT_TipoPN""
                              WHERE
                                    T1.""U_AGRT_CardCode"" = '{aCardCode}'");

            GdTiposPn.DataTable = DtTipoPN;

            GdTiposPn.Columns.Item(0).Width = 0;
            GdTiposPn.Columns.Item(1).Width = 0;
            GdTiposPn.Columns.Item(2).Width = 0;
            GdTiposPn.Columns.Item(3).Width = GdTiposPn.Width;
            GdTiposPn.Columns.Item(3).TitleObject.Caption = "Tipo do Parceiro de Negócio";

            CodigosRemover = new List<string>();
        }

        private void PosicionaItensForm()
        {
            oFolderAgrotis.Pane = ItensUIDs.FormPane.PaneFolderAgrotis;
            oFolderAgrotis.Height = oFolderReferencia.Height;
            oFolderAgrotis.Left = oFolderReferencia.Left + oFolderReferencia.Width;
            oFolderAgrotis.Width = oFolderReferencia.Width;
            oFolderAgrotis.GroupWith(oFolderReferencia.UniqueID);
            oFolderAgrotis.Caption = "Receituário";
            oFolderAgrotis.AutoPaneSelection = true;
            oFolderAgrotis.Visible = true;

            GdTiposPn.Top = oItemRefSt.Top;
            GdTiposPn.Left = oItemRefSt.Left;
            GdTiposPn.Height = oItemRefSt.Height;
            GdTiposPn.Width = 400;
            GdTiposPn.FromPane = ItensUIDs.FormPane.PaneFolderAgrotis;
            GdTiposPn.ToPane = ItensUIDs.FormPane.PaneFolderAgrotis;
            GdTiposPn.Visible = false;
            GdTiposPn.SelectionMode = BoMatrixSelect.ms_Single;

            BtAddTipoPN.Type = BoButtonTypes.bt_Image;
            BtAddTipoPN.Image = IconConstants.GetIconPath(IconConstants.Add);
            BtAddTipoPN.Top = GdTiposPn.Top;
            BtAddTipoPN.Left = GdTiposPn.Left + GdTiposPn.Width + 6;
            BtAddTipoPN.ToPane = GdTiposPn.ToPane;
            BtAddTipoPN.FromPane = GdTiposPn.FromPane;
            BtAddTipoPN.Width = 20;
            BtAddTipoPN.Height = 20;
            BtAddTipoPN.Visible = false;

            BtDelTipoPN.Type = BoButtonTypes.bt_Image;
            BtDelTipoPN.Image = IconConstants.GetIconPath(IconConstants.Remove);
            BtDelTipoPN.Top = BtAddTipoPN.Top + BtAddTipoPN.Width + 6;
            BtDelTipoPN.Left = BtAddTipoPN.Left;
            BtDelTipoPN.ToPane = BtAddTipoPN.ToPane;
            BtDelTipoPN.FromPane = BtAddTipoPN.FromPane;
            BtDelTipoPN.Width = 20;
            BtDelTipoPN.Height = 20;
            BtDelTipoPN.Visible = false;
        }
        
        private void AjustaPosicoesItens()
        {
            GdTiposPn.Top = oItemRefSt.Top;
            GdTiposPn.Left = oItemRefSt.Left;
            GdTiposPn.Width = 400;
            GdTiposPn.Height = oItemRefSt.Height;
            GdTiposPn.Columns.Item(0).Width = 0;
            GdTiposPn.Columns.Item(1).Width = 0;
            GdTiposPn.Columns.Item(2).Width = 0;
            GdTiposPn.Columns.Item(3).Width = GdTiposPn.Width;

            BtAddTipoPN.Top = GdTiposPn.Top;
            BtAddTipoPN.Left = GdTiposPn.Left + GdTiposPn.Width + 6;

            BtDelTipoPN.Top = BtAddTipoPN.Top + BtAddTipoPN.Width + 6;
            BtDelTipoPN.Left = BtAddTipoPN.Left;
        }
    }
}




