﻿using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using Form = Agrotis.Framework.Main.Interface.Controls.Forms.Base.Form;
using Grid = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Grid;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using Agrotis.Framework.Main.Interface.Events;
using SAPbouiCOM;
using Agrotis.AgrotisOne.Core.Utils;
using System;
using Agrotis.AgrotisOne.Core.Enums;
using Agrotis.AgrotisOne.Core.Constantes;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.CFLAgrotis
{
    [FormType(SAPFormConstants.AGRTFrmCFLAgrotisReceituario)]
    public partial class AGRTFrmCFLAgrotisReceituario : Form
    {
        OperacoesReceituario OpeReceituario = new OperacoesReceituario();
        public AGRTFrmCFLAgrotisReceituario(AddOn aAddOn, string aFormUID) : base(aAddOn, aFormUID)
        {
            BtOk = Items.Add<Button>(ItensUIDs.BtOk);
            BtOk.Caption = "Selecionar";
            BtOk.OnAfterClick += new EventVoidItem(BtOk_OnAfterClick);

            DefButton = ItensUIDs.BtOk;

            BtCancelar = Items.Add<Button>(ItensUIDs.BtCancelar);
            BtCancelar.OnAfterClick += new EventVoidItem(BtCancelar_OnAfterClick);

            BtPesquisar = Items.Add<Button>(ItensUIDs.BtPesquisar);
            BtPesquisar.OnAfterClick += new EventVoidItem(BtPesquisar_OnAfterClick);

            EdPesquisa = Items.Add<EditText>(ItensUIDs.EdPesquisa);
            EdPesquisa.OnBeforeKeyDown += new EventBoolItem(EdPesquisa_OnBeforeKeyDown);

            GdPesquisa = Items.Add<Grid>(ItensUIDs.GdPesquisa);
            GdPesquisa.SelectionMode = BoMatrixSelect.ms_Single;
            GdPesquisa.OnAfterClick += new EventVoidItem(GdPesquisa_OnAfterClick);
            GdPesquisa.OnAfterDoubleClick += new EventVoidItem(GdPesquisa_OnAfterDoubleClick);
            GdPesquisa.OnBeforeRightClick += new EventBoolContextMenu(GdPesquisa_OnBeforeRightClick);

            DtPesquisa = mForm.DataSources.DataTables.Item(ItensUIDs.DtPesquisa);

            DtoCflAgrotis = CFLTicket.DtoOpcoesPesquisa;

            OnAfterFormLoad += new EventVoidItem(AGRTFrmCFLAgrotis_OnAfterFormLoad);
            OnAfterFormClose += new EventVoidItem(AGRTFrmCFLAgrotis_OnAfterFormClose);
            OnBeforeFormKeyDown += new EventBoolItem(AGRTFrmCFLAgrotis_OnBeforeFormKeyDown);

            bPesquisaRealizada = false;
            With = string.Empty;            
        }

        private bool EdPesquisa_OnBeforeKeyDown(ItemEvent aEvent)
        {
            if (aEvent.CharPressed == (int)ASCIIActionKeys.Enter)
            {
                Pesquisar();
                return false;
            }

            return true;
        }

        private bool AGRTFrmCFLAgrotis_OnBeforeFormKeyDown(ItemEvent aEvent)
        {
            if (aEvent.CharPressed == (int)ASCIIActionKeys.Enter)
                return !EdPesquisa.Active;

            return true;
        }

        private bool GdPesquisa_OnBeforeRightClick(ContextMenuInfo aEvent)
        {
            return false;
        }

        private void GdPesquisa_OnAfterClick(ItemEvent aEvent)
        {
            if (aEvent.Row < 0)
                return;

            if (DtoCflAgrotis.MultiSelection)
            {
                if (!GdPesquisa.Rows.IsSelected(aEvent.Row))
                    GdPesquisa.Rows.SelectedRows.Add(aEvent.Row);
                else
                    GdPesquisa.Rows.SelectedRows.Remove(aEvent.Row);
            }
            else
            {
                GdPesquisa.Rows.SelectedRows.Clear();
                GdPesquisa.Rows.SelectedRows.Add(aEvent.Row);
            }
        }

        #region Eventos
        private void AGRTFrmCFLAgrotis_OnAfterFormClose(ItemEvent aEvent)
        {
            if (!bPesquisaRealizada)
                CFLTicket.DtResult_ToNull();

            CFLTicket.bIsPesquisaConcluida = true;
        }

        private void BtOk_OnAfterClick(ItemEvent aEvent)
        {
            if (GdPesquisa.Rows.SelectedRows.Count <= 0)
            {
                Util.ExibirMensagemStatusBar("Nenhum registro selecionado.", aErro: true);
                return;
            }

            SelecionarLinhasCFL();
        }

        private void BtCancelar_OnAfterClick(ItemEvent aEvent)
        {
            Close();
        }

        private void BtPesquisar_OnAfterClick(ItemEvent aEvent)
        {
            Pesquisar();
        }
        private void AGRTFrmCFLAgrotis_OnAfterFormLoad(ItemEvent aEvent)
        {
            CarregaDadosPesquisa();
            if (!string.IsNullOrEmpty(SQL))
                Pesquisar();
            else
                CarregarValidValuesColunas();
            SelecionaColunaPesquisa();
            EdPesquisa.Click();
        }

        private void GdPesquisa_OnAfterDoubleClick(ItemEvent aEvent)
        {
            if (aEvent.Row < 0)
                SelecionaColunaPesquisa(aEvent.ColUID);
            else
                SelecionarLinhasCFL();
        }
        #endregion

        #region Métodos

        private void SelecionarLinhasCFL()
        {
            if (DtPesquisa.IsEmpty)
            {
                Util.ExibirMensagemStatusBar("Não há registros na listagem.", aErro: true);
                return;
            }

            if (CFLTicket.DtResult_IsNull())
            {
                Util.ExibirMensagemStatusBar("Dev: Não há Datatable atribuído a este ChooseFromList.");
                return;
            }

            int iRowSelecionada = 0;

            CFLTicket.GetDtResult().Rows.Clear();
            for (int iCol = 0; iCol < CFLTicket.GetDtResult().Columns.Count; iCol++)
                CFLTicket.GetDtResult().Columns.Remove(iCol);

            CFLTicket.GetDtResult().Clear();

            for (int iCol = 0; iCol < DtPesquisa.Columns.Count; iCol++)
                CFLTicket.GetDtResult().Columns.Add(DtPesquisa.Columns.Item(iCol).Name, DtPesquisa.Columns.Item(iCol).Type);

            bool bAddRow = true;

            for (int iRow = 0; iRow < GdPesquisa.Rows.SelectedRows.Count; iRow++)
            {
                iRowSelecionada = GdPesquisa.Rows.SelectedRows.Item(iRow, BoOrderType.ot_RowOrder);
                for (int iCol = 0; iCol < DtPesquisa.Columns.Count; iCol++)
                {
                    dynamic dValor = DtPesquisa.GetValue(DtPesquisa.Columns.Item(iCol).Name, iRowSelecionada);
                    if (dValor != null && !string.IsNullOrWhiteSpace(Convert.ToString(dValor)))
                    {
                        if (bAddRow)
                        {
                            CFLTicket.GetDtResult().Rows.Add();
                            bAddRow = false;
                        }

                        CFLTicket.GetDtResult().SetValue(DtPesquisa.Columns.Item(iCol).Name, CFLTicket.GetDtResult().Rows.Count - 1, dValor);
                    }
                }
                bAddRow = true;
            }

            bPesquisaRealizada = true;
            Close();
        }

        /// <summary>
        /// Seleciona qual é a coluna Padrão que será usada para pesquisando quando
        /// clicar no Botão Pesquisar
        /// </summary>
        /// <param name="ColUID">UniqueID da Coluna - Se vazio, por padrão define a primeira</param>
        private void SelecionaColunaPesquisa(string ColUID = "")
        {
            if (!string.IsNullOrEmpty(ColUID))
                ColunaPesquisa = ColUID;
            else
                ColunaPesquisa = DtPesquisa.Columns.Item(0).Name;

            DataColumn Coluna = DtPesquisa.Columns.Item(ColunaPesquisa);

            switch (Coluna.Type)
            {
                case (SAPbouiCOM.BoFieldsType.ft_AlphaNumeric):
                    EdPesquisa.DataBind.SetBound(true, "", ItensUIDs.UdPesquisaTexto);
                    break;
                case (SAPbouiCOM.BoFieldsType.ft_Integer):
                case (SAPbouiCOM.BoFieldsType.ft_ShortNumber):
                    EdPesquisa.DataBind.SetBound(true, "", ItensUIDs.UdPesquisaInteiro);
                    break;
            }

            Util.ExibirMensagemStatusBar($"Coluna {Coluna.Name} configurada para pesquisa. Ordenação não disponível.", aDuracao: BoMessageTime.bmt_Short);

            if (DtoCflAgrotis.MultiSelection)
                GdPesquisa.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
            else
                GdPesquisa.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
        }

        /// <summary>
        /// Conforme o Tipo de Pesquisa enviado pelo DTO, monta a pesquisa e os Titulos 
        /// das colunas da Grid
        /// </summary>
        private void CarregaDadosPesquisa()
        {
            switch (DtoCflAgrotis.TipoPesquisa)
            {
                case CFLTipoAgrotis.Utilizacao:
                    {
                        SQL = PesquisasCFLAgrotis.PesquisaUtilizacao.SQL;
                        Titulos = PesquisasCFLAgrotis.PesquisaUtilizacao.Titulos;
                    }
                    break;
                case CFLTipoAgrotis.TiposComissao:
                    {
                        SQL = PesquisasCFLAgrotis.PesquisaTipoComissao.SQL;
                        Titulos = PesquisasCFLAgrotis.PesquisaTipoComissao.Titulos;
                    }
                    break;
                case CFLTipoAgrotis.Fabricante:
                    {
                        SQL = PesquisasCFLAgrotis.PesquisaFabricante.SQL;
                        Titulos = PesquisasCFLAgrotis.PesquisaFabricante.Titulos;
                    }
                    break;
                case CFLTipoAgrotis.FormulaAlternativaCopy:
                    {
                        SQL = PesquisasCFLAgrotis.PesquisaFormulaAlternativaCopy.SQL;
                        Titulos = PesquisasCFLAgrotis.PesquisaFormulaAlternativaCopy.Titulos;
                    }
                    break;                
                case CFLTipoAgrotis.UtilizacaoNaoContidaTextoNFe:
                    {
                        SQL = PesquisasCFLAgrotis.PesquisaUtilizacaoNaoContidaTextoNFe.SQL;
                        Titulos = PesquisasCFLAgrotis.PesquisaUtilizacaoNaoContidaTextoNFe.Titulos;
                        WhereClauseByParam = DtoCflAgrotis.WherePrincipal;
                    }
                    break;
                case CFLTipoAgrotis.OrdemProducao:
                    {
                        SQL = PesquisasCFLAgrotis.PesquisaOrdemProducao.SQL;
                        Titulos = PesquisasCFLAgrotis.PesquisaOrdemProducao.Titulos;
                    }
                    break;
                case CFLTipoAgrotis.Nutrientes:
                    {
                        SQL = PesquisasCFLAgrotis.PesquisaNutrientes.SQL;
                        Titulos = PesquisasCFLAgrotis.PesquisaNutrientes.Titulos;
                    }
                    break;                
                default: break;
            }
        }

        private void Pesquisar()
        {
            mForm.Freeze(true);
            try
            {
                string sSql = SQL;                

                if (!string.IsNullOrEmpty(sSql))
                {
                    string SQLWhere = string.Empty;

                    if (!string.IsNullOrWhiteSpace(WhereClauseDefault))
                        SQLWhere += $" {(SQLWhere.ToUpper().Contains("WHERE") ? "AND" : "WHERE")} { WhereClauseDefault }";

                    if (!string.IsNullOrWhiteSpace(WhereClauseByParam))
                        SQLWhere += $" {(SQLWhere.ToUpper().Contains("WHERE") ? "AND" : "WHERE")} { WhereClauseByParam }";

                    if (sSql.Contains(PesquisasCFLAgrotis.WhereClauseByParamConst))
                        sSql = sSql.Replace(PesquisasCFLAgrotis.WhereClauseByParamConst, SQLWhere);

                    if (sSql.Contains(PesquisasCFLAgrotis.ParamValue))
                        sSql = sSql.Replace(PesquisasCFLAgrotis.ParamValue, ParamValue);

                    if (!string.IsNullOrEmpty(EdPesquisa.Value))
                    {
                        string sColuna = $@"MainSQL.""{ ColunaPesquisa}""";
                        string sCondicional = string.Empty;

                        if (string.IsNullOrEmpty(ColunaPesquisa) && DtPesquisa.Columns.Count > 0)
                            SelecionaColunaPesquisa();

                        switch (DtPesquisa.Columns.Item(ColunaPesquisa).Type)
                        {
                            case (BoFieldsType.ft_AlphaNumeric):
                                sCondicional = $"upper({sColuna}) like '%{EdPesquisa.Value.ToUpper()}%'";
                                break;
                            case (BoFieldsType.ft_Integer):
                            case (BoFieldsType.ft_ShortNumber):
                                {
                                    int Valor;
                                    if (int.TryParse(EdPesquisa.Value, out Valor))
                                        sCondicional = $"{sColuna} = {EdPesquisa.Value}";
                                    else
                                        sCondicional = $"{sColuna}=-1";
                                }
                                break;
                            default: break;
                        }

                        if (!string.IsNullOrWhiteSpace(sCondicional))
                            sSql = $@"select MainSQL.* from ({sSql}) as MainSQL where {sCondicional}";
                    }
                    DtPesquisa.Clear();
                    DtPesquisa.ExecuteQuery(With + sSql);
                    GdPesquisa.DataTable = DtPesquisa;
                    GdPesquisa.AutoResizeColumns();

                    for (int iCol = 0; iCol < GdPesquisa.Columns.Count; iCol++)
                    {
                        GdPesquisa.Columns.Item(iCol).Editable = false;
                        GdPesquisa.Columns.Item(iCol).TitleObject.Sortable = true;
                        GdPesquisa.Columns.Item(iCol).TitleObject.Caption = Titulos[iCol];
                    }
                }
                else 
                {
                    if (!string.IsNullOrEmpty(EdPesquisa.Value))
                        DtoCflAgrotis.ValidValues = OpeReceituario.ListarProdutos(EdPesquisa.Value);
                    else
                        DtoCflAgrotis.ValidValues = OpeReceituario.ListarProdutos();
                    if (DtoCflAgrotis.ValidValues != null)
                        CarregarValidValues();
                }                
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
        private void CarregarValidValues()
        {
            DtPesquisa.Clear();
            if (DtPesquisa.Columns.Count <= 0)
                CarregarValidValuesColunas();
            CarregarValidValuesValores();
        }
        private void CarregarValidValuesColunas()
        {            
            if (DtPesquisa.IsEmpty)
            {
                DtPesquisa.Columns.Add(CFLColumnDefault.Code.ToString(), BoFieldsType.ft_AlphaNumeric);
                DtPesquisa.Columns.Add(CFLColumnDefault.Desc.ToString(), BoFieldsType.ft_AlphaNumeric);
                Titulos = new string[] {
                    Enumerators.GetDescription(CFLColumnDefault.Code),
                    Enumerators.GetDescription(CFLColumnDefault.Desc)
                };
                GdPesquisa.DataTable = DtPesquisa;
                GdPesquisa.AutoResizeColumns();

                for (int iCol = 0; iCol < GdPesquisa.Columns.Count; iCol++)
                {
                    GdPesquisa.Columns.Item(iCol).Editable = false;
                    GdPesquisa.Columns.Item(iCol).TitleObject.Sortable = true;
                    GdPesquisa.Columns.Item(iCol).TitleObject.Caption = Titulos[iCol];
                }
            }            
        }
        private void CarregarValidValuesValores()
        {
            if ((DtoCflAgrotis.ValidValues.Count > 0) && (DtPesquisa.IsEmpty))
            {
                int iRow = 0;                
                foreach (Tuple<string, string> Value in DtoCflAgrotis.ValidValues)
                {
                    if (string.IsNullOrEmpty(EdPesquisa.Value) || Value.Item2.ToUpper().Contains(EdPesquisa.Value.ToUpper()))
                    {
                        DtPesquisa.Rows.Add();
                        DtPesquisa.SetValue(CFLColumnDefault.Code.ToString(), iRow, Value.Item1.ToString());
                        DtPesquisa.SetValue(CFLColumnDefault.Desc.ToString(), iRow, Value.Item2.ToString());
                        iRow++;
                    }                    
                }
            }
        }
        #endregion

    }
}
