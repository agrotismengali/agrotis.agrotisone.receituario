﻿using Agrotis.AgrotisOne.Core.Model;
using Agrotis.AgrotisOne.Core.Utils;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.CFLAgrotis
{
    public class PesquisasCFLAgrotis
    {
        public const string WhereClauseByParamConst = "<?WhereClauseByParam?>";
        public const string WhereClauseByParamConst2 = "<?WhereClauseByParam2?>";
        public const string ParamValue = "<?ParamValue?>";
        public static class PesquisaUtilizacao
        {
            public static string[] Titulos = new string[] { "ID", "Descrição" };
            public static string SQL = "SELECT T0.\"ID\", T0.\"Usage\" FROM OUSG T0";
        }
        public static class PesquisaTipoComissao
        {
            public static string[] Titulos = new string[] { "Grupo Comissão", "Descrição" };
            public static string SQL = @"SELECT T0.""GroupCode"", T0.""GroupName"" FROM OCOG T0";
        }
        public static class PesquisaFabricante
        {
            public static string[] Titulos = new string[] { "Código", "Nome" };
            public static string SQL = @"SELECT T0.""FirmCode"", T0.""FirmName"" FROM OMRC T0";
        }        
        public static class PesquisaParceiroNegocio
        {
            public static string[] Titulos = new string[] { "Código do PN", "Nome do PN" };
            public static string SQL = $@"SELECT ""CardCode"", ""CardName"" FROM ""OCRD"" WHERE ""CardType"" = 'C' AND ""validFor"" = 'Y'";
        }
        public static class PesquisaUtilizacaoNaoContidaTextoNFe
        {
            private static string SubQuery = $@"select coalesce(""U_{Tabelas.TextoNFeUtilizacao.Campos.IdUtilizacao.Nome}"", -1)
                                                from   ""@{Tabelas.TextoNFeUtilizacao.Nome}""
                                                {WhereClauseByParamConst}";

            public static string[] Titulos = new string[] { "Id", "Utilização" };
            public static string SQL = $@"select ""ID"", ""Usage"" from ""OUSG"" where ""ID"" not in ({SubQuery})";
        }

        #region [ Indústria ]

        public static class PesquisaFormulaAlternativaCopy
        {
            public static string[] Titulos = new string[] { "Item", "Descrição Item", "Fórmula", "Código" };
            public static string SQL = $@"select United.""ItemCode"",
		                                         United.""Name"",
                                                 United.""Fórmula"",
                                                 United.""Code""
                                          from	(select	OITT.""Code"" as ""ItemCode"",
 				                                         OITT.""Name"",
                                                         OITT.""Code"",
                                                         'Padrão' as ""Fórmula"",
                                                         1 as ""Order""
                                                  from	OITT OITT
                                                  where	(select count(1) from ITT1 sITT1 where sITT1.""Father"" = OITT.""Code"") > 0
                                                  union all
                                                  select	FORMULA.""U_AGRT_ItemCode"" as ""ItemCode"",
         		                                         FORMULA.""U_AGRT_ItemName"" as ""Name"",
                                                         FORMULA.""Code"",
                                                         FORMULA.""Name"" as ""Fórmula"",
                                                         2 as ""Order""
                                                  from	""@AGRT_FRMULALTERNA"" FORMULA
                                                  where	(select count(1) from ""@AGRT_FRMULALTERN1"" sITENS where sITENS.""Code"" = FORMULA.""Code"") > 0
                                                ) as United
                                          order by ""ItemCode"", ""Order"", ""Fórmula""";
        }

        public static class PesquisaOrdemProducao
        {
            public static string[] Titulos = new string[] { "Número", "Produt", "Data ínicio", "Quantidade" };
            public static string SQL = $@"SELECT ORDEM.""DocNum"" NUMOP,
                                            CONCAT(ITEMPA.""ItemCode"", CONCAT(' - ', ITEMPA.""ItemName"")) PRODUTO,
                                         	ORDEM.""StartDate""  DATAINICIO,
                                         	ORDEM.""PlannedQty"" QTDADEOP
                                          FROM OWOR ORDEM
                                          LEFT JOIN OITM ITEMPA ON
                                            ORDEM.""ItemCode"" = ITEMPA.""ItemCode""";
        }

        public static class PesquisaNutrientes
        {
            public static string[] Titulos = new string[] { "Sigla", "Nome", "Unidade" };
            public static string SQL = $@"select    ""Code"",
                                                    ""Name"",
                                                    ""U_AGRT_Unidade""
                                          from      ""@AGRT_NUTRIENTE""
                                          order by  ""Name""";
        }

        public static class PesquisaEntregaPedido
        {
            public static string[] Titulos = new string[] { "Cód. pedido", "Nº pedido", "Código PN", "Parceiro de negócio", "Data pedido", "Item único", "Fórmula", "Disponível" };
            public static string With = $@"with Disponivel as (
                                             select 
                                               ItemPedido.""DocEntry"",
                                               ItemPedido.""WhsCode"",
                                               (case when count(1) = 1 
                                                  then min(cast(ItemPedido.""LineNum"" as varchar(10)))
                                                  else '-' 
                                                end) ""LineNum"",
                                               (case when count(1) = 1 
                                                  then min(case when ItemPedido.""U_AGRT_Formula"" = '0'
			                                                then (select coalesce(sOITT.""U_AGRT_Descricao"", 'Padrão')
			                                                      from   OITT sOITT
			                                                      where  sOITT.""Code"" = ItemPedido.""ItemCode"")
			                                                else ItemPedido.""U_AGRT_Formula""
			                                            end)
                                                  else '-' 
                                                end) ""Formula"",
                                               cast(round(sum(coalesce(ItemPedido.""OpenQty"" - coalesce(Entrega.""U_AGRT_Quantidade"", 0), 0.0000)), {ConfigUtil.CasasDecimaisQuantidade()}) as decimal(19, {ConfigUtil.CasasDecimaisQuantidade()})) ""Disponível""
                                             from RDR1 ItemPedido
                                               left join ""@AGRT_ENTREGAPED"" Entrega on
                                                 Entrega.""U_AGRT_Pedido"" = ItemPedido.""DocEntry"" and
											     Entrega.""U_AGRT_PedidoItem"" = ItemPedido.""LineNum"" and
										         exists(select ""Code"" from ""@AGRT_ENTREGA"" where ""U_AGRT_Situacao"" not in ('C', 'T'))
                                             group by ItemPedido.""DocEntry"" , ItemPedido.""WhsCode""
                                          )";
            public static string SQL = $@"select 
                                            Pedido.""DocEntry"" ""CódigoPedido"",
                                            Pedido.""DocNum"" ""NúmeroPedido"",
		                                    Pedido.""CardCode"" ""CódigoPN"",
		                                    Pedido.""CardName"" ""NomePN"",
		                                    Pedido.""DocDate"" ""DataPedido"",
                                            Disponivel.""LineNum"" ""CódigoPedidoItem"", 
                                            Disponivel.""Formula"" ""Fórmula"",
                                            (case when Disponivel.""LineNum"" = '-' 
                                               then '-' 
                                               else replace(Disponivel.""Disponível"", '.', ',')
                                             end) ""Disponível""
                                          from ORDR Pedido
                                            left join Disponivel on 
                                              Disponivel.""DocEntry"" = Pedido.""DocEntry""
                                          {WhereClauseByParamConst}
                                          order by 
                                            Pedido.""DocDate"" desc, 
                                            Pedido.""CardCode""";
            public static string WhereClauseDefault = @"where Pedido.""ObjType"" = '17'
	                                                      and Pedido.""DocStatus"" = 'O'";
        }

        public static class PesquisaEntregaPedidoItem
        {
            public static string[] Titulos = new string[] { "Cód. pedido", "Item pedido", "Código item", "Item", "Fórmula", "Data entrega", "Disponível", "ID" };
            public static string sTotalEntrega = $@"""TotalEntrega"" as (
                                                      select
                                                        EntregaPedido.""U_AGRT_Pedido"" ""DocEntry"",
                                                        EntregaPedido.""U_AGRT_PedidoItem"" ""LineNum"",
                                                        sum(EntregaPedido.""U_AGRT_Quantidade"") ""Quantidade""
                                                      from ""@AGRT_ENTREGAPED"" EntregaPedido
                                                        inner join ""@AGRT_ENTREGA"" Entrega on
                                                          Entrega.""Code"" = EntregaPedido.""Code"" and
                                                          Entrega.""U_AGRT_Situacao"" not in ('C', 'T')
                                                      {WhereClauseByParamConst2}
                                                      group by
                                                        EntregaPedido.""U_AGRT_Pedido"",
                                                        EntregaPedido.""U_AGRT_PedidoItem"")";
            public static string sQuantidadeDisponivel = @"ItemPedido.""OpenQty"" - coalesce(TotalEntrega.""Quantidade"", 0)";
            public static string sItemPedidoID = @"lpad(ItemPedido.""DocEntry"", 10, '0') || lpad(ItemPedido.""LineNum"", 10, '0')";
            public static string With = $@"with {sTotalEntrega.Replace(WhereClauseByParamConst2, string.Empty)}";
            public static string sFrom = @"RDR1 ItemPedido
                                           left join ""TotalEntrega"" TotalEntrega on	
                                             TotalEntrega.""DocEntry"" = ItemPedido.""DocEntry"" and
                                             TotalEntrega.""LineNum"" = ItemPedido.""LineNum""";
            public static string SQL = $@"select
                                            ItemPedido.""DocEntry"" ""CódigoPedido"",
		                                    ItemPedido.""LineNum"" ""CódigoPedidoItem"",
		                                    ItemPedido.""ItemCode"" ""CódigoItem"",
		                                    ItemPedido.""Dscription"" ""DescriçãoItem"",
                                            case when ItemPedido.""U_AGRT_Formula"" = '0'
                                                then (select coalesce(sOITT.""U_AGRT_Descricao"", 'Padrão') as ""Fórmula""
                                                      from   OITT sOITT
                                                      where  sOITT.""Code"" = ItemPedido.""ItemCode"")
                                                else ItemPedido.""U_AGRT_Formula""
                                            end as ""Fórmula"",
		                                    ItemPedido.""ShipDate"" ""DataEntrega"",
		                                    {sQuantidadeDisponivel} ""Disponível"",
                                            {sItemPedidoID} ""ID""
                                          from {sFrom}
                                          {WhereClauseByParamConst}
                                          order by
                                            ItemPedido.""ShipDate"" desc, 
                                            ItemPedido.""DocEntry"", 
                                            ItemPedido.""LineNum""";
        }

        public static class PesquisaEntregaOrigemOrdem
        {
            public static string[] Titulos = new string[] { "Código OP", "Nº OP", "Data planejada", "Lote", "Disponível" };
            public static string sTotalEntrega = $@"""TotalEntrega"" as (
                                                      select
                                                        EntregaOrig.""U_AGRT_Ordem"" ""DocEntry"",
                                                        sum(EntregaOrig.""U_AGRT_Quantidade"") ""Quantidade""
                                                      from ""@AGRT_ENTREGAORIG"" EntregaOrig
                                                        inner join ""@AGRT_ENTREGA"" Entrega on
                                                          Entrega.""Code"" = EntregaOrig.""Code"" and
                                                          Entrega.""U_AGRT_Situacao"" not in ('C', 'T')
                                                      {WhereClauseByParamConst2}
                                                      group by
                                                        EntregaOrig.""U_AGRT_Ordem"")";
            public static string sQuantidadeDisponivel = @"(case when Ordem.""Status"" = 'L'
			                                                  then Ordem.""CmpltQty""
			                                                  else Ordem.""PlannedQty""
		                                                    end) - coalesce(TotalEntrega.""Quantidade"",0)";
            public static string With = $@"with {sTotalEntrega.Replace(WhereClauseByParamConst2, string.Empty)},
                                          LoteUnico as (
                                            select
                                              SaidaPA.""BaseEntry"" ""DocEntry"",
                                              SaidaPA.""BaseType"" ""ObjType"",
                                              (case when count(1) = 1 then min(Lote.""SysNumber"") else null end) ""Lote"" 
                                            from IGN1 SaidaPA
                                              left join IBT1 SaidaLotePA on
                                                SaidaLotePA.""BaseEntry"" = SaidaPA.""DocEntry"" and
                                                SaidaLotePA.""ItemCode"" = SaidaPA.""ItemCode""
                                              left join OIBT Lote on
                                                Lote.""ItemCode"" = SaidaPA.""ItemCode"" and
                                                Lote.""BatchNum"" = SaidaLotePA.""BatchNum""
                                            group by SaidaPA.""BaseEntry"", 
                                              SaidaPA.""BaseType"" 
                                          )";
            public static string sFrom = $@"OWOR Ordem
                                            left join ""TotalEntrega"" TotalEntrega on
                                              TotalEntrega.""DocEntry"" = Ordem.""DocEntry""";
            public static string SQL = $@"select
                                            Ordem.""DocEntry"" ""CódigoOP"",
		                                    Ordem.""DocNum"" ""NúmeroOP"",
		                                    Ordem.""StartDate"" ""DataPlanejada"",
		                                    (case when LoteUnico.""Lote"" is null 
                                               then '-' 
                                               else cast(LoteUnico.""Lote"" as varchar(10))
                                             end) ""CódigoLoteÚnico"",
		                                    {sQuantidadeDisponivel} ""Disponível""
                                          from {sFrom}
                                            left join LoteUnico on
                                              LoteUnico.""DocEntry"" = Ordem.""DocEntry"" and
                                              LoteUnico.""ObjType"" = Ordem.""ObjType""
                                          {WhereClauseByParamConst}
                                          order by Ordem.""StartDate""";
        }

        public static class PesquisaEntregaOrigemLote
        {
            public static string[] Titulos = new string[] { "Cód. produto", "Cód. depósito", "Cód. lote", "Nº lote", "Validade", "Disponível" };
            public static string sDisponivel = $@"""TotalEntrega"" as (
                                                    select
                                                      EntregaOrig.""U_AGRT_Produto"" ""ItemCode"",
                                                      EntregaOrig.""U_AGRT_Lote"" ""SysNumber"",
                                                      sum(EntregaOrig.""U_AGRT_Quantidade"") ""Quantidade""
                                                    from ""@AGRT_ENTREGAORIG"" EntregaOrig
                                                      inner join ""@AGRT_ENTREGA"" Entrega on
                                                        Entrega.""Code"" = EntregaOrig.""Code"" and
                                                        Entrega.""U_AGRT_Situacao"" not in ('C', 'T')
                                                    {WhereClauseByParamConst2}
                                                    group by
                                                      EntregaOrig.""U_AGRT_Produto"",
                                                      EntregaOrig.""U_AGRT_Lote""
                                                  ),
                                                  Disponivel as (
                                                    select 
                                                      SaldoLoteDeposito.""ItemCode"", 
                                                      SaldoLoteDeposito.""WhsCode"",
                                                      SaldoLoteDeposito.""SysNumber"",
                                                      SaldoLoteDeposito.""BatchNum"",
                                                      sum(SaldoLoteDeposito.""Quantity"") - max(coalesce(TotalEntrega.""Quantidade"",0)) ""Quantidade""
                                                    from OIBT SaldoLoteDeposito
                                                      left join ""TotalEntrega"" TotalEntrega on
                                                        TotalEntrega.""ItemCode"" = SaldoLoteDeposito.""ItemCode"" and
                                                        TotalEntrega.""SysNumber"" = SaldoLoteDeposito.""SysNumber""
                                                    where round(SaldoLoteDeposito.""Quantity"", {ConfigUtil.CasasDecimaisQuantidade()}) > 0
                                                    group by
                                                      SaldoLoteDeposito.""ItemCode"",
                                                      SaldoLoteDeposito.""WhsCode"",
                                                      SaldoLoteDeposito.""SysNumber"",
                                                      SaldoLoteDeposito.""BatchNum""
                                                  )";
            public static string sQuantidadeDisponivel = @"Disponivel.""Quantidade""";
            public static string With = $@"with {sDisponivel.Replace(WhereClauseByParamConst2, string.Empty)}";
            public static string SQL = $@"select 
                                            Disponivel.""ItemCode"" ""CódigoItem"", 
                                            Disponivel.""WhsCode"" ""CódigoDepósito"",
                                            Disponivel.""SysNumber"" ""CódigoLote"",
                                            Disponivel.""BatchNum"" ""NúmeroLote"",
                                            Lote.""ExpDate"" ""Validade"",
                                            {sQuantidadeDisponivel} ""Disponível""
                                          from Disponivel
                                            left join OBTN Lote on
                                              Lote.""ItemCode"" = Disponivel.""ItemCode"" and
                                              Lote.""SysNumber"" = Disponivel.""SysNumber""
                                          {WhereClauseByParamConst}
                                          order by
                                            Disponivel.""ItemCode"", 
                                            Disponivel.""WhsCode"",
                                            Disponivel.""SysNumber""";
        }

        public static class PesquisaEntregaCopy
        {
            public static string[] Titulos = new string[] { "Referência", "Data", "Cód. Filial", "Filial", "Placa", "Motorista", "Observações" };
            public static string SQL = $@"select	 ENTR.""Name"",
			                                         ENTR.""U_AGRT_DataEntrega"",
			                                         ENTR.""U_AGRT_Filial"",
			                                         OBPL.""BPLName"" as ""Filial"",
			                                         ENTR.""U_AGRT_Placa"",
			                                         ENTR.""U_AGRT_NomeMotorista"",
			                                         ENTR.""U_AGRT_Observacao""
                                          from		 ""@AGRT_ENTREGA"" ENTR
                                          inner join OBPL on (OBPL.""BPLId"" = ENTR.""U_AGRT_Filial"")
                                          order by	 ENTR.""U_AGRT_DataEntrega"" desc,
			                                         ENTR.""Name""";
        }

        public static class PesquisaFormulasItemCode
        {
            public static string[] Titulos = new string[] { "Cód. item", "Descrição item", "Fórmula", "Cód. fórmula" };
            public static string SQL = $@"select United.""ItemCode"",
		                                         United.""Name"",
                                                 United.""Fórmula"",
                                                 United.""Code""
                                          from	(select	 '0' as ""ItemCode"",
 				                                         OITT.""Name"",
                                                         OITT.""Code"",
                                                         coalesce(OITT.""U_AGRT_Descricao"", 'Padrão') as ""Fórmula"",
                                                         1 as ""Order""
                                                  from	OITT OITT
                                                  where OITT.""Code"" = '{ParamValue}'

                                                  union all

                                                  select FORMULA.""U_AGRT_ItemCode"" as ""ItemCode"",
         		                                         FORMULA.""U_AGRT_ItemName"" as ""Name"",
                                                         FORMULA.""Code"",
                                                         FORMULA.""Name"" as ""Fórmula"",
                                                         2 as ""Order""
                                                  from	""@AGRT_FRMULALTERNA"" FORMULA
                                                  where FORMULA.""U_AGRT_ItemCode"" = '{ParamValue}'
                                                ) as United
                                          order by ""ItemCode"", ""Order"", ""Fórmula""";
        }

        #endregion
    }
}
