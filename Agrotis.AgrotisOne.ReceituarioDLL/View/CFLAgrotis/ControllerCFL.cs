﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Agrotis.AgrotisOne.Core.DTOs;
using Agrotis.AgrotisOne.Core.FormTicket;
using Agrotis.AgrotisOne.Core.Utils;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using SAPbouiCOM;
using Agrotis.AgrotisOne.Core;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.CFLAgrotis
{
    public class ControllerCFL
    {                
        public bool ChooseFromList(Form aForm, DTOCflAgrotis dtoCflAgrotis)
        {
            if (!Globals.Master.FormTicketCFL.NovoCFL(aForm, dtoCflAgrotis))
                return false;

            StreamReader SrForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(Core.Constantes.SAPFormConstants.AGRTFrmCFLAgrotisReceituarioSrf));
            Util.FormAbrir(SrForm);
            return true;
        }
        public void ChooseFromList(Form aForm, Action aMetodoRetorno, CFLTipoAgrotis aTipoPesquisa)
        {                         
            DTOCflAgrotis dtoCflAgrotis = new DTOCflAgrotis(false, null);
            if (!Globals.Master.FormTicketCFL.NovoCFL(aForm, dtoCflAgrotis))
                return;

            StreamReader SrForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(Core.Constantes.SAPFormConstants.AGRTFrmCFLAgrotisReceituarioSrf));
            Util.FormAbrir(SrForm);
            FormTicketThread ThReturnTicket = new FormTicketThread(aMetodoRetorno.Method.ReflectedType.Name, aMetodoRetorno, aTipoPesquisa);
            ThReturnTicket.Iniciar();
        }        
        public static bool FormSetAbrirTicketCFL(SAPbouiCOM.Form aForm, DTOCflAgrotis dtoCflAgrotis, string aFormSrf)
        {
            if (!Globals.Master.FormTicketCFL.NovoCFL(aForm, dtoCflAgrotis))
                return false;

            StreamReader SrForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(aFormSrf));
            Util.FormAbrir(SrForm);
            return true;
        }
    }    
}
