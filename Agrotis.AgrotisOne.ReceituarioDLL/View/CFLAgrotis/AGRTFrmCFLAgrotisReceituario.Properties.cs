﻿using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using Grid = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Grid;
using SAPbouiCOM;
using Agrotis.AgrotisOne.Core.DTOs;
using Agrotis.AgrotisOne.Core.FormTicket;
using Agrotis.AgrotisOne.Core;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.CFLAgrotis
{
    public partial class AGRTFrmCFLAgrotisReceituario
    {
        #region Properties
        bool bPesquisaRealizada;
        EditText EdPesquisa { get; set; }
		Button BtOk { get; set; }
        Button BtPesquisar { get; set; }
        Button BtCancelar { get; set; }
		Grid GdPesquisa { get; set; }
        DataTable DtPesquisa { get; set; }
        string ColunaPesquisa = string.Empty;
        FormParamTicketCFL CFLTicket = Globals.Master.FormTicketCFL;
        DTOCflAgrotis DtoCflAgrotis { get; set; }
        public CFLTipoAgrotis TipoCFL { get; set; }
        public bool MultiSelection { get; set; }
        private string SQL { get; set; }
        private string With { get; set; }
        private string[] Titulos { get; set; }
        private string WhereClauseByParam { get; set; }
        private string ParamValue { get; set; }
        private string WhereClauseDefault { get; set; }        
        #endregion

        #region ItensUIDs
        public static class ItensUIDs
        {
            public const string EdPesquisa = "EdPesq";
            public const string BtOk = "BtOk";
            public const string BtCancelar = "BtCanc";
            public const string GdPesquisa = "GdPesq";
            public const string DtPesquisa = "DtPesq";
            public const string DtSelectedObjects = "DtSelect";
            public const string UdPesquisaTexto = "UdPesqT";
            public const string UdPesquisaInteiro = "UdPesqI";
            public const string BtPesquisar = "BtPesq";
        }
        #endregion
    }
}
