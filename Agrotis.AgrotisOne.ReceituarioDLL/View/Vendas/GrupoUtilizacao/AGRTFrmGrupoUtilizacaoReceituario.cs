﻿using System;
using SAPbouiCOM;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Form = Agrotis.Framework.Main.Interface.Controls.Forms.Base.Form;
using Matrix = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Matrix;
using Agrotis.Framework.Main.Interface.Events;
using Agrotis.AgrotisOne.Core.Utils;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using Agrotis.AgrotisOne.Core.DTOs;
using Agrotis.AgrotisOne.Core.FormTicket;
using Agrotis.AgrotisOne.Core.Model;
using Agrotis.AgrotisOne.ReceituarioDLL.View.CFLAgrotis;
using Agrotis.AgrotisOne.Core;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Vendas.GrupoUtilizacao
{
    [FormType("AGRTFrmGrpUtilRec")]
    public class AGRTFrmGrupoUtilizacaoReceituario : Form
    {
        public AGRTFrmGrupoUtilizacaoReceituario(AddOn aAddOn, string aFormUID)
            : base(aAddOn, aFormUID)
        {
            edtCod = Items.Add<EditText>(ItensUIDs.edtCod);
            edtNome = Items.Add<EditText>(ItensUIDs.edtNome);
            edtNome.OnGotFocus += new EventVoidItem(edtNome_OnGotFocus);
            edtNome.OnLostFocus += new EventVoidItem(edtNome_OnLostFocus);

            mtxItens = Items.Add<Matrix>(ItensUIDs.mtxItens);
            mtxItens.OnGotFocus += new EventVoidItem(MtxItens_OnGotFocus);

            oDataSource = this.DataSources.DBDataSources.Item(DataSourceUIDs.Tabela);
            oDataSourceMtx01 = this.DataSources.DBDataSources.Item(DataSourceUIDs.DataSourceMtx01.Tabela);

            OnBeforeFormDataAdd += new EventBoolBusinessInfo(AGRTFrmGrupoUtilizacao_OnBeforeFormDataAdd);
            OnBeforeFormDataUpdate += new EventBoolBusinessInfo(AGRTFrmGrupoUtilizacao_OnBeforeFormDataUpdate);
            DtRetornoCFL = mForm.DataSources.DataTables.Item(ItensUIDs.DtUtil);
        }

        private void MtxItens_OnGotFocus(ItemEvent aEvent)
        {
            if (mForm.Mode != BoFormMode.fm_FIND_MODE)
            {
                if (mtxItens.GetCellFocus().ColumnIndex == 1)
                {
                    RowNumber = mtxItens.GetCellFocus().rowIndex - 1;
                    if (ControllerCFL.FormSetAbrirTicketCFL(mForm, new DTOCflAgrotis(CFLTipoAgrotis.Utilizacao, true), Core.Constantes.SAPFormConstants.AGRTFrmCFLAgrotisReceituarioSrf))
                    {
                        FormTicketThread ThReturnTicket = new FormTicketThread("AGRTFrmGrupoUtilizacaoReceituario", OnAfterReceberTicket, CFLTipoAgrotis.Utilizacao);
                        ThReturnTicket.Iniciar();
                    }                    
                }
            }
        }

        #region Eventos
        bool AGRTFrmGrupoUtilizacao_OnBeforeFormDataUpdate(BusinessObjectInfo aEvent)
        {
            return ValidateInsertUpdate();
        }

        bool AGRTFrmGrupoUtilizacao_OnBeforeFormDataAdd(BusinessObjectInfo aEvent)
        {
            return ValidateInsertUpdate();
        }

        private Boolean ValidateInsertUpdate()
        {
            if (string.IsNullOrWhiteSpace(edtNome.Value))
            {
                Util.ExibirMensagemStatusBar("Obrigatório Informar Descrição!");
                Util.ExibirDialogo("Obrigatório Informar Descrição!");
                return false;
            }

            if (mtxItens.RowCount == 0)
            {
                Util.ExibirMensagemStatusBar("Obrigatório adicionar ao menos uma utilização no grupo!");
                Globals.Master.Connection.Interface.MessageBox("Obrigatório adcionar ao menos uma utilização no grupo!");
            }
            return true;

        }

        void edtNome_OnGotFocus(ItemEvent aEvent)
        {
            InicioTelaGrupoUtilizacao();
        }
        void edtNome_OnLostFocus(ItemEvent aEvent)
        {
            InicioTelaGrupoUtilizacao();
        }
        #endregion

        #region Métodos
        private void PopularMatrixUtilizacao()
        {
            if (DtRetornoCFL != null)
            {
                mtxItens.FlushToDataSource();

                for (int iRow = 0; iRow < DtRetornoCFL.Rows.Count; iRow++)
                {
                    oDataSourceMtx01.SetValue("U_AGRTCodUti", RowNumber + iRow, DtRetornoCFL.GetValue("ID", iRow));
                    oDataSourceMtx01.SetValue("U_AGRTDesc", RowNumber + iRow, DtRetornoCFL.GetValue("Usage", iRow));
                    oDataSourceMtx01.InsertRecord(oDataSourceMtx01.Size);
                }
                mtxItens.LoadFromDataSource();

            }
        }
        private void InicioTelaGrupoUtilizacao()
        {
            if (mForm.Mode == BoFormMode.fm_ADD_MODE)
            {
                if (String.IsNullOrEmpty(edtCod.Value))
                {
                    edtCod.Value = Agrotis.Framework.Data.Database.ReturnCode.Code(DataSourceUIDs.Tabela);
                }
            }
            if (mForm.Mode == BoFormMode.fm_ADD_MODE || mForm.Mode == BoFormMode.fm_UPDATE_MODE)
            {
                if (mtxItens.RowCount == 0)
                    mtxItens.InserirLinha(oDataSourceMtx01);
            }
        }
        #endregion 

        #region Propriedades do form

        public EditText edtCod { get; set; }
        public EditText edtNome { get; set; }
        public Matrix mtxItens { get; set; }
        public DBDataSource oDataSource { get; set; }
        public DBDataSource oDataSourceMtx01 { get; set; }
        private Int32 RowNumber = 0;
        private CFLTipoAgrotis TipoPesquisa = new CFLTipoAgrotis();
        public DataTable DtRetornoCFL = null;

        #endregion

        #region Itens UIDs
        /// <summary>
        /// Classe com os UniqueIDs dos itens do form
        /// </summary>
        public static class ItensUIDs
        {

            public const string edtCod = "edtCod";
            public const string edtNome = "edtNome";
            public const string mtxItens = "mtxItens";
            public const string DtUtil = "DtUtil";
        }

        #endregion

        #region DataSourceUIDs
        public static class DataSourceUIDs
        {
            public const string Tabela = "@" + Tabelas.GrupoUtilizacao.Nome;
            public const string Codigo = "Code";
            public const string Nome = "Name";

            public static class DataSourceMtx01
            {
                public const string Tabela = "@" + Tabelas.GrupoUtilizacaoLinhas.Nome;
                public const string Codigo = "Code";
                public const string LineNum = "LineId";
                public const string ItemCode = "U_" + Tabelas.GrupoUtilizacaoLinhas.Campos.CodUtilizacao.Nome;
                public const string ItemName = "U_" + Tabelas.GrupoUtilizacaoLinhas.Campos.DescricaoUtilizacao.Nome;
                
            }

        }
        #endregion

        private void OnAfterReceberTicket()
        {
            try
            {
                if (Util.DataTableLoadData(Globals.Master.FormTicketCFL.GetDtResult(), ref DtRetornoCFL))
                {
                    switch (Globals.Master.FormTicketCFL.TipoPesquisa)
                    {
                        case (CFLTipoAgrotis.Utilizacao):
                            {
                                PopularMatrixUtilizacao();
                            }
                            break;
                    }
                }
            }
            finally
            {
                Globals.Master.FormTicketCFL.DtResult_ToNull();
                GC.Collect();
            }            
        }
    }
}
