﻿using Agrotis.Framework.Main.Interface.Controls.Menus.Attributes;
using Agrotis.Framework.Main.Interface.Controls.Menus.Base;
using System;
using System.IO;
using System.Reflection;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Vendas.GrupoUtilizacao
{    
    [MenuType("AGRTMnuGrupoUtililizacaoRec")]
    public class AGRTMnuGrupoUtililizacao : Menu
    {
        public AGRTMnuGrupoUtililizacao(Framework.Main.Application.AddOn aAddOn, string aMenuUID)
            : base(aAddOn, aMenuUID)
        {
            OnAfterClick += new Framework.Main.Interface.Events.EventVoidMenu(AGRTMnuGrupoUtililizacao_OnAfterClick);
        }

        private void AGRTMnuGrupoUtililizacao_OnAfterClick(SAPbouiCOM.MenuEvent aEvent)
        {
            using (StreamReader lForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(this.GetType().Namespace + ".AGRTFrmGrupoUtilizacaoReceituario.srf")))
            {
                string lFrmParametrizacoes = lForm.ReadToEnd();

                this.AddOn.Connection.Interface.LoadBatchActions(string.Format(lFrmParametrizacoes, new Random().Next().GetHashCode()));
            }
        }
    }
}
