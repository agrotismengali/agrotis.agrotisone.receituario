﻿using System;
using SAPbouiCOM;
using Agrotis.AgrotisOne.ReceituarioDLL.Model;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using ComboBox = Agrotis.Framework.Main.Interface.Controls.Items.Specific.ComboBox;
using CheckBox = Agrotis.Framework.Main.Interface.Controls.Items.Specific.CheckBox;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using Form = Agrotis.Framework.Main.Interface.Controls.Forms.Base.Form;
using IntegracaoPN = Agrotis.AgrotisOne.Core.Integracao.IntegracaoParceiroNegocio;
using Matrix = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Matrix;
using Agrotis.Framework.Main.Interface.Events;
using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.API;
using Agrotis.AgrotisOne.Core.Constantes;
using Agrotis.Framework.Commons;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using System.Collections.Generic;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Configuracoes
{    
    [FormType("AGRTFrmConfigRec")]
    public class AGRTFrmConfigRec : Form
    {
        OperacoesReceituario OpeReceituario = new OperacoesReceituario();
        public AGRTFrmConfigRec(AddOn aAddOn, string aFormUID)
            : base(aAddOn, aFormUID)
        {
            OnAfterFormLoad += new EventVoidItem(AGRTFrmConfigRec_OnAfterFormLoad);            
        }
        private bool BtnOk_OnBeforeItemPressed(ItemEvent aEvent)
        {
            if (mForm.Mode != BoFormMode.fm_ADD_MODE && mForm.Mode != BoFormMode.fm_UPDATE_MODE)
                return true;
            try
            {
                for (int RowID = 1; RowID <= Mtx01.VisualRowCount - 1; RowID++)
                {
                    string BPLId = Util.MatrixGetValorCelula(Mtx01, ItensUIDs.ColBPLId, RowID);
                    string CodeAgron = Util.MatrixGetValorCelula(Mtx01, ItensUIDs.ColCodeAgronomo, RowID);
                    if (string.IsNullOrEmpty(BPLId) ^ string.IsNullOrEmpty(CodeAgron))
                    {
                        Util.ExibirDialogo("Informação incompleta na Grid de configuração por Filial.");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Erro ao selecionar Filial", Ex);
                return false;
            }
            finally
            {
                GC.Collect();
            }
        }
        private void BtnTest_OnAfterItemPressed(ItemEvent aEvent)
        {
            try
            {
                if (WSConfiguracao.ConexaoValida(OpeReceituario.dtoAPI.Uri, EdToken.Value))
                {
                    Util.ExibirDialogo($"Conexão realizada com sucesso!");

                    string sMsg;
                    string sDataIni = EdDtIntegr.Value;
                    if (string.IsNullOrEmpty(sDataIni))
                        sMsg = "Deseja sincronizar TODOS os parceiros de negócio com o Agrotis Plataforma? Essa operação pode demorar alguns minutos.";
                    else
                        sMsg = $"Deseja sincronizar os parceiros de negócio criados/alterados a partir de {String.Format("{0:dd/MM/yyyy}", FormatValue.ToDate(sDataIni))}?";

                    if (Util.ExibirDialogoConfirmacao(sMsg))
                        new IntegracaoPN().FullLoad(sDataIni);                    
                }
                else
                    Util.ExibirDialogo($"Não foi possível realizar a conexão.");
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnTest_OnAfterItemPressed - Não foi possível realizar a conexão. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível realizar a conexão. " + Ex.Message);                
            }
            finally
            {
                GC.Collect();
            }                      
        }  
        private bool CarregarConfiguracao()
        {
            if (SqlUtils.DoQueryH($@"SELECT 1 FROM ""{DataSourceUIDs.Tabela}"" WHERE ""Code"" = 1").Count > 0)
            {
                this.Mode = BoFormMode.fm_FIND_MODE;
                mForm.Items.Item(ItensUIDs.EdCode).Click();
                oDataSource.SetValue(DataSourceUIDs.Codigo, 0, "1");
                mForm.Items.Item("1").Click();
                return true;
            }
            else
                return false;
        }  
        private List<Tuple<string, string>> GetValidValuesImpDoc()
        {
            var docList = new List<Tuple<string, string>>();
            docList.Add(new Tuple<string, string>("0", "Nota fiscal"));
            docList.Add(new Tuple<string, string>("1", "Pedido de venda"));
            docList.Add(new Tuple<string, string>("2", "Não imprimir"));
            return docList;
        }
        private void AGRTFrmConfigRec_OnAfterFormLoad(ItemEvent aEvent)
        {
            mForm.Freeze(true);
            try
            {
                mForm.EnableMenu(SAPMenuConstants.SAPAdicionar, false);
                mForm.EnableMenu(SAPMenuConstants.SAPRemover, false);
                mForm.EnableMenu(SAPMenuConstants.SAPProcurar, false);
                mForm.EnableMenu(SAPMenuConstants.SAPNavigatorFirst, false);
                mForm.EnableMenu(SAPMenuConstants.SAPNavigatorPrior, false);
                mForm.EnableMenu(SAPMenuConstants.SAPNavigatorNext, false);
                mForm.EnableMenu(SAPMenuConstants.SAPNavigatorLast, false);
                mForm.EnableMenu(SAPMenuConstants.SAPLinhaMatrizInserir, false);
                mForm.EnableMenu(SAPMenuConstants.SAPLinhaMatrizExcluir, false);

                EdCode = Items.Add<EditText>(ItensUIDs.EdCode);
                EdToken = Items.Add<EditText>(ItensUIDs.EdToken);
                CbGrpUtil = Items.Add<ComboBox>(ItensUIDs.CbGrpUtil);
                CbGrpUtil.DisplayDesc = true;
                CbImpRect = Items.Add<ComboBox>(ItensUIDs.CbImpRect);
                CbImpRect.DisplayDesc = true;                
                EdDtIntegr = Items.Add<EditText>(ItensUIDs.EdDtIntegr);

                CkInfoNf = Items.Add<CheckBox>(ItensUIDs.CkInfoNf);
                CkInfoNf.OnAfterItemPressed += new EventVoidItem(CkInfoNf_OnAfterItemPressed);

                BtnOk = Items.Add<Button>(ItensUIDs.BtnOk);
                BtnOk.OnBeforeItemPressed += new EventBoolItem(BtnOk_OnBeforeItemPressed);
                BtnTest = Items.Add<Button>(ItensUIDs.BtnTest);
                BtnTest.OnAfterItemPressed += new EventVoidItem(BtnTest_OnAfterItemPressed);

                Mtx01 = Items.Add<Matrix>(ItensUIDs.Mtx01);
                Mtx01.OnAfterChooseFromList += new EventVoidChooseFromList(Mtx01_OnAfterChooseFromList);
                BtAddMt01 = Items.Add<Button>(ItensUIDs.BtAddMt01);
                BtAddMt01.Image = IconConstants.GetIconPath(IconConstants.Add);
                BtAddMt01.OnAfterItemPressed += new EventVoidItem(BtAddMt01_OnAfterItemPressed);
                BtDelMt01 = Items.Add<Button>(ItensUIDs.BtDelMt01);
                BtDelMt01.Image = IconConstants.GetIconPath(IconConstants.Delete);
                BtDelMt01.OnAfterItemPressed += new EventVoidItem(BtDelMt01_OnAfterItemPressed);

                oDataSource = this.DataSources.DBDataSources.Item(DataSourceUIDs.Tabela);
                oDataSourceMtx01 = this.DataSources.DBDataSources.Item(DataSourceUIDs.DataSourceMtx01.Tabela);
                Util.ComboBoxSetValoresValidosPorSQL(CbGrpUtil, "@AGRT_GRUTIL", "Code", "Name", "Name");                

                if (!CarregarConfiguracao())
                {
                    this.Mode = BoFormMode.fm_ADD_MODE;
                    oDataSource.SetValue(DataSourceUIDs.Codigo, 0, "1");
                    oDataSource.SetValue(DataSourceUIDs.Token, 0, "");
                    mForm.Items.Item("1").Click();
                    CarregarConfiguracao();                    
                }
                // Tem que ficar depois de carregar os dados, pois precisa do TOKEN para consultar os RTs
                CarregarComboResponsavelTecnico();
            }
            catch (Exception Ex)
            {
                Logger.Trace("AGRTFrmConfigRec_OnAfterFormLoad - Não foi possível carregar a tela de configurações. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível carregar a tela de configurações. " + Ex.Message);                
            }
            finally
            {
                GC.Collect();
                mForm.Freeze(false);
            }            
        }

        private void CkInfoNf_OnAfterItemPressed(ItemEvent aEvent)
        {
            try
            {
                if (!CkInfoNf.Checked)
                {
                    if (Util.ExibirDialogoConfirmacao("Deseja sincronizar as mensagens do receituário plataforma com as mensagens de produtos perigosos do SAP?"))
                    {
                        OpeReceituario.SincronizarInfProdPerigoso();
                        Util.ExibeMensagensDialogoStatusBar("Sincronização das informações para produtos perigosos finalizada.");
                    }
                }
            }
            catch (ApplicationException Ex)
            {
                Util.ExibeErrosTela("", Ex);
                CkInfoNf.Checked = true;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Erro ao clicar no Checkbox. CkInfoNf_OnAfterItemPressed", Ex);
                CkInfoNf.Checked = true;
            }
        }

        void Mtx01_OnAfterChooseFromList(ChooseFromListEvent aEvent)
        {            
            try
            {
                if (aEvent.SelectedObjects == null)
                    return;

                int iLinha = Mtx01.GetCellFocus().rowIndex - 1;
                if (iLinha < -1)
                    return;

                Mtx01.FlushToDataSource();
                for (int RowID = 0; RowID < aEvent.SelectedObjects.Rows.Count; RowID++)
                {
                    oDataSourceMtx01.SetValue(DataSourceUIDs.DataSourceMtx01.BPLId, iLinha + RowID, aEvent.SelectedObjects.GetValue("BPLId", RowID).ToString());
                    oDataSourceMtx01.SetValue(DataSourceUIDs.DataSourceMtx01.BPLName, iLinha + RowID, aEvent.SelectedObjects.GetValue("BPLName", RowID).ToString());
                    oDataSourceMtx01.InsertRecord(oDataSourceMtx01.Size);
                }
                Mtx01.LoadFromDataSourceEx(true);

                if (this.Mode != BoFormMode.fm_ADD_MODE)
                    this.Mode = BoFormMode.fm_UPDATE_MODE;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Erro ao selecionar Filial", Ex);
            }            
        }
        private void BtAddMt01_OnAfterItemPressed(ItemEvent aEvent)
        {            
            try
            {
                Mtx01.InserirLinha(oDataSourceMtx01);
                Mtx01.SetCellFocus(Mtx01.RowCount - 1, 1);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Erro ao adicionar linha!", Ex);
            }            
        }
        private void BtDelMt01_OnAfterItemPressed(ItemEvent aEvent)
        {
            if (Mtx01.RowCount <= 0)
                return;            
            try
            {
                int rowSelecionada = Mtx01.LinhaSelecionada();                
                if (rowSelecionada >= 0)
                    if (Util.ExibirDialogoConfirmacao("Deseja mesmo excluir a linha selecionada?"))
                        Mtx01.ExcluirLinhas(oDataSourceMtx01);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Erro ao excluir linha!", Ex);
            }            
        }        
        bool AGRTFrmConfigRec_OnBeforeFormDataUpdate(BusinessObjectInfo aEvent)
        {
            try
            {
                return ValidateInsertUpdate();
            }
            catch (Exception Ex)
            {
                Logger.Trace("AGRTFrmConfigRec_OnBeforeFormDataUpdate - Não foi possível atualizar os dados. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível atualizar os dados. " + Ex.Message);
                return false;
            }
            finally
            {
                GC.Collect();
            }            
        }

        bool AGRTFrmConfigRec_OnBeforeFormDataAdd(BusinessObjectInfo aEvent)
        {
            try
            {
                return ValidateInsertUpdate();
            }
            catch (Exception Ex)
            {
                Logger.Trace("AGRTFrmConfigRec_OnBeforeFormDataAdd - Não foi possível inserir os dados. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível inserir os dados. " + Ex.Message);
                return false;
            }
            finally
            {
                GC.Collect();
            }            
        }
        private bool ValidateInsertUpdate()
        {
            if (string.IsNullOrWhiteSpace(EdToken.Value))
            {
                Globals.Master.Connection.Interface.SetStatusBarMessage("Obrigatório Informar Token!");
                Util.ExibirDialogo("Obrigatório Informar Token!");
                return false;
            }            

            return true;
        }        
        private void InicioTelaConfiguracoes()
        {
            if (mForm.Mode == BoFormMode.fm_ADD_MODE)
            {
                if (String.IsNullOrEmpty(EdCode.Value))
                {
                    EdCode.Value = Agrotis.Framework.Data.Database.ReturnCode.Code(DataSourceUIDs.Tabela);
                }
            }
        }
        private void CarregarComboResponsavelTecnico()
        {
            if (string.IsNullOrEmpty(EdToken.Value))
                return;
            try
            {
                Column sboCol = Mtx01.Columns.Item(ItensUIDs.ColCodeAgronomo);
                while (sboCol.ValidValues.Count > 0)
                    sboCol.ValidValues.Remove(0, BoSearchKey.psk_Index);

                IEnumerable<ResponsavelTecnico> Responsaveis = OpeReceituario.ListarResponsaveisTecnicos();
                if (Responsaveis != null)
                {
                    foreach (ResponsavelTecnico Responsavel in Responsaveis)
                    {
                        if (Responsavel.ativo.Equals(true))
                            sboCol.ValidValues.Add(Responsavel.id.ToString(), Responsavel.nome.ToString());
                    }
                }
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Erro ao carregar a lista de Responsáveis Técnicos!", Ex);
            }                        
        }

        public EditText EdCode { get; set; }
        public EditText EdToken { get; set; }
        public EditText EdDtIntegr { get; set; }
        public ComboBox CbGrpUtil { get; set; }
        public ComboBox CbImpRect { get; set; }
        public CheckBox CkInfoNf { get; set; }
        public Button BtnOk { get; set; }
        public Button BtnTest { get; set; }
        public Matrix Mtx01 { get; set; }
        public Button BtAddMt01 { get; set; }
        public Button BtDelMt01 { get; set; }
        public DBDataSource oDataSource { get; set; }
        public DBDataSource oDataSourceMtx01 { get; set; }

        public static class ItensUIDs
        {
            public const string EdCode = "EdCode";
            public const string EdToken = "EdToken";
            public const string CbGrpUtil = "CbGrpUtil";
            public const string CbImpRect = "CbImpRect"; 
            public const string BtnOk = "1";
            public const string BtnTest = "BtnTest";
            public const string EdDtIntegr = "EdDtIntegr";
            public const string Mtx01 = "Mtx01";
            public const string ColBPLId = "V_0";
            public const string ColBPLName = "V_1";
            public const string ColCodeAgronomo = "V_2";
            public const string ColNomeAgronomo = "V_3";
            public const string BtAddMt01 = "BtAddMt01";
            public const string BtDelMt01 = "BtDelMt01";
            public const string CkInfoNf = "CkInfoNf";
        }
        public static class DataSourceUIDs
        {
            public const string Tabela = "@" + Tabelas.ConfiguracoesReceituario.Nome;
            public const string Codigo = "Code";
            public const string Nome = "Name";
            public static string Token = $"U_{Tabelas.ConfiguracoesReceituario.Campos.Token.Nome}";
            public static class DataSourceMtx01
            {
                public const string Tabela = "@" + Tabelas.ConfiguracaoPorFilial.Nome;
                public const string BPLId = "U_" + Tabelas.ConfiguracaoPorFilial.Campos.BPLId.Nome;
                public const string BPLName = "U_" + Tabelas.ConfiguracaoPorFilial.Campos.BPLName.Nome;
                public const string CodeAgronomo = "U_" + Tabelas.ConfiguracaoPorFilial.Campos.CodeAgronomo.Nome;
                public const string NomeAgronomo = "U_" + Tabelas.ConfiguracaoPorFilial.Campos.NomeAgronomo.Nome;
            }
        }        
    }
}
