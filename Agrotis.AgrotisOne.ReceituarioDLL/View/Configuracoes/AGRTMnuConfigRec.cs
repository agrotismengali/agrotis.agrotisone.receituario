﻿using System;
using System.IO;
using System.Reflection;
using Agrotis.Framework.Main.Interface.Controls.Menus.Attributes;
using Agrotis.Framework.Main.Interface.Controls.Menus.Base;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Configuracoes
{
    [MenuType("AGRTMnuConfigRec")]
    public class AGRTMnuConfigRec : Menu
    {
        public AGRTMnuConfigRec(Agrotis.Framework.Main.Application.AddOn aAddOn, string aMenuUID)
            :base (aAddOn, aMenuUID)
        {
            OnAfterClick += new Agrotis.Framework.Main.Interface.Events.EventVoidMenu(AGRTMnuConfigRec_OnAfterClick);
        }

        private void AGRTMnuConfigRec_OnAfterClick(SAPbouiCOM.MenuEvent aEvent)
        {
            using (StreamReader lForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(this.GetType().Namespace + ".AGRTFrmConfigRec.srf")))
            {
                string lFrmParametrizacoes = lForm.ReadToEnd();

                this.AddOn.Connection.Interface.LoadBatchActions(string.Format(lFrmParametrizacoes, new Random().Next().GetHashCode()));
            }
        }
    }
}
