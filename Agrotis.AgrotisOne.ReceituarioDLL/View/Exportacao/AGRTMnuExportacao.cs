﻿using System;
using System.IO;
using System.Reflection;
using Agrotis.Framework.Main.Interface.Controls.Menus.Attributes;
using Agrotis.Framework.Main.Interface.Controls.Menus.Base;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Exportacao
{
    [MenuType("AGRTMnuExportacao")]
    public class AGRTMnuExportacao : Menu
    {
        public AGRTMnuExportacao(Framework.Main.Application.AddOn aAddOn, string aMenuUID) : base (aAddOn, aMenuUID)
        {
            OnAfterClick += new Framework.Main.Interface.Events.EventVoidMenu(AGRTMnuExportacao_OnAfterClick);
        }

        private void AGRTMnuExportacao_OnAfterClick(SAPbouiCOM.MenuEvent aEvent)
        {
            using (StreamReader lForm = new StreamReader(
                Assembly.GetExecutingAssembly().GetManifestResourceStream(GetType().Namespace + ".AGRTFrmExportacao.srf")))
            {
                string lFrmParametrizacoes = lForm.ReadToEnd();
                AddOn.Connection.Interface.LoadBatchActions(
                    string.Format(lFrmParametrizacoes, new Random().Next().GetHashCode()));
            }
        }
    }
}
