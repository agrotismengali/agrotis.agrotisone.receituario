﻿using System;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using Agrotis.Framework.Main.Interface.Events;
using SAPbouiCOM;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using Form = Agrotis.Framework.Main.Interface.Controls.Forms.Base.Form;
using ComboBox = Agrotis.Framework.Main.Interface.Controls.Items.Specific.ComboBox;
using Matrix = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Matrix;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Agrotis.Framework.Data;
using System.Collections.Generic;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using Agrotis.AgrotisOne.ReceituarioDLL.Model;
using Agrotis.AgrotisOne.Core;
using System.Diagnostics;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Exportacao
{
    [FormType(FormConstants.AGRTFrmExportacao)]
    public class AGRTFrmExportacao : Form
    {
        ControllerAgenteExportacao CtrlExportacao = new ControllerAgenteExportacao();
                
        public AGRTFrmExportacao(AddOn aAddOn, string aFormUID) : base(aAddOn, aFormUID)
        {
            FormCreated = false;
            OnBeforeFormClose += AGRTFrmExportacao_OnBeforeFormClose;
            OnAfterFormLoad += new EventVoidItem(AGRTFrmExportacao_OnAfterFormLoad);            
        }
        
        public ComboBox CbFilial { get; set; }
        public ComboBox CbAgente { get; set; }    
        public EditText EdDataIni { get; set; }
        public EditText EdDataFim { get; set; }
        public Button BtGerar { get; set; }
        public Button BtPesquisar { get; set; }
        public Matrix MtxDados { get; set; }
        public DataTable DtDados { get; set; }        
       
        public static class ItensUIDs
        {
            public const string CbFilial = "CbFilial";
            public const string CbAgente = "CbAgente";
            public const string EdDataIni = "EdDataIni";
            public const string EdDataFim = "EdDataFim";
            public const string BtGerar = "BtGerar";
            public const string BtPesquisar = "BtPesq";
            public const string MtxDados = "MtxDados";
            public const string DtDados = "DtDados";
        }        
       
        private bool AGRTFrmExportacao_OnBeforeFormClose(ItemEvent aEvent)
        {
            return FormCreated;
        }

        private void AGRTFrmExportacao_OnAfterFormLoad(ItemEvent aEvent)
        {
            Util.FormFreeze(mForm, true);
            try
            {
                Util.FormDesabilitarMenusSAP(mForm);
                CbFilial = Items.Add<ComboBox>(ItensUIDs.CbFilial);
                CbFilial.DisplayDesc = true;
                CarregarComboFilial();

                CbAgente = Items.Add<ComboBox>(ItensUIDs.CbAgente);
                CbAgente.DisplayDesc = true;
                CarregarComboAgente();

                EdDataIni = Items.Add<EditText>(ItensUIDs.EdDataIni);
                EdDataFim = Items.Add<EditText>(ItensUIDs.EdDataFim);

                BtGerar = Items.Add<Button>(ItensUIDs.BtGerar);
                BtGerar.OnAfterClick += new EventVoidItem(BtGerar_OnAfterClick);

                BtPesquisar = Items.Add<Button>(ItensUIDs.BtPesquisar);
                BtPesquisar.OnAfterClick += new EventVoidItem(BtnPesquisar_OnAfterClick);

                DtDados = DataSources.DataTables.Item(ItensUIDs.DtDados);
                MtxDados = Items.Add<Matrix>(ItensUIDs.MtxDados);                                
                MtxDados.OnAfterChooseFromList += new EventVoidChooseFromList(MtxDados_OnAfterChooseFromList);

                MatrixSetBound();
                Pesquisar("0", "0", "", "");
                AddChooseFromListEmbalagem();                
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Ocorreu um erro ao carregar o formulário: ", Ex);                
            }
            finally
            {
                FormCreated = true;
                Util.FormFreeze(mForm, false);
            }
        }

        public void AddChooseFromListEmbalagem()
        {            
            ChooseFromListCreationParams oCFLCreationParams = ((ChooseFromListCreationParams)
                (Globals.Master.Connection.Interface.CreateObject(BoCreatableObjectType.cot_ChooseFromListCreationParams)));

            oCFLCreationParams.MultiSelection = false;
            oCFLCreationParams.ObjectType = UDOs.EmbalagemPadrao.UniqueID;
            oCFLCreationParams.UniqueID = "CflEmbPad";

            ChooseFromList oCFL = mForm.ChooseFromLists.Add(oCFLCreationParams);
            Column oColumn = MtxDados.Columns.Item(ControllerAgenteExportacao.MtxDadosColumns.Embalagem.ToString());            
            oColumn.ChooseFromListUID = "CflEmbPad";
            oColumn.ChooseFromListAlias = "U_AGRT_CodeEmbalagem";
        }

        public void MtxDados_OnAfterChooseFromList(ChooseFromListEvent aEvent)
        {
            Util.FormFreeze(mForm, true);
            try
            {
                if (aEvent.SelectedObjects == null)
                    return;
                switch (aEvent.ChooseFromListUID)
                {
                    case "CflEmbPad":
                        {
                            string Embalagem = aEvent.SelectedObjects.GetValue("U_AGRT_CodeEmbalagem", 0).ToString();
                            string UniMedida = aEvent.SelectedObjects.GetValue("U_AGRT_DescUniMed", 0).ToString();
                            string ItemCode = Util.MatrixGetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.ItemCode.ToString(), aEvent.Row);
                            string ProdMapa = Util.MatrixGetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.ProdMapa.ToString(), aEvent.Row);

                            Util.MatrixSetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.Embalagem.ToString(), aEvent.Row, Embalagem);
                            Util.MatrixSetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.UniMedida.ToString(), aEvent.Row, UniMedida);
                            CtrlExportacao.CriarRelacaoEmbalagemProdutoMapa(
                                CbAgente.Selected.Value, ItemCode, ProdMapa, Embalagem);
                            BtPesquisar.Click();
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível atribuir Valor no campo Embalagem.", Ex);                
            }
            finally
            {
                GC.Collect();
                Util.FormFreeze(mForm, false);
            }
        }

        private void ReplicarEmbalagemProdutosSimilares(int aRow, string aItemCode, string aProdMapa, string aEmbalagem, string aUniMedida)
        {            
            for (int iRow = 1; iRow < DtDados.Rows.Count+1; iRow++)
            {
                if (Util.MatrixGetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.ItemCode.ToString(), iRow).Equals(aItemCode) &&
                    Util.MatrixGetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.ProdMapa.ToString(), iRow).Equals(aProdMapa) &&
                    aRow != iRow)
                {
                    Util.MatrixSetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.Embalagem.ToString(), iRow, aEmbalagem);
                    Util.MatrixSetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.UniMedida.ToString(), iRow, aUniMedida);
                }
            }           
        }

        private bool ExisteProdSemEmbalagem()
        {
            for (int iRow = 1; iRow < DtDados.Rows.Count + 1; iRow++)
            {
                if (string.IsNullOrEmpty(Util.MatrixGetValorCelula(MtxDados, ControllerAgenteExportacao.MtxDadosColumns.Embalagem.ToString(), iRow)))
                    return true;
            }
            return false;
        }

        private void BtGerar_OnAfterClick(ItemEvent aEvent)
        {
            if (DtDados.Rows.Count <= 0)
            {
                Util.ExibirDialogo("Não foram identificadas movimentações de agrodefensivos no período informado.");
                return;
            }
            if (ExisteProdSemEmbalagem())
            {
                Util.ExibirDialogo("Atenção! Todos os produtos devem ter uma embalagem preenchida.");
                return;
            }
            Util.FormFreeze(mForm, true);
            try
            {
                string Cnpj = CtrlExportacao.RetirarCarcteresEspeciais(
                    DtDados.GetValue(ControllerAgenteExportacao.DtDadosColumns.FilialCnpj.ToString(), 0));
                string Arquivo = System.IO.Path.GetTempPath() + $"00{Cnpj}.csv";                
                if (CtrlExportacao.GerarRelatorioMovimentacao(Arquivo, DtDados))
                    Process.Start("notepad.exe", Arquivo);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível gerar o relatório de movimentação.", Ex);
            }
            finally
            {
                GC.Collect();
                Util.FormFreeze(mForm, false);
            }
        }

        private void BtnPesquisar_OnAfterClick(ItemEvent aEvent)
        {            
            if (string.IsNullOrEmpty(CbFilial.Selected?.Value))
            {
                Util.ExibirDialogo("É obrigatório informar uma Filial para gerar a movimentação.");
                return;
            }
            if (string.IsNullOrEmpty(CbAgente.Selected?.Value))
            {
                Util.ExibirDialogo("É obrigatório informar um Agente para gerar a movimentação.");
                return;
            }
            Util.ExibirMensagemStatusBar("Consultando movimentação de agrotóxicos...", BoMessageTime.bmt_Medium);
            Util.FormFreeze(mForm, true);
            try
            {
                Pesquisar(CbFilial.Selected.Value, CbAgente.Selected.Value, EdDataIni.Value, EdDataFim.Value);
                if (MtxDados.RowCount <= 0)
                    Util.ExibirMensagemStatusBar("Não há dados para os filtros selecionados!", BoMessageTime.bmt_Medium);                
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Erro ao realizar a pesquisa: ", Ex);
            }
            finally
            {
                Util.FormFreeze(mForm, false);
            }
        }

        private void MatrixSetBound()
        {
            Column oColumn = null;
            oColumn = MtxDados.Columns.Item("Col_-1");
            oColumn.Width = 0;

            for (int i = 0; i < DtDados.Columns.Count; i++)
            {
                oColumn = MtxDados.Columns.Item(DtDados.Columns.Item(i).Name);
                oColumn.DataBind.Bind(ItensUIDs.DtDados, DtDados.Columns.Item(i).Name);
                if (!Enum.IsDefined(typeof(ControllerAgenteExportacao.MtxDadosColumns), DtDados.Columns.Item(i).Name))
                    oColumn.Width = 0;
                if (!DtDados.Columns.Item(i).Name.Equals(ControllerAgenteExportacao.MtxDadosColumns.Embalagem.ToString()) &&
                    !DtDados.Columns.Item(i).Name.Equals(ControllerAgenteExportacao.MtxDadosColumns.UniMedida.ToString()))
                    oColumn.Editable = false;                
            }
        }
                
        private void Pesquisar(string aFilial, string aAgente, string aDataIni, string aDataFin)
        {                        
            List<Dictionary<string, object>> Dic = SqlUtils.DoQueryH(
                DaoExportacao.GetSQLMovimentacaoAgrotoxico(aFilial, aAgente, aDataIni, aDataFin));

            DtDados.Rows.Clear();
            DtDados.Rows.Add(Dic.Count);
            int i = 0;
            foreach (Dictionary<string, object> Elemento in Dic)
            {
                DtDados.SetValue("FilialCnpj", i, Elemento[("FilialCnpj").ToUpper()].ToString());
                DtDados.SetValue("NumeroNota", i, Elemento[("NumeroNota").ToUpper()]);
                DtDados.SetValue("SerieNota", i, Elemento[("SerieNota").ToUpper()].ToString());
                DtDados.SetValue("DataDoc", i, Elemento[("DataDocumento").ToUpper()]);
                DtDados.SetValue("PnRazaoSoc", i, Elemento[("PnRazaoSocial").ToUpper()].ToString());
                DtDados.SetValue("ProdCnpj", i, Elemento[("ProdCnpj").ToUpper()].ToString());
                DtDados.SetValue("ProdCpf", i, Elemento[("ProdCpf").ToUpper()].ToString());
                DtDados.SetValue("ProdRua", i, Elemento[("ProdRua").ToUpper()].ToString());
                DtDados.SetValue("ProdBairro", i, Elemento[("ProdBairro").ToUpper()].ToString());
                DtDados.SetValue("ProdComple", i, Elemento[("ProdComplemento").ToUpper()].ToString());
                DtDados.SetValue("ProdCidade", i, Elemento[("ProdCidade").ToUpper()].ToString());
                DtDados.SetValue("ProdCep", i, Elemento[("ProdCep").ToUpper()].ToString());
                DtDados.SetValue("ProdEstado", i, Elemento[("ProdEstado").ToUpper()].ToString());
                DtDados.SetValue("ProdNumero", i, Elemento[("ProdNumero").ToUpper()]);
                DtDados.SetValue("PropNome", i, Elemento[("PropriedadeNome").ToUpper()].ToString());
                DtDados.SetValue("PropIE", i, Elemento[("PropriedadeIE").ToUpper()].ToString());
                DtDados.SetValue("ProdIbge", i, Elemento[("ProdIbgeCode").ToUpper()]);
                DtDados.SetValue("FilialIbge", i, Elemento[("FilialIbgeCode").ToUpper()]);
                DtDados.SetValue("ItemCode", i, Elemento[("ItemCodeSap").ToUpper()].ToString());
                DtDados.SetValue("ProdMapa", i, Elemento[("ProdutoMapa").ToUpper()].ToString());
                DtDados.SetValue("DescMapa", i, Elemento[("DescProdutoMapa").ToUpper()].ToString());
                DtDados.SetValue("Quantidade", i, FormatValue.ToDoubleOrDefault(Elemento[("Quantidade").ToUpper()].ToString()));
                DtDados.SetValue("Embalagem", i, Elemento[("Embalagem").ToUpper()].ToString());
                DtDados.SetValue("UniMedida", i, Elemento[("UnidadeMedida").ToUpper()].ToString());
                DtDados.SetValue("DocEntry", i, Elemento[("DocEntry").ToUpper()].ToString());
                DtDados.SetValue("TipoMov", i, Elemento[("TipoMovimentacao").ToUpper()].ToString());
                i++;
            }
                                   
            MtxDados.Clear();
            MtxDados.LoadFromDataSource();
            MtxDados.AutoResizeColumns();
            mForm.Mode = BoFormMode.fm_OK_MODE;
        }

        private void CarregarComboFilial()
        {            
            Util.ComboBoxSetValoresValidosPorSQL(CbFilial, DaoExportacao.GetSQLFilial());            
        }
        private void CarregarComboAgente()
        {
            Util.ComboBoxSetValoresValidosPorSQL(CbAgente, DaoExportacao.GetSQLAgente());            
        }        

    }
}