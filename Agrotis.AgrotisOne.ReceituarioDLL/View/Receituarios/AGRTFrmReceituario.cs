﻿using SAPbouiCOM;
using Agrotis.AgrotisOne.ReceituarioDLL.Model;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using Form = Agrotis.Framework.Main.Interface.Controls.Forms.Base.Form;
using Matrix = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Matrix;
using ComboBox = Agrotis.Framework.Main.Interface.Controls.Items.Specific.ComboBox;
using Folder = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Folder;
using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.Framework.Main.Interface.Events;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using System.IO;
using System.Reflection;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.Core.Constantes;
using System.Collections.Generic;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons;
using System;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Agrotis.Framework.Data.UDO;
using Agrotis.Framework.Data;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using Agrotis.Framework.Commons;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Receituarios
{
    [FormType("AGRTFrmReceituario")]
    public class AGRTFrmReceituario : Form
    {        
        DTOReceituario dto;
        OperacoesReceituario OpeReceituario = new OperacoesReceituario();
        public AGRTFrmReceituario(AddOn aAddOn, string aFormUID)
            : base(aAddOn, aFormUID)
        {
            mForm.Freeze(true);
            try
            {
                mForm.EnableMenu(SAPMenuConstants.SAPAdicionar, false);
                mForm.EnableMenu(SAPMenuConstants.SAPRemover, false);
                mForm.EnableMenu(SAPMenuConstants.SAPProcurar, false);
                mForm.EnableMenu(SAPMenuConstants.SAPNavigatorFirst, false);
                mForm.EnableMenu(SAPMenuConstants.SAPNavigatorPrior, false);
                mForm.EnableMenu(SAPMenuConstants.SAPNavigatorNext, false);
                mForm.EnableMenu(SAPMenuConstants.SAPNavigatorLast, false);
                mForm.EnableMenu(SAPMenuConstants.SAPLinhaMatrizInserir, false);
                mForm.EnableMenu(SAPMenuConstants.SAPLinhaMatrizExcluir, false);
                // Leitura dos parâmetros passados no Ticket
                string[] FormParameters = Globals.Master.FormTicket?.GetFormParam(FormConstants.AGRTFrmReceituario);
                // Conversão para o DTO
                dto = new DTOReceituario(FormParameters);
                DtResponsavel = DataSources.DataTables.Item(ItensUIDs.DtResponsavel);
                DtCultura = DataSources.DataTables.Item(ItensUIDs.DtCultura);

                EdCode = Items.Add<EditText>(ItensUIDs.EdCode);
                MtAgrup = Items.Add<Matrix>(ItensUIDs.MtAgrup);
                //MtAgrup.AutoResizeColumns();

                oPane1 = Items.Add<Folder>(ItensUIDs.oPane1);
                oPane1.OnAfterItemPressed += new EventVoidItem(oPane1_OnAfterItemPressed);
                oPane2 = Items.Add<Folder>(ItensUIDs.oPane2);
                oPane2.OnAfterItemPressed += new EventVoidItem(oPane2_OnAfterItemPressed);

                BtnRct = Items.Add<Button>(ItensUIDs.BtnRct);
                BtnRct.Image = IconConstants.GetIconPath(IconConstants.Replace);
                BtnRct.OnAfterClick += new EventVoidItem(BtnRct_OnAfterClick);

                BtnDel = Items.Add<Button>(ItensUIDs.BtnDel);
                BtnDel.Image = IconConstants.GetIconPath(IconConstants.Delete);
                BtnDel.OnAfterClick += new EventVoidItem(BtnDel_OnAfterClick);

                BtnNovo = Items.Add<Button>(ItensUIDs.BtnAdd);
                BtnNovo.Image = IconConstants.GetIconPath(IconConstants.New);
                BtnNovo.OnAfterClick += new EventVoidItem(BtnNovo_OnAfterClick);

                BtnOk = Items.Add<Button>(ItensUIDs.BtnOk);
                BtnOk.OnBeforeClick += new EventBoolItem(BtnOk_OnBeforeClick);

                BtnCanc = Items.Add<Button>(ItensUIDs.BtnCanc);
                BtnCanc.OnAfterClick += new EventVoidItem(BtnCanc_OnAfterClick);

                BtnReimp = Items.Add<Button>(ItensUIDs.BtnReimp);
                BtnReimp.OnAfterClick += new EventVoidItem(BtnReimp_OnAfterClick);

                CbRespTec = Items.Add<ComboBox>(ItensUIDs.CbRespTec);

                oDataSource = mForm.DataSources.DBDataSources.Item(DataSourceUIDs.Tabela);
                oDataSourceMtAgrup = mForm.DataSources.DBDataSources.Item(DataSourceUIDs.DataSourceMtAgrup.Tabela);
                OnAfterFormLoad += new EventVoidItem(AGRTFrmReceituario_OnAfterFormLoad);

                OnAfterFormActivate += AGRTFrmReceituario_OnAfterFormActivate;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro ao abrir Form - AGRTFrmReceituario", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }                     
        }
        private void BtnNovo_OnAfterClick(ItemEvent aEvent)
        {            
            try
            {
                int RowID = MtAgrup.GetNextSelectedRow();
                if ((mForm.Mode != BoFormMode.fm_OK_MODE) && (mForm.Mode != BoFormMode.fm_VIEW_MODE))
                    BtnOk.Click();

                if (CbRespTec.Value == null || string.IsNullOrEmpty(CbRespTec.Value))
                {
                    Util.ExibirDialogo("Selecione o Responsável Técnico.");
                    return;
                }

                if (!ValidarVinculoReceita(RowID))
                    return;

                if (string.IsNullOrEmpty(SqlUtils.GetValue($@"SELECT T0.""U_AGRT_IdFitoCult"" FROM ""@AGRT_CULTURA""  T0 WHERE T0.""Code"" = '{Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColItensCodCult, RowID)}'")))
                {
                    Util.ExibirMensagemStatusBar("Não há IdFitoCult cadastrado para a cultura selecionada!", BoMessageTime.bmt_Medium, true);
                    Util.ExibirDialogo("Não há IdFitoCult cadastrado para a cultura selecionada!");
                    return;
                }

                DTOCriarReceita DtoCriarReceita = new DTOCriarReceita();
                DtoCriarReceita.CodigoReceituarioB1 = EdCode.Value;
                DtoCriarReceita.CodigoCultura = Convert.ToInt32(SqlUtils.GetValue($@"SELECT T0.""U_AGRT_IdFitoCult"" FROM ""@AGRT_CULTURA""  T0 WHERE T0.""Code"" = '{Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColItensCodCult, RowID)}'")); //Sqlu Convert.ToInt32();
                DtoCriarReceita.CodCultAgrotis = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupCodCult, RowID);
                DtoCriarReceita.NomeCultura = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColItensNomeCul, RowID);
                DtoCriarReceita.SiglaUF = dto.SiglaUF;
                DtoCriarReceita.CodMapaProduto = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColItensRegMapa, RowID);
                DtoCriarReceita.NomeProduto = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColItensNomeRegMapa, RowID);
                DtoCriarReceita.LineId = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColLineId, RowID);
                DtoCriarReceita.SequenceId = dto.SequenceId;
                DtoCriarReceita.IdProdutor = OpeReceituario.GetIDProdutorRuralByUUID(dto.UUIDProdutor).ToString();
                if (DtoCriarReceita.IdProdutor.Equals("0"))
                {
                    Util.ExibirDialogo($@"Não foi encontrado Produtor Rural cadastrado com o ID [{dto.UUIDProdutor}] no Agrotis Plataforma.");
                    return;
                }
                DtoCriarReceita.IdFazenda = OpeReceituario.GetIDPropriedadeRuralByUUID(dto.UUIDPropriedade);
                if (DtoCriarReceita.IdFazenda <= 0)
                {
                    Util.ExibirDialogo($@"Não foi encontrada Propriedade Rural cadastrada com o ID [{dto.UUIDPropriedade}] no Agrotis Plataforma.");
                    return;
                }
                DtoCriarReceita.IdRascunho = oDataSource.GetValue($"U_{Tabelas.Receituario.Campos.IdRascunho.Nome}", 0).ToString();
                DtoCriarReceita.IdRT = Convert.ToInt32(CbRespTec.Value);
                DtoCriarReceita.Quantidade = FormatValue.ToDoubleFromSAP(Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupQtd, RowID));
                DtoCriarReceita.IdEmpresa = OpeReceituario.GetIdEmpresa(dto.CNPJEmpresa);
                if (DtoCriarReceita.IdEmpresa <= 0)
                {
                    Util.ExibirDialogo($@"Não foi encontrada Empresa cadastrada com o CNPJ [{dto.CNPJEmpresa}] no Agrotis Plataforma.");
                    return;
                }

                StreamReader lForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(
                   $@"{this.GetType().Namespace}.CriarReceita.AGRTFrmCriarReceita.srf"));
                Util.FormSetAbrirTicket(UniqueID,
                    FormConstants.AGRTFrmReceituario,
                    lForm,
                    FormConstants.AGRTFrmCriarReceita,
                    DtoCriarReceita.GetDataRecord()
                   );
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnNovo_OnAfterClick - Não foi possível criar nova receita. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível criar nova receita." + Ex.Message);                
            }
            finally
            {
                GC.Collect();
            }
        }
        private void BtnCanc_OnAfterClick(ItemEvent aEvent)
        {
            try
            {
                if (!ReceitasConsumidasOuFinalizadas())
                {
                    if (Util.ExibirDialogoConfirmacao($@"Esta nota fiscal possui um ou mais itens " +
                        "que precisam de receita agronômica. Deseja cancelar assim mesmo?"))
                    {
                        Close();
                    }
                }
                else
                    Close();
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnCanc_OnAfterClick - Não foi possível cancelar a operação. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível cancelar a operação." + Ex.Message);                
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void BtnReimp_OnAfterClick(ItemEvent aEvent)
        {
            try
            {
                if (ReceitasFinalizadas())
                {
                    ImpressaoRelatorios oImpressaoRel = new ImpressaoRelatorios(dto.CodeReceituario, dto.CodFilial);
                    oImpressaoRel.Executar();
                }
                else
                    Util.ExibirDialogo("Para reimprimir os documentos, todas as receitas devem estar finalizadas.");
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnReimp_OnAfterClick - Não foi possível reimprimir. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível reimprimir." + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void AGRTFrmReceituario_OnAfterFormActivate(ItemEvent aEvent)
        {
            try
            {
                if ((mForm.Mode == BoFormMode.fm_OK_MODE) || (mForm.Mode == BoFormMode.fm_VIEW_MODE))
                    if (Globals.Master.Connection.Interface.Menus.Item(SAPMenuConstants.SAPAtualizar).Enabled)
                        Globals.Master.Connection.Interface.ActivateMenuItem(SAPMenuConstants.SAPAtualizar);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método BtnCancelar_OnAfterClick - AGRTFrmCriarReceita", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }
        private bool ValidarVinculoReceita(int aLinha)
        {            
            if ((mForm.Mode != BoFormMode.fm_OK_MODE) && (mForm.Mode != BoFormMode.fm_VIEW_MODE))
            {
                Util.ExibirDialogo("Grave as alterações antes de vincular uma receita.");
                return false;
            }            
            if (aLinha < 0)
            {
                Util.ExibirDialogo($"Selecione um dos itens para realizar a consulta.");
                return false;
            }
            string sIdReceita = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupIdRec, aLinha);
            int iStatus = int.Parse(Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupSituacao, aLinha));
            if (!iStatus.Equals((int)StatusReceita.NAODEFINIDA))
            {
                Util.ExibirDialogo($"Apenas relações com situação 'Não definida' podem ser alteradas.");
                return false;
            }
            if ((!string.IsNullOrEmpty(sIdReceita)) && (!sIdReceita.Equals("0")))
            {
                Util.ExibirDialogo($"Esta relação já está vinculada a uma receita. Caso queira alterar, desvincule a receita atual.");
                return false;
            }
            return true;
        }
        private void BtnRct_OnAfterClick(ItemEvent aEvent)
        {
            try
            {
                int iLinha = MtAgrup.GetNextSelectedRow();
                if ((mForm.Mode != BoFormMode.fm_OK_MODE) && (mForm.Mode != BoFormMode.fm_VIEW_MODE))
                    BtnOk.Click();
                if (ValidarVinculoReceita(iLinha))
                {
                    dto.CodCultura = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupCodCult, iLinha);
                    dto.RegistroMapa = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupRegMapa, iLinha);
                    dto.Quantidade = FormatValue.ToUserEditValueFromDouble(FormatValue.ToDoubleFromSAP(
                        Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupQtd, iLinha)));

                    StreamReader lForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(
                        $@"Agrotis.AgrotisOne.ReceituarioDLL.View.Receituarios.AGRTFrmConsultaReceita.srf"));
                    Util.FormSetAbrirTicket(UniqueID, FormConstants.AGRTFrmReceituario, lForm, FormConstants.AGRTFrmConsultaReceita,
                        dto.GetDataRecord());
                }
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnRct_OnAfterClick - Não foi possível abrir a tela de consumo de receita. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível abrir a tela de consumo de receita. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void BtnDel_OnAfterClick(ItemEvent aEvent)
        {
            try
            {
                if ((mForm.Mode != BoFormMode.fm_OK_MODE) && (mForm.Mode != BoFormMode.fm_VIEW_MODE))
                {
                    Util.ExibirDialogo("Grave as alterações antes de desvincular uma receita.");
                    return;
                }
                int iLinha = MtAgrup.GetNextSelectedRow();
                if (iLinha < 0)
                {
                    Util.ExibirDialogo($"Selecione um dos itens da Grid.");
                    return;
                }
                StatusReceita oStatus = (StatusReceita)int.Parse(Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupSituacao, iLinha));
                string sIdReceita = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupIdRec, iLinha);
                string sRegistroMapa = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupRegMapa, iLinha);
                string sIdItemReceita = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupIdItemRec, iLinha);
                string sIdRascunho = oDataSource.GetValue($"U_{Tabelas.Receituario.Campos.IdRascunho.Nome}", 0).ToString();

                bool bDesvinculou = false;
                switch (oStatus)
                {
                    case (StatusReceita.CONSUMIDA):
                        {
                            bDesvinculou = OpeReceituario.DesvincularPedido(dto.CodeReceituario, dto.SequenceId, sIdReceita);
                            break;
                        }
                    case (StatusReceita.RASCUNHO):
                        {
                            bDesvinculou = OpeReceituario.DesvincularItemRascunho(dto.CodeReceituario, sIdRascunho, sIdItemReceita);
                            break;
                        }
                }
                if (bDesvinculou)
                    AGRTFrmReceituario_OnAfterFormActivate(aEvent);
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnDel_OnAfterClick - Não foi possível excluir vínculo com receita. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível excluir vínculo com receita. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }                               
        }
        private bool EfetivarReceita()
        {
            string NumeroRascunho = oDataSource.GetValue($"U_{Tabelas.Receituario.Campos.IdRascunho.Nome}", 0).ToString();            

            if (NumeroRascunho != "0" && !String.IsNullOrEmpty(NumeroRascunho))
            {
                IEnumerable<ReceitaRetorno> RecRetorno = OpeReceituario.EfetivarReceita(NumeroRascunho);

                if (RecRetorno == null)
                {
                    Util.ExibirMensagemStatusBar("Erro ao Efetivar Receita!", BoMessageTime.bmt_Medium, true);
                    Util.ExibirDialogo("Erro ao Efetivar Receita!");
                    return false;
                }
                else
                {
                    DTOMasterData DtoMasterDataReceituario = new DTOMasterData()
                    {
                        UDO = "AGRT_RECEIT",
                        DataFields = new List<Tuple<string, string, Type>>()
                                    {
                                        new Tuple<string, string, Type>("Code", oDataSource.GetValue("Code", 0).ToString(), typeof(string))
                                    },
                        DtoMasterDataLines = new List<DTOMasterDataLine>()
                    };

                    DTOMasterDataLine DtoMasterDataLine = new DTOMasterDataLine()
                    {
                        LineTable = "AGRT_RECEITAGRUP",
                        DataLineFields = new List<Tuple<string, Type>>()
                                        {
                                            new Tuple<string, Type>($"LineId", typeof(int)),
                                            new Tuple<string, Type>($"U_AGRT_IDReceita", typeof(string)),
                                            new Tuple<string, Type>($"U_AGRT_Situacao", typeof(string)),
                                            new Tuple<string, Type>($"U_AGRT_IDItemReceita", typeof(string)),
                                            new Tuple<string, Type>($"U_AGRT_NumReceita", typeof(string))
                                        }
                    };
                    DtoMasterDataLine.DataLines = new List<Dictionary<string, object>>();
                    foreach (var Receita in RecRetorno)
                    {
                        string IdReceita = Receita.detalhe.id.ToString();
                        foreach (var Item in Receita.itens)
                        {
                            if (OpeReceituario.VincularPedido(dto.SequenceId, FormatValue.ToUserEditValueFromDouble(Item.quantidade), Item.idMapaProduto, IdReceita, Item.idItem.ToString()) == "CONSUMIDA")
                            {
                                Util.MatrixSetValorCelula(MtAgrup, ItensUIDs.ColAgrupSituacao, Item.idPedidoItem, (Int32)StatusReceita.CONSUMIDA);

                                Dictionary<string, object> dic = new Dictionary<string, object>();
                                dic.Add("LineId", Item.idPedidoItem);
                                dic.Add("U_AGRT_IDReceita", IdReceita);
                                dic.Add("U_AGRT_Situacao", ((Int32)StatusReceita.CONSUMIDA).ToString());
                                dic.Add("U_AGRT_IDItemReceita", Item.idItem.ToString());
                                dic.Add("U_AGRT_NumReceita", Receita.numeroReceita.ToString());
                                DtoMasterDataLine.DataLines.Add(dic);
                            }
                        }
                    }
                    DtoMasterDataReceituario.DtoMasterDataLines.Add(DtoMasterDataLine);

                    string oError = string.Empty;
                    if (string.IsNullOrEmpty(UtilUDO.UDOPersistirDados(DtoMasterDataReceituario, out oError)))
                        throw new Exception($"Erro ao atualizar agrupamento Receituário {oError}");
                }
            }
            return true;
        }
        private bool ReceitasProntasParaConsumir()
        {
            mForm.Freeze(true);
            try
            {
                for (int RowID = 1; RowID <= MtAgrup.VisualRowCount - 1; RowID++)
                {
                    string sIdReceita = Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupIdRec, RowID);
                    if (string.IsNullOrEmpty(sIdReceita))
                        return false;
                }
            }
            finally
            {
                mForm.Freeze(false);
            }
            return true;
        }
        private bool ReceitasConsumidasOuFinalizadas()
        {
            if (SqlUtils.SqlCount($@"SELECT 
                                            1
                                        FROM 
                                            ""@AGRT_RECEITAGRUP"" 
                                        WHERE 
                                            ""U_AGRT_Situacao"" <> '{((int)StatusReceita.FINALIZADA).ToString()}'  AND
                                            ""U_AGRT_Situacao"" <> '{((int)StatusReceita.CONSUMIDA).ToString()}'  AND
                                            ""U_AGRT_Situacao"" <> '{((int)StatusReceita.RASCUNHO).ToString()}'  AND
                                            ""Code"" = '{EdCode.Value}'") > 0)
                return false;
            else
                return true;
        }        
        private bool ReceitasFinalizadas()
        {
            mForm.Freeze(true);
            try
            {
                for (int RowID = 1; RowID <= MtAgrup.VisualRowCount - 1; RowID++)                    
                {
                    int iStatus = int.Parse(Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupSituacao, RowID));
                    if (!iStatus.Equals((int)StatusReceita.FINALIZADA))
                        return false;
                }
            }
            finally
            {
                mForm.Freeze(false);
            }
            return true;
        }
        private bool ReceitasConsumidasOuRascunho()
        {
            mForm.Freeze(true);
            try
            {
                for (int RowID = 1; RowID <= MtAgrup.VisualRowCount - 1; RowID++)                    
                {                    
                    int iStatus = int.Parse(Util.MatrixGetValorCelula(MtAgrup, ItensUIDs.ColAgrupSituacao, RowID));
                    if ((!iStatus.Equals((int)StatusReceita.CONSUMIDA)) && (!iStatus.Equals((int)StatusReceita.RASCUNHO)))
                        return false;
                }
            }
            finally
            {
                mForm.Freeze(false);
            }
            return true;
        }
        private bool BtnOk_OnBeforeClick(ItemEvent aEvent)
        {
            try
            {
                if (mForm.Mode == BoFormMode.fm_OK_MODE)
                {
                    if (!ReceitasConsumidasOuFinalizadas())
                    {
                        Util.ExibirDialogo($@"Todas as receitas devem estar com o status ""Consumida"", ""Finalizada"", ou ""Rascunho"".");
                        return false;
                    }
                    if (OperacoesReceituario.VerificaReceitaRascunho(EdCode.Value))
                    {
                        if (!EfetivarReceita())
                            return false;
                    }                    
                }
                return true;
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnOk_OnBeforeClick - Não foi possível prosseguir com a operação. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível prosseguir com a operação." + Ex.Message);
                return false;
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void AGRTFrmReceituario_OnAfterFormLoad(ItemEvent aEvent)
        {
            mForm.Freeze(true);
            try
            {
                mForm.PaneLevel = 1;
                mForm.Mode = BoFormMode.fm_FIND_MODE;

                EdCode.Click();
                oDataSource.SetValue(DataSourceUIDs.Codigo, 0, dto.CodeReceituario);
                mForm.Items.Item("1").Click();
                Util.CampoSetFocus(mForm, CbRespTec);
                EdCode.Enabled = false;
                IEnumerable<ResponsavelTecnico> Responsaveis = OpeReceituario.ListarResponsaveisTecnicos();
                if (Responsaveis != null)
                    CarregarComboResponsavelTecnico(Responsaveis);
                if (string.IsNullOrEmpty(CbRespTec.Value))
                {
                    string CodResp = OpeReceituario.GetResponsavelTecnicoPadrao(dto.CodFilial);
                    if (!string.IsNullOrEmpty(CodResp))
                    {
                        Util.ComboBoxForceSetValue(CbRespTec, CodResp);
                        mForm.Mode = BoFormMode.fm_UPDATE_MODE;
                    }
                }
            }
            catch (Exception Ex)
            {
                Logger.Trace("AGRTFrmReceituario_OnAfterFormLoad - Não foi possível carregar a tela do receituário. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível carregar a tela do receituário. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
                mForm.Freeze(false);
            }                      
        }
        private void CarregarComboResponsavelTecnico(IEnumerable<ResponsavelTecnico> aResponsaveis)
        {            
            string sCodeAgron = oDataSource.GetValue(DataSourceUIDs.CodeAgron, 0);
            if ((string.IsNullOrEmpty(sCodeAgron)) || (sCodeAgron.Equals("0")))
            {
                CbRespTec.Value = "";
            }
            foreach (ResponsavelTecnico Responsavel in aResponsaveis)
            {
                if (Responsavel.ativo.Equals(true))
                    CbRespTec.ValidValues.Add(Responsavel.id.ToString(), Responsavel.nome.ToString());                
            }            
        }                    
        void oPane2_OnAfterItemPressed(ItemEvent aEvent)
        {
            try
            {
                switch (aEvent.ItemUID)
                {
                    case "Pane2":
                        PaneLevel = 2;
                        break;
                }
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método oPane2_OnAfterItemPressed - AGRTFrmReceituario", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }
        private void oPane1_OnAfterItemPressed(ItemEvent aEvent)
        {
            try
            {
                switch (aEvent.ItemUID)
                {
                    case "Pane1":
                        PaneLevel = 1;
                        break;
                }
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método oPane2_OnAfterItemPressed - AGRTFrmReceituario", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }        
        #region ItensUIDs
        public static class ItensUIDs
        {
            public const string BtnCanc = "btnCanc";
            public const string BtnReimp = "BtnReimp";
            public const string EdCode = "EdCode";            
            public const string BtnRct = "BtnRct";
            public const string BtnDel = "BtnDel";
            public const string BtnAdd = "BtnAdd";
            public const string MtAgrup = "MtAgrup";
            public const string oPane1 = "Pane1";
            public const string oPane2 = "Pane2";
            public const string BtnOk = "1";           
            // Matrix fieds
            public const string ColLineId = "ColLineId";
            public const string ColItensCode = "ColItem";
            public const string ColItensCodCult = "ColCodCult";
            public const string ColItensRegMapa = "ColRegMapa";
            public const string ColItensNomeRegMapa = "ColNomMapa";
            public const string ColItensNomeCul = "ColNomeCul";
            public const string ColItensQtd = "ColQtd";            
            
            public const string ColAgrupSituacao = "ColSituac";
            public const string ColAgrupCodCult = "ColCodCult";
            public const string ColAgrupRegMapa = "ColRegMapa";
            public const string ColAgrupNomeCul = "ColNomeCul";
            public const string ColAgrupQtd = "ColQtd";
            public const string ColAgrupNumRec = "ColNumRec";
            public const string ColAgrupIdRec = "ColIdRec";
            public const string ColAgrupStsEmit = "ColStsEmit";
            public const string ColAgrupIdItemRec = "ColIdItRct";

            public const string CbRespTec = "CbRespTec";
            public const string DtResponsavel = "DtResponsavel";
            public const string DtCultura = "DtCultura";
        }
        #endregion

        #region Propriedades
        public EditText EdCode { get; set; }
        public EditText EdCodigoAgronomo { get; set; }
        public Button BtnNovo { get; set; }
        public Button BtnRct { get; set; }
        public Button BtnDel { get; set; }
        public Button BtnOk { get; set; }
        public Button BtnCanc { get; set; }
        public Button BtnReimp { get; set; }
        public ComboBox CbRespTec { get; set; }
        public Matrix MtAgrup { get; set; }
        public Folder oPane1 { get; set; }
        public Folder oPane2 { get; set; }
        public DBDataSource oDataSource { get; set; }
        public DBDataSource oDataSourceMtAgrup { get; set; }
        public DataTable DtResponsavel { get; set; }
        public DataTable DtCultura { get; set; }

        #endregion

        #region DataSourceUIDs
        public static class DataSourceUIDs
        {
            public const string Tabela = "@" + Tabelas.Receituario.Nome;
            public const string Codigo = "Code";
            public const string CodeAgron = "U_AGRT_CodeAgron";
            public const string Nome = "Name";

            public static class DataSourceMtAgrup
            {
                public const string Tabela = "@" + Tabelas.ReceituarioLinhasAgrup.Nome;
                public const string Codigo = "Code";
                public const string LineNum = "LineId";                
            }
            public static class DataSourceMtItens
            {
                public const string Tabela = "@" + Tabelas.ReceituarioLinhas.Nome;
                public const string Codigo = "Code";
                public const string LineNum = "LineId";
            }
        }
        #endregion
    }
}
