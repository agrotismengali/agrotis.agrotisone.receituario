﻿using Agrotis.Framework.Main.Interface.Controls.Forms.Base;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using System.Collections.Generic;
using System;
using Agrotis.AgrotisOne.Core.Constantes;
using Agrotis.Framework.Main.Interface.Controls.Items.Specific;
using Agrotis.Framework.Main.Interface.Events;
using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.Core.Utils;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;

namespace Agrotis.AgrotisOne.ReceituarioDLLs.View.Receituarios
{
    [FormType("AGRTFrmConsultaReceita")]
    public partial class AGRTFrmConsultaReceita : Form
    {
        DTOReceituario dto;
        OperacoesReceituario OpeReceituario = new OperacoesReceituario();
        public AGRTFrmConsultaReceita(AddOn aAddOn, string aFormUID)
            : base(aAddOn, aFormUID)
        {
            mForm.EnableMenu(SAPMenuConstants.SAPProcurar, false);
            mForm.EnableMenu(SAPMenuConstants.SAPAdicionar, false);
            mForm.EnableMenu(SAPMenuConstants.SAPRemover, false);
            mForm.EnableMenu(SAPMenuConstants.SAPNavigatorFirst, false);
            mForm.EnableMenu(SAPMenuConstants.SAPNavigatorPrior, false);
            mForm.EnableMenu(SAPMenuConstants.SAPNavigatorNext, false);
            mForm.EnableMenu(SAPMenuConstants.SAPNavigatorLast, false);
            mForm.EnableMenu(SAPMenuConstants.SAPAtualizar, false);
            mForm.EnableMenu(SAPMenuConstants.SAPFiltrar, false);

            string[] FormParameters = Globals.Master.FormTicket?.GetFormParam(FormConstants.AGRTFrmConsultaReceita);
            dto = new DTOReceituario(FormParameters);

            GrReceitas = Items.Add<Grid>(ItensUIDs.GrReceitas);            
            GrReceitas.OnAfterDoubleClick += new EventVoidItem(GrReceitas_OnAfterDoubleClick);
            GrReceitas.AutoResizeColumns();

            DtIni = Items.Add<EditText>(ItensUIDs.DtIni);
            DtIni.Value = DateTime.Today.AddDays(-60).ToString("yyyyMMdd");            
            DtFin = Items.Add<EditText>(ItensUIDs.DtFin);
            DtFin.Enabled = false;
            DtFin.Value = dto.DataDoc;             

            BtnFiltrar = Items.Add<Button>(ItensUIDs.BtnFiltrar);
            BtnFiltrar.OnAfterClick += new EventVoidItem(BtnFiltrar_OnAfterClick);

            BtnOk = Items.Add<Button>(ItensUIDs.BtnOk);
            BtnOk.OnAfterClick += new EventVoidItem(BtnOk_OnAfterClick);

            DtReceita = DataSources.DataTables.Item(ItensUIDs.DtReceita);
            DtRetornoCFL = DataSources.DataTables.Item(ItensUIDs.DtRetornoCFL);

            BtnFiltrar_OnAfterClick(null);
        }
        private void BtnFiltrar_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {
            if ((DtIni.Value.Length != 8) || (DtFin.Value.Length != 8)) {
                Util.ExibirDialogo("Data inicial, e/ou final inválida(s).");
                return;
            }
            string sParamDtIni = FormatValue.ToDate(DtIni.Value).ToString("yyyy-MM-dd");
            string sParamDtFin = FormatValue.ToDate(DtFin.Value).ToString("yyyy-MM-dd");
            string sCodCultReceituario = OperacoesReceituario.GetCodCulturaReceituario(dto.CodCultura);
            if (string.IsNullOrEmpty(sCodCultReceituario))
            {
                Util.ExibirDialogo($"Cultura {dto.CodCultura} não encontrada no receituário agronômico.");
                return;
            }
            int iIDProdutor = OpeReceituario.GetIDProdutorRuralByUUID(dto.UUIDProdutor);
            if (iIDProdutor <= 0)
            {
                Util.ExibirDialogo("Produtor Rural não encontrado no Agrotis Plataforma.");
                return;
            }
            int iIDPropriedade = OpeReceituario.GetIDPropriedadeRuralByUUID(dto.UUIDPropriedade);
            if (iIDPropriedade <= 0)
            {
                Util.ExibirDialogo("Propriedade Rural não encontrada no Agrotis Plataforma.");
                return;
            }
            IEnumerable<ReceitaConsulta> Receitas = OpeReceituario.ListarReceitas(sParamDtIni, sParamDtFin,
                iIDProdutor.ToString(), iIDPropriedade.ToString(), dto.RegistroMapa, sCodCultReceituario);
            if (Receitas != null)
                CarregarGrid(Receitas);            
        }
        private void BtnOk_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {
            if (GrReceitas.Rows.Count > 0)
            {
                if (GrReceitas.Rows.SelectedRows.Count <= 0)
                {
                    Util.ExibirDialogo("Atenção! Selecione uma receita na grid superior.");
                    return;
                }
                else
                    GrReceitas_OnAfterDoubleClick(aEvent);
            }
            else
                mForm.Close();
        }
        private void GrReceitas_OnAfterDoubleClick(SAPbouiCOM.ItemEvent aEvent)
        {            
            if (GrReceitas.Rows.SelectedRows.Count > 0)
            {
                int iRowSelecionada = GrReceitas.Rows.SelectedRows.Item(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder);
                dto.IdReceita = Convert.ToString(GrReceitas.DataTable.GetValue("ClIdReceita", iRowSelecionada));                
                dto.NumReceita = Convert.ToString(GrReceitas.DataTable.GetValue("ClNumRct", iRowSelecionada));
                string sIdItemRct = Convert.ToString(GrReceitas.DataTable.GetValue("ClIdItRct", iRowSelecionada));

                string sResult = OpeReceituario.VincularPedido(dto.SequenceId, dto.Quantidade, dto.RegistroMapa, dto.IdReceita, sIdItemRct);
                if (sResult.Equals(StatusReceita.CONSUMIDA.ToString()))
                {
                    OperacoesReceituario.AtualizarNumeroReceita(dto.CodeReceituario, dto.RegistroMapa,
                        dto.IdReceita, dto.NumReceita, dto.CodCultura, (int)StatusReceita.CONSUMIDA, sIdItemRct);
                    mForm.Close();
                }
                else
                {
                    Util.ExibirDialogo(sResult);
                }                
            }            
        }                
        private void CarregarGrid(IEnumerable<ReceitaConsulta> aReceitas)
        {
            mForm.Freeze(true);
            int i = 0;            
            try
            {                
                foreach (ReceitaConsulta Receita in aReceitas) {
                    if (Receita.quantidade > 0)
                    {
                        DtReceita.Rows.Add();
                        DtReceita.SetValue("ClCode", i, (i + 1));
                        DtReceita.SetValue("ClIdReceita", i, Receita.idReceita.ToString());
                        DtReceita.SetValue("ClIdItRct", i, Receita.idItemReceita);
                        DtReceita.SetValue("ClQuantidade", i, Receita.quantidade);
                        DtReceita.SetValue("ClNumRct", i, Receita.numeroReceita);
                        DtReceita.SetValue("ClDosagem", i, Receita.dosagem);
                        DtReceita.SetValue("ClArea", i, Receita.area);
                        DtReceita.SetValue("ClStatus", i, Receita.status);
                        i++;
                    }
                }
                if (DtReceita.Rows.Count == 0)
                    Util.ExibirDialogo($"Não foram identificadas receitas com quantidade disponível.");
                else
                    GrReceitas.Update();
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
    }
}