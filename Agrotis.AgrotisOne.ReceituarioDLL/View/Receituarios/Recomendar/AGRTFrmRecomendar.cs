﻿using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using Agrotis.Framework.Main.Interface.Controls.Forms.Base;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using ExtendedEditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.ExtendedEditText;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons;
using System;
using Agrotis.Framework.Data.UDO;
using System.Collections.Generic;
using Agrotis.Framework.Main.Interface.Events;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using Agrotis.Framework.Commons;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Receituarios.Recomendar
{
    [FormType(FormConstants.AGRTFrmRecomendar)]
    public class AGRTFrmRecomendar : Form
    {
        public AGRTFrmRecomendar(AddOn aAddOn, string aFormUniqueId) :
            base(aAddOn, aFormUniqueId)
        {
            EdProduto = Items.Add<EditText>(ItensUIDs.EdProduto);

            EdDose = Items.Add<EditText>(ItensUIDs.EdDose);
            EdDose.OnGotFocus += new EventVoidItem(EdDose_OnGotFocus);
            EdDose.OnLostFocus += new EventVoidItem(EdDose_OnLostFocus);
            EdDose.OnBeforeValidate += new EventBoolItem(EdDose_OnBeforeValidate);

            EdQuantidade = Items.Add<EditText>(ItensUIDs.EdQuantidade);
            EdQuantidade.OnLostFocus += new EventVoidItem(EdQuantidade_OnLostFocus);

            EdCalda = Items.Add<EditText>(ItensUIDs.EdCalda);
            EdCalda.OnGotFocus += new EventVoidItem(EdCalda_OnGotFocus);
            EdCalda.OnLostFocus += new EventVoidItem(EdCalda_OnLostFocus);

            EdNroAplicacoes = Items.Add<EditText>(ItensUIDs.EdNroAplicacoes);
            EdNroAplicacoes.OnGotFocus += new EventVoidItem(EdNroAplicacoes_OnGotFocus);
            EdNroAplicacoes.OnLostFocus += new EventVoidItem(EdNroAplicacoes_OnLostFocus);
            EdNroAplicacoes.OnBeforeValidate += new EventBoolItem(EdNroAplicacoes_OnBeforeValidate);

            EdArea = Items.Add<EditText>(ItensUIDs.EdArea);

            ExtModalidadeEquipamentoAplicacao = Items.Add<ExtendedEditText>(ItensUIDs.ExtModalidadeEquipamentoAplicacao);
            ExtEpocaAplicacao = Items.Add<ExtendedEditText>(ItensUIDs.ExtEpocaAplicacao);
            ExtMensagem = Items.Add<ExtendedEditText>(ItensUIDs.ExtMensagem);

            BtnOk = Items.Add<Button>(ItensUIDs.BtnOk);
            BtnOk.OnAfterClick += new EventVoidItem(BtnOk_OnAfterClick);

            BtnCancelar = Items.Add<Button>(ItensUIDs.BtnCancelar);
            BtnCancelar.OnAfterClick += new EventVoidItem(BtnCancelar_OnAfterClick);

            DtoCriarReceita = new DTOCriarReceita(Globals.Master.FormTicket?.GetFormParam(FormConstants.AGRTFrmRecomendar));

            CarregarDadosRecomendacao();
        }
        private void EdQuantidade_OnLostFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                LostFocusGeral();
            }
            catch (Exception Ex)
            {
                Logger.Trace("EdQuantidade_OnLostFocus - Não foi possível calcular a área. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível calcular a área. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }
        }
        private void BtnCancelar_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {
            if (Util.ExibirDialogoConfirmacao("Deseja cancelar a Recomendação?"))
                this.Close();
        }
        private void BtnOk_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                RecomendarItem();
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnOk_OnAfterClick - Não foi possível recomendar Item. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível recomendar Item. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        private bool EdNroAplicacoes_OnBeforeValidate(SAPbouiCOM.ItemEvent aEvent)
        {
            return true;
        }
        private bool EdDose_OnBeforeValidate(SAPbouiCOM.ItemEvent aEvent)
        {
            return true;
        }
        private void EdNroAplicacoes_OnLostFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                LostFocusGeral();
            }
            catch (Exception Ex)
            {
                Logger.Trace("EdNroAplicacoes_OnLostFocus - Não foi possível calcular a área. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível calcular a área. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void EdNroAplicacoes_OnGotFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            ExtMensagem.Value = $"{oRelacaoEspecifica.labelNumAplic} ";
        }
        private void EdCalda_OnLostFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                LostFocusGeral();
            }
            catch (Exception Ex)
            {
                Logger.Trace("EdCalda_OnLostFocus - Não foi possível calcular a área. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível calcular a área. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void EdCalda_OnGotFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            ExtMensagem.Value = oRelacaoEspecifica.orientacaoCalda;
        }
        private void EdDose_OnLostFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                LostFocusGeral();
            }
            catch (Exception Ex)
            {
                Logger.Trace("EdCalda_OnLostFocus - Não foi possível calcular a área. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível calcular a área. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void EdDose_OnGotFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            ExtMensagem.Value = $"{oRelacaoEspecifica.labelDosagem} - {oRelacaoEspecifica.orientacaoDosagem}";
        }        
        private void RecomendarItem()
        {
            ReceituarioItemRetorno oRecItemRetorno;            
            if (string.IsNullOrEmpty(DtoCriarReceita.IdRascunho))
            {
                DtoCriarReceita.IdRascunho = CriarRascunho();

                if (string.IsNullOrEmpty(DtoCriarReceita.IdRascunho))
                {
                    Util.ExibirMensagemStatusBar("Erro ao gerar Rascunho Receituário!", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                    Util.ExibirDialogo("Erro ao gerar Rascunho Receituário!");
                    return;
                }
            }

            DTOReceituarioItem DtoReceituarioItem = new DTOReceituarioItem();
            DtoReceituarioItem.areaAplicacao = FormatValue.ToDoubleFromSAP(EdArea.Value);
            DtoReceituarioItem.calda = FormatValue.ToDoubleFromSAP(EdCalda.Value);
            DtoReceituarioItem.codCultura = oRelacaoEspecifica.codCultura;
            DtoReceituarioItem.codModeloReceita = string.Empty; 
            DtoReceituarioItem.codPedidoItem = Convert.ToInt32(DtoCriarReceita.LineId);
            DtoReceituarioItem.tipoDeSolo = Convert.ToInt32(DtoCriarReceita.CodTipoSolo);
            DtoReceituarioItem.codRelacao = oRelacaoEspecifica.codRelacao;
            DtoReceituarioItem.dose = FormatValue.ToDoubleFromSAP(EdDose.Value);
            DtoReceituarioItem.idReceita = DtoCriarReceita.IdRascunho;
            DtoReceituarioItem.numeroAplicacoes = FormatValue.ToDoubleFromSAP(EdNroAplicacoes.Value);
            DtoReceituarioItem.obsEquipAplicacao = ExtModalidadeEquipamentoAplicacao.Value;
            DtoReceituarioItem.obsRecomendacoes = ExtEpocaAplicacao.Value;
            DtoReceituarioItem.quantidade = FormatValue.ToDoubleFromSAP(EdQuantidade.Value);

            oRecItemRetorno = Operacoes.AdicionarItemReceituario(DtoReceituarioItem);

            if (oRecItemRetorno == null)
            {
                Util.ExibirMensagemStatusBar("Item não adicionado ao rascunho!", SAPbouiCOM.BoMessageTime.bmt_Medium, true);                
            }
            else
            {
                DTOMasterData DtoMasterDataReceituario = new DTOMasterData()
                {
                    UDO = "AGRT_RECEIT",
                    DataFields = new List<Tuple<string, string, Type>>()
                                    {
                                        new Tuple<string, string, Type>("Code", DtoCriarReceita.CodigoReceituarioB1, typeof(string)),
                                        new Tuple<string, string, Type>($"U_AGRT_IdRascunho", DtoCriarReceita.IdRascunho, typeof(string))
                                    },
                    DtoMasterDataLines = new List<DTOMasterDataLine>()
                };

                DTOMasterDataLine DtoMasterDataLine = new DTOMasterDataLine()
                {
                    LineTable = "AGRT_RECEITAGRUP",
                    DataLineFields = new List<Tuple<string, Type>>()
                                        {
                                            new Tuple<string, Type>($"LineId", typeof(int)),
                                            new Tuple<string, Type>($"U_AGRT_RegMapa", typeof(string)),
                                            new Tuple<string, Type>($"U_AGRT_NomeRegMapa", typeof(string)),
                                            new Tuple<string, Type>($"U_AGRT_CodCultura", typeof(string)),
                                            new Tuple<string, Type>($"U_AGRT_NomeCultura", typeof(string)),
                                            new Tuple<string, Type>($"U_AGRT_QtdeKgLt", typeof(double)),
                                            new Tuple<string, Type>($"U_AGRT_Situacao", typeof(int)),
                                            new Tuple<string, Type>($"U_AGRT_IDItemReceita", typeof(string))
                                        }
                };

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("LineId", DtoCriarReceita.LineId);
                dic.Add("U_AGRT_RegMapa", DtoCriarReceita.CodMapaProduto);
                dic.Add("U_AGRT_NomeRegMapa", DtoCriarReceita.NomeProduto);
                dic.Add("U_AGRT_CodCultura", DtoCriarReceita.CodCultAgrotis);
                dic.Add("U_AGRT_NomeCultura", DtoCriarReceita.NomeCultura);
                dic.Add("U_AGRT_QtdeKgLt", FormatValue.ToDoubleFromSAP(EdQuantidade.Value));
                dic.Add("U_AGRT_Situacao", StatusReceita.RASCUNHO);
                dic.Add("U_AGRT_IDItemReceita", oRecItemRetorno.idItem.ToString());

                DtoMasterDataLine.DataLines = new List<Dictionary<string, object>>();
                DtoMasterDataLine.DataLines.Add(dic);

                if (FormatValue.ToDoubleFromSAP(EdQuantidade.Value) < DtoCriarReceita.Quantidade)
                {
                    dic = new Dictionary<string, object>();
                    //dic.Add("LineId", -1);
                    dic.Add("U_AGRT_RegMapa", DtoCriarReceita.CodMapaProduto);
                    dic.Add("U_AGRT_NomeRegMapa", DtoCriarReceita.NomeProduto);
                    dic.Add("U_AGRT_CodCultura", DtoCriarReceita.CodCultAgrotis);
                    dic.Add("U_AGRT_NomeCultura", DtoCriarReceita.NomeCultura);
                    dic.Add("U_AGRT_QtdeKgLt", DtoCriarReceita.Quantidade - FormatValue.ToDoubleFromSAP(EdQuantidade.Value));
                    dic.Add("U_AGRT_Situacao", StatusReceita.NAODEFINIDA);
                    dic.Add("U_AGRT_StatusRelReceita", StatusRelatorio.NAOEMITIDO);
                    dic.Add("U_AGRT_StatusRelParaquate", StatusRelatorio.NAOEMITIDO);
                    dic.Add("U_AGRT_IDItemReceita", String.Empty);
                    DtoMasterDataLine.DataLines.Add(dic);
                }

                DtoMasterDataReceituario.DtoMasterDataLines.Add(DtoMasterDataLine);

                string oError = string.Empty;
                if (string.IsNullOrEmpty(UtilUDO.UDOPersistirDados(DtoMasterDataReceituario, out oError)))
                    throw new Exception($"Erro ao atualizar agrupamento Receituário {oError}");
                else
                {
                    this.Close();
                }
            }
        }
        private string CriarRascunho()
        {
            DTORascunhoReceituario DtoRascunho = new DTORascunhoReceituario();
            DtoRascunho.dataEmissao = DateTime.Now.ToString("yyyy-MM-dd");
            DtoRascunho.idEmpresa = DtoCriarReceita.IdEmpresa;
            DtoRascunho.idProdutor = DtoCriarReceita.IdProdutor;
            DtoRascunho.idFazenda = DtoCriarReceita.IdFazenda.ToString();
            DtoRascunho.idRT = DtoCriarReceita.IdRT;
            return Operacoes.GeraRascunhoReceituario(DtoRascunho);
        }
        public void LostFocusGeral()
        {
            ExtMensagem.Value = string.Empty;
            CalcularArea();
        }
        private void CalcularArea()
        {
            CalculaAreaEnvio oCalculaArea = new CalculaAreaEnvio();

            oCalculaArea.descTipoDosagem = oRelacaoEspecifica.descTipoDosagem;
            oCalculaArea.quantidade = FormatValue.ToDoubleFromSAP(EdQuantidade.Value); ;
            oCalculaArea.dosagem = FormatValue.ToDoubleFromSAP(EdDose.Value);
            oCalculaArea.numeroAplicacoes = FormatValue.ToDoubleFromSAP(EdNroAplicacoes.Value);
            oCalculaArea.calda = FormatValue.ToDoubleFromSAP(EdCalda.Value);

            CalculaArea oCalculaAreaRetorno = null;
            if (oCalculaArea.calda > 0)
            {
                EdCalda.Enabled = true;
                EdArea.Enabled = true;
                oCalculaAreaRetorno = Operacoes.CalculaArea(oCalculaArea);
                EdArea.Value = oCalculaAreaRetorno != null ? FormatValue.ToSAPFromDouble(oCalculaAreaRetorno.area) : "0";
            }
            else
            {
                EdCalda.Enabled = false;
                EdArea.Enabled = oRelacaoEspecifica.temArea;
                EdArea.Value = "0";
            }
            ExtModalidadeEquipamentoAplicacao.Click();
        }
        private void CarregarDadosRecomendacao()
        {
            DTORelacaoEspecifica DtoRelEspecifica = new Model.DTO.DTORelacaoEspecifica();
            DtoRelEspecifica.codRelacao = Convert.ToInt32(DtoCriarReceita.CodRelacao);
            DtoRelEspecifica.codCultura = Convert.ToInt32(DtoCriarReceita.CodigoCultura);
            DtoRelEspecifica.codTipoSolo = Convert.ToInt32(DtoCriarReceita.CodTipoSolo);
            
            oRelacaoEspecifica = Operacoes.ConsultaRelacaoEspecifia(DtoRelEspecifica);

            EdProduto.Value = DtoCriarReceita.NomeProduto;
            EdCalda.Value = FormatValue.ToSAPFromDouble(oRelacaoEspecifica.calda);
            EdDose.Value = FormatValue.ToSAPFromDouble(oRelacaoEspecifica.doseMinima);
            EdQuantidade.Value = FormatValue.ToSAPFromDouble(DtoCriarReceita.Quantidade);
            EdNroAplicacoes.Value = oRelacaoEspecifica.numMinAplic.ToString();
            ExtEpocaAplicacao.Value = oRelacaoEspecifica?.modoAplicacao;

            CalcularArea();
        }

        #region ItensUIDs
        public class ItensUIDs
        {
            public const string EdProduto = "EdProduto";
            public const string EdDose = "EdDose";
            public const string EdQuantidade = "EdQtde";
            public const string EdCalda = "EdCalda";
            public const string EdNroAplicacoes = "EdNroAplic";
            public const string EdArea = "EdArea";
            public const string ExtModalidadeEquipamentoAplicacao = "ExModEquip";
            public const string ExtEpocaAplicacao = "ExtEpocaAp";
            public const string BtnOk = "BtnOk";
            public const string BtnCancelar = "BtnCanc";
            public const string ExtMensagem = "ExtMsg";
        }
        #endregion

        #region Propriedades
        public ExtendedEditText ExtEpocaAplicacao { get; set; }
        public ExtendedEditText ExtModalidadeEquipamentoAplicacao { get; set; }
        public ExtendedEditText ExtMensagem { get; set; }
        public EditText EdArea { get; set; }        
        public EditText EdNroAplicacoes { get; set; }
        public EditText EdCalda { get; set; }
        public EditText EdQuantidade { get; set; }
        public EditText EdDose { get; set; }
        public EditText EdProduto { get; set; }
        public Button BtnCancelar { get; set; }
        public Button BtnOk { get; set; }
        public DTOCriarReceita DtoCriarReceita { get; set; }
        public Relacao oRelacaoEspecifica { get; set; }
        OperacoesReceituario Operacoes = new OperacoesReceituario();

        #endregion

    }
}

