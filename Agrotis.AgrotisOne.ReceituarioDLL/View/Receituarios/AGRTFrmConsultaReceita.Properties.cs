﻿using Agrotis.Framework.Main.Interface.Controls.Items.Specific;

namespace Agrotis.AgrotisOne.ReceituarioDLLs.View.Receituarios
{
    public partial class AGRTFrmConsultaReceita
    {
        #region [Propriedades]        
        Grid GrReceitas { get; set; }
        SAPbouiCOM.EditTextColumn ColCode { get; set; }
        SAPbouiCOM.EditTextColumn ColIdReceita { get; set; }
        SAPbouiCOM.EditTextColumn ColQuantidade { get; set; }
        EditText DtIni { get; set; }
        EditText DtFin { get; set; }
        Button BtnFiltrar { get; set; }
        Button BtnOk { get; set; }
        SAPbouiCOM.DataTable DtReceita { get; set;}
        SAPbouiCOM.DataTable DtRetornoCFL;
        #endregion

        #region [ ItensUIDs ]
        public static class ItensUIDs
        {
            //Nomes devem conter no máximo 10 caracteres.            
            public const string GrReceitas = "GrReceitas";            
            public const string DtReceita = "DtReceita";
            public const string DtRetornoCFL = "DtRetCFL";
            public const string DtIni = "DtIni";
            public const string DtFin = "DtFin";
            public const string BtnFiltrar = "BtnFiltrar";
            public const string BtnOk = "BtnOk";
        }

        #endregion
    }
}
