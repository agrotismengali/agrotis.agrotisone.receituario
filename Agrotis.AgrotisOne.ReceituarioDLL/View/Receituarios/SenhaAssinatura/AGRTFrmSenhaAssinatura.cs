﻿using Agrotis.Framework.Main.Interface.Controls.Forms.Base;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using Agrotis.Framework.Main.Interface.Controls.Items.Specific;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using System;
using Agrotis.Framework.Commons;

namespace Agrotis.AgrotisOne.ReceituarioDLLs.View.Receituarios.SenhaAssinatura
{
    [FormType(FormConstants.AGRTFrmSenhaAssinatura)]
    public partial class AGRTFrmSenhaAssinatura : Form
    {
        public AGRTFrmSenhaAssinatura(AddOn aAddOn, string aFormUID)
            : base(aAddOn, aFormUID)
        {
            mForm.Freeze(true);
            try
            {
                EdSenha = Items.Add<EditText>(ItensUIDs.EdSenha);

                OnBeforeFormClose += AGRTFrmSenhaAssinatura_OnBeforeFormClose;

                BtnOk = Items.Add<Button>(ItensUIDs.BtnOk);
                BtnOk.OnAfterClick += BtnOk_OnAfterClick;

                BtCancelar = Items.Add<Button>(ItensUIDs.BtCancelar);
                BtCancelar.OnAfterClick += BtCancelar_OnAfterClick;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro ao abrir Form - AGRTFrmSenhaAssinatura", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }            
        }
        private bool AGRTFrmSenhaAssinatura_OnBeforeFormClose(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                if (!AcaoForm)
                    return ValidaCancelar();
                return AcaoForm;
            }
            catch (Exception Ex)
            {
                Logger.Trace("AGRTFrmSenhaAssinatura_OnBeforeFormClose - Não foi possível fechar a tela de assinatura da receita. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível fechar a tela de assinatura da receita." + Ex.Message);
                return false;
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void BtCancelar_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                if (ValidaCancelar())
                {
                    AcaoForm = true;
                    Close();
                }
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtCancelar_OnAfterClick - Não foi possível cancelar a tela de assinatura da receita. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível cancelar a tela de assinatura da receita." + Ex.Message);                
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void BtnOk_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                EnviarSenha();
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnOk_OnAfterClick - Não foi possível enviar a senha ao receituário. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível enviar a senha ao receituário." + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void EnviarSenha()
        {
            if (string.IsNullOrEmpty(EdSenha.Value))
            {
                if (ValidaCancelar())
                {
                    AcaoForm = true;
                    this.Close();
                }
            }
            else
            {
                Globals.Master.FormTicket.AddFormParam(mForm.UniqueID, FormConstants.AGRTFrmSenhaAssinatura, FormConstants.AGRTFrmSenhaAssinatura, new string[] { EdSenha.Value });
                AcaoForm = true;
                this.Close();
            }
        }
        private bool ValidaCancelar()
        {
            if (Util.ExibirDialogoConfirmacao("Deseja emitir sem assinatura digital?"))
            {
                Globals.Master.FormTicket.AddFormParam(mForm.UniqueID, FormConstants.AGRTFrmSenhaAssinatura, FormConstants.AGRTFrmSenhaAssinatura, new string[] { string.Empty});
                return true;
            }

            return false;
        }

        #region Properties
        public EditText EdSenha { get; set; }
        public Button BtnOk { get; set; }
        public Button BtCancelar { get; set; }
        public bool AcaoForm = false;
        #endregion

        #region ItensUIDs
        public class ItensUIDs
        {
            public const string EdSenha = "EdSenha";
            public const string BtnOk = "BtnOk";
            public const string BtCancelar = "BtCanc";
        }
        #endregion
    }
}

