﻿using Agrotis.Framework.Main.Interface.Controls.Forms.Base;
using Agrotis.Framework.Main.Application;
using Matrix = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Matrix;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using System;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.Objects;
using System.Collections.Generic;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.API;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using ComboBox = Agrotis.Framework.Main.Interface.Controls.Items.Specific.ComboBox;
using Agrotis.AgrotisOne.Core.Utils;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using Agrotis.Framework.Main.Interface.Events;
using System.Reflection;
using System.IO;
using Agrotis.Framework.Commons;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Utils;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Receituarios.CriarReceita
{
    [FormType(FormConstants.AGRTFrmCriarReceita)]
    public class AGRTFrmCriarReceita : Form
    {
        public AGRTFrmCriarReceita(AddOn aAddOn, string aFormUniqueId) : 
            base(aAddOn, aFormUniqueId)
        {
            UtilReceituario.FormFreeze(mForm, true);
            try
            {
                BtnRecomendar = Items.Add<Button>(ItensUIDs.BtnRecomendar);
                BtnRecomendar.OnAfterClick += new EventVoidItem(BtnRecomendar_OnAfterClick);
                BtnRecomendar.AffectsFormMode = false;

                BtnCancelar = Items.Add<Button>(ItensUIDs.BtnCancelar);
                BtnCancelar.OnAfterClick += new EventVoidItem(BtnCancelar_OnAfterClick);
                BtnCancelar.AffectsFormMode = false;

                CbProblema = Items.Add<ComboBox>(ItensUIDs.CbProblema);
                CbProblema.OnAfterComboSelect += new EventVoidItem(CbProblema_OnAfterComboSelect);
                CbProblema.AffectsFormMode = false;

                MtRecomendar = Items.Add<Matrix>(ItensUIDs.MtRecomendar);
                MtRecomendar.AffectsFormMode = false;

                EdProduto = Items.Add<EditText>(ItensUIDs.EdProduto);
                EdProduto.AffectsFormMode = false;

                EdCultura = Items.Add<EditText>(ItensUIDs.EdCultura);
                EdCultura.AffectsFormMode = false;

                DtRestricao = mForm.DataSources.DataTables.Item(ItensUIDs.DtRestricao);
                DtoCriarReceita = new DTOCriarReceita(Globals.Master.FormTicket?.GetFormParam(FormConstants.AGRTFrmCriarReceita));

                CarregarDados();
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro ao abrir Form AGRTFrmCriarReceita", Ex);
            }
            finally
            {
                UtilReceituario.FormFreeze(mForm, false);
            }
        }
        private void BtnCancelar_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {            
            try
            {
                if (Util.ExibirDialogoConfirmacao("Deseja cancelar a Criação da receita?"))
                    this.Close();
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método BtnCancelar_OnAfterClick - AGRTFrmCriarReceita", Ex);
            }
            finally
            {
                UtilReceituario.FormFreeze(mForm, false);
            }            
        }
        private void BtnRecomendar_OnAfterClick(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                Recomendar();
            }
            catch (Exception Ex)
            {
                Logger.Trace("BtnRecomendar_OnAfterClick - Não foi possível realizar a recomendação. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível realizar a recomendação. " + Ex.Message);                
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void CbProblema_OnAfterComboSelect(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                CarregaRestricaoRelacao();
            }
            catch (Exception Ex)
            {
                Logger.Trace("CbProblema_OnAfterComboSelect - Não foi possível carregar os problemas relacionados. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível carregar os problemas relacionados. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void Recomendar()
        {
            int Row = MtRecomendar.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_RowOrder);
            if (Row == -1)
            {
                Util.ExibirMensagemStatusBar("Não há linha selecionada!", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                Util.ExibirDialogo("Não há linha selecionada!");
                return;
            }

            Row -= 1;

            DtoCriarReceita.CodRelacao = DtRestricao.GetValue("codRelacao", Row).ToString();
            DtoCriarReceita.CodProblema = DtRestricao.GetValue("codProblema", Row).ToString();
            DtoCriarReceita.Problema = DtRestricao.GetValue("problema", Row).ToString();
            DtoCriarReceita.CodTipoSolo = DtRestricao.GetValue("codTipoDeSolo", Row).ToString();
            
            StreamReader lForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(
              $@"Agrotis.AgrotisOne.ReceituarioDLL.View.Receituarios.Recomendar.AGRTFrmRecomendar.srf"));
            Util.FormSetAbrirTicket(UniqueID,
                FormConstants.AGRTFrmCriarReceita,
                lForm,
                FormConstants.AGRTFrmRecomendar,
                DtoCriarReceita.GetDataRecord()
               );

            this.Close();
        }        
        private void CarregaRestricaoRelacao()
        {
            Util.ExibirMensagemStatusBar("Carregando Restrições...", SAPbouiCOM.BoMessageTime.bmt_Long);            
            if (CbProblema.Value == null)
            {
                Util.ExibirMensagemStatusBar("Não há Problema selecionado!", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                Util.ExibirDialogo("Não há Problema selecionado!");
                return;
            }
            Relacoes oRelacoesRestricoes = WSRelacoes.ConsultaRestricaoRelacao(DtoCriarReceita.CodMapaProduto, DtoCriarReceita.CodigoCultura, CbProblema.Value != "-1" ? new List<int> { Convert.ToInt16(CbProblema.Value) } : new List<int> { }, false, DtoCriarReceita.SiglaUF, false);
            CarregarMatrixRelacoesRestricoes(oRelacoesRestricoes);
            Util.ExibirMensagemStatusBar(String.Empty, SAPbouiCOM.BoMessageTime.bmt_Short);
        }
        private void CarregarMatrixRelacoesRestricoes(Relacoes RelacoesRestricoes)
        {
            if (RelacoesRestricoes == null || RelacoesRestricoes.Relacao == null || RelacoesRestricoes.Relacao.Length == 0)
            {
                DtRestricao.Clear();
                for (int iRow = MtRecomendar.RowCount; iRow > 0; iRow--)
                    MtRecomendar.DeleteRow(iRow);
                return;
            }

            Util.DataTableCarregaDadosObjeto(RelacoesRestricoes.Relacao, ref DtRestricao);
            MtRecomendar.Columns.Item(ItensUIDs.Col_EstagioProblema).DataBind.Bind(ItensUIDs.DtRestricao, ItensUIDs.EstagioProblema);
            MtRecomendar.Columns.Item(ItensUIDs.Col_EstagioCultura).DataBind.Bind(ItensUIDs.DtRestricao, ItensUIDs.EstagioCultura);
            MtRecomendar.Columns.Item(ItensUIDs.Col_MetodoAplicacao).DataBind.Bind(ItensUIDs.DtRestricao, ItensUIDs.MetodoAplicacao);
            MtRecomendar.Columns.Item(ItensUIDs.Col_OrientacaoDosagem).DataBind.Bind(ItensUIDs.DtRestricao, ItensUIDs.OrientacaoDosagem);
            MtRecomendar.Columns.Item(ItensUIDs.Col_FormaConducaoCultura).DataBind.Bind(ItensUIDs.DtRestricao, ItensUIDs.FormaConducaoCultura);
            MtRecomendar.Columns.Item(ItensUIDs.Col_ModoAplicacao).DataBind.Bind(ItensUIDs.DtRestricao, ItensUIDs.ModoAplicacao);
            MtRecomendar.Columns.Item(ItensUIDs.Col_Problema).DataBind.Bind(ItensUIDs.DtRestricao, ItensUIDs.Problema);
            MtRecomendar.Columns.Item(ItensUIDs.Col_TipoSolo).DataBind.Bind(ItensUIDs.DtRestricao, ItensUIDs.TipoSolo);
            
            MtRecomendar.LoadFromDataSource();
            //MtRecomendar.AutoResizeColumns();           
        }
        private void CarregarDados()
        {
            Util.ExibirMensagemStatusBar("Carregando Problemas...");            
            EdCultura.Value = DtoCriarReceita.NomeCultura;
            EdProduto.Value = DtoCriarReceita.NomeProduto;
            //MtRecomendar.AutoResizeColumns();
            CarregarComboProblemas(WSProblemas.ListarProblemas(dtoAPI, DtoCriarReceita.CodMapaProduto, DtoCriarReceita.SiglaUF, DtoCriarReceita.CodigoCultura));            
        }
        private void CarregarComboProblemas(IEnumerable<Problema> aProblemas)
        {
            Util.ComboBoxClearValidValues(CbProblema);
            CbProblema.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            CbProblema.DisplayDesc = true;

            CbProblema.ValidValues.Add("-1", "Nenhum Problema");
            if (aProblemas != null)
            {
                foreach (var Problema in aProblemas)
                {
                    CbProblema.ValidValues.Add(Problema.codProblema.ToString(), $"{Problema.nomeVulgar} ({Problema.nomeCientifico})");
                }
            }
        }        
                
        DTOApi dtoAPI = new DTOApi();
        public string Estado { get; set; }
        public DTOCriarReceita DtoCriarReceita { get; set; }
        public Matrix MtRecomendar { get; set; }
        public EditText EdCultura { get; set; }
        public EditText EdProduto { get; set; }
        public Button BtnRecomendar { get; set; }
        public Button BtnCancelar { get; set; }
        public ComboBox CbProblema { get; set; }
        public SAPbouiCOM.DataTable DtRestricao;
        
        public class ItensUIDs
        {
            public const string CbProblema = "CbProblema";
            public const string MtRecomendar = "MtRecemend";   
            public const string EdProduto = "EdProduto";
            public const string EdCultura = "EdCultura";
            public const string BtnRecomendar = "BtRecomend";
            public const string BtnCancelar = "BtCancela";
            public const string DtRestricao = "DtRestrica";


            public const string Col_EstagioProblema = "Col_0";
            public const string Col_EstagioCultura = "Col_1";
            public const string Col_MetodoAplicacao = "Col_2";
            public const string Col_OrientacaoDosagem = "Col_3";
            public const string Col_FormaConducaoCultura = "Col_4";
            public const string Col_ModoAplicacao = "Col_6";
            public const string Col_Problema = "Col_5";
            public const string Col_TipoSolo = "Col_7";

            public const string EstagioProblema = "estagioProblema";
            public const string EstagioCultura = "estagioCultura";
            public const string MetodoAplicacao = "metodoAplicacao";
            public const string OrientacaoDosagem = "orientacaoDosagem";
            public const string FormaConducaoCultura = "formaConducaoCultura";
            public const string ModoAplicacao = "modoAplicacao";
            public const string Problema = "problema";
            public const string TipoSolo = "tipoDeSolo";
        }
    }
}
