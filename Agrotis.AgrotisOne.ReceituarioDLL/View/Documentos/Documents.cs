﻿using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.Core.Constantes;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons;
using Agrotis.Framework.Commons;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Items.Specific;
using System;
using System.Collections.Generic;
using Form = Agrotis.Framework.Main.Interface.Controls.Forms.Base.Form;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Documentos
{
    public abstract partial class Documents : Form
    {
        protected string sCardCode = "";
        protected OperacoesReceituario OpeReceituario = new OperacoesReceituario();
        private SAPbobsCOM.BusinessPartners oBusinessPartner = 
            Globals.Master.Connection.Database.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
        protected string SequenceID = "";
        protected string CodigoReceituario = "";        
        public Documents(AddOn aAddOn, string aFormUID)
            : base(aAddOn, aFormUID)
        {
            try
            {
                EdCodigoReceituario = Items.Add<EditText>(ItensUIDs.EdCodigoControleReceituario);
                EdCodigoReceituario.DataBind.SetBound(true, DataSourceUIDs.Tabela(mForm.TypeEx), "U_AGRT_CodRec");
                EdCodigoReceituario.Visible = true;
                EdCodigoReceituario.Top = -100;

                EdSequenceID = Items.Add<EditText>(ItensUIDs.EdSeqID);
                EdSequenceID.DataBind.SetBound(true, DataSourceUIDs.Tabela(mForm.TypeEx), "U_AGRT_SequenceID");
                EdSequenceID.Visible = true;
                EdSequenceID.Top = -100;

                EdNumero = (SAPbouiCOM.EditText)mForm.Items.Item(ItensUIDs.EdNumero).Specific;
                EdSerie = (SAPbouiCOM.EditText)mForm.Items.Item(ItensUIDs.EdSerie).Specific;
                EdDocDate = (SAPbouiCOM.EditText)mForm.Items.Item(ItensUIDs.EdDocDate).Specific;

                DataSourceLinhas = this.DataSources.DBDataSources.Item(DataSourceUIDs.DataSourcesLinhasUIDs.Tabela(mForm.TypeEx));
                DataSourceParcelas = this.DataSources.DBDataSources.Item(DataSourceUIDs.DataSourcesParcelasUIDs.Tabela(mForm.TypeEx));
                DataSourceDoc = this.DataSources.DBDataSources.Item(DataSourceUIDs.Tabela(mForm.TypeEx));
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método EdCardCode_OnGotFocus - Documents", Ex);
            }
        }
        public void AddChooseToMatrix()
        {
            SAPbouiCOM.ChooseFromListCollection oCFLs = null;
            oCFLs = mForm.ChooseFromLists;

            SAPbouiCOM.ChooseFromList oCFL = null;
            SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
            oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)
                (Globals.Master.Connection.Interface.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));

            oCFLCreationParams.MultiSelection = false;
            oCFLCreationParams.ObjectType = Core.Model.UDOs.Cultura.UniqueID;
            oCFLCreationParams.UniqueID = "CflCultura";

            oCFL = oCFLs.Add(oCFLCreationParams);

            SAPbouiCOM.Columns oColumns = MtItens.Columns;

            SAPbouiCOM.Column oColumn = oColumns.Item(ItensUIDs.ColCodigoCultura);
            oColumn.ChooseFromListUID = "CflCultura";
            oColumn.ChooseFromListAlias = "Code";
        }
        public void MtItens_OnAfterChooseFromList(SAPbouiCOM.ChooseFromListEvent aEvent)
        {
            try
            {
                if (aEvent.SelectedObjects == null)
                    return;
                switch (aEvent.ChooseFromListUID)
                {
                    case "CflCultura":
                        {                            
                            Core.Utils.Util.MatrixSetValorCelula(MtItens,
                                $"U_{Model.Campos.INV1.Campos.CodigoCultura.Nome}", aEvent.Row, 
                                aEvent.SelectedObjects.GetValue("Code", 0).ToString());
                            Core.Utils.Util.MatrixSetValorCelula(MtItens, 
                                $"U_{Model.Campos.INV1.Campos.NomeCultura.Nome}", aEvent.Row, 
                                aEvent.SelectedObjects.GetValue("Name", 0).ToString());
                        }
                        break;
                    default: break;
                }
            }
            catch (Exception Ex)
            {
                Logger.Trace("MtItens_OnAfterChooseFromList - Não foi possível atribuir Valor no campo Cultura. " + Ex.ToString());
                Util.ExibirDialogo($"Não foi possível atribuir Valor no campo Cultura {Ex.Message}");                
            }
            finally
            {
                GC.Collect();
            }            
        }
        public void EdCardCode_OnGotFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                if (Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                    sCardCode = EdCardCode.Value;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método EdCardCode_OnGotFocus - Documents", Ex);
            }                        
        }
        public void EdCardCode_OnLostFocus(SAPbouiCOM.ItemEvent aEvent)
        {
            try
            {
                if (Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {
                    // Ao inserir/duplicar uma nota fiscal, o usuário sempre terá que informar o PN novamente então o vínculo com receituário é zerado
                    if (!sCardCode.Equals(EdCardCode.Value))
                    {
                        if (!string.IsNullOrEmpty(EdCardCode.Value))
                            oBusinessPartner.GetByKey(EdCardCode.Value);
                        if (string.IsNullOrEmpty(sCardCode))
                        {                            
                            CodigoReceituario = "";
                            SequenceID = "";
                        }
                        ItensReceituario.Clear();
                        if ((mForm.TypeEx == SAPFormConstants.SAPFrmNotaFiscalSaida) || 
                            (mForm.TypeEx == SAPFormConstants.SAPFrmPedidoDeVenda))
                            BtnReceita.Visible = OpeReceituario.FilialGeraReceita(DataSourceDoc.GetValue("BPLId", 0));
                    }
                }
            }
            catch (Exception Ex)
            {
                Logger.Trace("EdCardCode_OnLostFocus - Não foi possível zerar parâmetros de inicialização para receita. " + Ex.ToString());
                Util.ExibirDialogo("Não foi possível zerar parâmetros de inicialização para receita. " + Ex.Message);
            }
            finally
            {
                GC.Collect();
            }            
        }
        public string GetBusinessPartnerState(string aCardCode)
        {
            if (!aCardCode.Equals(oBusinessPartner.CardCode))
                oBusinessPartner.GetByKey(EdCardCode.Value);
            for (int iEndereco = 0; iEndereco < oBusinessPartner.Addresses.Count; iEndereco++)
            {
                oBusinessPartner.Addresses.SetCurrentLine(iEndereco);
                if (oBusinessPartner.Addresses.AddressName == DataSourceDoc.GetValue("ShipToCode", 0)) break;
            }
            return oBusinessPartner.Addresses.State;
        }
        protected SAPbobsCOM.BusinessPartners GetBusinessPartner(string aCardCode)
        {
            if (!aCardCode.Equals(oBusinessPartner.CardCode))
                oBusinessPartner.GetByKey(EdCardCode.Value);
            return oBusinessPartner;
        }
        protected string GetSequenceID()
        {              
            if (string.IsNullOrEmpty(SequenceID))
                SequenceID = OpeReceituario.GetSequenceID();            
            return SequenceID;            
        }
        protected string GetCodigoReceituario()
        {                        
            return CodigoReceituario;
        }
        protected int GetCountBaseDocLinked()
        {            
            int countBase = 0;
            string baseEntryAnt = "";
            foreach (Tuple<int, string, string, string, double> linkedItem in GetBaseDocLinked())
            {
                string baseEntry = linkedItem.Item3;
                if ((!string.IsNullOrEmpty(baseEntry)) && (baseEntryAnt != baseEntry))
                {
                    baseEntryAnt = baseEntry;
                    countBase++;
                }                
            }
            return countBase;
        }
        protected List<Tuple<int, string, string, string, double>> GetBaseDocLinked()
        {
            List<Tuple<int, string, string, string, double>> lResult = new List<Tuple<int, string, string, string, double>>();            
            for (int RowID = 1; RowID <= MtItens.VisualRowCount - 1; RowID++)
            {
                string itemCode = Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColCodigoItem, RowID);
                string baseEntry = Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColBaseEntry, RowID);
                string baseLine = Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColBaseLine, RowID);
                double quantity = FormatValue.ToDoubleFromSAP(Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColQtde, RowID));
                lResult.Add(new Tuple<int, string, string, string, double>(RowID, itemCode, baseEntry, baseLine, quantity));
            }
            return lResult;
        }

    }
}
