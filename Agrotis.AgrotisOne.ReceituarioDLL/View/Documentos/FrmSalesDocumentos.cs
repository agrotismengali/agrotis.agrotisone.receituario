﻿using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Constantes;
using Agrotis.Framework.Main.Application;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons;
using Agrotis.AgrotisOne.ReceituarioDLL.Model.DTO;
using SAPbouiCOM;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using Agrotis.AgrotisOne.Core.Constantes;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Documentos
{
    public abstract class FrmSalesDocuments : Documents
    {        
        public FrmSalesDocuments(AddOn aAddOn, string aFormUID)
            : base(aAddOn, aFormUID)
        {
            
        }
        protected void EdBPLId_OnAfterComboSelect(ItemEvent aEvent)
        {
            try
            {
                if (!string.IsNullOrEmpty(GetCodigoReceituario()))
                {
                    if (Util.ExibirDialogoConfirmacao("Este documento está vinculado a uma receita. Ao trocar a filial, a receita será desvinculada. Deseja continuar?"))
                    {
                        BtnReceita.Visible = OpeReceituario.FilialGeraReceita(DataSourceDoc.GetValue("BPLId", 0));
                        CodigoReceituario = "";
                        SequenceID = "";
                    }
                }
                if (NotaFiscalPossuiReceita())
                    Util.ExibirDialogo("A análise das receitas foi descartada, devido a alteração da Filial.");
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível alterar a filial. ", Ex);
            }
            finally
            {
                GC.Collect();
            }                         
        }
        public bool FrmSalesDocument_OnBeforeFormDataAdd(BusinessObjectInfo aEvent)
        {
            try
            {
                if (string.IsNullOrEmpty(GetCodigoReceituario()))
                {
                    // Se existem itens com receituário
                    if (PreencherItensReceituario() > 0)
                    {
                        return Util.ExibirDialogoConfirmacao("Esta nota possui itens que necessitam de receita agronômica vinculada. Deseja emitir a nota sem as receitas?");
                    }
                    else
                        return true;
                }
                else if (CodigoReceituario.Equals("0"))
                    return true;
                else if (!OpeReceituario.PodeEmitirNota(CodigoReceituario))
                {
                    return Util.ExibirDialogoConfirmacao("Atenção! Foram identificadas receitas com status diferente de [Consumida]. Deseja emitir a nota mesmo assim?");
                }
                else
                {                                        
                    return true;
                }                    
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível adicionar a nota fiscal. ", Ex);
                return false;
            }
            finally
            {
                GC.Collect();
            }            
        }
        public void FrmSalesDocument_OnAfterFormDataAdd(BusinessObjectInfo aEvent)
        {
            try
            {
                if (aEvent.ActionSuccess)
                {
                    ItensReceituario.Clear();
                    if ((NotaFiscalPossuiReceita()) && (!OpeReceituario.ReceitasFinalizadas(GetCodigoReceituario())))
                    {
                        ImpressaoRelatorios oImpressaoRel = new ImpressaoRelatorios(CodigoReceituario, DataSourceDoc.GetValue("BPLId", 0));
                        if (mForm.TypeEx != SAPFormConstants.SAPFrmPedidoDeVenda)
                        {
                            string sRetorno = OpeReceituario.VincularFaturamento(GetSequenceID(), EdNumero.Value, EdSerie.Value,
                            FormatValue.ToDate(EdDocDate.Value).ToString("yyyy-MM-dd"));
                            if (!sRetorno.Equals(StatusReceita.FINALIZADA.ToString()))
                                Util.ExibirDialogo(sRetorno);
                            if (oImpressaoRel.DocumentoImprimeReceita() == "0")
                                oImpressaoRel.Executar();
                            OperacoesReceituario.AtualizarSituacaoReceita(CodigoReceituario, StatusReceita.FINALIZADA);
                        }
                        else if (oImpressaoRel.DocumentoImprimeReceita() == "1")
                        {
                            oImpressaoRel.Executar();
                        }                        
                    }
                    CodigoReceituario = "";
                    SequenceID = "";
                }
                if (!MtItens.Columns.Item(ItensUIDs.ColCodigoItem).Editable)
                {
                    MtItens.Columns.Item(ItensUIDs.ColCodigoItem).Editable = true;
                    MtItens.Columns.Item(ItensUIDs.ColQtde).Editable = true;
                    MtItens.Columns.Item(ItensUIDs.ColCodigoCultura).Editable = true;
                }                
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Erro ao salvar o documento. FrmSalesDocument_OnAfterFormDataAdd ", Ex);
            }
            finally
            {
                GC.Collect();
            }            
        }
        public void FrmSalesDocument_OnAfterFormDataLoad(BusinessObjectInfo aEvent)
        {
            try
            {
                CodigoReceituario = EdCodigoReceituario.Value;
                SequenceID = EdSequenceID.Value;
                if (!MtItens.Columns.Item(ItensUIDs.ColCodigoItem).Editable)
                {
                    MtItens.Columns.Item(ItensUIDs.ColCodigoItem).Editable = true;
                    MtItens.Columns.Item(ItensUIDs.ColQtde).Editable = true;
                    MtItens.Columns.Item(ItensUIDs.ColCodigoCultura).Editable = true;
                }
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível carregar o documento. ", Ex);
            }
            finally
            {
                GC.Collect();
            }            
        }                        
        public void MtItensQtde_OnAfterValidate(ItemEvent aEvent)
        {    
            //            
        }
        public bool MtItensQtde_OnBeforeValidate(ItemEvent aEvent)
        {            
            return true;
        }
        public void MtItens_OnGotFocus(ItemEvent aEvent)
        {
            try
            {
                if (Mode != BoFormMode.fm_ADD_MODE)
                    return;
                bool bPodeAlterar = (string.IsNullOrEmpty(CodigoReceituario) || (CodigoReceituario.Equals("0")));
                if ((!bPodeAlterar) && (MtItens.Columns.Item(ItensUIDs.ColCodigoItem).Editable))
                {
                    Util.CampoSetFocus(mForm, EdCodigoReceituario);
                    MtItens.Columns.Item(ItensUIDs.ColCodigoItem).Editable = bPodeAlterar;
                    MtItens.Columns.Item(ItensUIDs.ColQtde).Editable = bPodeAlterar;
                    MtItens.Columns.Item(ItensUIDs.ColCodigoCultura).Editable = bPodeAlterar;
                }
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível dar foco na Grid. ", Ex);
            }
            finally
            {
                GC.Collect();
            }            
        }
        public void MtItens_OnLostFocus(ItemEvent aEvent)
        {
            //
        }        
        public void BtnReceita_OnAfterItemPressed(ItemEvent aEvent)
        {
            try
            {
                // Se CodReceituario está em branco é porquê a análise ainda não rodou
                if (Mode == BoFormMode.fm_ADD_MODE)
                {
                    if (string.IsNullOrEmpty(GetCodigoReceituario()))
                    {
                        if (!ValidaFilial())
                            return;

                        if (!ValidaUtilizacoes())
                            return;

                        if (!Util.ExibirDialogoConfirmacao("Atenção! Após a análise das Receitas, as quantidades e itens da nota não poderão mais ser alterados! Deseja continuar?"))
                            return;
                        CodigoReceituario = CriarReceituario();
                        if (NotaFiscalPossuiReceita())
                            AbrirReceituario(CodigoReceituario);
                    }
                    // Se CodReceituario é zero, significa que não existem itens com receita
                    else if (CodigoReceituario.Equals("0"))
                    {
                        Util.ExibirDialogo("Não foi identificada a necessidade de receita agronômica para este documento. Verifique a configuração do parceiro de negócio, da utilização, do item e seu respectivo grupo.");
                    }
                    else
                        AbrirReceituario(CodigoReceituario);
                }
                // Senão abre o form para manutenção do receituário
                else if (NotaFiscalPossuiReceita())
                    AbrirReceituario(CodigoReceituario);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível validar/abrir as receitas relacionadas. ", Ex);
            }
            finally
            {
                GC.Collect();
            }            
        }               
        private string CriarReceituario()
        {
            ItensReceituario.Clear();
            bool TemPedidoVinculado = false;
            try
            {
                if (PreencherItensReceituario() > 0)
                {
                    string codReceituario = "";
                    if (mForm.TypeEx != SAPFormConstants.SAPFrmPedidoDeVenda)
                    {
                        // Remove os itens que vieram de pedido de venda
                        foreach (Tuple<int, string, string, string, double> linkedItem in GetBaseDocLinked())
                        {
                            int index = linkedItem.Item1 - 1;
                            string baseEntry = linkedItem.Item3;
                            if ((!string.IsNullOrEmpty(baseEntry)) && 
                                (!string.IsNullOrEmpty(OperacoesReceituario.GetReceitaVinculadaPedido(baseEntry))))
                                ItensReceituario.RemoveAt(index);
                        }
                        // Gera o cabeçalho do receituário com os itens inseridos manualmente
                        codReceituario = OperacoesReceituario.GravaReceituario(ItensReceituario);
                        // Copia as receitas provenientes de pedido
                        foreach (Tuple<int, string, string, string, double> linkedItem in GetBaseDocLinked())
                        {
                            string baseEntry = linkedItem.Item3;
                            string baseLine = linkedItem.Item4;
                            double quantity = linkedItem.Item5;
                            if (!string.IsNullOrEmpty(baseEntry))
                            {
                                foreach (Dictionary<string, object> BaseRec in OperacoesReceituario.GetReceitasPedido(baseEntry))
                                {
                                    if ((BaseRec["IDReceita"] != null) && (BaseRec["SequenceID"] != null) && (BaseRec["CodRec"] != null))
                                        OpeReceituario.DesvincularPedido(BaseRec["CodRec"].ToString(), 
                                                                         BaseRec["SequenceID"].ToString(), 
                                                                         BaseRec["IDReceita"].ToString(), false);
                                }
                                OperacoesReceituario.CopiarReceituarioPedido(codReceituario, baseEntry, baseLine, quantity);
                                TemPedidoVinculado = true;
                            }                            
                        }
                    }
                    else
                        codReceituario = OperacoesReceituario.GravaReceituario(ItensReceituario);

                    if (TemPedidoVinculado)
                    {
                        foreach (Dictionary<string, object> Receita in OperacoesReceituario.GetReceitasConcluidas(codReceituario))
                        {
                            double quantidade = FormatValue.ToDoubleOrDefault(Receita["Qtde"].ToString()); 
                            if ((Receita["IDReceita"] != null) && (Receita["IDItemReceita"] != null) && (Receita["RegMapa"] != null) && (quantidade > 0))
                                OpeReceituario.VincularPedido(GetSequenceID(), FormatValue.ToSAPFromDouble(quantidade), Receita["RegMapa"].ToString(),
                                    Receita["IDReceita"].ToString(), Receita["IDItemReceita"].ToString());
                        }
                    }
                    return codReceituario;
                }
                else
                    return "0"; // Nenhum item da nota necessita de receita                
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível gerar as receitas. ", Ex);
                return "";
            }            
        }
        private bool ValidaFilial()
        {
            string sBplId = DataSourceDoc.GetValue("BPLId", 0);
            if (string.IsNullOrEmpty(sBplId) || sBplId == "0")
            {
                Util.ExibirMensagemStatusBar("Selecionar a Filial para o Documento!", BoMessageTime.bmt_Medium, true);
                Util.ExibirDialogo("Selecionar a Filial para o Documento!");
                return false;
            }
            return true;
        }
        private bool ValidaUtilizacoes()
        {               
            return true;           
        }
        private void AbrirReceituario(string aCodReceituario)
        {
            if ((string.IsNullOrEmpty(aCodReceituario)) || (aCodReceituario.Equals("0")))
            {
                Util.ExibirDialogo("Esta nota fiscal não possui Receita(s) vinculada(s).");
                return;
            }
            DTOReceituario dto = new DTOReceituario();
            GetBusinessPartner(EdCardCode.Value);

            dto.CodeReceituario = aCodReceituario;
            dto.SequenceId = GetSequenceID();
            dto.UUIDProdutor = GetBusinessPartner(EdCardCode.Value).UserFields.Fields.Item("U_AGRT_UUID_PDR").Value;
            dto.SiglaUF = GetBusinessPartnerState(EdCardCode.Value);

            if (string.IsNullOrEmpty(DataSourceDoc.GetValue("ShipToCode", 0).ToString()))
            {
                Util.ExibirDialogo("Atenção! Informe o endereço de entrega na aba Logística.");
                return;
            }            
            dto.UUIDPropriedade = OperacoesReceituario.GetUUIDPropriedade(DataSourceDoc.GetValue("CardCode", 0).ToString(), DataSourceDoc.GetValue("ShipToCode", 0).ToString());
            if (string.IsNullOrEmpty(dto.UUIDPropriedade))
            {
                Util.ExibirDialogo($"Não foi encontrada uma prodpriedade rural sincronizada com o plataforma para a propriedade do SAP [{DataSourceDoc.GetValue("ShipToCode", 0).ToString()}] ");
                return;
            }
            dto.CNPJEmpresa = SqlUtils.GetValue($@"SELECT COALESCE(T0.""TaxIdNum"",'') FROM OBPL T0 WHERE T0.""BPLId"" = 
                {DataSourceDoc.GetValue("BPLId", 0).ToString()}").Replace("-", "").Replace("/", "").Replace(".", "");
            dto.DataDoc = FormatValue.ToDate(EdDocDate.Value).ToString("yyyyMMdd");
            dto.CodFilial = DataSourceDoc.GetValue("BPLId", 0);
            StreamReader lForm = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(
                $@"Agrotis.AgrotisOne.ReceituarioDLL.View.Receituarios.AGRTFrmReceituario.srf"));
            Util.FormSetAbrirTicket(UniqueID, FormConstants.FrmSalesDocuments, lForm,
                FormConstants.AGRTFrmReceituario, dto.GetDataRecord());
        }
        private int PreencherItensReceituario(bool aTestarPNGeraReceita = true)
        {            
            int maxSubProd = 0;
            int countSubProd = 0;
            bool PnGeraReceita = (!aTestarPNGeraReceita) || OpeReceituario.ParceiroNegocioGeraReceita(EdCardCode.Value);
            ItensReceituario.Clear();

            for (int RowID = 1; RowID <= MtItens.VisualRowCount - 1; RowID++)
            {
                string sItemCode = Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColCodigoItem, RowID);                                
                double nQtdade = FormatValue.ToDoubleFromSAP(Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColQtde, RowID));
                string sCodCultura = Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColCodigoCultura, RowID);
                string sUtilizacao = Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColUtilizacao, RowID);                

                if (string.IsNullOrEmpty(sUtilizacao))
                    throw new Exception($@"Informe a utilização para o item {sItemCode}, linha {RowID}");
                if (PnGeraReceita && (OpeReceituario.UtilizacaoGeraReceita(sUtilizacao)))
                    countSubProd = PreencherSubProdutoReceituario(RowID, sItemCode, sCodCultura, nQtdade);
                maxSubProd = (countSubProd > maxSubProd) ? countSubProd : maxSubProd;
            }
            return maxSubProd;
        }
        private int PreencherSubProdutoReceituario(int aLine, string aItemCode, string aCodCultura, double aQtdade)
        {
            List<Dictionary<string, object>> listSubProduto = OperacoesReceituario.GetSubProdutos(aItemCode, aCodCultura);
            foreach (Dictionary<string, object> SubProduto in listSubProduto)
            {
                string filial = DataSourceDoc.GetValue("BPLId", 0);
                string estado = GetBusinessPartnerState(EdCardCode.Value);
                string produtoMapa = SubProduto["RegMapa"].ToString();                

                if (SubProduto["ObrigReceit"].ToString().Equals("Y"))
                {
                    if (string.IsNullOrEmpty(aCodCultura))
                        throw new Exception($@"Obrigatório informar Cultura para o item {aItemCode}");
                    if (string.IsNullOrEmpty(produtoMapa))
                        throw new Exception($@"Registro MAPA não informado para o item {aItemCode}.");

                    string nomeMapa = SubProduto["NomeRegMapa"].ToString();
                    string nomeCultura = SubProduto["NomeCultura"].ToString();
                    string qtdeKgLt = SubProduto["QtdeKgLt"].ToString();

                    if (SubProduto["IdFitoCult"] == null)
                        throw new Exception($@"Cultura {nomeCultura} não sincronizada com a plataforma.");

                    string idFitoCult = SubProduto["IdFitoCult"].ToString();
                    double nQtdadeSubProd = aQtdade * FormatValue.ToDoubleOrDefault(qtdeKgLt);
                    if (nQtdadeSubProd <= 0)
                        throw new Exception($@"Equivalência Kg/Lt incorreta para o Registro MAPA {produtoMapa} Item {aItemCode}.");

                    if (OpeReceituario.ValidarCulturaPorProduto(filial))
                        if (!OpeReceituario.CulturaValidaParaProduto(idFitoCult, produtoMapa, estado))
                            throw new Exception($@"A Cultura [{nomeCultura}] não está habilitada para utilização junto ao Produto MAPA [{nomeMapa}] no estado [{estado}].");

                    ItensReceituario.Add(Tuple.Create(aLine, aItemCode, nomeMapa, aCodCultura, nomeCultura, produtoMapa, nQtdadeSubProd));
                }
            }
            return (ItensReceituario.Count > 0) ? listSubProduto.Count : 0;
        }
        private bool NotaFiscalPossuiReceita()
        {
            if (string.IsNullOrEmpty(GetCodigoReceituario()))
                return false;
            else if (CodigoReceituario.Equals("0"))
                return false;
            else
                return true;
        }
        public bool FrmSalesDocument_OnBeforeFormClose(ItemEvent aEvent)
        {
            try
            {
                if (!MtItens.Columns.Item(ItensUIDs.ColCodigoItem).Editable)
                {
                    MtItens.Columns.Item(ItensUIDs.ColCodigoItem).Editable = true;
                    MtItens.Columns.Item(ItensUIDs.ColQtde).Editable = true;
                    MtItens.Columns.Item(ItensUIDs.ColCodigoCultura).Editable = true;
                }
                return true;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível fechar a tela de nota fiscal. ", Ex);                
                return false;
            }
            finally
            {
                GC.Collect();
            }            
        }
        private void PreencherDadosNotaFiscal()
        {            
            if (ItensReceituario.Count <= 0)
                return;
            if (mForm.PaneLevel != 1)
                mForm.PaneLevel = 1;
            Util.ExibirMensagemStatusBar("Buscando informações de receituário para NFe. Aguarde...", BoMessageTime.bmt_Short);
            for (int iRow = 1; iRow <= MtItens.VisualRowCount - 1; iRow++)               
            {
                MtItens.SetCellFocus(iRow, MtItens.Columns.Count - 1);
                string sItemCode = Util.MatrixGetValorCelula(MtItens, ItensUIDs.ColCodigoItem, iRow);
                string sCommentLine = OpeReceituario.GetCommentLine(CodigoReceituario, iRow.ToString(), ItensReceituario);
                if (!string.IsNullOrEmpty(sCommentLine))
                    Util.MatrixSetValorCelula(MtItens, ItensUIDs.ColCommentLine, iRow, sCommentLine);
            }
        }
        protected void FrmSalesDocument_OnAfterFormActivate(ItemEvent aEvent)
        {
            //
        }
        public bool BtnOk_OnBeforeItemPressed(ItemEvent aEvent)
        {
            try
            {
                if (mForm.Mode == BoFormMode.fm_ADD_MODE)
                {
                    if (!OpeReceituario.FilialGeraReceita(DataSourceDoc.GetValue("BPLId", 0)))
                    {
                        if (mForm.TypeEx != SAPFormConstants.SAPFrmPedidoDeVenda)
                        {
                            int maxSubProd = PreencherItensReceituario(false);                            
                            if ((maxSubProd > 1) || (maxSubProd > 0) && OpeReceituario.BuscarInformacaoNfPlataforma())
                                PreencherDadosNotaFiscal();
                        }                            
                        CodigoReceituario = "0";
                    }
                    else
                    {
                        if ((mForm.TypeEx != SAPFormConstants.SAPFrmPedidoDeVenda) && (NotaFiscalPossuiReceita()))
                        {
                            int maxSubProd = PreencherItensReceituario();                            
                            if ((maxSubProd > 1) || (maxSubProd > 0) && OpeReceituario.BuscarInformacaoNfPlataforma())
                                PreencherDadosNotaFiscal();
                        }                            
                    }
                    EdCodigoReceituario.Value = CodigoReceituario;
                    EdSequenceID.Value = GetSequenceID();                    
                }
                return true;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível gerar as informações adicionais de receituário para a NFe. BtnOk_OnBeforeItemPressed", Ex);
                return false;
            }
            finally
            {
                GC.Collect();
            }          
        }
        protected bool BtnCopy_OnBeforeComboSelect(ItemEvent aEvent)
        {
            bool result = true;
            try
            {                
                if ((mForm.Mode == BoFormMode.fm_ADD_MODE) && (NotaFiscalPossuiReceita()))
                {
                    if (!Util.ExibirDialogoConfirmacao($@"Atenção! Este documento já possui análise das receitas. 
                    Ao copiar os dados para a nota, uma nova análise deverá ser realizada. Deseja continuar?"))
                        result = false;
                    else
                    {
                        CodigoReceituario = "";
                        SequenceID = "";
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela("Não foi possível utilizar a função [Copiar de]. BtnCopy_OnBeforeComboSelect", Ex);
                return false;
            }            
        }           
    }
}
