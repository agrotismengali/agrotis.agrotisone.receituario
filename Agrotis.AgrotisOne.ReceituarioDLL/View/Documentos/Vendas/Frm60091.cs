﻿using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using Agrotis.Framework.Main.Interface.Events;
using System;
using Agrotis.Framework.Main.Interface.Controls.Items.Specific;
using Agrotis.AgrotisOne.Core.Utils;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Documentos.Vendas
{
    [FormType("60091")]
    public class Frm60091 : FrmSalesDocuments
    {
        public Frm60091(AddOn aAddOn, string aFormUID) : base(aAddOn, aFormUID)
        {
            mForm.Freeze(true);
            try
            {
                EdBPLId = Items.Add<ComboBox>(ItensUIDs.EdBPLId);
                EdBPLId.OnAfterComboSelect += new EventVoidItem(EdBPLId_OnAfterComboSelect);
                EdCardCode = Items.Add<EditText>(ItensUIDs.EdCardCode);
                EdCardName = Items.Add<EditText>(ItensUIDs.EdCardName);
                EdCardCode.OnGotFocus += new EventVoidItem(EdCardCode_OnGotFocus);
                EdCardCode.OnLostFocus += new EventVoidItem(EdCardCode_OnLostFocus);
                EdCardName.OnGotFocus += new EventVoidItem(EdCardCode_OnGotFocus);
                EdCardName.OnLostFocus += new EventVoidItem(EdCardCode_OnLostFocus);

                SAPbouiCOM.Item oItemRef = mForm.Items.Item("2"); // Botão de cancelar

                MtItens = Items.Add<Matrix>(ItensUIDs.MtxItens);
                MtItens.OnAfterValidate += new EventVoidItem(MtItensQtde_OnAfterValidate);
                MtItens.OnBeforeValidate += new EventBoolItem(MtItensQtde_OnBeforeValidate);
                MtItens.OnGotFocus += new EventVoidItem(MtItens_OnGotFocus);
                MtItens.OnLostFocus += new EventVoidItem(MtItens_OnLostFocus);
                MtItens.OnAfterChooseFromList += new EventVoidChooseFromList(base.MtItens_OnAfterChooseFromList);

                BtnOk = Items.Add<Button>(ItensUIDs.BtnOk);
                BtnOk.OnBeforeItemPressed += new EventBoolItem(BtnOk_OnBeforeItemPressed);

                BtnReceita = Items.Add<Button>(ItensUIDs.BtnReceita);
                BtnReceita.Caption = "Receitas";
                BtnReceita.Top = oItemRef.Top;
                BtnReceita.Left = oItemRef.Left + oItemRef.Width + 6;
                BtnReceita.ToPane = oItemRef.ToPane;
                BtnReceita.FromPane = oItemRef.FromPane;
                BtnReceita.Width = oItemRef.Width;
                BtnReceita.Visible = OpeReceituario.FilialGeraReceita(DataSourceDoc.GetValue("BPLId", 0));
                BtnReceita.OnAfterItemPressed += new EventVoidItem(BtnReceita_OnAfterItemPressed);

                OnBeforeFormDataAdd += new EventBoolBusinessInfo(FrmSalesDocument_OnBeforeFormDataAdd);
                OnAfterFormDataAdd += new EventVoidBusinessInfo(FrmSalesDocument_OnAfterFormDataAdd);
                OnAfterFormDataLoad += new EventVoidBusinessInfo(FrmSalesDocument_OnAfterFormDataLoad);
                OnBeforeFormClose += new EventBoolItem(FrmSalesDocument_OnBeforeFormClose);
                OnAfterFormActivate += new EventVoidItem(FrmSalesDocument_OnAfterFormActivate);
                base.AddChooseToMatrix();
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro ao abrir o formulário: ", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }                      
        }        
    }
}
