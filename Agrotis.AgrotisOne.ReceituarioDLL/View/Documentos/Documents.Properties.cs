﻿using SAPbouiCOM;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Matrix = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Matrix;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using Form = Agrotis.Framework.Main.Interface.Controls.Forms.Base.Form;
using ComboBox = Agrotis.Framework.Main.Interface.Controls.Items.Specific.ComboBox;
using ButtonCombo = Agrotis.Framework.Main.Interface.Controls.Items.Specific.ButtonCombo;
using Agrotis.AgrotisOne.Core.Constantes;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using System;
using System.Collections.Generic;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Documentos
{
    public abstract partial class Documents : Form
    {
        #region Properties
        public EditText EdCardCode { get; set; }
        public EditText EdCardName { get; set; }
        public EditText EdCodigoReceituario { get; set; }
        public EditText EdSequenceID { get; set; }
        public SAPbouiCOM.EditText EdNumero { get; set; }
        public SAPbouiCOM.EditText EdSerie { get; set; }
        public SAPbouiCOM.EditText EdDocDate { get; set; }
        public Button BtnReceita { get; set; }
        public Button BtnOk { get; set; }
        public ButtonCombo BtnCopy { get; set; }
        public DBDataSource DataSourceLinhas { get; set; }
        public DBDataSource DataSourceParcelas { get; set; }
        public DBDataSource DataSourceDoc { get; set; }
		public Matrix MtItens { get; set; }
        public ComboBox EdBPLId { get; set; }
        /// <summary>
        /// Tuple - LineNum, ItemCode, ItemName, Código Cultura, Cultura, RegMapa, Qtde
        /// </summary>
        public List<Tuple<int, string, string, string, string, string, double>> 
            ItensReceituario = new List<Tuple<int, string, string,  string, string, string, double>>();

        #endregion

        #region Data Source
        public static class DataSourceUIDs
        {
            public static string Tabela(string FormType)
            {
                return SAPTablesConstants.RetornaTabelaFormType(FormType);
            }
            public const string DocEntry = "DocEntry";            

            public static class DataSourcesLinhasUIDs
            {
                public static string Tabela(string FormType)
                {
                    return SAPTablesConstants.RetornaTabelaFormType(FormType, TipoTabela.MasterDataLines);
                }
                public const string DocEntry = "DocEntry";
                public const string LineNum = "LineNum";
            }

            public static class DataSourcesParcelasUIDs
            {
                public static string Tabela(string FormType)
                {
                    return SAPTablesConstants.RetornaTabelaFormType(FormType, TipoTabela.MasterDataParcelas);
                }
                public const string DocEntry = "DocEntry";
                public const string InstlmntID = "InstlmntID";
            }

            public static string ReceituarioUDO = "AGRT_RECEIT";
        }
        #endregion

        #region Itens UIDs
        public static class ItensUIDs
        {
            public const string MtxItens = "38";
            public const string ColCodigoItem = "1";
            public const string ColDescricaoItem = "3";
            public const string ColCodigoCultura = "U_AGRT_CodCult";
            public const string ColCommentLine = "U_AGRT_CommentLine";
            public const string ColUtilizacao = "2011";
            public const string ColQtde = "11";
            public const string EdCardCode = "4";
            public const string EdCardName = "54";
            public const string EdCodigoControleReceituario = "EdCodRec";
            public const string EdSeqID = "EdSeqID";
            public const string EdNumero = "2036";
            public const string EdSerie = "2037";
            public const string EdDocDate = "10";
            public const string BtnReceita = "BtnReceita";
            public const string EdBPLId = "2001";
            public const string BtnOk = "1";
            public const string BtnCopy = "10000330";
            public const string ColBaseEntry = "45";
            public const string ColBaseLine = "46";
        }
        #endregion
    }
}
