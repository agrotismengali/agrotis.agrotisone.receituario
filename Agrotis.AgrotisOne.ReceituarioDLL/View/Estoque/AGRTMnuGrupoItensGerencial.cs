﻿using Agrotis.AgrotisOne.Core.View.Estoque.GrupoItensGerencial;
using Agrotis.Framework.Main.Interface.Controls.Menus.Attributes;
using Agrotis.Framework.Main.Interface.Controls.Menus.Base;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Estoque
{
    [MenuType("AGRTMnuGrItensGerRec")]
    public class AGRTMnuGrupoItensGerencialRec : Menu
    {        
        public AGRTMnuGrupoItensGerencialRec(Framework.Main.Application.AddOn aAddOn, string aMenuUID)
            : base(aAddOn, aMenuUID)
        {
            OnAfterClick += new Agrotis.Framework.Main.Interface.Events.EventVoidMenu(AGRTMnuGrupoItensGerencialRec_OnAfterClick);
        }
        private void AGRTMnuGrupoItensGerencialRec_OnAfterClick(SAPbouiCOM.MenuEvent aEvent)
        {
            AGRTMnuGrupoItensGerencialCore mnuAGRTMnuGrupoItensGerencial = new AGRTMnuGrupoItensGerencialCore();
            mnuAGRTMnuGrupoItensGerencial.AGRTMnuGrupoItensGerencial_OnAfterClick();
        }
    }
}
