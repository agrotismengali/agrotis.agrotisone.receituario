﻿using SAPbouiCOM;
using Agrotis.Framework.Main.Application;
using Agrotis.Framework.Main.Interface.Controls.Forms.Attributes;
using EditText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.EditText;
using Button = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Button;
using StaticText = Agrotis.Framework.Main.Interface.Controls.Items.Specific.StaticText;
using ComboBox = Agrotis.Framework.Main.Interface.Controls.Items.Specific.ComboBox;
using Grid = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Grid;
using Folder = Agrotis.Framework.Main.Interface.Controls.Items.Specific.Folder;
using Agrotis.Framework.Main.Interface.Events;
using Agrotis.AgrotisOne.ReceituarioDLL.Model;
using System;
using Agrotis.AgrotisOne.Core;
using Agrotis.AgrotisOne.Core.Utils;
using Agrotis.AgrotisOne.Core.Constantes;
using static Agrotis.AgrotisOne.Core.Enums.Enumerators;
using Agrotis.AgrotisOne.ReceituarioDLL.View.CFLAgrotis;
using Agrotis.Framework.Commons;
using Agrotis.AgrotisOne.Core.View.Estoque;
using Agrotis.AgrotisOne.ReceituarioDLL.Controller.Commons;

namespace Agrotis.AgrotisOne.ReceituarioDLL.View.Estoque
{
    [FormType("150")]
    public class Frm150 : Frm150Base
    {
        #region Propriedades        
        public StaticText StGrupoItem { get; set; }
        public ComboBox CbGrupoItem { get; set; }
        public DataTable DtProdMapa { get; set; }
        public EditTextColumn CbGrProdMapaCode { get; set; }
        public UdtProdutoMapa UDTProdMapa { get; set; }
        public Grid GrProdMapa { get; set; }
        public Button BtAddPrdMp { get; set; }
        public Button BtDelPrdMp { get; set; }
        public StaticText StMargem { get; set; }
        public StaticText StExclNf { get; set; }
        public EditText EdMargem { get; set; }
        public ComboBox CbExclNf { get; set; }
        public Item ItmFldRef { get; set; }
        public Folder FldAgrotis { get; set; }
        private string[] Titulos { get; set; }

        private ControllerCFL CtrlChooseFromList = new ControllerCFL();
        private OperacoesReceituario OpeReceituario = new OperacoesReceituario();
        #endregion


        #region Itens UIDs
        public static class ItensUIDs
        {
            public const string Tabela = "OITM";
            public const string TabelaSubProd = "@AGRT_RECEITSUBPROD";
            public const string ColCode = "Code";
            public const string ColName = "Name";
            public const string ColProdMapa = "U_AGRT_CodSubProd";
            public const string ColNomeProdMapa = "U_AGRT_NomeSubProd";
            public const string ColQtdeEquiv = "U_AGRT_QtdeKgLt";
            public const string ColTarjaFichaEmergencia = "U_AGRT_TarjaFichaEmergencia";
            public const string DtProdMapa = "DtProdMapa";
            public const string CbGrupoItem = "CbGrpItem";
            public const string StGrupoItem = "StGrpItem";

            public const string StMargem = "StMargem";
            public const string StExclNf = "StExclNf";
            public const string EdMargem = "EdMargem";
            public const string CbExclNf = "CbExclNf";
            public const string GrProdMapa = "GrProdMapa";
            public const string BtAddPrdMp = "BtAddPrdMp";
            public const string BtDelPrdMp = "BtDelPrdMp";
            public const string FldAgrotis = "FldAgrotis";
        }
        #endregion

        public Frm150(AddOn aAddOn, string aFormUID)
            : base(aAddOn, aFormUID)
        {
            mForm.Freeze(true);
            try
            {
                EdItemCode = Items.Add<EditText>(ItensUIDsBase.EdItemCode);
                DtProdMapa = mForm.DataSources.DataTables.Add(ItensUIDs.DtProdMapa);
                GrProdMapa = Items.Add<Grid>(ItensUIDs.GrProdMapa);
                GrProdMapa.Visible = false;
                BtAddPrdMp = Items.Add<Button>(ItensUIDs.BtAddPrdMp);
                BtAddPrdMp.Visible = false;
                BtDelPrdMp = Items.Add<Button>(ItensUIDs.BtDelPrdMp);
                BtDelPrdMp.Visible = false;

                Titulos = new string[] { "#", "Produto MAPA", "Descrição", "Equiv. KG/L/UN", "Imprime Tarja" };
                UDTProdMapa = new UdtProdutoMapa();
                OnAfterFormLoad += Frm150_OnAfterFormLoad;
                OnAfterFormDataLoad += new EventVoidBusinessInfo(Frm150_OnAfterFormDataLoad);
                OnAfterFormDataAdd += new EventVoidBusinessInfo(Frm150_OnAfterFormDataAdd);
                OnAfterFormDataUpdate += new EventVoidBusinessInfo(Frm150_OnAfterFormDataUpdate);
                OnFormResize += new EventVoidItem(Frm150_OnFormResize);
                OnAfterFormActivate += new EventVoidItem(Frm150_OnAfterFormActivate);
                FldAgrotis = Items.Add<Folder>(ItensUIDs.FldAgrotis);
                FldAgrotis.Visible = false;
                FldAgrotis.OnAfterClick += new EventVoidItem(FldAgrotis_OnAfterClick);
                StGrupoItem = Items.Add<StaticText>(ItensUIDs.StGrupoItem);
                StGrupoItem.Visible = false;
                CbGrupoItem = Items.Add<ComboBox>(ItensUIDs.CbGrupoItem);
                CbGrupoItem.Visible = false;
                CbGrupoItem.DataBind.SetBound(true, ItensUIDs.Tabela, "U_AGRT_GrpItem");
                PosicionaItensForm();
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro ao abrir Form", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
        public void Frm150_OnAfterFormActivate(ItemEvent aEvent)
        {
            //
        }
        public void FldAgrotis_OnAfterClick(ItemEvent aEvent)
        {
            mForm.Freeze(true);
            try
            {
                mForm.PaneLevel = FldAgrotis.Pane;
                CarregarGridProdutoMapa(EdItemCode.Value);
                LoadGrupoItemComboBox();
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método FldAgrotis_OnAfterClick - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
        private void Frm150_OnAfterFormLoad(ItemEvent aEvent)
        {
            try
            {
                AjustarItensTela();
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm150_OnAfterFormLoad - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
        public void Frm150_OnFormResize(ItemEvent aEvent)
        {
            try
            {
                AjustarItensTela();
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm150_OnFormResize - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
        private void UpsertProdutoMapa(string aItemCode)
        {
            int countRegMapa = 0;
            mForm.Freeze(true);
            try
            {
                if (DtProdMapa.Rows.Count > 0)
                {
                    for (int iRow = 0; iRow < DtProdMapa.Rows.Count; iRow++)
                    {
                        if (!string.IsNullOrEmpty(DtProdMapa.GetValue(ItensUIDs.ColProdMapa, iRow)))
                        {
                            UDTProdMapa.Code = DtProdMapa.GetValue(ItensUIDs.ColCode, iRow);
                            UDTProdMapa.ItemCode = aItemCode;
                            UDTProdMapa.Name = "";
                            UDTProdMapa.QtdeKgLt = DtProdMapa.GetValue(ItensUIDs.ColQtdeEquiv, iRow);
                            UDTProdMapa.CodSubProd = DtProdMapa.GetValue(ItensUIDs.ColProdMapa, iRow);
                            UDTProdMapa.NomeSubProd = DtProdMapa.GetValue(ItensUIDs.ColNomeProdMapa, iRow);
                            if (string.IsNullOrEmpty(DtProdMapa.GetValue(ItensUIDs.ColTarjaFichaEmergencia, iRow)))
                                UDTProdMapa.TarjaFichaEmergencia = "0";
                            else
                                UDTProdMapa.TarjaFichaEmergencia = DtProdMapa.GetValue(ItensUIDs.ColTarjaFichaEmergencia, iRow);

                            if ((UDTProdMapa.QtdeKgLt > 0) && (string.IsNullOrEmpty(UDTProdMapa.CodSubProd)))
                                throw new Exception("A coluna [Produto Mapa] é de preenchimento obrigatório!");

                            UDTProdMapa.Upsert();
                            countRegMapa++;
                        }
                    }
                    CarregarGridProdutoMapa(EdItemCode.Value);
                    // Se possui apenas um Reg. Mapa e está desabilitado para buscar Infos. do plataforma, alimenta os campos de prod. perigosos
                    if (!OpeReceituario.BuscarInformacaoNfPlataforma())
                    {
                        if (countRegMapa == 1)
                            OpeReceituario.SincronizarInfProdPerigoso(EdItemCode.Value);
                        else if (countRegMapa > 1)
                            Util.ExibirDialogo("Atenção! Revise as informações para produtos perigosos nos [Dados adicionais] do item.");
                    }
                    if (DtProdMapa.Rows.Count < 4)
                        DtProdMapa.Rows.Add(1);                    
                }
            }
            catch (Exception Ex)
            {
                Logger.Trace("Não foi possível salvar Produtos MAPA relacionados - " + Ex.ToString());
                Globals.Master.Connection.Interface.StatusBar.SetText("Não foi possível salvar Produtos MAPA relacionados - " +
                    Ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                Util.ExibirDialogo("Não foi possível salvar Produtos MAPA relacionados - " + Ex.Message +
                    Globals.Master.Connection.Database.GetLastErrorDescription());
            }
            finally
            {
                mForm.Freeze(false);
            }
        }

        private void Frm150_OnAfterFormDataAdd(BusinessObjectInfo aEvent)
        {
            try
            {
                UpsertProdutoMapa(EdItemCode.Value);
                base.Frm150Base_OnAfterFormDataAdd(aEvent);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm150_OnAfterFormDataAdd - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
        private void Frm150_OnAfterFormDataUpdate(BusinessObjectInfo aEvent)
        {
            try
            {
                UpsertProdutoMapa(EdItemCode.Value);
                base.Frm150Base_OnAfterFormDataUpdate(aEvent);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm150_OnAfterFormDataUpdate - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }

        private void Frm150_OnAfterFormDataLoad(BusinessObjectInfo aEvent)
        {
            try
            {
                if (mForm.PaneLevel == FldAgrotis.Pane)
                    CarregarGridProdutoMapa(EdItemCode.Value);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método Frm150_OnAfterFormDataLoad - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }

        private void CarregarGridProdutoMapa(string aItemCode)
        {
            DtProdMapa.ExecuteQuery($@"SELECT ""Code"", 
                                              ""U_AGRT_CodSubProd"", 
                                              ""U_AGRT_NomeSubProd"", 
                                              ""U_AGRT_QtdeKgLt"",
                                              ""U_AGRT_TarjaFichaEmergencia""
                                      FROM ""@AGRT_RECEITSUBPROD""
                                      WHERE ""U_AGRT_ItemCode"" = '{aItemCode}'");

            GrProdMapa.DataTable = DtProdMapa;

            for (int iCol = 0; iCol < GrProdMapa.Columns.Count; iCol++)
            {
                if ((iCol == 0) || (iCol == 2))
                    GrProdMapa.Columns.Item(iCol).Editable = false;
                switch (iCol)
                {
                    case 0:
                        GrProdMapa.Columns.Item(iCol).Editable = false;
                        GrProdMapa.Columns.Item(iCol).Width = 0;
                        break;
                    case 1:
                        GrProdMapa.Columns.Item(iCol).Width = 50;
                        GrProdMapa.Columns.Item(iCol).Type = BoGridColumnType.gct_EditText;
                        CbGrProdMapaCode = (EditTextColumn)GrProdMapa.Columns.Item(iCol);
                        break;
                    case 2:
                        GrProdMapa.Columns.Item(iCol).Editable = false;
                        break;
                    case 3:
                        GrProdMapa.Columns.Item(iCol).Width = 50;
                        break;
                    case 4:
                        GrProdMapa.Columns.Item(iCol).Width = 50;
                        GrProdMapa.Columns.Item(iCol).Type = BoGridColumnType.gct_ComboBox;
                        ComboBoxColumn sboCol = (ComboBoxColumn)GrProdMapa.Columns.Item(ItensUIDs.ColTarjaFichaEmergencia);
                        sboCol.Description = "Imprime tarja ao emitir a ficha de emergência em uma receita que possua esse produto";
                        sboCol.DisplayType = BoComboDisplayType.cdt_Description;
                        if (sboCol.ValidValues.Count == 0)
                        {
                            sboCol.ValidValues.Add("0", "Não");
                            sboCol.ValidValues.Add("1", "Sim");
                        }
                        break;
                }
                GrProdMapa.Columns.Item(iCol).TitleObject.Sortable = true;
                GrProdMapa.Columns.Item(iCol).TitleObject.Caption = Titulos[iCol];
            }
            GrProdMapa.AutoResizeColumns();
        }
        private void LoadGrupoItemComboBox()
        {
            Util.ComboBoxSetValoresValidosPorSQL(CbGrupoItem,
                "SELECT \"Code\", \"Name\" FROM \"@AGRT_GRITENSGER\" ORDER BY \"Name\" ");
        }
        private void OnAfterReceberTicket()
        {
            try
            {
                DataTable DtRetornoCFL = Globals.Master.FormTicketCFL.GetDtResult();
                string sCodeResult = DtRetornoCFL.GetValue(CFLColumnDefault.Code.ToString(), 0);
                string sNameResult = DtRetornoCFL.GetValue(CFLColumnDefault.Desc.ToString(), 0);
                if ((mForm.Mode == BoFormMode.fm_ADD_MODE) || (mForm.Mode == BoFormMode.fm_UPDATE_MODE))
                {
                    int iRow = GrProdMapa.GetCellFocus().rowIndex;
                    GrProdMapa.DataTable.SetValue(1, iRow, sCodeResult);
                    GrProdMapa.DataTable.SetValue(2, iRow, sNameResult);
                }
            }
            catch (Exception Ex)
            {
                //
            }
            finally
            {
                Globals.Master.FormTicketCFL.DtResult_ToNull();
                GC.Collect();
            }
        }
        private void LoadProdutoMapaCFL()
        {
            CtrlChooseFromList.ChooseFromList(mForm, OnAfterReceberTicket, CFLTipoAgrotis.ReceituarioProdutoMapa);
        }
        private void PosicionaItensForm()
        {
            ItmFldRef = mForm.Items.Item("1980000483");
            FldAgrotis.AffectsFormMode = false;
            FldAgrotis.Top = ItmFldRef.Top;
            FldAgrotis.Height = ItmFldRef.Height;
            FldAgrotis.Left = ItmFldRef.Left + ItmFldRef.Width;
            FldAgrotis.Width = ItmFldRef.Width;
            FldAgrotis.GroupWith(ItmFldRef.UniqueID);
            FldAgrotis.Pane = Util.GetPaneNumByModulo(9);
            FldAgrotis.Caption = "Receituário";
            FldAgrotis.AutoPaneSelection = true;

            StGrupoItem.FromPane = FldAgrotis.Pane;
            StGrupoItem.ToPane = FldAgrotis.Pane;
            StGrupoItem.Caption = "Grupo Item";

            CbGrupoItem.FromPane = FldAgrotis.Pane;
            CbGrupoItem.ToPane = FldAgrotis.Pane;
            CbGrupoItem.ExpandType = BoExpandType.et_DescriptionOnly;
            CbGrupoItem.DisplayDesc = true;

            GrProdMapa.FromPane = FldAgrotis.Pane;
            GrProdMapa.ToPane = FldAgrotis.Pane;
            GrProdMapa.OnAfterValidate += new EventVoidItem(GrProdMapa_OnAfterValidate);
            GrProdMapa.OnGotFocus += new EventVoidItem(GrProdMapa_OnGotFocus);
            GrProdMapa.OnAfterDoubleClick += new EventVoidItem(GrProdMapa_OnAfterDoubleClick);

            BtAddPrdMp.Type = BoButtonTypes.bt_Image;
            BtAddPrdMp.Image = IconConstants.GetIconPath(IconConstants.Add);
            BtAddPrdMp.ToPane = GrProdMapa.ToPane;
            BtAddPrdMp.FromPane = GrProdMapa.FromPane;
            BtAddPrdMp.OnAfterItemPressed += new EventVoidItem(BtAddPrdMp_OnAfterItemPressed);

            BtDelPrdMp.Type = BoButtonTypes.bt_Image;
            BtDelPrdMp.Image = IconConstants.GetIconPath(IconConstants.Remove);
            BtDelPrdMp.ToPane = BtAddPrdMp.ToPane;
            BtDelPrdMp.FromPane = BtAddPrdMp.FromPane;
            BtDelPrdMp.OnAfterItemPressed += new EventVoidItem(BtDelPrdMp_OnAfterItemPressed);
            mForm.Resize(mForm.Width, mForm.Height); ;
        }
        private void AjustarItensTela()
        {
            StGrupoItem.Top = ItmFldRef.Top + 34;
            StGrupoItem.Left = 18;
            CbGrupoItem.Width = 180;
            CbGrupoItem.Top = StGrupoItem.Top;
            CbGrupoItem.Left = StGrupoItem.Left + StGrupoItem.Width + 6;

            GrProdMapa.Top = CbGrupoItem.Top + CbGrupoItem.Height + 6;
            GrProdMapa.Left = StGrupoItem.Left;
            GrProdMapa.Height = 200;
            GrProdMapa.Width = 450;

            BtAddPrdMp.Top = GrProdMapa.Top;
            BtAddPrdMp.Left = GrProdMapa.Left + GrProdMapa.Width + 6;
            BtAddPrdMp.Width = 20;
            BtAddPrdMp.Height = 20;

            BtDelPrdMp.Top = BtAddPrdMp.Top + BtAddPrdMp.Width + 6;
            BtDelPrdMp.Left = BtAddPrdMp.Left;
            BtDelPrdMp.Width = 20;
            BtDelPrdMp.Height = 20;

            FldAgrotis.Visible = true;
            StGrupoItem.Visible = true;
            CbGrupoItem.Visible = true;
            GrProdMapa.Visible = true;
            BtAddPrdMp.Visible = true;
            BtDelPrdMp.Visible = true;
            mForm.PaneLevel = mForm.PaneLevel;
        }
        private void GrProdMapa_OnAfterDoubleClick(ItemEvent aEvent)
        {
            //
        }
        private void GrProdMapa_OnGotFocus(ItemEvent aEvent)
        {
            try
            {
                if (mForm.Mode != BoFormMode.fm_FIND_MODE)
                {
                    if (GrProdMapa.GetCellFocus().ColumnIndex == 1)
                    {
                        int iRow = GrProdMapa.GetCellFocus().rowIndex;
                        string sProdMapa = DtProdMapa.GetValue(ItensUIDs.ColProdMapa, iRow);
                        if (string.IsNullOrEmpty(sProdMapa))
                        {
                            if ((mForm.Mode != BoFormMode.fm_UPDATE_MODE) && (!string.IsNullOrEmpty(EdItemCode.Value)))
                                mForm.Mode = BoFormMode.fm_UPDATE_MODE;
                            LoadProdutoMapaCFL();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método GrProdMapa_OnGotFocus - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
        private void GrProdMapa_OnAfterValidate(ItemEvent aEvent)
        {
            //
        }
        private void ExcluirProdutoMapa(string aItemCode)
        {
            int iRow = 0;
            string sProdMapa = "";
            if (GrProdMapa.GetCellFocus() != null)
            {
                iRow = GrProdMapa.GetCellFocus().rowIndex;
                sProdMapa = DtProdMapa.GetValue(ItensUIDs.ColProdMapa, iRow);
            }
            if (iRow < 0)
                return;
            if (!Util.ExibirDialogoConfirmacao($@"Deseja excluir o Produto MAPA {sProdMapa}?"))
                return;
            mForm.Freeze(true);
            try
            {
                UDTProdMapa.Code = DtProdMapa.GetValue(ItensUIDs.ColCode, iRow);
                UDTProdMapa.Delete();
                if (!OpeReceituario.BuscarInformacaoNfPlataforma())
                    Util.ExibirDialogo("Atenção! Revise as informações para produtos perigosos nos [Dados adicionais] do item.");
            }
            finally
            {
                CarregarGridProdutoMapa(EdItemCode.Value);
                mForm.Freeze(false);
            }
        }

        private void BtDelPrdMp_OnAfterItemPressed(ItemEvent aEvent)
        {
            try
            {
                ExcluirProdutoMapa(EdItemCode.Value);
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método BtDelPrdMp_OnAfterItemPressed - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }

        private void BtAddPrdMp_OnAfterItemPressed(ItemEvent aEvent)
        {
            try
            {
                // Cadastro limitado em 4 registros por regra de negócio
                if (DtProdMapa.Rows.Count < 4)
                    DtProdMapa.Rows.Add(1);
                else
                    Util.ExibirDialogo("São permitidos no máximo 4 produtos MAPA vinculados a cada item de estoque.");
            }
            catch (Exception Ex)
            {
                Util.ExibeErrosTela($"Erro no método BtAddPrdMp_OnAfterItemPressed - Frm150", Ex);
            }
            finally
            {
                mForm.Freeze(false);
            }
        }
    }
}